//
//  CustomNodeView.swift
//  Tree
//
//  Created by Dee on 19/05/17.
//  Copyright © 2017 Dee. All rights reserved.
//

import UIKit

class CustomNodeView: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var customView: UIView!

    func createView(with node: Node, with color: UIColor) {
        
        let dict = node.info
        let nodeName = " " + node.name

        if(dict["is_shared"].stringValue == "1")
        {
            if(dict["tree_noti"].intValue > 0)
            {
                titleLabel.addTwoImage(imageName: "man", imageName2: "ic_steering.png" , afterLabel: false, strText: nodeName)

            }else{
                titleLabel.addImage(imageName: "man" , afterLabel: false, strText: nodeName)
            }
            
        }else if(dict["tree_noti"].intValue > 0)
        {
             titleLabel.addImage(imageName: "ic_steering.png" , afterLabel: false, strText: nodeName)
        }
        else{
            titleLabel?.text = node.name
        }
        customView.backgroundColor = color.withAlphaComponent(0.35)
        //customView.backgroundColor = getRandomColor()
        customView.layer.cornerRadius = 10
        customView.layer.masksToBounds = true

        customView.layer.borderColor = color.cgColor
        customView.layer.borderWidth = 5
        titleLabel.textColor = .black
        
        // UpSideDown Fix
        titleLabel.transform = CGAffineTransform(rotationAngle: .pi)
    }
}

