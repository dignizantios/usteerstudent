//
//  Node.swift
//  Tree
//
//  Created by Dee on 15/05/17.
//  Copyright © 2017 Dee. All rights reserved.
//

import UIKit
import SwiftyJSON

class Node: NSObject, TreeViewerDelegate {
    var children: [Node]
    var identifier: String
    var backgroundColor: UIColor?
    var textColor: UIColor?

    var startX: CGFloat
    var weight: CGFloat
    var nodeView: UIView?
    var name: String
    var info = JSON()
    
    @objc func viewTapped(_ recognizer: UITapGestureRecognizer) {
        
        print("Node details - ",info)

        print("\(identifier)")
        
        if(info["is_clickable"].boolValue == true)
        {
            
            info["rel_gender"].stringValue = decreptedString(string: info["rel_gender"].stringValue)
            info["rname"].stringValue = decreptedString(string: info["rname"].stringValue)
            info["default_name"].stringValue = decreptedString(string: info["default_name"].stringValue)
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showPopup"), object: info)
        }
    }
    
    init(identifier: String, andInfo info: JSON, weight: CGFloat, startX: CGFloat, nodeView: UIView, backgroundColor: UIColor, children: [Node],textcolor : UIColor) {
        
        name = decreptedString(string: info["rname"].stringValue)
        self.identifier = identifier
        self.children = children
        self.startX = startX
        self.backgroundColor = backgroundColor
        self.nodeView = nodeView
        self.weight = weight
        self.info = info
        self.textColor = textcolor
        super.init()
    }
    
    init(identifier: String, info: JSON,textcolor : UIColor)
    {
        name = decreptedString(string: info["rname"].stringValue)
        self.identifier = identifier
        self.children = []
        self.startX = 0
        self.backgroundColor = UIColor.white
        self.weight = 0
        self.info = info
        self.textColor = textcolor
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(children: [Any], andIdentifier identifier: String, andInfo info: JSON, weight: CGFloat, startX: CGFloat, nodeView: UIView, backgroundColor: UIColor,textcolor : UIColor) {
        name = decreptedString(string: info["rname"].stringValue)
        self.identifier = identifier
        self.children = [Any]() as! [Node] /* capacity: children.count */
        self.startX = startX
        self.backgroundColor = backgroundColor
        self.nodeView = nodeView
        self.weight = weight
        self.info = info
        self.textColor = textcolor

        for child: Any in children {
            if (child is Node) {
                self.children.append(child as! Node)
            }
        }
        super.init()
    }
}
