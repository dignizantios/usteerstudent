//
//  HierarchyScrollView.swift
//  Tree
//
//  Created by Dee on 19/05/17.
//  Copyright © 2017 Dee. All rights reserved.
//

import UIKit

class HierarchyScrollView: UIScrollView, UIScrollViewDelegate {
    var treeView: UIView?
    
    // return the scrollable view.
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return treeView!
    }
   
    init(frame: CGRect, andWithRoot root: TreeViewerDelegate?) {
        // The scroll view that contains the tree.
        super.init(frame: frame)
        backgroundColor = UIColor.white
        
        minimumZoomScale = 0.1
        maximumZoomScale = 1.2
        delegate = self
        contentSize = frame.size
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(self.doubleTapScreen))
        doubleTap.numberOfTapsRequired = 2
        addGestureRecognizer(doubleTap)
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.singleTapScreen))
        singleTap.numberOfTapsRequired = 1
        addGestureRecognizer(singleTap)
        
        let tripleTap = UITapGestureRecognizer(target: self, action: #selector(self.tripleTapScreen))
        tripleTap.numberOfTapsRequired = 3
        addGestureRecognizer(tripleTap)
        
        
        let fourthTap = UITapGestureRecognizer(target: self, action: #selector(self.fourthTapScreen))
        fourthTap.numberOfTapsRequired = 4
        addGestureRecognizer(fourthTap)
        
        treeView = TreeViewer(frame: frame, andWithRoot: root)
        // Set scrollview background to provided background color from the user or with white color as a default color.
        treeView?.backgroundColor = UIColor.clear
        
        addSubview(treeView!)
        //TODO:- default offset
//        self.contentOffset = treeConsetOffset
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)
        
        
    }
    func setDefaultContentOffset()
    {
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // Double tapping resize the view.
    @IBAction func singleTapScreen(_ sender: UITapGestureRecognizer)
    {
        zoomScale = 0.4
        treeZoomLevel = 0.4
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)

    }
    @IBAction func doubleTapScreen(_ sender: UITapGestureRecognizer)
    {
        zoomScale = 0.5
        treeZoomLevel = 0.5
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)

    }
    
    @IBAction func tripleTapScreen(_ sender: UITapGestureRecognizer)
    {
        zoomScale = 0.8
        treeZoomLevel = 0.8
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)

        
        //TODO:-
        /*guard let location = gestureRecognizer?.location(in: gestureRecognizer?.view)
        let rect = CGRect(x: location.x, y: location.y, width: 0, height: 0)
        scrollView.zoom(to: rect, animated: true)*/
    }
    @IBAction func fourthTapScreen(_ sender: UITapGestureRecognizer)
    {
        zoomScale = 1.2
        treeZoomLevel = 1.2
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)
    }
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
//        print("scrollview zoom draw- ",treeZoomLevel)
        zoomScale = treeZoomLevel
        backgroundColor = UIColor.white
//        self.contentOffset = treeConsetOffset
        //TODO:- default offset
        //        self.contentOffset = treeConsetOffset
//        print("Tree content size width - ",self.contentSize.width)
//        let offsetX = max((self.bounds.width - self.contentSize.width) * 0.5, 0)
//        let offsetY = max((self.bounds.height - self.contentSize.height) * 0.5, 0)
//        print("offsetX - ",offsetX)
//        print("offsetY - ",offsetY)

//        self.contentInset = UIEdgeInsetsMake(offsetY, offsetX, 0, 0)
//        self.contentOffset = CGPoint(x:offsetX,y:offsetY)
        
        self.contentOffset = CGPoint(x:self.contentSize.width/2 - 200,y:0)
//        self.contentOffset = CGPoint(x:self.contentSize.width/2,y:0)

    }

    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
//        print("scrollview zoom - ",scale)
//
        //TODO:- default offset
        //        treeConsetOffset = scrollView.contentOffset
//        self.contentOffset = CGPoint(x:self.contentSize.width/2,y:0)
        
        treeZoomLevel = scale
//        print("Scrollview end zoom offset - ",scrollView.contentOffset)

    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        treeConsetOffset = scrollView.contentOffset
        //TODO:- default offset
        //        treeConsetOffset = scrollView.contentOffset
//        self.contentOffset = CGPoint(x:self.contentSize.width/2,y:0)
//        print("Scrollview end content offset - ",scrollView.contentOffset)

    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        print("Scrollview content offset - ",scrollView.contentOffset)
    }
}
