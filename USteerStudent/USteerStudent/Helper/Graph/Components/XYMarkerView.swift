//
//  XYMarkerView.swift
//  ChartsDemo-iOS
//
//  Created by Jacob Christie on 2017-07-09.
//  Copyright © 2017 jc. All rights reserved.
//

import Foundation
import Charts
import UIKit

public class XYMarkerView: BalloonMarker {
    public var xAxisValueFormatter: IAxisValueFormatter
    fileprivate var yFormatter = NumberFormatter()
    
    public init(color: UIColor, font: UIFont, textColor: UIColor, insets: UIEdgeInsets,
                xAxisValueFormatter: IAxisValueFormatter) {
        self.xAxisValueFormatter = xAxisValueFormatter
        yFormatter.minimumFractionDigits = 2
        yFormatter.maximumFractionDigits = 2
        super.init(color: color, font: font, textColor: textColor, insets: insets)
    }
    
    public override func refreshContent(entry: ChartDataEntry, highlight: Highlight) {
        /*let string = "x: "
            + xAxisValueFormatter.stringForValue(entry.x, axis: XAxis())
            + ", \ny: "
            + yFormatter.string(from: NSNumber(floatLiteral: entry.y))!*/
        
        let string = "Date: "
            + xAxisValueFormatter.stringForValue(entry.x, axis: XAxis())
            + ", \nSteering bias: "
            + "\(entry.y)"
        
        let stringForBackTarget = "'What's happening here?'" + "\nDate: "
            + xAxisValueFormatter.stringForValue(entry.x, axis: XAxis())
        
        if(isFromTargetBack)
        {
            setLabel(stringForBackTarget)
        }else{
            setLabel(string)
        }
        
    }
    
    /*public override func draw(context: CGContext, point: CGPoint)
    {
        let offset = self.offset
        let size = self.size
        
        let rect = CGRect(x: point.x + offset.x, y: point.y + offset.y, width: size.width, height: size.height)
        
        UIGraphicsPushContext(context)
        image!.draw(in: rect)
        UIGraphicsPopContext()
    }
    */
}
