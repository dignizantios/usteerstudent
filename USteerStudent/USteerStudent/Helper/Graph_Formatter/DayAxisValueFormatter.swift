//
//  DayAxisValueFormatter.swift
//  ChartsDemo-iOS
//
//  Created by Jacob Christie on 2017-07-09.
//  Copyright © 2017 jc. All rights reserved.
//

import Foundation
import Charts

public class DayAxisValueFormatter: NSObject, IAxisValueFormatter {
    weak var chart: LineChartView?
    var arrayXlabels : [String] = []

    init(chart: LineChartView,array:[String]) {
        self.chart = chart
        self.arrayXlabels = array
    }
    
    public func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        return arrayXlabels[Int(value) % arrayXlabels.count]
    }
  
}

