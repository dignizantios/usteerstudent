/*
The MIT License (MIT)

Copyright (c) 2015 Max Konovalov

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

import UIKit

//@IBDesignable
class RingProgressGroupView: UIView {

    let ring1 = RingProgressView()
    let ring2 = RingProgressView()
    let ring3 = RingProgressView()
    let ring4 = RingProgressView()
    let ring5 = RingProgressView()
    let ring6 = RingProgressView()
    let ring7 = RingProgressView()
    let ring8 = RingProgressView()

    @IBInspectable var ring1StartColor: UIColor = .red {
        didSet {
            ring1.startColor = ring1StartColor
        }
    }
    
    @IBInspectable var ring1EndColor: UIColor = .blue {
        didSet {
            ring1.endColor = ring1EndColor
        }
    }
    
    @IBInspectable var ring2StartColor: UIColor = .red {
        didSet {
            ring2.startColor = ring2StartColor
        }
    }
    
    @IBInspectable var ring2EndColor: UIColor = .blue {
        didSet {
            ring2.endColor = ring2EndColor
        }
    }
    
    @IBInspectable var ring3StartColor: UIColor = .red {
        didSet {
            ring3.startColor = ring3StartColor
        }
    }
    
    @IBInspectable var ring3EndColor: UIColor = .blue {
        didSet {
            ring3.endColor = ring3EndColor
        }
    }
    
    @IBInspectable var ring4StartColor: UIColor = .red {
        didSet {
            ring4.startColor = ring4StartColor
        }
    }
    
    @IBInspectable var ring4EndColor: UIColor = .blue {
        didSet {
            ring4.endColor = ring4EndColor
        }
    }
    
    @IBInspectable var ring5StartColor: UIColor = .red {
        didSet {
            ring5.startColor = ring5StartColor
        }
    }
    
    @IBInspectable var ring5EndColor: UIColor = .blue {
        didSet {
            ring5.endColor = ring5EndColor
        }
    }
    
    @IBInspectable var ring6StartColor: UIColor = .red {
        didSet {
            ring6.startColor = ring6StartColor
        }
    }
    
    @IBInspectable var ring6EndColor: UIColor = .blue {
        didSet {
            ring6.endColor = ring6EndColor
        }
    }
    
    @IBInspectable var ring7StartColor: UIColor = .red {
        didSet {
            ring7.startColor = ring7StartColor
        }
    }
    
    @IBInspectable var ring7EndColor: UIColor = .blue {
        didSet {
            ring7.endColor = ring7EndColor
        }
    }
    
    @IBInspectable var ring8StartColor: UIColor = .red {
        didSet {
            ring8.startColor = ring8StartColor
        }
    }
    
    @IBInspectable var ring8EndColor: UIColor = .blue {
        didSet {
            ring8.endColor = ring8EndColor
        }
    }
    
    @IBInspectable var ringWidth: CGFloat = 20 {
        didSet
        {
            [ring1,ring2,ring3,ring4,ring5,ring6,ring7,ring8].forEach({
                $0.ringWidth = ringWidth
            })
            setNeedsLayout()
        }
    }
    
    @IBInspectable var ringSpacing: CGFloat = 2 {
        didSet {
            setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
       [ring1,ring2,ring3,ring4,ring5,ring6,ring7,ring8].forEach({
            $0.shadowOpacity = 0.0
            addSubview($0)
        })
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        ring1.frame = bounds
        ring2.frame = bounds.insetBy(dx: ringWidth + ringSpacing, dy: ringWidth + ringSpacing)
        ring3.frame = bounds.insetBy(dx: 2 * ringWidth + 2 * ringSpacing, dy: 2 * ringWidth + 2 * ringSpacing)
        ring4.frame = bounds.insetBy(dx: 3 * ringWidth + 3 * ringSpacing, dy: 3 * ringWidth + 3 * ringSpacing)
        ring5.frame = bounds.insetBy(dx: 4 * ringWidth + 4 * ringSpacing, dy: 4 * ringWidth + 4 * ringSpacing)
        ring6.frame = bounds.insetBy(dx: 5 * ringWidth + 5 * ringSpacing, dy: 5 * ringWidth + 5 * ringSpacing)
        ring7.frame = bounds.insetBy(dx: 6 * ringWidth + 6 * ringSpacing, dy: 6 * ringWidth + 6 * ringSpacing)
        ring8.frame = bounds.insetBy(dx: 7 * ringWidth + 7 * ringSpacing, dy: 7 * ringWidth + 7 * ringSpacing)
    }
}
