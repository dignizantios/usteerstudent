//
//  VersionCheck.swift
//  ASTrackingPupil
//
//  Created by Usteer on 13/05/19.
//  Copyright © 2019 Usteer. All rights reserved.
//

import Foundation
import Alamofire

class VersionCheck
{
    public static let shared = VersionCheck()
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    var newVersionAvailable: Bool?
    var appStoreVersion: String?
    
    func checkAppStore(callback: ((_ versionAvailable: Bool?, _ version: String?)->Void)? = nil)
    {
        let ourBundleId = Bundle.main.infoDictionary!["CFBundleIdentifier"] as! String
        Alamofire.request("https://itunes.apple.com/lookup?bundleId=\(ourBundleId)").responseJSON { response in
            var isNew: Bool?
            var versionStr: String?
            
            if let json = response.result.value as? NSDictionary,
                let results = json["results"] as? NSArray,
                let entry = results.firstObject as? NSDictionary,
                let appVersion = entry["version"] as? String
            {
                isNew = self.appVersion != appVersion
                versionStr = appVersion
            }
            
            self.appStoreVersion = versionStr
            self.newVersionAvailable = isNew
            callback?(isNew, versionStr)
        }
    }
}
