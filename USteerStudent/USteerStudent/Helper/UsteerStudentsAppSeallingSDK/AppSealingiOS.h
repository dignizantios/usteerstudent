//
//  AppsealingiOS.h
//  AppsealingiOS
//
//  Created by puzznic on 23/01/2019.
//  Copyright © 2019 Inka. All rights reserved.
//

#ifndef AppsealingiOS_h
#define AppsealingiOS_h

#define DETECTED_JAILBROKEN     0x00000001
#define DETECTED_DRM_DECRYPTED  0x00000002
#define DETECTED_DEBUG_ATTACHED 0x00000004


#import <Foundation/Foundation.h>

extern void Appsealing(void);
extern int ObjC_IsAbnormalEnvironmentDetected();

@interface AppSealingInterface : NSObject
- (int)_IsAbnormalEnvironmentDetected;
@end

#endif /* AppsealingiOS_h */
