#include "AppsealingiOS.h"

void iOS()
{
	Appsealing();
}

@interface AppSealingInterface()
@end

@implementation AppSealingInterface
- (instancetype)init
{
    return self;
}

-(int)_IsAbnormalEnvironmentDetected
{
    return ObjC_IsAbnormalEnvironmentDetected() ;
}
@end

/*
-------------------------------------------------------------------------------------------------------------
  Insert following code into "ViewController.swift" to show Simple UI (swift)
--------------------------------------------------------------------------------------------------------------
override func viewDidAppear(_ animated: Bool)
{
    super.viewDidAppear( animated );
    let inst: AppSealingInterface = AppSealingInterface();
    let tamper: Int32 = inst._IsAbnormalEnvironmentDetected();
    if ( tamper > 0 )
    {
        var msg = "Abnormal Environment Detected !!";
        if ( tamper & DETECTED_JAILBROKEN ) > 0
        { msg += "\n - Jailbroken"; }
        if ( tamper & DETECTED_DRM_DECRYPTED ) > 0
        { msg += "\n - Executable is not encrypted"; }
        if ( tamper & DETECTED_DEBUG_ATTACHED ) > 0
        { msg += "\n - App is debugged"; }
        let alertController = UIAlertController(title: "AppSealing", message: msg, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Confirm", style: .default,
                                                handler: { (action:UIAlertAction!) -> Void in exit(0); } ));
        self.present(alertController, animated: true, completion: nil);
    }
}

-------------------------------------------------------------------------------------------------------------
  Insert following code into "ViewController.mm" to show Simple UI (Objective-C)
--------------------------------------------------------------------------------------------------------------
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    int tamper = ObjC_IsAbnormalEnvironmentDetected();
    NSLog( @"tamper = %d", tamper );
    if ( tamper > 0 )
    {
        NSString* msg = @"Abnormal Environment Detected !!";
        if (( tamper & DETECTED_JAILBROKEN ) > 0 )
            msg = [msg stringByAppendingString:@"\n - Jailbroken"];
            if (( tamper & DETECTED_DRM_DECRYPTED ) > 0 )
                msg = [msg stringByAppendingString:@"\n - Executable is not encrypted"];
                if (( tamper & DETECTED_DEBUG_ATTACHED ) > 0 )
                    msg = [msg stringByAppendingString:@"\n - App is debugged"];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"AppSealing"
                                                                                   message:msg
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *confirm = [UIAlertAction actionWithTitle:@"Confirm" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) { exit(0); }];
                    [alert addAction:confirm];
        [self presentViewController:alert animated:YES completion:nil];
    }
}
*/
