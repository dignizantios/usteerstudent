//
//  ViewController.swift
//  USteerStudent
//
//  Created by Usteer on 1/4/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
import ZendeskSDK
import ZendeskCoreSDK

class ViewController: UIViewController
{
    @IBOutlet weak var lblLoginHeading: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnViewPolicy: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnPolicy: UIButton!
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnResendVerification: UIButton!

    @IBOutlet weak var vwUsernameSeperator: UIView!
    @IBOutlet weak var vwPasswordSeperator: UIView!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    @IBOutlet weak var lblIAgree: UILabel!

    @IBOutlet weak var vwForgotPassword: UIView!
    @IBOutlet weak var scrollviewForgotPassword: UIScrollView!
    @IBOutlet weak var txtForgotPasswordEmail: UITextField!
    @IBOutlet weak var btnForgotPasswordSubmit: UIButton!
    @IBOutlet weak var btnForgotPasswordCancel: UIButton!
    @IBOutlet weak var lblForgotPasswordHeading: UILabel!

    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        appDelegate.checkForVersionUpdate()
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()

        let secretKey = UserDefaults.standard.value(forKey:"secretKey")
        if secretKey == nil
        {
            self.fetchKeyAPI()
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        btnLogin.layer.cornerRadius = btnLogin.bounds.size.height / 2
        btnLogin.layer.masksToBounds = true
    }
}
//MARK:- UI Setup
extension ViewController
{
    func setUpUI()
    {
        [lblLoginHeading,lblUserName,lblPassword].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
        })
        lblLoginHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 30)
        lblLoginHeading.text = getCommonString(key: "Login_key")
        
        lblForgotPasswordHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblForgotPasswordHeading.text = getCommonString(key: "Forgot_password_key")
        lblForgotPasswordHeading.textColor = UIColor.white
        lblForgotPasswordHeading.backgroundColor = MySingleton.sharedManager.themeYellowColor

        [vwUsernameSeperator,vwPasswordSeperator].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        
        [txtUsername,txtPassword,txtForgotPasswordEmail].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 13)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
            $0?.delegate = self
        })
        
        btnViewPolicy.setTitleColor(MySingleton.sharedManager.themeYellowColor, for: .normal)
        btnViewPolicy.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        btnRegister.setTitleColor(MySingleton.sharedManager.themeYellowColor, for: .normal)
        btnRegister.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 13)
        
        btnLogin.setTitle(getCommonString(key: "Login_key").uppercased(), for: .normal)
        buttonUISetup(btnLogin, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        btnForgotPassword.setTitleColor(MySingleton.sharedManager.themeYellowColor, for: .normal)
        btnForgotPassword.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        btnForgotPassword.setTitle(getCommonString(key: "Forgot_password_key").capitalized + " ?", for: .normal)
        
        btnResendVerification.setTitleColor(MySingleton.sharedManager.themeYellowColor, for: .normal)
        btnResendVerification.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        [btnForgotPasswordCancel,btnForgotPasswordSubmit].forEach { (button) in
            button?.setTitleColor(UIColor.white, for: .normal)
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            button?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        }
        btnForgotPasswordCancel.backgroundColor = UIColor.clear
        btnForgotPasswordSubmit.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        btnViewPolicy.setTitle(getCommonString(key: "Privacy_policy_key"), for: .normal)
        
        lblIAgree.text =  getCommonString(key:"I_agree_to_the_key")
        lblIAgree.textColor = MySingleton.sharedManager.themeYellowColor
        lblIAgree.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        
        scrollviewForgotPassword.isHidden = true
    }
}

//MARK:- UITextfield Setup
extension ViewController : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
//MARK:- Button Action Zone
extension ViewController 
{
    @IBAction func btnLoginAction(_ sender: UIButton)
    {
        hideKeyboardDismiss()
        let objUserLocal = User()
        objUserLocal.strUsername = txtUsername.text?.trimmed() ?? ""
        objUserLocal.strPassword = txtPassword.text?.trimmed() ?? ""
        objUserLocal.strConsent = btnPolicy.isSelected ? "y" : "n"

        if(objUserLocal.isValidForLogin())
        {
            if(getFirebaseToken() == "")
            {
                appDelegate.connectToFcm()
                self.perform(#selector(loginCheckService), with: nil, afterDelay: 3.0)
            }
            else
            {
                loginCheckService()
            }
        }
        else
        {
            self.view.endEditing(true)
            showToast(message: objUserLocal.strValidationMessage)
        }
    }
    
    @IBAction func btnPolicyAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func btnViewAgreePolicyAction(_ sender: UIButton)
    {
        hideKeyboardDismiss()
        let obj = objStoryboard.instantiateViewController(withIdentifier: "PrivacyPoilcyVC")
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnRegisterAction(_ sender: UIButton)
    {
        hideKeyboardDismiss()
        let obj = objStoryboard.instantiateViewController(withIdentifier: "RegisterVC")
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnForgotPasswordAction(_ sender: UIButton)
    {
       scrollviewForgotPassword.isHidden = false
    }
    @IBAction func btnForgotPasswordSubmitAction(_ sender: UIButton)
    {
        if(txtForgotPasswordEmail.text?.trimmed().isEmpty)!
        {
            showToast(message: R.string.validationMessage.enter_email_key())
        }else if(!(txtForgotPasswordEmail.text?.isValidEmail() ?? "".isValidEmail()))
        {
            showToast(message: R.string.validationMessage.enter_username_key())
        }
        else{
            forgotPasswordService()
        }
    }
    @IBAction func btnForgotPasswordCancelAction(_ sender: UIButton)
    {
        scrollviewForgotPassword.isHidden = true
    }
    @IBAction func btnShowHidePassword(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = !sender.isSelected
    }
    @IBAction func btnResendVerificationCode(_ sender: UIButton)
    {
        if((txtUsername.text?.trimmed().isEmpty)! || (txtPassword.text?.trimmed().isEmpty)!)
        {
            showToast(message: " Re-enter your username and password and we will send you another email")
        }else if(!(txtUsername.text?.isValidEmail() ?? "".isValidEmail()))
        {
            showToast(message: R.string.validationMessage.enter_username_key())
        }
        else{
            resendEmail()
        }
    }
    
    //MARK:- Other methods
    func navigateToHome()
    {
        if(Defaults.value(forKey: R.string.keys.isLaunchFirstTime()) != nil)
        {
            if(Defaults.value(forKey: R.string.keys.isLaunchFirstTime()) as? Bool == true)
            {
                let frontViewController = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
                frontViewController.selectedParentVC = .userGuidance
                navigateToHome(frontViewController : frontViewController)
            }
            else
            {
                selectedMenuIndex = 1
                let frontViewController = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
                frontViewController.selectedVC = selectedTreeController.AppLaunch
                navigateToHome(frontViewController : frontViewController)
            }
        }
    }
    
    func navigateToHome(frontViewController : UIViewController)
    {
        let rearViewController = objStoryboard.instantiateViewController(withIdentifier: "SideMenuVC")
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        revealController?.rearViewRevealWidth = frontViewController.view.frame.size.width - 75
        revealController?.navigationController?.isNavigationBarHidden = false
        revealController?.delegate = self
        let window = UIApplication.shared.delegate?.window as? UIWindow
        window?.rootViewController = revealController
        window?.makeKeyAndVisible()
    }
    
}
//MARK:- Service
extension ViewController 
{
    @objc func loginCheckService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let strUserName = txtUsername.text ?? ""
            let strPassword = txtPassword.text ?? ""
            let strConsent = btnPolicy.isSelected ? "y" : "n"
            LoginService().loginRequest(username:strUserName,password: strPassword,consent :strConsent,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    self.view.endEditing(true)
                    if(json["status"].stringValue == "1")
                    {
                        var data = json["data"]
                        var dictDecrepted = JSON()
                        dictDecrepted["user_id"].stringValue = decreptedString(string: data["user_id"].stringValue)
                        dictDecrepted["name"].stringValue = decreptedString(string: data["name"].stringValue)
                        dictDecrepted["lastname"].stringValue = decreptedString(string: data["lastname"].stringValue)
                        dictDecrepted["fullname"].stringValue = decreptedString(string: data["fullname"].stringValue)
                        dictDecrepted["username"].stringValue = decreptedString(string: data["username"].stringValue)
                        dictDecrepted["gender"].stringValue = decreptedString(string: data["gender"].stringValue)
                        
                        data["user_id"].stringValue = dictDecrepted["user_id"].stringValue
                        data["name"].stringValue = dictDecrepted["name"].stringValue
                        data["lastname"].stringValue = dictDecrepted["lastname"].stringValue
                        data["fullname"].stringValue = dictDecrepted["fullname"].stringValue
                        data["username"].stringValue = dictDecrepted["username"].stringValue
                        data["gender"].stringValue = dictDecrepted["gender"].stringValue
                        print("Data Decripted:", data)
                        
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: R.string.keys.userDetail())
                        if(Defaults.value(forKey: R.string.keys.isLaunchFirstTime()) != nil)
                        {
                            Defaults.setValue(false, forKey: R.string.keys.isLaunchFirstTime())
                        }
                        else
                        {
                            Defaults.setValue(true, forKey: R.string.keys.isLaunchFirstTime())
                        }
                        selectedMenuIndex = 0
                        if(data["is_private"].stringValue == "")
                        {
                            appDelegate.navigateToUserSelect()
                        }
                        else
                        {
                            self.navigateToHome()
                        }
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "5")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func resendEmail()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let strUserName = txtUsername.text ?? ""
            let strPassword = txtPassword.text ?? ""
            LoginService().resendRequest(username:strUserName,password: strPassword,completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    self.view.endEditing(true)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func forgotPasswordService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param = ["email" : txtForgotPasswordEmail.text ?? ""]
            BasicTaskService().ForgotPasswordService(param:param,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    self.view.endEditing(true)
                    if(json["status"].stringValue == "1")
                    {
                        self.scrollviewForgotPassword.isHidden = true
                        self.txtForgotPasswordEmail.text = ""
                        showToast(message: json["message"].stringValue)
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func fetchKeyAPI()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            BasicTaskService().fetchGeneratedKey(completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    self.view.endEditing(true)
                    Defaults.removeObject(forKey: "secretKey")
                    Defaults.removeObject(forKey: "secretivKey")
                    Defaults.removeObject(forKey: "ZenDeskAppID")
                    Defaults.removeObject(forKey: "ZenDeskClientId")
                    Defaults.removeObject(forKey: "ZenDeskSupportURL")
                    Defaults.synchronize()
                    let zendeskDetails = json["zendesk_pupil"]
                    Defaults.setValue(decreptedString(string: zendeskDetails["kZenDeskAppID"].stringValue), forKey: "ZenDeskAppID")
                    Defaults.setValue(decreptedString(string: zendeskDetails["kZenDeskClientId"].stringValue), forKey: "ZenDeskClientId")
                    Defaults.setValue(decreptedString(string: zendeskDetails["kZenDeskSupportURL"].stringValue), forKey: "ZenDeskSupportURL")
                    Defaults.synchronize()
                    
                    if Defaults.value(forKey: "ZenDeskAppID") as? String != nil && Defaults.value(forKey: "ZenDeskClientId") as? String != nil && Defaults.value(forKey: "ZenDeskSupportURL") as? String != nil
                    {
                        if let appID = Defaults.value(forKey: "ZenDeskAppID") as? String,let clientID = Defaults.value(forKey: "ZenDeskClientId") as? String,let supportURL = Defaults.value(forKey: "ZenDeskSupportURL") as? String{
                            Zendesk.initialize(appId: appID, clientId: clientID, zendeskUrl: supportURL)
                            Support.initialize(withZendesk: Zendesk.instance)
                        }
                    }
                    Defaults.setValue(decreptedString(string: json["secret_key"].stringValue), forKey: "secretKey")
                    Defaults.setValue(decreptedString(string: json["secret_iv"].stringValue), forKey: "secretivKey")
                    Defaults.synchronize()
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
            self.hideLoader()
        }
    }
}

