//
//  UITextview+Extension.swift
//  USteerTeacher
//
//  Created by Usteer on 1/3/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import UIKit
extension UITextView
{
    func numberOfLines(textView: UITextView) -> Int
    {
        let layoutManager = textView.layoutManager
        let numberOfGlyphs = layoutManager.numberOfGlyphs
        var lineRange: NSRange = NSMakeRange(0, 1)
        var index = 0
        var numberOfLines = 0
        
        while index < numberOfGlyphs {
            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
            index = NSMaxRange(lineRange)
            numberOfLines += 1
        }
        return numberOfLines
    }
}
