//
//  UIButton+Extension.swift
//  USteerStudent
//
//  Created by Usteer on 4/30/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation


extension UIButton
{
    func setUpGlow()
    {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shouldRasterize = true
    }
    func startBlink() {
        UIView.animate(withDuration: 0.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}
