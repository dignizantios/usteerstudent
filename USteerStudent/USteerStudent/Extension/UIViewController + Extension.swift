//
//  UIViewController + Extension.swift
//  VeryEasyCar
//
//  Created by Usteer on 09/10/17.
//  Copyright © 2017 Usteer. All rights reserved.
//

import Foundation
import UIKit
import Toaster
import Charts
import SwiftyJSON
import NVActivityIndicatorView
import Alamofire
import UserNotifications
import CryptoSwift
import CommonCrypto

extension UIViewController : SWRevealViewControllerDelegate
{
    //MARK: - Methods
    func hideKeyboardDismiss()
    {
        self.view.endEditing(true)
    }
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    //MARK: - Done button on keyboard
    func addDoneButtonOnKeyboard(textfield : UITextField)
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x:0,y: 0,width: UIScreen.main.bounds.width,height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(doneButtonAction))
        done.tintColor = MySingleton.sharedManager.themeDarkGrayColor
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        textfield.inputAccessoryView = doneToolbar
    }
    @objc func doneButtonAction()
    {
        self.view.endEditing(true)
    }
    //MARK: -
    func navigateToLogin()
    {
        Defaults.removeObject(forKey: R.string.keys.userDetail())
        Defaults.removeObject(forKey: R.string.keys.kIsClickOnUpdate())
        Defaults.removeObject(forKey: "secretKey")
        Defaults.removeObject(forKey: "secretivKey")
        Defaults.synchronize()
        treeZoomLevel = 0.3
        treeConsetOffset  = CGPoint(x:0,y:0)
        
        let vc = objStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        let rearNavigation = UINavigationController(rootViewController: vc)
        appDelegate.window?.rootViewController = rearNavigation
    }
    func getFactorName(str : String) -> String
    {
        if(str.lowercased() == "too")
        {
            return "Trust of others"
        }
        else if(str.lowercased() == "sd")
        {
            return "Self-disclosure"
        }
        else if(str.lowercased() == "tos")
        {
            return "Trust of self"
        }
        else  //Seeking Change
        {
            return "Seeking change"
        }
    }
    func getFactors(str : String) -> String
    {
        if(str == "TOO")
        {
            return TargetFactor.TOO.rawValue
        }
        else if(str == "SD")
        {
            return TargetFactor.SD.rawValue
        }
        else if(str == "TOS")
        {
            return TargetFactor.TOS.rawValue
        }
        else  //Seeking Change
        {
            return TargetFactor.SC.rawValue
        }
    }
    func getSelectedFactors(str : String) -> TargetFactor
    {
        if(str == "TOO")
        {
            return TargetFactor.TOO
        }
        else if(str == "SD")
        {
            return TargetFactor.SD
        }
        else if(str == "TOS")
        {
            return TargetFactor.TOS
        }
        else  //Seeking Change
        {
            return TargetFactor.SC
        }
    }
    func setUpdatedUserDetails(json : JSON)
    {
        let data = json
        guard let rowdata = try? data.rawData() else {return}
        Defaults.setValue(rowdata, forKey: R.string.keys.userDetail())
        Defaults.synchronize()
    }
    func setPrivateUserValueToUserDetails(is_private : String)
    {
        let userDetail = UserDefaults.standard.value(forKey: R.string.keys.userDetail()) as? Data
        var dictData = JSON(userDetail!)
        dictData["is_private"].stringValue = is_private
        setUpdatedUserDetails(json : dictData)
    }
    //MARK: - Navigation bar setup
    func setUpNavigationBarWithTitle(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false

        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.navigationItem.titleView = HeaderLabel
        self.navigationItem.hidesBackButton = true
    }
    func setUpNavigationBarWithTitleAndBack(strTitle : String) 
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false

        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)

        self.navigationItem.titleView = HeaderLabel
    }
    func setUpNavigationBarWithTitleAndBackPolicy(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        self.navigationItem.titleView = HeaderLabel
    }
    @objc func backButtonAction(_ sender : UIBarButtonItem)
    {
        self.navigationController?.popViewController(animated: true)
    }
    func setUpNavigationBarWithTitleAndSideMenu(strTitle : String)
    {
        //rearViewRevealWidth
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false

        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_menubar_white_header"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 44))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.textAlignment = .center
        HeaderLabel.numberOfLines = 2
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.navigationItem.titleView = HeaderLabel
    }
    func setUpNavigationBarWithTitleAndBackAndSideMenu(strTitle : String)
    {
        //rearViewRevealWidth
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        // create the button
        let leftImage  = UIImage(named: "ic_menubar_white_header")!.withRenderingMode(.alwaysOriginal)
        let leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 20))
        leftButton.setBackgroundImage(leftImage, for: .normal)
        leftButton.addTarget(SWRevealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
        
        let leftBackImage  = UIImage(named: "ic_back_arrow_white_header")!.withRenderingMode(.alwaysOriginal)
        let leftBackButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 20))
        leftBackButton.setBackgroundImage(leftBackImage, for: .normal)
        leftBackButton.addTarget(self, action: #selector(backButtonAction), for:.touchUpInside)
        
        let menuButton = UIBarButtonItem(customView: leftButton)
        let backButton = UIBarButtonItem(customView: leftBackButton)
        
        self.navigationItem.hidesBackButton = false
        self.navigationItem.setLeftBarButtonItems([menuButton,backButton], animated: true)
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.textAlignment = .center
        HeaderLabel.numberOfLines = 2
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.navigationItem.titleView = HeaderLabel
    }
    
    //MARK: - Button Action
    @IBAction func btnBackAction(_ sender : UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: - UI setup
    func buttonUISetup(_ sender : UIButton,textColor : UIColor)
    {
        sender.backgroundColor = MySingleton.sharedManager.themeYellowColor
        sender.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        sender.setTitleColor(textColor, for: .normal)
    }
    
    //MARK: - Assessment delete from local
    func deleteAllAssessments()
    {
        let deleteAnswerQuery = "delete from tbl_answers where question_id In (select question_id from tbl_questions where assessment_id In(select assessment_id from tbl_assessment where user_id = \(Int(getUserDetail("user_id")) ?? 0)))"
        print("delete answer - ",deleteAnswerQuery)
        if(executeQuery(strQuery: deleteAnswerQuery)){
            print("Answer removed")
            let updateAnswerIdOrder = "UPDATE sqlite_sequence SET seq = 0 WHERE name = 'tbl_answers'"
            if(executeQuery(strQuery: updateAnswerIdOrder)){
                print("Order updated")
            }
        }else{
            print("Answer not removed")
        }
        //---------
        
        let deleteQuestionQuery = "delete from tbl_questions where assessment_id In(select assessment_id from tbl_assessment where user_id = \(Int(getUserDetail("user_id")) ?? 0))"
        if(executeQuery(strQuery: deleteQuestionQuery)){
            print("Questions removed")
            let updateQuestionIdOrder = "UPDATE sqlite_sequence SET seq = 0 WHERE name = 'tbl_questions'"
            if(executeQuery(strQuery: updateQuestionIdOrder)){
                print("Order updated")
            }
        }else{
            print("Questions not removed")
        }
        //--------
        
        let deleteIntroQuery = "delete from tbl_intro_sentences where assessment_id In(select assessment_id from tbl_assessment where user_id = \(Int(getUserDetail("user_id")) ?? 0))"
        if(executeQuery(strQuery: deleteIntroQuery)){
            print("Intro statements removed")
            let updateIntroIdOrder = "UPDATE sqlite_sequence SET seq = 0 WHERE name = 'tbl_intro_sentences'"
            if(executeQuery(strQuery: updateIntroIdOrder)){
                print("Order updated")
            }
        }else{
            print("Intro statements not removed")
        }
        
        //------------
        let deleteClosersQuery = "delete from tbl_closer_statements where assessment_id In(select assessment_id from tbl_assessment where user_id = \(Int(getUserDetail("user_id")) ?? 0))"
        if(executeQuery(strQuery: deleteClosersQuery))
        {
            print("Closer statements removed")
            let updateCloserIdOrder = "UPDATE sqlite_sequence SET seq = 0 WHERE name = 'tbl_closer_statements'"
            if(executeQuery(strQuery: updateCloserIdOrder)){
                print("Order updated")
            }
        }else{
            print("Closer statements not removed")
        }
        
        //------------
        let userid = Int(getUserDetail("user_id")) ?? 0
        let deleteAssessmentQuery = "delete from tbl_assessment where user_id = \(userid)"
        if(executeQuery(strQuery: deleteAssessmentQuery))
        {
            print("Assessments removed")
            let updateAssessmentIdOrder = "UPDATE sqlite_sequence SET seq = 0 WHERE name = 'tbl_assessment'"
            if(executeQuery(strQuery: updateAssessmentIdOrder)){
                print("Order updated")
            }
        }else{
            print("Assessments not removed")
        }
    }
    
    func SubStringColorChange(string : String,substring : String) -> NSAttributedString
    {
        print("substring - ",substring)
        let str = string.replacingOccurrences(of: substring, with: substring.uppercased())
        print("str - ",str)
        let strNumber = str as NSString
        let range = (strNumber).range(of: substring.uppercased())
        let attribute = NSMutableAttributedString.init(string: str)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: MySingleton.sharedManager.themeYellowColor , range: range)
        attribute.addAttribute(NSAttributedStringKey.font, value: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16) , range: range)
        return attribute
    }
}

//MARK: - Chart helper methods
extension UIViewController : ChartViewDelegate
{
    //MARK: - reward
    func rewardText(reward : Int) -> String
    {
        var strMessage = ""
        if(reward == 1)
        {
            strMessage = R.string.staticMessages.signpost_happy_key()
        }else if(reward == 2)
        {
            strMessage = R.string.staticMessages.signpost_average_key()
        }else{
            strMessage = R.string.staticMessages.signpost_sad_key()
        }
        return strMessage
    }
    func rewardEmoji(reward : Int) -> UIImage
    {
        var img : UIImage?
        if(reward == 1)
        {
            img = #imageLiteral(resourceName: "ic_smile_1")
        }else if(reward == 2)
        {
            img = #imageLiteral(resourceName: "ic_smile_2")
        }else{
            img = #imageLiteral(resourceName: "ic_smile_3")
        }
        return img!
    }
    func setLimitLinesDataSet(arrayLimitLine : [ChartDataEntry]) -> LineChartDataSet
    {
        let dataSet = dataSetCreate(values : arrayLimitLine , dataSetHeading : "" , lineColor : MySingleton.sharedManager.themeDarkGrayColor)
        dataSet.lineDashLengths = [15, 5.5]
        dataSet.highlightLineDashLengths = [15, 5.5]
        dataSet.circleRadius = 0
        dataSet.highlightEnabled = false
        return dataSet
    }
    
    //MARK: - Get Selected car
    func getSelectedCar(chartIcon : Int) -> UIImage
    {
        var img : UIImage?
        if(chartIcon == 1)
        {
            img = #imageLiteral(resourceName: "small_car_1")
        }else if(chartIcon == 2)
        {
            img = #imageLiteral(resourceName: "small_car_2")
        }else if(chartIcon == 3)
        {
            img = #imageLiteral(resourceName: "small_car_3")
        }else if(chartIcon == 4)
        {
            img = #imageLiteral(resourceName: "small_car_4")
        }else if(chartIcon == 5)
        {
            img = #imageLiteral(resourceName: "small_car_5")
        }else if(chartIcon == 6)
        {
            img = #imageLiteral(resourceName: "small_car_6")
        }
        else if(chartIcon == 7)
        {
            img = #imageLiteral(resourceName: "wheel_gray_one")
        }else if(chartIcon == 8)
        {
            img = #imageLiteral(resourceName: "wheel_gray_two")
        }else if(chartIcon == 9)
        {
            img = #imageLiteral(resourceName: "wheel_gray_three")
        }
        else{
            img = #imageLiteral(resourceName: "wheel_gray_four")
        }
        return img!
    }
    func getSelectedCarForProfile(chartIcon : Int) -> UIImage
    {
        var img : UIImage?
        if(chartIcon == 1)
        {
            img = #imageLiteral(resourceName: "car_1")
        }else if(chartIcon == 2)
        {
            img = #imageLiteral(resourceName: "car_2")
        }else if(chartIcon == 3)
        {
            img = #imageLiteral(resourceName: "car_3")
        }
        else if(chartIcon == 4)
        {
            img = #imageLiteral(resourceName: "car_4")
        }else if(chartIcon == 5)
        {
            img = #imageLiteral(resourceName: "car_5")
        }else if(chartIcon == 6)
        {
            img = #imageLiteral(resourceName: "car_6")
        }else if(chartIcon == 7)
        {
            img = #imageLiteral(resourceName: "wheel_gray_one")
        }else if(chartIcon == 8)
        {
            img = #imageLiteral(resourceName: "wheel_gray_two")
        }else if(chartIcon == 9)
        {
            img = #imageLiteral(resourceName: "wheel_gray_three")
        }
        else{
            img = #imageLiteral(resourceName: "wheel_gray_four")
        }
        
        return img!
    }
    //MARK: - Report point view set
    func getColorForCar(color : String) -> UIColor
    {
        var carColor : UIColor!
        if(color == "red")
        {
            carColor = MySingleton.sharedManager.themeRedColor
        }else if(color == "amber")
        {
            carColor = MySingleton.sharedManager.themeYellowColor
        }else if(color == "green")
        {
            carColor = MySingleton.sharedManager.themeGreenColor
        }else if(color == "blue")
        {
            carColor = MySingleton.sharedManager.themeBlueColor
        }else //if(color == "grey")
        {
            carColor = MySingleton.sharedManager.themeLightGrayColor
        }
        return carColor.withAlphaComponent(1.0)
    }
    func colorSetForDataPoint(score : Double) -> UIColor
    {
        var color : UIColor!
        if(score >= 0 && score <= 3)
        {
            color = MySingleton.sharedManager.themeRedColor
        }else if(score >= 3 && score <= 4.5)
        {
            color = MySingleton.sharedManager.themeYellowColor
        }else if(score >= 4.5 && score <= 6.75)
        {
            color = MySingleton.sharedManager.themeGreenColor
        }else if(score >= 6.75 && score <= 7.5)
        {
            color = MySingleton.sharedManager.themeBlueColor
        }else if(score >= 7.5 && score <= 9.75)
        {
            color = MySingleton.sharedManager.themeGreenColor
        }else if(score >= 9.75 && score <= 11.25)
        {
            color = MySingleton.sharedManager.themeYellowColor
        }else //if(score >= 11.25 && score <= 15)
        {
            color = MySingleton.sharedManager.themeRedColor
        }
        return color.withAlphaComponent(1.0)
    }
    func dataSetCreate(values : [ChartDataEntry] , dataSetHeading : String , lineColor : UIColor) -> LineChartDataSet
    {
        let dataSet = LineChartDataSet(entries: values, label: dataSetHeading)
        dataSet.axisDependency = .left
        dataSet.setColor(lineColor)
        dataSet.setCircleColor(lineColor)
        dataSet.lineWidth = 2
        dataSet.circleRadius = 0
        dataSet.fillAlpha = 0.65
        dataSet.drawCircleHoleEnabled = false
        return dataSet
    }
    func setDataCount(chartView: LineChartView, dataSetArray1: [ChartDataEntry], dataSetArray2: [ChartDataEntry],dataSetHeading1: String,dataSetHeading2: String, dataLineColor1: UIColor, dataLineColor2: UIColor)
    {
        let set1 = dataSetCreate(values : dataSetArray1, dataSetHeading : dataSetHeading1 , lineColor : dataLineColor1)
        let set2 = dataSetCreate(values : dataSetArray2, dataSetHeading : dataSetHeading2 , lineColor : dataLineColor2)
        
        set1.lineDashLengths = [5, 2.5]
        set1.highlightLineDashLengths = [5, 2.5]
        set1.circleRadius = 0
        
        ///----
        let data = LineChartData(dataSets: [set1,set2])
        data.setValueTextColor(.clear)
        data.setValueFont(.systemFont(ofSize: 9))
        
        chartView.data = data
    }
    //MARK: - Chart data set
    func xLabelsArray(mergeXLabelsArray : [JSON]) -> [String]
    {
        var arrayDateLocal : [String] = []
        mergeXLabelsArray.forEach({ (json) in
            let strDate = json["dateTobedisplay"].stringValue
            let strDateValue = strDate.prefix(8)
            arrayDateLocal.append("\(strDateValue)")
        })
        
        var arrayDistinct = Array(Set(arrayDateLocal)).sorted()
        arrayDistinct.forEach { (str) in
            if(str == "00000000"){
                let index = arrayDistinct.index(of: str)!
                arrayDistinct.remove(at: index)
            }
        }
        
        let strMinDate = arrayDistinct.min()
        let strMaxDate = arrayDistinct.max()
        
        let dateMin = StringToDate(Formatter: "yyyyMMdd", strDate: strMinDate!)
        let dateMax = StringToDate(Formatter: "yyyyMMdd", strDate: strMaxDate!)
//        let dateMaxOneDay = getDate(timeInterval: 5)
        
        let arrayXlabelsLocal = generateStringArrayBetweenTwoDates(startDate: dateMin, endDate: dateMax)
        return arrayXlabelsLocal
    }
    func getDate(timeInterval:Int) -> Date
    {
        var beforeTimeInterval = DateComponents()
        beforeTimeInterval.day = timeInterval
        let beforeDate = Calendar.current.date(byAdding: beforeTimeInterval, to: Date())
        return beforeDate!
    }
    
    func configureChart(chartview : LineChartView)
    {
        //let backgroundImage = UIImage(named: "background_chart.png")?.stretchableImage(withLeftCapWidth: 0, topCapHeight: 0)
        
        chartview.delegate = self
        chartview.chartDescription?.enabled = false
        chartview.dragEnabled = true
        chartview.setScaleEnabled(false)
        chartview.pinchZoomEnabled = true
        chartview.scaleXEnabled = true
        chartview.drawGridBackgroundEnabled = true
        chartview.doubleTapToZoomEnabled = true
        chartview.autoScaleMinMaxEnabled = false
        chartview.clipsToBounds = false
        chartview.clipValuesToContentEnabled = false
        chartview.setVisibleXRangeMaximum(3)
        chartview.extraRightOffset = 30
        chartview.extraLeftOffset = 10
        chartview.gridBackgroundColor = MySingleton.sharedManager.themeChatGrayColor
        chartview.leftAxis.drawGridLinesEnabled = false
        chartview.leftAxis.axisMaximum = 15
        chartview.leftAxis.drawTopYLabelEntryEnabled = true
        
        let rightAxis = chartview.rightAxis
        rightAxis.drawLabelsEnabled = false
        rightAxis.drawGridLinesEnabled = false
//        rightAxis.drawLimitLinesBehindDataEnabled = true

        let xAxis = chartview.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 5)
        xAxis.labelTextColor = .black
        xAxis.labelRotationAngle = -90
        xAxis.granularity = 1
        xAxis.labelCount = 3
        xAxis.wordWrapEnabled = false
        xAxis.drawGridLinesEnabled = true
        xAxis.drawAxisLineEnabled = true
        xAxis.avoidFirstLastClippingEnabled = false
        xAxis.axisRange = 3
        xAxis.gridColor = MySingleton.sharedManager.themeChartLinesGrayColor
        
        //Legend
        let l = chartview.legend
        l.form = .square
        l.formSize = 3
        l.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 8)
        l.textColor = .black
        l.horizontalAlignment = .left
        l.verticalAlignment = .top
        l.orientation = .horizontal
        l.drawInside = false
        l.enabled = false
        
        let leftAxis = chartview.leftAxis
        leftAxis.labelTextColor = UIColor(red: 186/255, green: 29/255, blue: 43/255, alpha: 1)
        leftAxis.labelFont = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 6)
        leftAxis.axisMaximum = 15
        leftAxis.axisMinimum = 0
        leftAxis.drawGridLinesEnabled = true
        leftAxis.drawAxisLineEnabled = true
        leftAxis.granularityEnabled = true
        leftAxis.gridColor = MySingleton.sharedManager.themeChartLinesGrayColor
        leftAxis.gridLineWidth = 1
        leftAxis.granularity = 1
        leftAxis.drawTopYLabelEntryEnabled = false
        leftAxis.drawLimitLinesBehindDataEnabled = true
        
//        let labelMiddle = UILabel(frame: CGRect(x:10,y:0,width:100,height:chartview.viewPortHandler.chartHeight))
//        labelMiddle.text = "7.5"
//        labelMiddle.backgroundColor = UIColor.red
//        chartview.addSubview(labelMiddle)
//
//        print("Chart drawing height - ",chartview.viewPortHandler.chartHeight)
        
        
        let limitLine = ChartLimitLine(limit: Double(7.5) , label: "")
        limitLine.lineColor = .white
        limitLine.lineWidth = 5
        limitLine.lineDashLengths = [24.0]
        leftAxis.addLimitLine(limitLine)
        
        let limitLine1 = ChartLimitLine(limit: Double(2.75) , label: "")
        limitLine1.lineColor = .black
        limitLine1.lineWidth = 2
        limitLine1.lineDashLengths = [10.0]
        leftAxis.addLimitLine(limitLine1)
        
        let limitLine2 = ChartLimitLine(limit: Double(3.0) , label: "")
        limitLine2.lineColor = .black
        limitLine2.lineWidth = 2
        limitLine2.lineDashLengths = [10.0]
        leftAxis.addLimitLine(limitLine2)
        
        let limitLine3 = ChartLimitLine(limit: Double(12.0) , label: "")
        limitLine3.lineColor = .black
        limitLine3.lineWidth = 2
        limitLine3.lineDashLengths = [10.0]
        leftAxis.addLimitLine(limitLine3)
        
        let limitLine4 = ChartLimitLine(limit: Double(12.25) , label: "")
        limitLine4.lineColor = .black
        limitLine4.lineWidth = 2
        limitLine4.lineDashLengths = [10.0]
        leftAxis.addLimitLine(limitLine4)

        //
    }
    func createViewForPoint(tintedImage : UIImage,showLeft : Bool,showRight:Bool,chartIcon: Int) -> UIView
    {
        /**/
        
        if(chartIcon > 6)
        {
            let contentView = UIView(frame: CGRect(x:0,y:0,width:55,height:36))
            contentView.backgroundColor = UIColor.white
            let carImage = UIImageView(frame:CGRect(x:12.5,y:3,width:30,height:30))
            carImage.contentMode = .scaleAspectFit
            carImage.image = tintedImage
            
            let leftImage = UIImageView(frame:CGRect(x:4.5,y:23,width:8,height:10))
            leftImage.image = #imageLiteral(resourceName: "ic_notes_small")
            
            let rightImage = UIImageView(frame:CGRect(x:43,y:3,width:10,height:10))
            rightImage.image = #imageLiteral(resourceName: "ic_warning")
            
            contentView.addSubview(carImage)
            
            if(showLeft){
                contentView.addSubview(leftImage)
            }
            if(showRight){
                contentView.addSubview(rightImage)
            }
            return contentView
        }else{
            //for car icon
            let contentView = UIView(frame: CGRect(x:0,y:0,width:40,height:28))
            contentView.backgroundColor = UIColor.white
            let carImage = UIImageView(frame:CGRect(x:5,y:6.5,width:30,height:15))
            carImage.contentMode = .scaleAspectFit
            carImage.image = tintedImage
            
            let leftImage = UIImageView(frame:CGRect(x:2,y:9,width:8,height:10))
            leftImage.image = #imageLiteral(resourceName: "ic_notes_small")
            
            let rightImage = UIImageView(frame:CGRect(x:30,y:1.5,width:9,height:9))
            rightImage.image = #imageLiteral(resourceName: "ic_warning")
            
            contentView.addSubview(carImage)
            
            if(showLeft){
                contentView.addSubview(leftImage)
            }
            if(showRight){
                contentView.addSubview(rightImage)
            }
            return contentView
        }
        
    }
    
    
}
//MARK:-
extension UIViewController : UNUserNotificationCenterDelegate
{
    func RemoveAllNotifications()
    {
        
        let center = UNUserNotificationCenter.current()
        center.removeAllDeliveredNotifications() // To remove all delivered notifications
        center.removeAllPendingNotificationRequests()
    }
    func openAppStore()
    {
        
        //TODO:- Change URL of  pupil app
        if let url = URL(string: "itms-apps://itunes.apple.com/app/usteer-student/id1434958703?ls=1&mt=8"),
            UIApplication.shared.canOpenURL(url){
            UIApplication.shared.open(url, options: [:]) { (opened) in
                if(opened){
                    print("App Store Opened")
//                      showToast(message: "App Store Opened")
                }
            }
        } else {
            print("Can't Open URL on Simulator")
            showToast(message: "Can't Open URL on Simulator")
        }
    }
}
//MARK:- Service
extension UIViewController
{
    func popUpParentAlert()
    {
        let name = "Hi \(getUserDetail("fullname"))"
        let Message = "\(name) \nYour free period using USTEER which was given to you by child’s school has now ended\n\nIt’s time to update to continue accessing all the benefits of USTEER"
        let attributedString = NSMutableAttributedString(string:Message)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: NSRange(location: 0, length: Message.length))
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15), range: NSRange(location: 0, length: Message.length))
        let range = (Message as NSString).range(of:name)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: MySingleton.sharedManager.themeYellowColor , range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
        let obj = InfoPopUpVC()
        obj.attributed = attributedString
        obj.colorPopUpContent = UIColor.white
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
    
    func getAppVersion()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            BasicTaskService().GetAppVersionService(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    let dict = json["data"]
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        //TODO:- check correct dict
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: R.string.keys.appVersionDetail())
                        Defaults.synchronize()
                        
                        let dictUpdate = data["is_update"]
                        guard let rowdataVersion = try? dictUpdate.rawData() else {return}
                        Defaults.setValue(rowdataVersion, forKey: R.string.keys.kVersionUpdateDetail())
                        Defaults.synchronize()
                        
                        let phase = dict["phase"].intValue
                        let linkType = dict["link_type"].intValue
                        
                        Defaults.setValue(phase, forKeyPath: "appVersion")
                        Defaults.setValue(linkType, forKeyPath: "link_type")
                        Defaults.synchronize()
                        
                        self.setPrivateUserValueToUserDetails(is_private: dict["is_private"].stringValue)
            
                        let dictNotification = dict["notification"]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "relo adSideMenuTable"), object: dictNotification)
                        
                        if dict["is_parent_link_expired"].intValue == 1
                        {
                            self.popUpParentAlert()
                        }
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                        
                        let dictNotification = dict["notification"]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
        print("Application version :::::: ",Defaults.value(forKey: "appVersion") as? Int ?? 1)
    }

}
extension UIViewController : NVActivityIndicatorViewable
{
    func showLoader()
    {
        let LoaderString:String = getCommonString(key: "Loading_key")
        let LoaderSize = CGSize(width: 30, height: 30)
        startAnimating(LoaderSize, message: LoaderString, type: NVActivityIndicatorType.circleStrokeSpin)
    }
    func hideLoader()
    {
        stopAnimating()
    }
}

extension UIViewController {
    func embed(_ viewController:UIViewController, inView view:UIView){
        viewController.willMove(toParentViewController: self)
        viewController.view.frame = view.bounds
        view.addSubview(viewController.view)
        self.addChildViewController(viewController)
        viewController.didMove(toParentViewController: self)
    }

    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.visibleViewController!.topMostViewController()
        }
        if let tab = self.presentedViewController as? UITabBarController {
            if let selectedTab = tab.selectedViewController {
                return selectedTab.topMostViewController()
            }
            return tab.topMostViewController()
        }
        return self.presentedViewController!.topMostViewController()
    }
}

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }
}
