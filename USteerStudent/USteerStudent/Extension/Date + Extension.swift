//
//  Date + Extension.swift
//  VeryEasyCar
//
//  Created by Usteer on 12/20/17.
//  Copyright © 2017 Usteer. All rights reserved.
//

import Foundation

extension Date {
    
    func interval(ofComponent comp: Calendar.Component, fromDate date: Date) -> Int {
        
        let currentCalendar = Calendar.current
        guard let start = currentCalendar.ordinality(of: comp, in: .era, for: date) else { return 0 }
        guard let end = currentCalendar.ordinality(of: comp, in: .era, for: self) else { return 0 }
        
        print("start - ",start)
        print("end - ",end)
        return end - start
    }
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    func dateByAddingDays(inDays:NSInteger)->Date{
        let date = self
        return Calendar.current.date(byAdding: .day, value: inDays, to: date)!
    }
    func dateByAddingYears(inYears:NSInteger)->Date{
        let date = self
        return Calendar.current.date(byAdding: .year, value: inYears, to: date)!
    }
}
func generateDatesArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[Date]
{
    var datesArray: [Date] =  [Date]()
    var startDate = startDate
    let calendar = Calendar.current
    
    while startDate <= endDate {
        datesArray.append(startDate)
        startDate = calendar.date(byAdding: .day, value: 1, to: startDate)!
    }
    return datesArray
}
func generateStringArrayBetweenTwoDates(startDate: Date , endDate:Date) ->[String]
{
    print("startDate - ",startDate)
    print("endDate - ",endDate)

    var strDatesArray: [String] =  [String]()
    
    let dateStart = startDate
    let dateEnd = endDate
    
    let calendar = Calendar.current
    let formatter = "d MMM,yyyy"
    
    let strStartDate = DateToString(Formatter: formatter, date: dateStart)
    var dateStartConverted = StringToDate(Formatter: formatter, strDate: strStartDate)
    
    let strEndDate = DateToString(Formatter: formatter,date: dateEnd)
    let dateEndConverted = StringToDate(Formatter: formatter, strDate: strEndDate)

    while dateStartConverted <= dateEndConverted
    {
        let strStartConvertedNew = DateToString(Formatter: formatter, date: dateStartConverted)
        let dateStartConvertedNew = StringToDate(Formatter: formatter, strDate: strStartConvertedNew)
        strDatesArray.append(strStartConvertedNew)
        
        dateStartConverted = calendar.date(byAdding: .day, value: 1, to: dateStartConvertedNew)!
    }
    return strDatesArray
}
