//
//  UIView+Extension.swift
//  USteerTeacher
//
//  Created by Usteer on 1/3/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    func fadeIn(_ duration: TimeInterval = 0.40, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            self.isHidden = false
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.40, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
           // self.isHidden = true

        }, completion: completion)
    }
    
    func TreeFadeIn(_ duration: TimeInterval = 0.40, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            self.isHidden = false
        }, completion: completion)  }
    
    func TreeFadeOut(_ duration: TimeInterval = 0.40, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
            // self.isHidden = true
            
        }, completion: completion)
    }
    func Enter()
    {
        self.alpha = 1
        let top = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = top
        }, completion: nil)
    }
    func Out()
    {
        let height = self.frame.size.height
        let bottom = CGAffineTransform(translationX: 0, y: -height)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = bottom
            
        }, completion:  { (finished: Bool) in
            self.alpha = 0
            self.entry()
        })
    }
    func entry()
    {
        let height = self.bounds.height
        let bottom = CGAffineTransform(translationX: 0, y: height+height)
        self.transform = bottom
        
        self.Enter()
    }
    func BackOut()
    {
        let height = self.frame.size.height
        let bottom = CGAffineTransform(translationX: 0, y: height+height)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = bottom
            
        }, completion:  { (finished: Bool) in
            self.alpha = 0
            self.BackEntry()
        })
    }
    func BackEntry()
    {
        let height = self.bounds.height
        let bottom = CGAffineTransform(translationX: 0, y: -height)
        self.transform = bottom
        
        self.Enter()
    }
    /*func convertViewToImage() -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, 0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //        self.init(CGImage: image.CGImage!)
//        let data = UIImagePNGRepresentation(image!)
//        return UIImage(data: data!)!
        
        return image!
    }*/
    func convertViewToImage(carIcon : Int) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, 0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let colorMasking : [CGFloat] = [222.0, 255.0, 222.0, 255.0, 222.0, 255.0]
        let imageRef = img?.cgImage?.copy(maskingColorComponents: colorMasking)
        let resultThumbImage = UIImage(cgImage: imageRef!, scale: 0, orientation: (img?.imageOrientation)!)
//        let finalimg = UIImage(cgImage: imageRef!)
        
        //TODO:- View size from createViewForPoint
        var finalImage  : UIImage?
        if(carIcon > 6)
        {
            finalImage = resizeImage(image: resultThumbImage, targetSize: CGSize(width:55, height:36))
        }else{
            finalImage = resizeImage(image: resultThumbImage, targetSize: CGSize(width:40, height:28))
        }
        
        return finalImage!
        
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: targetSize.width, height: targetSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func createGradientLayer()
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [UIColor.white.withAlphaComponent(0.35).cgColor, UIColor.lightGray.withAlphaComponent(0.85).cgColor]
        self.layer.addSublayer(gradientLayer)
    }
    func startViewBlink() {
        UIView.animate(withDuration: 0.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0.1 },
                       completion: nil)
    }
    
    func stopViewBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
    
    func startButtonViewBlink() {
        UIView.animate(withDuration: 1.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0.1 },
                       completion: nil)
    }
}


extension UIView
{
    func  addTapGesture(action : @escaping ()->Void ){
        let tap = MyTapGestureRecognizer(target: self , action: #selector(self.handleTap(_:)))
        tap.action = action
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
    }
    @objc func handleTap(_ sender: MyTapGestureRecognizer) {
        sender.action!()
    }
}

class MyTapGestureRecognizer: UITapGestureRecognizer {
    var action : (()->Void)? = nil
}
