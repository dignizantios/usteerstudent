//
//  String+Validations.swift
//  Salonx
//
//  Created by Usteer  on 15/06/16.
//  Copyright © 2016 Usteer. All rights reserved.
//

import Foundation
import SwiftyJSON
public extension String
{
   /*func toJSON() -> JSON
   {
//        guard let data = self.data(using: .utf8, allowLossyConversion: false) else { return JSON() }; return try? JSONSerialization.jsonObject(with: data, options: .mutableContainers)
    
    }*/
    
    /*
 
     public static String extractYTId(String ytUrl) {
     String vId = null;
     Pattern pattern = Pattern.compile(
     "^https?://.*(?:youtu.be/|v/|u/\\w/|embed/|watch?v=)([^#&?]*).*$",
     Pattern.CASE_INSENSITIVE);
     Matcher matcher = pattern.matcher(ytUrl);
     if (matcher.matches()){
     vId = matcher.group(1);
     }
     return vId;
     }
    */
    var removingWhitespacesAndNewlines: String {
        return components(separatedBy: .whitespacesAndNewlines).joined()
    }
    var html2AttributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: Data(utf8),
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    var youtubeID: String?
    {
        let pattern = "((?<=(v|V)/)|(?<=be/)|(?<=(\\?|\\&)v=)|(?<=embed/))([\\w-]++)"
        
        let regex = try? NSRegularExpression(pattern: pattern, options: .caseInsensitive)
        let range = NSRange(location: 0, length: count)
        
        guard let result = regex?.firstMatch(in: self, options: [], range: range) else {
            return nil
        }
        
        return (self as NSString).substring(with: result.range)
    }
    var html2String: String {
        return html2AttributedString?.string ?? ""
    }
    var length: Int { return self.count }
    
    func toURL() -> URL? {
        return URL(string: self)
    }
    
    func trimmed() -> String {
        return self.trimmingCharacters(in: .whitespaces)
    }
    func encodeQueryString() -> String
    {
        return self.replacingOccurrences(of: "'", with: "''", options:.regularExpression)
    }
    func encodedURLString() -> String
    {
        let escapedString = self.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)
        return escapedString ?? self
    }
    func encodedNameString() -> String
    {
        
        /*
 
         String newUrl = finalUrl.replaceAll(" ", "%20");
         newUrl = newUrl.replaceAll("\\r", "");
         newUrl = newUrl.replaceAll("\\t", "");
         newUrl = newUrl.replaceAll("\\n\\n", "%20");
         newUrl = newUrl.replaceAll("\\n", "%20");
         newUrl = newUrl.replaceAll("\\|", "%7C");
         newUrl = newUrl.replaceAll("\\+", "%2B");
         
         newUrl = newUrl.replaceAll("\\#", "%23");
 
        */
        
        let allowedCharacterSet = (CharacterSet(charactersIn: "@#$*^&+= ").inverted)

        let escapedString = self.addingPercentEncoding(withAllowedCharacters:allowedCharacterSet)

        return escapedString ?? self
    }
    
    func encodeString() -> String
    {
        var str = self
        str = str.replacingOccurrences(of: " ", with: "%20")
        str = str.replacingOccurrences(of: "\\r", with: "")
        str = str.replacingOccurrences(of: "\\t", with: "")
        str = str.replacingOccurrences(of: "\\n\\n", with: "%20")
        str = str.replacingOccurrences(of: "\\n", with: "%20")
        str = str.replacingOccurrences(of: "\\|", with: "%7C")
        str = str.replacingOccurrences(of: "\\+", with: "%2B")
        str = str.replacingOccurrences(of: "\\#", with: "%23")

        return str
        
        
    }
    
    func isEmail() throws -> Bool {
        let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9-]+\\.[A-Z]{2,4}$", options: [.caseInsensitive])
        return regex.firstMatch(in: self, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, self.count)) != nil
    }
    
    func isAlphaSpace() throws -> Bool {
        let regex = try NSRegularExpression(pattern: "^[A-Za-z ]*$", options: [])
        return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
    }
    
    func isNumeric() throws -> Bool {
        let regex = try NSRegularExpression(pattern: "^[0-9]*$", options: [])
        
        return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
    }
    
    func isRegistrationNumber() throws -> Bool {
        let regex = try NSRegularExpression(pattern: "^[A-Za-z0-9 ]*$", options: [])
        
        return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
    }
    func hexStringToUIColor () -> UIColor {
        let hex = self
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    
}

public extension String {
    
    
    func validateFirstName() -> Bool {
        do {
            if !(try self.isAlphaSpace()) {
                return false
            }
        } catch {
            return false
        }
        
        return true
    }
    
    func lineSpacing(space : Int) -> NSAttributedString
    {
        let attributedString = NSMutableAttributedString(string: self)
        
        // *** Create instance of `NSMutableParagraphStyle`
        let paragraphStyle = NSMutableParagraphStyle()
        
        // *** set LineSpacing property in points ***
        paragraphStyle.lineSpacing = CGFloat(space) // Whatever line spacing you want in points
        
        // *** Apply attribute to string ***
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, attributedString.length))
        
        // *** Set Attributed String to your label ***
        return attributedString
    }
    
    func isValidEmail() -> Bool
    {
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
    
    func substring(_ from: Int) -> String {
        return self.substring(from: self.index(self.startIndex, offsetBy: from))
    }
}





