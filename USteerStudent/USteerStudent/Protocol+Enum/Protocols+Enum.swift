//
//  Protocols+Enum.swift
//  USteerStudent
//
//  Created by Usteer on 1/26/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation


enum selectedTreeController
{
    case AssesmentLaunchVC
    case PlanMyRouteMap
    case Sorting
    case AppLaunch
    case socialAssistant
    case LinkSchool
}

enum selectedReportController
{
    case ChooseRelationVC
    case QuestionsVC
    case myjourney
    case myjourneyAssessment
}

enum TargetFactor : String
{
    case SC = "sc"
    case SD = "sd"
    case TOO = "too"
    case TOS = "tos"
}
enum FilterOption : String
{
    case ByRelationship = "relationship"
//    case BySignpost = "signpost"
    case ByTrustOfOthers = "too"
    case ByTrustOfSelf = "tos"
    case BySelfDisclosure = "sd"
    case BySeekingChange = "sc"

}

enum Color
{
    case red
    case amber
    case green
    case blue
}


extension Color {
    var value: UIColor {
        get
        {
            switch self
            {
                case .red:
                    return MySingleton.sharedManager.themeRedColor
                case .amber:
                    return MySingleton.sharedManager.themeYellowColor
                case .green:
                    return MySingleton.sharedManager.themeGreenColor
                case .blue:
                    return MySingleton.sharedManager.themeBlueColor
            }
        }
    }
}
