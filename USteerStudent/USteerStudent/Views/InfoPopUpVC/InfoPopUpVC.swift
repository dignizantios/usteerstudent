//
//  InfoPopUpVC.swift
//  USteerStudent
//
//  Created by Usteer on 8/1/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

@objc protocol InfoPopUpDelegate
{
    @objc optional func dismissPopUp()
    @objc optional func closeButtonTap(from:String)
}

class InfoPopUpVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var vwContent : UIView!
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var vwBack : UIView!
    @IBOutlet var btn_Close:UIButton!
    var isCloseHidden = true
    var from:String?

    //MARK: - Variables
    var popUpDelegate : InfoPopUpDelegate?
    var strMessage = ""
    var colorPopUpContent = MySingleton.sharedManager.themeYellowColor
    var colorText = MySingleton.sharedManager.themeDarkGrayColor
    var attributed:NSAttributedString?
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
    }

    func setUpUI()
    {
        btn_Close.isHidden = self.isCloseHidden
        vwContent.layer.cornerRadius = 5
        vwContent.layer.masksToBounds = true
        vwContent.backgroundColor = colorPopUpContent
        lblMessage.textColor = colorText
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        if attributed != nil
        {
            lblMessage.attributedText = attributed
        }
        else
        {
            lblMessage.text = strMessage
        }
        let tapRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwBack.addGestureRecognizer(tapRelation)
    }
    @objc func hideView()
    {
        self.dismiss(animated: false, completion: nil)
        popUpDelegate?.dismissPopUp!()
    }
    
    @IBAction func clk_Close()
    {
        self.dismiss(animated: false, completion: nil)
        popUpDelegate?.closeButtonTap!(from: self.from!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
