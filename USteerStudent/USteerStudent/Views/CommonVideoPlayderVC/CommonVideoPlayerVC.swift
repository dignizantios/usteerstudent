//
//  CommonVideoPlayerVC.swift
//  USteerStudent
//
//  Created by Usteer on 8/1/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class CommonVideoPlayerVC: UIViewController {

    
    @IBOutlet var vwVideo : UIView!
    
    var player: AVPlayer!
    var playerController = AVPlayerViewController()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        playVideo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - Video Player
extension CommonVideoPlayerVC
{
    func playVideo()
    {
        let videoURL = Bundle.main.url(forResource: "USTEER_sort_1", withExtension: "mp4")

//        let videoURL = NSURL(string: moviePath)
        player = AVPlayer(url: videoURL! as URL)
        let playerController = AVPlayerViewController()
        playerController.player = player
        self.addChildViewController(playerController)
        
        // Add your view Frame
        playerController.view.frame = self.vwVideo.frame
        
        // Add sub view in your view
        self.vwVideo.addSubview(playerController.view)
        
        player.play()
    }
    
}
