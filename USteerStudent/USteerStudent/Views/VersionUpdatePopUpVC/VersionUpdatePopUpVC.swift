//
//  VersionUpdatePopUpVC.swift
//  ASTrackingPupil
//
//  Created by Usteer on 12/13/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
import YoutubePlayer_in_WKWebView
import XCDYouTubeKit

class VersionUpdatePopUpVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var vwHeader : UIView!
    @IBOutlet var vwBack : UIView!

    @IBOutlet var lblHeader : UILabel!
    @IBOutlet var lblVersionInfo : UILabel!
    
    @IBOutlet var btnUpdate : UIButton!
    @IBOutlet var btnUpdateLater : UIButton!
    @IBOutlet weak var btnCloseOutlet: UIButton!
    
    @IBOutlet weak var vwVideo: WKYTPlayerView!
    @IBOutlet weak var constraintViewVideoHeight: NSLayoutConstraint!

    
    //MARK:- Variables
    var player: AVPlayer!
    var avpController = AVPlayerViewController()

    
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- UI setup
extension VersionUpdatePopUpVC
{
    func setUpUI()
    {
        
        lblHeader.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblHeader.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        lblVersionInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblVersionInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12)
        
        
        [btnUpdate,btnUpdateLater].forEach { (button) in
            
            
            button?.layer.cornerRadius = (button?.frame.size.height)!/2
            button?.layer.masksToBounds = true
            button?.backgroundColor = MySingleton.sharedManager.themeYellowColor
            button?.setTitleColor(UIColor.white, for: .normal)
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12)
        }
        
        
        vwVideo.backgroundColor = UIColor.clear
        
//        let tapRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
//        vwBack.addGestureRecognizer(tapRelation)
        
        setUpData()
        self.btnUpdateLater.isHidden = true
        self.btnCloseOutlet.isHidden = true
        
//        if getVersionDetail("is_forcefully") == "1"{
//            self.btnUpdateLater.isHidden = true
//            self.btnCloseOutlet.isHidden = true
//        }
        
    }
    func setUpData()
    {
        
        let attr = try? NSAttributedString(htmlString: getVersionDetail("message"), font: UIFont.systemFont(ofSize: 34, weight: .thin))
        self.lblVersionInfo.attributedText = attr
        
        var strVideoURL = getVersionDetail("link")
        print("strVideoURL - ",strVideoURL)
//        strVideoURL = "https://steer.global/usteer/media/video/1548399961.mp4"
        if(strVideoURL == "" || strVideoURL.length == 0 || strVideoURL == nil)
        {
            constraintViewVideoHeight.constant = 0
        }else{
            constraintViewVideoHeight.constant = 180
            if(isCheckUserLocalChina())
            {
                playServerVideo(strVideoURL: strVideoURL, view: vwVideo)
            }else{
//               playVideo(strVideoURL : strVideoURL)
                playVideoInView(strVideoURL: strVideoURL, view: vwVideo)
            }
            
        }
        
    }
    func getVideoString(strStreamingURL : String) -> String
    {
        let arrayContents = strStreamingURL.split(separator: "/")
        if(arrayContents.count > 0)
        {
            print("Link end - ",String(arrayContents[arrayContents.count-1]))
            return String(arrayContents[arrayContents.count-1])
        }else{
            return ""
        }
    }
}
//MARK:- Geture methods
extension VersionUpdatePopUpVC
{
    @objc func hideView()
    {
        self.dismiss(animated: false, completion: nil)
    }
}

//MARK:- Setup Video UI

extension VersionUpdatePopUpVC {
    func playVideoInView(strVideoURL:String,view:UIView) {
        let playerViewController = AVPlayerViewController()
        //        present(playerViewController, animated: true)
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height:view.frame.height)
        view.addSubview(playerViewController.view)
        self.addChildViewController(playerViewController)
        weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
        //        weakPlayerViewController?.player?.isMuted = false
        
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL) { [weak self] (video, error) in
            /*if let obj = self {
             obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
             }*/
            if video != nil {
                var streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    weakPlayerViewController?.player = AVPlayer(url: streamURL)
                }
                weakPlayerViewController?.player?.play()
            } else {
                //                self?.dismiss(animated: true)
            }
        }
        
    }
}
//MARK: - Player delegate
extension VersionUpdatePopUpVC : WKYTPlayerViewDelegate
{
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState)
    {
        print("state - ",state.rawValue)
        if(state.rawValue == 1)
        {
            playerView.stopVideo()
        }
    }
    func playerViewPreferredWebViewBackgroundColor(_ playerView: WKYTPlayerView) -> UIColor {
        return UIColor.clear
    }
}
//MARK:- video play methods
extension VersionUpdatePopUpVC
{
    func playServerVideo(strVideoURL : String,view:UIView)
    {
        if(strVideoURL == "")
        {
            return
        }
        
        //        let strVideo = "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.mp4"
        //        let strVideo = "e2jFYvDXIRI"
        let strVideo = strVideoURL
        let url = URL(string:strVideo)
        player = AVPlayer(url: url!)
        avpController = AVPlayerViewController()
        avpController.player = player
        avpController.view.frame.size.height = view.frame.size.height
        avpController.view.frame.size.width = view.frame.size.width
        view.addSubview(avpController.view)
        
        //--------
        //        let strVideo = getVideoString(strStreamingURL: strVideoURL)
        //        vwVideo.delegate = self
        //        vwVideo.load(withVideoId: strVideo)
    }
    func playVideo(strVideoURL : String)
    {
        
//        let strVideo = "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.mp4"
//        let strVideo = "e2jFYvDXIRI"
        let strVideo = getVideoString(strStreamingURL: strVideoURL)
        /*let url = URL(string:strVideo)
        player = AVPlayer(url: url!)
        avpController.player = player
        avpController.view.frame.size.height = vwVideo.frame.size.height
        avpController.view.frame.size.width = vwVideo.frame.size.width
        self.vwVideo.addSubview(avpController.view)*/

        vwVideo.delegate = self
        vwVideo.load(withVideoId: strVideo)
    }
    
}
//MARK:- Button Action
extension VersionUpdatePopUpVC
{
    @IBAction func btnClosePopUp(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnUpdateAction(_ sender : UIButton)
    {
        Defaults.set("1", forKey: R.string.keys.kIsClickOnUpdate())
        Defaults.synchronize()
        openAppStore()
    }
    @IBAction func btnUpdateLaterAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
