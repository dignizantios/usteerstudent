//
//  AddTargtePopUpVC.swift
//  USteerStudent
//
//  Created by Usteer on 2/13/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
protocol AddTargetDelegate {
    
    func btnAddTargetSubmitAction(strTargetName:String,strTargetDate:String)
}

class AddTargtePopUpVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var lblHeading : UILabel!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var txtTargetName : UITextField!
    @IBOutlet var txtTargetDate : UITextField!
    @IBOutlet weak var vwTargetNameSeperator: UIView!
    @IBOutlet weak var vwTargetDateSeperator: UIView!
    @IBOutlet var lblTargetName : UILabel!
    @IBOutlet var lblTargetDate : UILabel!

    //MARK: - Varibales
    var addTargetDelegate : AddTargetDelegate?
    var strTargetDate = ""
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews()
    {
        btnDone.layer.cornerRadius = btnDone.bounds.size.height / 2
        btnDone.layer.masksToBounds = true
    }

}
//MARK: - UI setup

extension AddTargtePopUpVC
{
    func setupUI()
    {
        lblHeading.text = getCommonString(key: "Add_target_key")
        lblHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblHeading.textColor = .white
        lblHeading.backgroundColor = MySingleton.sharedManager.themeYellowColor
        
        [lblTargetName,lblTargetDate].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        })
        [txtTargetName,txtTargetDate].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
            $0?.delegate = self
        })
        
        [vwTargetNameSeperator,vwTargetDateSeperator].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        
        
        btnDone.backgroundColor = MySingleton.sharedManager.themeYellowColor
        btnDone.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        btnDone.setTitleColor(.white, for: .normal)
        
        
    }
}
//MARK:- UITextfield Setup
extension AddTargtePopUpVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        txtTargetDate.resignFirstResponder()
        if(textField == txtTargetDate)
        {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.isSetMaximumDate = false
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePickerMode.date)
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
}
//MARK: - SetDate Picker Value
extension AddTargtePopUpVC : datePickerDelegate
{
    func setDateValue(dateValue: Date)
    {
        self.txtTargetDate.text = DateToString(Formatter: "dd-MM-yyyy", date: dateValue)
        strTargetDate = DateToString(Formatter: server_dt_format, date: dateValue) //getFormatedDate(date: dateValue)
    }
}
//MARK:- Button Actions
extension AddTargtePopUpVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        if(txtTargetName.text == "" || txtTargetDate.text == "")
        {
            if(txtTargetName.text == "")
            {
                showToast(message: R.string.validationMessage.enter_target_name_key())
            }else{
                showToast(message: R.string.validationMessage.enter_target_date_key())
            }
            return
        }else{
            addTargetDelegate?.btnAddTargetSubmitAction(strTargetName: txtTargetName.text ?? "", strTargetDate: strTargetDate)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    @IBAction func btnCloseAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)

    }
}
