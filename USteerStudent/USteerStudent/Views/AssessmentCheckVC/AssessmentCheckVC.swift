//
//  AssessmentCheckVC.swift
//  USteerStudent
//
//  Created by Usteer on 05/02/19.
//  Copyright © 2019 Usteer. All rights reserved.
//

import UIKit
protocol AssessmentCheckDelegate
{
    func btnOption1Selected(selectedOption : CheckOption)
    func btnOption2Selected(selectedOption : CheckOption)
}

enum CheckOption
{
    case link
    case share
}

class AssessmentCheckVC: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var btnOption1 : UIButton!
    @IBOutlet var btnOption2 : UIButton!
    @IBOutlet var vwBack : UIView!
    
    var strMessage = ""
    var selectedOption = CheckOption.link
    var delegateAssessmentCheck : AssessmentCheckDelegate?
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUI()
        
    }

}
extension AssessmentCheckVC
{
    func setUpUI()
    {
        
        lblMessage.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        
        [btnOption1,btnOption2].forEach { (button) in
            button?.backgroundColor = MySingleton.sharedManager.themeYellowColor
            button?.layer.cornerRadius = 5
            button?.layer.masksToBounds = true
            button?.setTitleColor(UIColor.white, for: .normal)
        }

        
        if(selectedOption == .link)
        {
            btnOption1.setTitle("Link now", for: .normal)
            btnOption2.setTitle("Not now", for: .normal)
        }
        else{
            btnOption1.setTitle("Yes", for: .normal)
            btnOption2.setTitle("No", for: .normal)
        }
        lblMessage.text = strMessage
        
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwBack.addGestureRecognizer(tapOptions)
    }
    @objc func hideView(_ sender: UITapGestureRecognizer)
    {
        self.dismiss(animated: false, completion: nil)
    }
}
//MARK:- Button Action
extension AssessmentCheckVC
{
    @IBAction func btnOption1Action(_ sender : UIButton)
    {
        delegateAssessmentCheck?.btnOption1Selected(selectedOption : selectedOption)
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func btnOption2Action(_ sender : UIButton)
    {
        delegateAssessmentCheck?.btnOption2Selected(selectedOption : selectedOption)
        self.dismiss(animated: false, completion: nil)

    }
}
