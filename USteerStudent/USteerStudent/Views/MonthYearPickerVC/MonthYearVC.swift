//
//  MonthYearVC.swift
//  USteerStudent
//
//  Created by Usteer on 11/23/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

protocol SelectedMonthYearDelegate {
    func SetSelectedMonthYear(strMonth:String,strYear:String,month:Int)
}

class MonthYearVC: UIViewController {

    
    //MARK:- Outlets
    @IBOutlet var pickerViewMonthYear : UIPickerView!
    
    @IBOutlet var vwButtons : UIView!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var btnCancel : UIButton!

    
    //MARK:- variables
    var arrayMonth = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    var arrayYear : [String] = []
    var strSelectedYear = getCurrentYear()
    var selectedMonth = getCurrentMonth()
    var strSelectedMonth = ""

    var monthYearDelegate : SelectedMonthYearDelegate?
    var isSetMaximum = true
    
    
    //MARK:- Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
//MARK:- UI setup
extension MonthYearVC
{
    func setUpUI()
    {
        [btnDone,btnCancel].forEach { (button) in
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
            button?.setTitleColor(UIColor.white, for: .normal)
            
        }
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        
        vwButtons.backgroundColor = MySingleton.sharedManager.themeYellowColor
        setUpMonthYearArray()
        
        strSelectedMonth = arrayMonth[selectedMonth-1]

        
    }
    func setUpMonthYearArray()
    {
        var selectedYearIndex = 0
        let strCurrentYear = getCurrentYear()
        
        let intStartYear = Int(strCurrentYear)! - 15
        for i in startYear...Int(intStartYear)
        {
            arrayYear.append("\(i)")
        }
        
        if(arrayYear.contains(strSelectedYear))
        {
            selectedYearIndex = arrayYear.index(of: strSelectedYear)!
        }
        
        pickerViewMonthYear.reloadAllComponents()
        
        pickerViewMonthYear.selectRow(selectedMonth-1, inComponent: 0, animated: true)
        pickerViewMonthYear.selectRow(selectedYearIndex, inComponent: 1, animated: true)

        

    }
}
//MARK:- Picker delegate
extension MonthYearVC : UIPickerViewDelegate,UIPickerViewDataSource
{
    
    
  
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        if(component == 0)
        {
            return arrayMonth.count
        }else{
            return arrayYear.count
        }
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        
        if(component == 0)
        {
            return arrayMonth[row]
        }else{
            return arrayYear[row]
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if(component == 0)
        {
            if(strSelectedYear == getCurrentYear())
            {
                if(row+1<=getCurrentMonth())
                {
                    strSelectedMonth = arrayMonth[row]
                    selectedMonth = row+1
                }
                else{
                    showToast(message: R.string.validationMessage.birthdate_select_key())
                    pickerViewMonthYear.selectRow(getCurrentMonth()-1, inComponent: 0, animated: true)
                }
            }
            else{
                strSelectedMonth = arrayMonth[row]
                selectedMonth = row+1
            }
            
        }else{
            if(arrayYear[row] == getCurrentYear())
            {
                if(selectedMonth<=getCurrentMonth())
                {
                    strSelectedYear = arrayYear[row]
                }
                else{
                    showToast(message: R.string.validationMessage.birthdate_select_key())
                    pickerViewMonthYear.selectRow(getCurrentMonth()-1, inComponent: 0, animated: true)
                    pickerViewMonthYear.selectRow(arrayYear.count-1, inComponent: 1, animated: true)
                }
            }else{
                strSelectedYear = arrayYear[row]
            }
            
        }
    }
}
//MARK:- Button Action Setup
extension MonthYearVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        monthYearDelegate?.SetSelectedMonthYear(strMonth: strSelectedMonth, strYear: strSelectedYear,month:selectedMonth)
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
