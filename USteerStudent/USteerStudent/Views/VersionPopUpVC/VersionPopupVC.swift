//
//  VersionPopupVC.swift
//  USteerStudent
//
//  Created by Usteer on 9/25/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import XCDYouTubeKit
import AVFoundation
import AVKit

class VersionPopupVC: UIViewController
{

    //MARK:- Outlets
    @IBOutlet var collectionView : UICollectionView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblSubTitle : UILabel!

    @IBOutlet var lblContent : UILabel!
    @IBOutlet var constraintCollectionViewHeight : NSLayoutConstraint!

    @IBOutlet var pageControlMedia : UIPageControl!

    
    //MARK:- Varibales
    var count = 0
    var lastContentOffset: CGPoint = .zero

    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        collectionView.register(UINib(nibName: "MediaCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        
        pageControlMedia.numberOfPages = 3
        setUpUI()
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setUpUI()
    {
        let strText = "<!DOCTYPE html><html><body><h1>This is heading 1</h1><h2>This is heading 2</h2><h3>This is heading 3</h3><h4>This is heading 4</h4><h5>This is heading 5</h5><h6>This is heading 6</h6><h1>This is heading 1</h1><h2>This is heading 2</h2><h3>This is heading 3</h3><h4>This is heading 4</h4><h5>This is heading 5</h5><h6>This is heading 6</h6><h1>This is heading 1</h1><h2>This is heading 2</h2><h3>This is heading 3</h3><h4>This is heading 4</h4><h5>This is heading 5</h5><h6>This is heading 6</h6><h1>This is heading 1</h1><h2>This is heading 2</h2><h3>This is heading 3</h3><h4>This is heading 4</h4><h5>This is heading 5</h5><h6>This is heading 6</h6></body></html>"
        
        let attr = try? NSAttributedString(htmlString: strText, font: MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12))
        self.lblContent.attributedText = attr
        
        
    }

}
//MARK:- Collectionview Datasource Methods
extension VersionPopupVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return 3
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! MediaCollectionViewCell
        
//        cell.vwVideoPlayer.delegate = self
        
        
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        var cellWidth : CGFloat = 0.0
        var cellHeight : CGFloat = 0.0
        print("collectionView  width  - \(collectionView.frame.size.width)")
        cellWidth  = (collectionView.frame.size.width)
        cellHeight  = (collectionView.frame.size.height)
        print("width - \(cellWidth) ------ height - \(cellHeight)")
        constraintCollectionViewHeight.constant = cellHeight
        return CGSize(width: cellWidth , height:cellHeight)
        
    }
    
}
//MARK: - Player delegate
extension VersionPopupVC : WKYTPlayerViewDelegate
{
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        print("state - ",state.rawValue)
        if(state.rawValue == 1)
        {
            playerView.stopVideo()
        }
    }
}
//MARK:- Scrollview Methods
extension VersionPopupVC
{
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if(scrollView == collectionView)
        {
            count = pageControlMedia.currentPage
            
            if(count == 2)
            {
                if (self.lastContentOffset.x < scrollView.contentOffset.x)
                {
                    // moved right
                    print("Move right")
                    //                    navigateToQuestions()
                } else if (self.lastContentOffset.x > scrollView.contentOffset.x) {
                    // moved left
                    print("Move Left")
                }
                else{
                    print("Didn't move")
                }
            }
            
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if(scrollView == collectionView)
        {
            let pageNumber = Int(round(collectionView.contentOffset.x / collectionView.frame.size.width))
            pageControlMedia.currentPage = Int(pageNumber)
            
            self.lastContentOffset.x = scrollView.contentOffset.x
        }
    }
}
//MARK:- Button Action
extension VersionPopupVC
{
    @IBAction func btnCloseAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
extension NSAttributedString {
    
    
    
    convenience init(htmlString html: String, font: UIFont? = nil, useDocumentFontSize: Bool = true) throws {
        
        let options: [NSAttributedString.DocumentReadingOptionKey : Any] = [
            
            .documentType: NSAttributedString.DocumentType.html,
            
            .characterEncoding: String.Encoding.utf8.rawValue
            
        ]
        
        
        
        let data = html.data(using: .utf8, allowLossyConversion: true)
        
        guard (data != nil), let fontFamily = font?.familyName, let attr = try? NSMutableAttributedString(data: data!, options: options, documentAttributes: nil) else {
            
            try self.init(data: data ?? Data(html.utf8), options: options, documentAttributes: nil)
            
            return
            
        }
        
        
        
        let fontSize: CGFloat? = useDocumentFontSize ? nil : font!.pointSize
        
        let range = NSRange(location: 0, length: attr.length)
        
        attr.enumerateAttribute(.font, in: range, options: .longestEffectiveRangeNotRequired) { attrib, range, _ in
            
            if let htmlFont = attrib as? UIFont {
                
                let traits = htmlFont.fontDescriptor.symbolicTraits
                
                var descrip = htmlFont.fontDescriptor.withFamily(fontFamily)
                
                
                
               /* if (traits.rawValue & UIFontDescriptorSymbolicTraits.traitBold.rawValue) != 0 {
                    
                    descrip = descrip.withSymbolicTraits(.traitBold)!
                    
                }
                
                
                
                if (traits.rawValue & UIFontDescriptorSymbolicTraits.traitItalic.rawValue) != 0 {
                    
                    descrip = descrip.withSymbolicTraits(.traitItalic)!
                    
                }*/
                
                
                
                attr.addAttribute(.font, value: UIFont(descriptor: descrip, size: fontSize ?? htmlFont.pointSize), range: range)
                
            }
            
        }
        
        
        
        self.init(attributedString: attr)
        
    }
    
    
    
}

//MARK:- Setup Video UI

extension VersionPopupVC {
    func playVideoInView(strVideoURL:String,view:UIView) {
        let playerViewController = AVPlayerViewController()
        //        present(playerViewController, animated: true)
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height:view.frame.height)
        view.addSubview(playerViewController.view)
        self.addChildViewController(playerViewController)
        weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
        //        weakPlayerViewController?.player?.isMuted = false
        
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL) { [weak self] (video, error) in
            /*if let obj = self {
             obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
             }*/
            if video != nil {
                var streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    weakPlayerViewController?.player = AVPlayer(url: streamURL)
                }
                weakPlayerViewController?.player?.play()
            } else {
                //                self?.dismiss(animated: true)
            }
        }
        
    }
}

