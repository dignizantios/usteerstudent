//
//  DatePickerVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/4/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

protocol datePickerDelegate {
    func setDateValue(dateValue : Date)
}

struct datepickerMode
{
    var pickerMode : UIDatePickerMode
    
    init(customPickerMode:UIDatePickerMode)
    {
        self.pickerMode = customPickerMode
    }
}

class DatePickerVC: UIViewController {

    
    //MARK: - Outlets
    @IBOutlet var vwButtonHeader : UIView!
    @IBOutlet var btnCancel : UIButton!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet var datePicker : UIDatePicker!

    //MARK: - Variables
    var datePickerCustomMode = datepickerMode(customPickerMode:UIDatePickerMode.date)
    var pickerDelegate : datePickerDelegate?
    var isSetMaximumDate = false
    var isSetMinimumDate = false

    var maximumDate = getCurrentTimeStamp()
    var minimumDate = getCurrentTimeStamp()
    var isSetDate = false
    var setDateValue : Date?
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SetupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - Setup UI
extension DatePickerVC
{
    func SetupUI()
    {
        [btnDone,btnCancel].forEach({
            $0?.setTitleColor(UIColor.white, for: .normal)
            $0?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        })
        
        btnDone.setTitle(getCommonString(key: "Done_key"), for: .normal)
        btnCancel.setTitle(getCommonString(key: "Cancel_key"), for: .normal)
        
        vwButtonHeader.backgroundColor = MySingleton.sharedManager.themeYellowColor
        
        datePicker.datePickerMode = datePickerCustomMode.pickerMode
        datePicker.date = Date()
        
        if(isSetMaximumDate)
        {
            datePicker.maximumDate = maximumDate
        }
        if(isSetMinimumDate)
        {
            datePicker.minimumDate = minimumDate
        }
        
        if(isSetDate)
        {
            datePicker.setDate(setDateValue ?? Date(), animated: false)
        }
    }
}
//MARK: - Button Action
extension DatePickerVC
{
    @IBAction func btnDoneAction(_ sender : UIButton)
    {
        pickerDelegate?.setDateValue(dateValue: datePicker.date)
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
}
