//
//  VideoPlayerVC.swift
//  USteerStudent
//
//  Created by Usteer on 5/26/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
import AVFoundation
import AVKit
import XCDYouTubeKit

protocol DismissPlayerViewDelegate
{
    func dismissVideoPlayer()
}
class VideoPlayerVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var vwVideoPlayer: WKYTPlayerView!
    
    //MARK: - Variables
    var strStreamingURL = ""
    var playerDelegate : DismissPlayerViewDelegate?
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.vwVideoPlayer.delegate = self
//        self.vwVideoPlayer.load(withVideoId: "BgpS1GMjR2E")
        
        if(isCheckUserLocalChina())
        {
            playVideo(strVideoURL: strStreamingURL, view: vwVideoPlayer)
        }else{
            let arrayContents = strStreamingURL.split(separator: "/")
            if(arrayContents.count > 0)
            {
                print("Link end - ",String(arrayContents[arrayContents.count-1]))
//                self.vwVideoPlayer.load(withVideoId: String(arrayContents[arrayContents.count-1]))
                playVideoInView(strVideoURL:  String(arrayContents[arrayContents.count-1]), view:  vwVideoPlayer)
            }else{
                showToast(message: "URL not available")
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK:- Video
extension VideoPlayerVC
{
    func playVideo(strVideoURL : String,view:UIView)
    {
        if(strVideoURL == "")
        {
            return
        }
        
        //        let strVideo = "https://wolverine.raywenderlich.com/content/ios/tutorials/video_streaming/foxVillage.mp4"
        //        let strVideo = "e2jFYvDXIRI"
        let strVideo = strVideoURL
        let url = URL(string:strVideo)
        player = AVPlayer(url: url!)
        avpController = AVPlayerViewController()
        avpController.player = player
        avpController.view.frame.size.height = view.frame.size.height
        avpController.view.frame.size.width = view.frame.size.width
        view.addSubview(avpController.view)
        
        //--------
        //        let strVideo = getVideoString(strStreamingURL: strVideoURL)
        //        vwVideo.delegate = self
        //        vwVideo.load(withVideoId: strVideo)
    }
}
//MARK: - Geture method
extension VideoPlayerVC
{
    //MARK: - Tap geture
    /// Tap reconizer on table to hide keyboard
    ///
    /// - Parameter sender: geture reconizer
    @objc func hideKeyboard(sender: UITapGestureRecognizer? = nil) {
        // handling code
        playerDelegate?.dismissVideoPlayer()
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Setup Video UI

extension VideoPlayerVC {
    func playVideoInView(strVideoURL:String,view:UIView) {
        let playerViewController = AVPlayerViewController()
        //        present(playerViewController, animated: true)
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height:view.frame.height)
        view.addSubview(playerViewController.view)
        self.addChildViewController(playerViewController)
        weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
        //        weakPlayerViewController?.player?.isMuted = false
        
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL) { [weak self] (video, error) in
            /*if let obj = self {
             obj.theCurrentView.statusActivityIndicator(isLoading: false, activityIndicator: obj.theCurrentView.activityIndicator)
             }*/
            if video != nil {
                var streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    weakPlayerViewController?.player = AVPlayer(url: streamURL)
                }
                weakPlayerViewController?.player?.play()
            } else {
                //                self?.dismiss(animated: true)
            }
        }
        
    }
}

//MARK: - Button Action
extension VideoPlayerVC
{
    @IBAction func btnClosePlayerAction(_ sender : UIButton)
    {
        playerDelegate?.dismissVideoPlayer()
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - Player delegate
extension VideoPlayerVC : WKYTPlayerViewDelegate
{
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        print("state - ",state.rawValue)
        if(state.rawValue == 1)
        {
            playerDelegate?.dismissVideoPlayer()
            self.dismiss(animated: true, completion: nil)
        }
    }
}
