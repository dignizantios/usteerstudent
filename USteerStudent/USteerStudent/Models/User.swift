//
//  User.swift
//  USteerStudent
//
//  Created by Usteer on 1/11/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation

class User: NSObject
{
    var strUserId : String  = ""
    var strName : String  = ""
    var strLastName : String  = ""

    var strUsername : String  = ""
    var strEmail : String  = ""
    var strBirthdate : String  = ""
    var strPassword : String  = ""
    var strValidationMessage : String  = ""
    var strGender : String  = ""

    var strOldPassword : String  = ""
    var strNewPassword : String  = ""
    var strConfirmPassword : String  = ""
    var strConsent : String  = ""
    var birthDateValue : Date?
    var strSelectedCar : String  = ""
    var strSelectedUserRole:String = ""

    func isValidForRegistration() -> Bool
    {
        if(self.strName.isEmpty || self.strLastName.isEmpty || self.strUsername.isEmpty || self.strPassword.isEmpty)
        {
            if(self.strName.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_name_key()
                return false
            }
            else if(self.strLastName.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_lastname_key()
                return false
            }
            else if(self.strUsername.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_username_key()
                return false
            }
            /*else if(self.strEmail.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_email_key()
                return false
            }*/
            else if(!self.strUsername.isValidEmail())
            {
                self.strValidationMessage = R.string.validationMessage.enter_username_key()
                return false
            }
            /*else if(self.strBirthdate.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.select_birthdate_key()
                return false
            }*/
            else if(self.strPassword.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_password_key()
                return false
            }
           /* else if(self.strGender.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.select_gender_option_key()
                return false
                
            }*/
            else if(self.strSelectedUserRole == "")
            {
                self.strValidationMessage = R.string.validationMessage.select_role_key()
                return false
            }
            return true
        }
        else if(!self.strUsername.isValidEmail())
        {
            self.strValidationMessage = R.string.validationMessage.enter_username_key()
            return false
        }
        else if(self.strPassword.count < 6)
        {
            self.strValidationMessage = R.string.validationMessage.enter_password_length_key()
            return false
        }
        
        /*else if(self.birthDateValue! > Date())
        {
            self.strValidationMessage = R.string.validationMessage.birthdate_select_key()
            return false
        }*/
        else if(self.strConsent == "n")
        {
            self.strValidationMessage = R.string.validationMessage.agree_terms_condition_key()
            return false
        }
        return true
    }
    
    func isValidForLogin() -> Bool
    {
        if(self.strUsername.isEmpty || self.strPassword.isEmpty)
        {
            if(self.strUsername.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_username_key()
                return false
            }
            else if(self.strPassword.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_password_key()
                return false
            }
            return true
        }
        else if(!self.strUsername.isValidEmail())
        {
            self.strValidationMessage = R.string.validationMessage.enter_username_key()
            return false
        }
        /*else if(self.strPassword.count < 6)
        {
            self.strValidationMessage = R.string.validationMessage.enter_password_length_key()
            return false
        }*/
        else if(self.strConsent == "n")
        {
            self.strValidationMessage = R.string.validationMessage.agree_terms_condition_key()
            return false
        }
        return true
    }
    func isValidForUpdateProfile() -> Bool
    {
        if(self.strName.isEmpty || self.strLastName.isEmpty || self.strUsername.isEmpty )
        {
            if(self.strName.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_name_key()
                return false
            }
            else if(self.strLastName.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_lastname_key()
                return false
            }
            else if(self.strUsername.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_username_key()
                return false
            }
            /*else if(self.strEmail.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_email_key()
                return false
            }*/
            /*else if(self.strBirthdate.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.select_birthdate_key()
                return false
            }*/
           /* else if(self.strGender.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.select_gender_option_key()
                return false
                
            }*/
            return true
        }
        /*else if(!self.strEmail.isValidEmail())
        {
            self.strValidationMessage = R.string.validationMessage.enter_valid_email_key()
            return false
        }*/
        /*else if(self.birthDateValue! > Date())
        {
            self.strValidationMessage = R.string.validationMessage.birthdate_select_key()
            return false
        }*/
        return true
    }
    func isValidForChangePassword() -> Bool
    {
        if(self.strOldPassword.isEmpty || self.strNewPassword.isEmpty || self.strConfirmPassword.isEmpty)
        {
            if(self.strOldPassword.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_old_password_key()
                return false
            }
            else if(self.strNewPassword.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_new_password_key()
                return false
            }
            else if(self.strConfirmPassword.isEmpty)
            {
                self.strValidationMessage = R.string.validationMessage.enter_confirm_password_key()
                return false
            }
            
            return true
        }
        else if(self.strOldPassword.count < 6)
        {
            self.strValidationMessage = R.string.validationMessage.enter_old_password_length_key()
            return false
        }
        else if(self.strNewPassword.count < 6)
        {
            self.strValidationMessage = R.string.validationMessage.enter_new_password_length_key()
            return false
        }
        else if(self.strConfirmPassword.count < 6)
        {
            self.strValidationMessage = R.string.validationMessage.enter_confirm_password_length_key()
            return false
        }
        else if(self.strNewPassword.trimmed() != self.strConfirmPassword.trimmed())
        {
            self.strValidationMessage = R.string.validationMessage.enter_password_not_match_key()
            return false
        }
        else if(self.strOldPassword.trimmed() == self.strNewPassword.trimmed())
        {
            self.strValidationMessage = R.string.validationMessage.enter_password_same_key()
            return false
        }
        
        return true
    }
}
