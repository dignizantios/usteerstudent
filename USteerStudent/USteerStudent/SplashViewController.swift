//
//  SplashViewController.swift
//  USteerStudent
//
//  Created by HARSHIT on 01/05/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import DeviceKit
import Alamofire
import AlamofireSwiftyJSON
import ZendeskSDK
import ZendeskCoreSDK

class SplashViewController: UIViewController
{
    @IBOutlet var bottomLogoConstraint:NSLayoutConstraint?
    @IBOutlet var widthLogoConstraint:NSLayoutConstraint?
    
    let screenHeight = Double(UIScreen.main.bounds.height)
    let screenWidth  = Double(UIScreen.main.bounds.width)
    
    let device = Device.current

    override func viewDidLoad()
    {
        super.viewDidLoad()
        switch device
        {
        case .iPhoneSE,.iPhone5,.iPhone5c,.iPhone5s:
            let ref_image_Width = 75.0
            let ref_device_Width = 320.0
            self.widthLogoConstraint?.constant = CGFloat(ref_image_Width*ref_device_Width)/CGFloat(self.screenWidth)
            break
        case .iPhone6,.iPhone7,.iPhone8,.iPhone6sPlus,.iPhone7Plus,.iPhone8Plus,.simulator(.iPhone8Plus):
            let ref_device_Width = 375.0
            let ref_image_Width = 85.0
            self.widthLogoConstraint?.constant = CGFloat(ref_image_Width*ref_device_Width)/CGFloat(self.screenWidth)
            break
        default:
            let ref_device_Width = 375.0
            let ref_image_Width = 100.0
            self.widthLogoConstraint?.constant = CGFloat(ref_image_Width*ref_device_Width)/CGFloat(self.screenWidth)
            break
        }
        self.view.layoutIfNeeded()
        self.fetchKeyAPI()
    }
    
    @objc func loadLogoWithAnimation()
    {
        switch device
        {
        case .iPhoneSE,.iPhone5,.iPhone5c,.iPhone5s:
            let ref_device_Height = 562.0
            let ref_image_Bottom = 150.0
            UIView.animate(withDuration: 2) {
                self.bottomLogoConstraint?.constant = CGFloat(ref_image_Bottom*self.screenHeight)/CGFloat(ref_device_Height)
                self.view.layoutIfNeeded()
            }
            break
        case .iPhone6,.iPhone7,.iPhone8,.iPhone6sPlus,.iPhone7Plus,.iPhone8Plus,.simulator(.iPhone8Plus):
            let ref_device_Height = 667.0
            let ref_image_Bottom = 175.0
            UIView.animate(withDuration: 2) {
                self.bottomLogoConstraint?.constant = CGFloat(ref_image_Bottom*self.screenHeight)/CGFloat(ref_device_Height)
                self.view.layoutIfNeeded()
            }
            break
        default:
            let ref_device_Height = 812.0
            let ref_image_Bottom = 240.0
            UIView.animate(withDuration: 2) {
                self.bottomLogoConstraint?.constant = CGFloat(ref_image_Bottom*self.screenHeight)/CGFloat(ref_device_Height)
                self.view.layoutIfNeeded()
            }
            break
        }
        appDelegate.perform(#selector(appDelegate.UserFlowScreen), with: nil, afterDelay: 3.0)
    }
    
    func fetchKeyAPI()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            BasicTaskService().fetchGeneratedKey(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    self.view.endEditing(true)
                    Defaults.removeObject(forKey: "secretKey")
                    Defaults.removeObject(forKey: "secretivKey")
                    Defaults.removeObject(forKey: "ZenDeskAppID")
                    Defaults.removeObject(forKey: "ZenDeskClientId")
                    Defaults.removeObject(forKey: "ZenDeskSupportURL")
                    Defaults.synchronize()
                    let zendeskDetails = json["zendesk_pupil"]
                    Defaults.setValue(decreptedString(string: zendeskDetails["kZenDeskAppID"].stringValue), forKey: "ZenDeskAppID")
                    Defaults.setValue(decreptedString(string: zendeskDetails["kZenDeskClientId"].stringValue), forKey: "ZenDeskClientId")
                    Defaults.setValue(decreptedString(string: zendeskDetails["kZenDeskSupportURL"].stringValue), forKey: "ZenDeskSupportURL")
                    Defaults.synchronize()
                    
                    if Defaults.value(forKey: "ZenDeskAppID") as? String != nil && Defaults.value(forKey: "ZenDeskClientId") as? String != nil && Defaults.value(forKey: "ZenDeskSupportURL") as? String != nil
                    {
                        if let appID = Defaults.value(forKey: "ZenDeskAppID") as? String,let clientID = Defaults.value(forKey: "ZenDeskClientId") as? String,let supportURL = Defaults.value(forKey: "ZenDeskSupportURL") as? String{
                            Zendesk.initialize(appId: appID, clientId: clientID, zendeskUrl: supportURL)
                            Support.initialize(withZendesk: Zendesk.instance)
                        }
                    }
                    Defaults.setValue(decreptedString(string: json["secret_key"].stringValue), forKey: "secretKey")
                    Defaults.setValue(decreptedString(string: json["secret_iv"].stringValue), forKey: "secretivKey")
                    Defaults.synchronize()
                    self.loadLogoWithAnimation()
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

extension UIDevice
{
    var hasNotch: Bool
    {
        var bottom = 0
        if #available(iOS 11.0, *) {
            bottom = Int(UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0)
        } else {
            bottom = 0
        }
        return bottom > 0
    }
}
