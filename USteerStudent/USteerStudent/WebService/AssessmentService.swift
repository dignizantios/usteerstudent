//
//  AssessmentService.swift
//  USteerStudent
//
//  Created by Usteer on 1/12/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation

import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

struct AssessmentService
{
    func getQuestions(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetQuestionsURL()
            
//            + "?uid=\(param["uid"] ?? "")&sid=\(param["sid"] ?? "")&qid=\(param["qid"] ?? "")&cq=\(param["cq"] ?? "")&qs=\(param["qs"] ?? "")&ans=\(param["ans"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"  //uid=7&sid=23&qid=5&cq=1
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "")
        dictParam["sid"] = param["sid"] ?? ""
        dictParam["qid"] = param["qid"] ?? ""
        dictParam["cq"] = param["cq"] ?? ""
        dictParam["qs"] = param["qs"] ?? ""
        dictParam["ans"] = param["ans"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("dictParam:\(dictParam)")
        print("url - ",kURL)

        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getQuestions(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func checkAssessmentQuestions(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""  //
        
        kURL = kApplicationBaseURL + R.string.webURL.kCheckAssessmentURL()
            
//            + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "")
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("dictParam:\(dictParam)")
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.checkAssessmentQuestions(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func SaveShareChangeService(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""  //
        
        kURL = kApplicationBaseURL + R.string.webURL.kSaveShareChangesURL()
            
//            + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&type=\(param["type"] ?? "")&type_val=\(param["type_val"] ?? "")&timezone=\(getCurrentTimeZone())&timestamp=\(getFormatedDate(date: Date()))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "") 
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["type"] = param["type"] ?? ""
        dictParam["type_val"] = param["type_val"] ?? ""
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.SaveShareChangeService(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func getAssessmentQuestions(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""  //
        
        kURL = kApplicationBaseURL + R.string.webURL.kGetQuestionsURL()
            
//            + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "") 
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getQuestions(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func submitAnswers(param:[String:Any], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSubmitAnswersURL()
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.submitAnswers(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func downLoadAudio()
    {
        
        let kURL = kApplicationBaseURL + "media/audio/assessment/1523267618.mp4"
        let destination = DownloadRequest.suggestedDownloadDestination()
        
        Alamofire.download(kURL, to: destination).downloadProgress(queue: DispatchQueue.global(qos: .utility)) { (progress) in
            print("Progress: \(progress.fractionCompleted)")
            } .validate().responseData { ( response ) in
                print(response.destinationURL!.lastPathComponent)
        }
        
        /*
         download(kURL,method: .get,parameters: nil,encoding: JSONEncoding.default,headers: nil,to: destinationURL).downloadProgress(closure: { (progress) in
         //progress closure
         }).response(completionHandler: { (DefaultDownloadResponse) in
         //here you able to access the DefaultDownloadResponse
         //result closure
         })
 
        */
    }
}
