//
//  BasicTaskService.swift
//  USteerStudent
//
//  Created by Usteer on 1/31/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct BasicTaskService
{
    func saveUserPrivate(is_private : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSaveUserPrivateURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)&is_private=\(is_private)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["is_private"] = is_private
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.saveUserPrivate(is_private : is_private,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetAppVersionService(completion: @escaping (Result<JSON>) -> ())
    {
        //TODO:- Set is_china variable
        var kURL = kApplicationBaseURL + R.string.webURL.kGetAppVersionURL()
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["version"] = VersionCheck.shared.appVersion ?? "0.0"
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["is_china"] = isCheckUserLocalChina() ? "1" : "0"
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetAppVersionService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        completion($0.result)
                    }else{
                        completion($0.result)
                    }
                }
                else
                {
                    completion($0.result)
                }
        })
    }
    func GetUserGuidanceService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGuidanceURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetUserGuidanceService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetPrivacyPolicyService(isPrivacy : Bool,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
            //kApplicationBaseURL + R.string.webURL.kPrivacyPolicyURL() + "?device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        
        if(isPrivacy)
        {
            kURL = kApplicationBaseURL + R.string.webURL.kPrivacyPolicyURL()
              //  + "?device_id=\(getFirebaseToken())&device_type=\(deviceType)"
           
            dictParam["device_id"] = getFirebaseToken()
            dictParam["device_type"] = deviceType
            
        }else{
            kURL = kApplicationBaseURL + R.string.webURL.kLicenceAgreementURL()
//                + "?device_id=\(getFirebaseToken())&device_type=\(deviceType)"
            
            dictParam["device_id"] = getFirebaseToken()
            dictParam["device_type"] = deviceType
            
        }
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetPrivacyPolicyService(isPrivacy : isPrivacy,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func ForgotPasswordService(param:[String:String],completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kForgotPasswordURL()
            
//            + "?username=\(param["email"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)&user_type=\(userType)&timestamp=\(getFormatedDate(date:Date()))&timezone=\(getCurrentTimeZone())"
        
        var dictParam = [String:String]()
        dictParam["username"] = encryptString(string: param["email"] ?? "")
        dictParam["user_type"] = userType
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["timezone"] = getCurrentTimeZone()
        
        
        if(isCheckUserLocalChina())
        {
//            kURL = kURL + "&territory_name=2"
            dictParam["territory_name"] = "2"
        }
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("Dictparam : \(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.ForgotPasswordService(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetConnectedTutorService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetDefaultTutorURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)&user_type=\(userType)"
        
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id")) 
        dictParam["user_type"] = userType
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetConnectedTutorService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func ReportIssueService(param : [String:String],completion: @escaping (Result<JSON>) -> ())
    {
        //https://steer.global/usteer/apps/common/report_issue.php?userid=&issue_description=&issue_category=&device_id=&device_type=&created=&timezone=
        
        var kURL = kApplicationBaseURL + R.string.webURL.kReportIssueURL()
            
//            + "?userid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)&created=\(getFormatedDate(date:Date()))&timezone=\(getCurrentTimeZone())&issue_description=\(String(describing: param["issue_description"] ?? ""))&issue_category=\(String(describing: param["issue_category"] ?? ""))"
        
        var dictParam = [String:String]()
        dictParam["userid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["created"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["issue_description"] =  encryptString(string: String(describing: param["issue_description"] ?? ""))
        dictParam["issue_category"] = encryptString(string: String(describing: param["issue_category"] ?? ""))
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.ReportIssueService(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    
    func fetchGeneratedKey(completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kGetSecretKey()
        
        //            + "?userid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)&created=\(getFormatedDate(date:Date()))&timezone=\(getCurrentTimeZone())&issue_description=\(String(describing: param["issue_description"] ?? ""))&issue_category=\(String(describing: param["issue_category"] ?? ""))"
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: [:], encoding: URLEncoding.httpBody, headers: nil).responseJSON(completionHandler:
            {
                print("completion($0.result):\($0.result))")
                if $0.result.isSuccess
                {
                    
                    let jsonData = JSON($0.result.value)
                    print("jsonData:\(jsonData)")
                    completion(.success(jsonData))

                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.fetchGeneratedKey(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        let jsonData = JSON($0.result.value)
                        print("jsonData:\(jsonData)")
                        completion(.success(jsonData))
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        let jsonData = JSON($0.result.value)
                        print("jsonData:\(jsonData)")
                        completion(.success(jsonData))
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    let jsonData = JSON($0.result.value)
                    print("jsonData:\(jsonData)")
                    completion(.success(jsonData))
                }
        })
    }
}
