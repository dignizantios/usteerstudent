//
//  MyTargetsService.swift
//  USteerStudent
//
//  Created by Usteer on 2/7/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct MyTargetsService
{
    func GetMyTarget(relationID : String,isFromMyTargets : Bool ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["user_type"] = "1"
        dictParam["timezone"] = getCurrentTimeZone()
        
        if(isFromMyTargets)
        {
            kURL = kApplicationBaseURL + R.string.webURL.kGetTargetsURL()
                
//                + "?uid=\(getUserDetail("user_id"))&user_type=1&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        }else{
            kURL = kApplicationBaseURL + R.string.webURL.kGetTargetsURL()
                
                dictParam["rid"] = relationID
                    
//                + "?uid=\(getUserDetail("user_id"))&rid=\(relationID)&user_type=1&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        }
        
        kURL = kURL.encodedURLString()
        print("dictParam:\(dictParam)")
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetMyTarget(relationID : relationID,isFromMyTargets : isFromMyTargets ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    //
    func GetTargetStatements(targetId : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetMyTargetStatementsURL()
            
//            + "?uid=\(getUserDetail("user_id"))&tid=\(targetId)&user_type=1&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["user_type"] = "1"
        dictParam["tid"] = targetId
        dictParam["timezone"] = getCurrentTimeZone()
        
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetTargetStatements(targetId : targetId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetMyTargetStatements(param : [String:Any],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetRiskStatementsURL()
            
//            + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&factor=\(param["factor"] ?? "")&tscore=\(param["tscore"] ?? "")&tdate=\(param["tdate"] ?? "")&cscore=\(param["cscore"] ?? "")&timezone=\(param["timezone"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:Any]()
        dictParam["uid"] = encryptString(string:param["uid"] as! String)
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["factor"] = encryptString(string: param["factor"] as! String) 
        dictParam["tscore"] = encryptString(string: param["tscore"] as! String)
        dictParam["tdate"] = param["tdate"] ?? ""
        dictParam["cscore"] = encryptString(string: param["cscore"] as! String)
        dictParam["timezone"] = param["timezone"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetMyTargetStatements(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func SaveMyTargetStatements(param : [String:Any],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSaveTargetURL()
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.SaveMyTargetStatements(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    //
    func DeleteMyTarget(targetId : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kDeleteTargetURL()
            
//            + "?uid=\(getUserDetail("user_id"))&tid=\(targetId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["tid"] = targetId
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.DeleteMyTarget(targetId : targetId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func GetPotholsStatement(param : [String:Any],completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kGetPotholesStatementURL()
            
//            + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&factor=\(param["factor"] ?? "")&tscore=\(param["tscore"] ?? "")&tdate=\(param["tdate"] ?? "")&cscore=\(param["cscore"] ?? "")&timezone=\(param["timezone"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:Any]()
        dictParam["uid"] = encryptString(string: param["uid"] as? String ?? "")
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["factor"] = encryptString(string: param["factor"] as? String ?? "")
        dictParam["tscore"] =  encryptString(string: param["tscore"] as? String ?? "")
        dictParam["tdate"] = param["tdate"] ?? ""
        dictParam["cscore"] = encryptString(string: param["cscore"] as? String ?? "")
        dictParam["timezone"] = param["timezone"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetMyTargetStatements(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
