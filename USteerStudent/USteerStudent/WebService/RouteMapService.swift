//
//  RouteMapService.swift
//  USteerStudent
//
//  Created by Usteer on 1/31/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct RouteMapService
{
    func AddRelationService(relation : String,type : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kAddRelationURL()
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["cname"] = relation
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.AddRelationService(relation : relation,type : type,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetRelationService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetRelationURL()
        var dictParam = [String:String]()
        dictParam["uid"] = getUserDetail("user_id")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetRelationService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func AddRouteMapService(strIndividual : String,strGroup : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSubmitRouteMapURL()
        var dictParam = [String:String]()
        dictParam["uid"] = getUserDetail("user_id")
        dictParam["ind"] = strIndividual
        dictParam["grp"] = strGroup
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType

        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.AddRouteMapService(strIndividual : strIndividual,strGroup : strGroup,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetTreeMapService(sort_by : String,completion: @escaping (Result<JSON>) -> ())
    {
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        var kURL = ""
        if(FilterOption.ByRelationship.rawValue == sort_by)
        {
            kURL = kApplicationBaseURL + R.string.webURL.kGetTreeMapURL()
        }
        else
        {
            kURL = kApplicationBaseURL + R.string.webURL.kGetTreeMapURL()
            dictParam["sort_by"] = encryptString(string: sort_by)
        }
        
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam , encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetTreeMapService(sort_by : sort_by,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        //showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    //showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetSingleRelatinoTreeMapService(relationId : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetTreeMapURL()
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = encryptString(string: relationId)
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetSingleRelatinoTreeMapService(relationId : relationId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func AddSubRelationService(param : [String:String],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kAddRelationURL()
        let contactNumber = param["contact"]?.encodedNameString() ?? ""
        let email = encryptString(string: param["email"]?.encodedNameString() ?? "") 

        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["pid"] = param["pid"] ?? ""  //already come deprecated
        dictParam["rname"] = encryptString(string: param["rname"] ?? "")
        dictParam["rel_type"] = encryptString(string: param["rel_type"] ?? "")
        dictParam["created"] = param["created"] ?? ""
        dictParam["timezone"] = param["timezone"] ?? ""
        dictParam["link_id"] = param["link_id"] ?? "" //already come deprecated
        dictParam["email"] = email
        dictParam["contact"] = encryptString(string: contactNumber)
        if(param["rel_type"] != "3")
        {
            dictParam["rel_gender"] = encryptString(string: param["rel_gender"] ?? "")
        }
        kURL = kURL.encodedURLString()

        print("url - ",kURL)
        print("DictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.AddSubRelationService(param : param,completion: completion)
                    }
                    else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }
                else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    //
    func EditSubRelationService(param : [String:String],completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = ""
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["rname"] = encryptString(string: param["rname"] ?? "")
        dictParam["rel_type"] = param["rel_type"] ?? ""
        dictParam["modified"] = param["modified"] ?? ""
        dictParam["timezone"] = param["timezone"] ?? ""
        dictParam["link_id"] = param["link_id"] ?? ""

        
        if(param["rel_type"] == "3")
        {
            kURL = kApplicationBaseURL + R.string.webURL.kEditRelationURL()
                
//                + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&rname=\(param["rname"] ?? "")&rel_type=\(param["rel_type"] ?? "")&modified=\(param["modified"] ?? "")&timezone=\(param["timezone"] ?? "")&link_id=\(param["link_id"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
            
        }else{
            kURL = kApplicationBaseURL + R.string.webURL.kEditRelationURL()
                
//                + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&rname=\(param["rname"] ?? "")&rel_type=\(param["rel_type"] ?? "")&rel_gender=\(param["rel_gender"] ?? "")&modified=\(param["modified"] ?? "")&timezone=\(param["timezone"] ?? "")&link_id=\(param["link_id"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
            
            dictParam["rel_gender"] = encryptString(string: param["rel_gender"] ?? "")
            
        }
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.EditSubRelationService(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func DeleteSubRelationService(strRelationId : String,completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kDeleteRelationURL()
            
//            + "?uid=\(getUserDetail("user_id"))&rid=\(strRelationId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = strRelationId
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.DeleteSubRelationService(strRelationId : strRelationId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func shareWithTutorService(strRelationId : String,isShared : String,completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kShareWithTutorURL()
            
//            + "?uid=\(getUserDetail("user_id"))&rid=\(strRelationId)&is_shared=\(isShared)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = strRelationId
        dictParam["is_shared"] = isShared
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.shareWithTutorService(strRelationId : strRelationId,isShared : isShared,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func getParentNodesService(strRelationId : String,completion: @escaping (Result<JSON>) -> ())
    {
        //
        var kURL = kApplicationBaseURL + R.string.webURL.kGetParentsNodeURL()
            
//            + "?uid=\(getUserDetail("user_id"))&rid=\(strRelationId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = strRelationId
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getParentNodesService(strRelationId : strRelationId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func moveToNodeService(sourceId:String,destinationId:String,completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kMoveToNodeURL()
            
//            + "?uid=\(getUserDetail("user_id"))&sourceId=\(sourceId)&destinationId=\(destinationId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id")) 
        dictParam["sourceId"] = sourceId
        dictParam["destinationId"] = destinationId
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.moveToNodeService(sourceId:sourceId,destinationId:destinationId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetRelationDetailService(relationID : String ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetTreeMapURL()
            
//            + "?uid=\(getUserDetail("user_id"))&rid=\(relationID)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = encryptString(string: relationID)
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetRelationDetailService(relationID : relationID ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetAudioListService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSteerMymindURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetAudioListService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func PhaseChangeService(strUrl : String,param: [String:String],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
        
        kURL = strUrl.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.PhaseChangeService(strUrl : strUrl, param: param ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
