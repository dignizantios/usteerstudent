//
//  ChatService.swift
//  USteerStudent
//
//  Created by Usteer on 3/1/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct ChatService
{
    func sendMessage(param : [String : Any],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSendMessageURL()
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.sendMessage(param : param ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func getChatList(userId : String ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetChatListURL()
            
//            + "?uid=\(userId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: userId)
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("dictParam:\(dictParam)")
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getChatList(userId : userId ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func getChatMessagesList(param : [String:Any] ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetChatMessageListURL()
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getChatMessagesList(param : param ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func deleteMessage(param : [String : Any],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kDeleteChatURL()
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.deleteMessage(param : param ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetGroupMembersList(GroupId : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetGroupMembersURL()
            
//            + "?uid=\(getUserDetail("user_id"))&group_id=\(GroupId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["group_id"] = encryptString(string: GroupId)
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetGroupMembersList(GroupId : GroupId ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func ExitGroup(GroupId : String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kExitGroupURL()
            
//            + "?uid=\(getUserDetail("user_id"))&group_id=\(GroupId)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["group_id"] = encryptString(string: GroupId)
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.ExitGroup(GroupId : GroupId ,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    //MARK: - Pupil pupil chat
    func getConnectedPupilListService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetConnectedPupilURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = getUserDetail("user_id")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getConnectedPupilListService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func getRequestedPupilListService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetRequestPupilURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = getUserDetail("user_id")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType

        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getRequestedPupilListService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }//
    func sendRequestActionService(param : [String:String],completion: @escaping (Result<JSON>) -> ())
    {
        
        
        var kURL = kApplicationBaseURL + R.string.webURL.kSendRequestActionURL()
            
//            + "?uid=\(getUserDetail("user_id"))&sender_id=\(getUserDetail("user_id"))&receiver_id=\(param["receiver_id"] ?? "")&rname=\(param["rname"] ?? "")&status=\(param["status"] ?? "")&timestamp=\(getFormatedDate(date:Date()))&timezone=\(getCurrentTimeZone())&rid=\(param["rid"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = getUserDetail("user_id")
        dictParam["sender_id"] = getUserDetail("user_id")
        dictParam["receiver_id"] = param["receiver_id"] ?? ""
        dictParam["rname"] = param["rname"] ?? ""
        dictParam["status"] = param["status"] ?? ""
        dictParam["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType

        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.sendRequestActionService(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    //
    func sendPupilRequestActionService(param : [String:String],completion: @escaping (Result<JSON>) -> ())
    {
        
        /*
         
         */
        var kURL = kApplicationBaseURL + R.string.webURL.kSendPupilRequestURL()
            
//            + "?uid=\(getUserDetail("user_id"))&sender_id=\(getUserDetail("user_id"))&receiver_id=\(param["receiver_id"] ?? "")&rname=\(param["rname"] ?? "")&status=\(param["status"] ?? "")&timestamp=\(getFormatedDate(date:Date()))&timezone=\(getCurrentTimeZone())&rid=\(param["rid"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        
        var dictParam = [String:String]()
        dictParam["uid"] = getUserDetail("user_id")
        dictParam["sender_id"] = getUserDetail("user_id")
        dictParam["receiver_id"] = param["receiver_id"] ?? ""
        dictParam["rname"] = param["rname"] ?? ""
        dictParam["status"] = param["status"] ?? ""
        dictParam["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.sendRequestActionService(param : param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
