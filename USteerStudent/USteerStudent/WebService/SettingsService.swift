//
//  SettingsService.swift
//  USteerStudent
//
//  Created by Usteer on 3/1/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct SettingsService
{
    func fetchNotificationSettingService(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kFetchSettingsURL()
            
//            + "?uid=\(getUserDetail("user_id"))&token=\(getFirebaseToken())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["token"] = getFirebaseToken()
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.fetchNotificationSettingService(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func saveSettingsService(isOn : String ,switchValue : String ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["switch"] = switchValue
        dictParam["value"] = isOn
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        if(switchValue == "em")
        {
             kURL = kApplicationBaseURL + R.string.webURL.kChangeEmailSettingsURL()
                
//                + "?uid=\(getUserDetail("user_id"))&switch=\(switchValue)&value=\(isOn)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        }else{
             kURL = kApplicationBaseURL + R.string.webURL.kChangePushSettingsURL()
                
//                + "?uid=\(getUserDetail("user_id"))&switch=\(switchValue)&value=\(isOn)&token=\(getFirebaseToken())&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
            
             dictParam["token"] = getFirebaseToken()
            
        }
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.saveSettingsService(isOn : isOn, switchValue: switchValue,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    //
    func saveSoundNotificationService(isOn : String ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
        
        kURL = kApplicationBaseURL + R.string.webURL.kSaveNotificationSoundKURL()
            
//            + "?uid=\(getUserDetail("user_id"))&value=\(isOn)&token=\(getFirebaseToken())&device_type=\(deviceType)&device_id=\(getFirebaseToken())"
       
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["value"] = isOn
        dictParam["token"] = getFirebaseToken()
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.saveSoundNotificationService(isOn : isOn,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
