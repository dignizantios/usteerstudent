//
//  ReportService.swift
//  USteerStudent
//
//  Created by Usteer on 2/13/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct ReportService
{
    func GetReportData(param:[String:String],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetReportsURL()
            
//            + "?uid=\(getUserDetail("user_id"))&rid=\(param["rid"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)&timezone=\(getCurrentTimeZone())"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["timezone"] = getCurrentTimeZone()
        
        print("dictParam:\(dictParam)")
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetReportData(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func AddPointNotes(param:[String:Any],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kAddNotesURL()
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.AddPointNotes(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func ViewPointNotes(param:[String:String],completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kViewNotesURL()
            
//            + "?uid=\(getUserDetail("user_id"))&rid=\(param["rid"] ?? "")&score_id=\(param["score_id"] ?? "")&is_historic=\(param["is_historic"] ?? "")&factor=\(param["factor"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)&timezone=\(getCurrentTimeZone())"
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["score_id"] = param["score_id"] ?? ""
        dictParam["is_historic"] = param["is_historic"] ?? ""
        dictParam["factor"] = param["factor"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["timezone"] = getCurrentTimeZone()
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.AddPointNotes(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func GetPointData(param:[String:String],completion: @escaping (Result<JSON>) -> ())
    {
        
        
        var kURL = kApplicationBaseURL + R.string.webURL.kGetPointsDataURL()
            
//            + "?uid=\(param["uid"] ?? "")&rid=\(param["rid"] ?? "")&score_id=\(param["score_id"] ?? "")&score=\(param["score"] ?? "")&is_historic=\(param["is_historic"] ?? "")&factor=\(param["factor"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)&timezone=\(getCurrentTimeZone())"

        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("param:\(param)")
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "") 
        dictParam["rid"] = param["rid"] ?? ""
        dictParam["score_id"] = param["score_id"] ?? ""
        dictParam["score"] = encryptString(string: param["score"] as? String ?? "")
        dictParam["is_historic"] = encryptString(string: param["is_historic"] ?? "")
        dictParam["factor"] = encryptString(string: param["factor"] ?? "") 
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["timezone"] = getCurrentTimeZone()
        
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetReportData(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
