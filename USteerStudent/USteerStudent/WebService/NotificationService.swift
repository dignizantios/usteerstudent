//
//  NotificationService.swift
//  USteerStudent
//
//  Created by Usteer on 2/14/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct NotificationService
{
    func GetNotifications(next_offset:String = "0",completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetNotificationURL()
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["next_offset"] = next_offset
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictParam :\(dictParam)")
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.GetNotifications(next_offset: next_offset, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func DeleteNotifications(notification_id : String,status:String = "",completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = String()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        if status == ""{
            kURL = kApplicationBaseURL + R.string.webURL.kDeleteSocialAssistantURL()
            dictParam["notification_id"] = notification_id
            
        } else if status == "delete_all"{
             kURL = kApplicationBaseURL + R.string.webURL.kDeleteSocialAssistantURL()
             dictParam["action"] = status
        }
        
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictParam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.DeleteNotifications(notification_id : notification_id,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
