//
//  LinkSchoolService.swift
//  USteerStudent
//
//  Created by Usteer on 2/13/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct LinkSchoolService
{
    func LinkSchoolAccount(param:[String:String],userType:String,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kLinkSchoolAccountURL()
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        var dictParam = [String:Any]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["user_type"] = param["user_type"] ?? ""
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["timestamp"] = param["timestamp"] ?? ""
        dictParam["link_type"] = param["link_type"] ?? ""
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["username"] = encryptString(string: param["username"] ?? "")
        dictParam["password"] = encryptString(string: param["password"] ?? "")
        print("DictParam : \(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.LinkSchoolAccount(param:param,userType:userType,completion: completion)
                    }
                    else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func LinkAccountWithCode(param:[String:String],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kLinkParentCodeURL()
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        var dictParam = [String:Any]()
        dictParam["user_id"] = encryptString(string: getUserDetail("user_id"))
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["timestamp"] = param["timestamp"] ?? ""
        dictParam["code"] = encryptString(string: param["code"] ?? "")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType

        print("DictParam : \(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.LinkSchoolAccount(param:param,userType:userType,completion: completion)
                    }
                    else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        let str = String(decoding: $0.data!, as: UTF8.self)
                        print(str)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }
                else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func SendRequest(coach_id : String,school_id : String ,status: Int,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kSendRequestURL()
        var dictParam = [String:Any]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["coach_id"] = coach_id
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["school_id"] = school_id
        dictParam["status"] = status
        dictParam["device_id"] = getFirebaseToken()
        dictParam["time"] = DateToString(Formatter: server_dt_format, date: Date())
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.SendRequest(coach_id : coach_id,school_id : school_id ,status: status,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func getCoachList(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetCoachPupilListURL()
        var dictParam = [String:Any]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getCoachList(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
