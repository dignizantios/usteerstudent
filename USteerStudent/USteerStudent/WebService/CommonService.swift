//
//  CommonService.swift
//  LitCircle
//
//  Created by ASTrackingPupil on 9/18/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

struct CommonService
{
    func PostService(url: String,param: Parameters,completion:@escaping(Result<JSON>)-> ())
    {
        print("url - ",url)
        print("Parma - ",param.prettyPrint())
        let strConvertedURL = url.encodedURLString()
        Alamofire.request(strConvertedURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                       APIResponseHandle(statusCode: statusCode!)
                       completion($0.result)
                    }
                    else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
        
    }
    
    func GetService(url: String,completion:@escaping(Result<JSON>)-> ())
    {
        let strURL = url.encodeString()
        print("strURL - ",strURL)

        Alamofire.request(strURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).authenticate(user:R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                print("Request Response - ",$0.result)
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        completion($0.result)
                    }
                    else if(statusCode != nil)
                    {
                        completion($0.result)
                    }
                    else
                    {
                        completion($0.result)
                    }
                }
                else
                {
                    completion($0.result)
                }
        })
    }
    /*
    func PostMultiPartFormData(url: String,parameter: Parameters,completion:@escaping(Result<JSON>)-> ())
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let authData = "\(R.string.keys.basicAuthenticationUsername()):\(R.string.keys.basicAuthenticationPassword())".data(using: String.Encoding.utf8)!
            var headers = ["Authorization":"Basic \(authData.base64EncodedString())"]
            if let accesstoken = GlobalVariables.Defaults.value(forKey:"token") as? String
            {
                headers = ["Authorization":"Basic \(authData.base64EncodedString())","token":accesstoken]
            }
            
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                /*for data in files {
                 multipartFormData.append(data, withName: withName[index], fileName: withFileName[index] , mimeType: mimeType[0])
                 index += 1
                 }*/
                print("multipartFormData:=",multipartFormData)
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    print("statusCode:=",upload.response?.statusCode ?? "nil")
                    upload.responseSwiftyJSON(completionHandler: { (response) in
                        let statusCode = response.response?.statusCode
                        if statusCode == 200
                        {
                            let st_encrypted = String(data: response.data!, encoding: String.Encoding.utf8)
                            let json = JSON.init(parseJSON:decreptedString(string: st_encrypted!))
                            if(json["status"].stringValue == "4")
                            {
                                makeToast(strMessage: json["message"].stringValue)
                                UIViewController().navigateToLogin()
                                completion(.success(JSON(["status":"00","msg":""])))
                            }
                            completion(.success(json))
                        }
                        else
                        {
                            print("ResponseDecodeError--------------------------------")
                            APIResponseHandle(statusCode: statusCode ?? 1)
                            completion(.success(JSON(["status":"0","msg":""])))
                        }
                    })
                    break
                    
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    makeToast(strMessage: R.string.errorMessage.something_went_wrong_key())
                    completion(.failure(error))
                    break
                }
            }
        }
        else{
            makeToast(strMessage: R.string.errorMessage.no_internet_key())
        }
    }
    
    func ServiceCall_getEncryptionKeys(parameter:NSDictionary, stURL:String) -> (NSDictionary?, URLResponse?, Error?)
    {
        var dict_data: NSDictionary?
        var responsesup: URLResponse?
        var errorsup: Error?
        let semaphore = DispatchSemaphore(value: 0)
        let authData = "\(R.string.keys.basicAuthenticationUsername()):\(R.string.keys.basicAuthenticationPassword())".data(using: String.Encoding.utf8)!
        let headers = ["Authorization":"Basic \(authData.base64EncodedString())"]
        let parameterData = NSMutableData.init()
        do
        {
            let postData = try JSONSerialization.data(withJSONObject: parameter, options: [])
            let datastring = NSString(data: postData, encoding: String.Encoding.utf8.rawValue)
            print(datastring ?? "")
            parameterData.append(("\(datastring ?? "")").data(using: String.Encoding.utf8)!)
        }
        catch
        {
            print(error.localizedDescription)
            errorsup = error
            semaphore.signal()
        }
        
        let datastring = NSString(data: parameterData as Data, encoding: String.Encoding.utf8.rawValue)
        print("Request Body: \(stURL) : \(datastring!)")
        
        let request = NSMutableURLRequest(url:URL(string: stURL)!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 60.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = parameterData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            responsesup = response
            if (error != nil)
            {
                errorsup = error
                semaphore.signal()
            }
            else
            {
                do
                {
                    let datastring = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Response body from api: \(stURL): \(datastring!)")
                    let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    dict_data = jsonDictionary
                    semaphore.signal()
                }
                catch
                {
                    print(error.localizedDescription)
                    errorsup = error
                    semaphore.signal()
                }
            }
        })
        dataTask.resume()
        _ = semaphore.wait(timeout: .distantFuture)
        return (dict_data, responsesup, errorsup)
    }
    
    func PostMultiPartFormDataNoEncryption(url: String,parameter: Parameters,completion:@escaping(Result<JSON>)-> ())
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let authData = "\(R.string.keys.basicAuthenticationUsername()):\(R.string.keys.basicAuthenticationPassword())".data(using: String.Encoding.utf8)!
            var headers = ["Authorization":"Basic \(authData.base64EncodedString())"]
            if let accesstoken = GlobalVariables.Defaults.value(forKey:"token") as? String
            {
                headers = ["Authorization":"Basic \(authData.base64EncodedString())","token":accesstoken]
            }
            Alamofire.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in parameter {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
            }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
                
                switch result
                {
                case .success(let upload, _, _):
                    print("statusCode:=",upload.response?.statusCode ?? "nil")
                    upload.responseSwiftyJSON(completionHandler: { (response) in
                        let statusCode = response.response?.statusCode
                        if statusCode == 200
                        {
                            let st_non_encrypted = String(data: response.data!, encoding: String.Encoding.utf8)
                            let json = JSON.init(parseJSON:st_non_encrypted!)
                            if(json["status"].stringValue == "4")
                            {
                                makeToast(strMessage: json["message"].stringValue)
                                UIViewController().navigateToLogin()
                                completion(.success(JSON(["status":"00","msg":""])))
                            }
                            completion(.success(json))
                        }
                        else
                        {
                            APIResponseHandle(statusCode: statusCode ?? 1)
                            completion(.success(JSON(["status":"0","msg":""])))
                        }
                    })
                    break
                    
                case .failure(let error):
                    print("Error in upload: \(error.localizedDescription)")
                    makeToast(strMessage: R.string.errorMessage.something_went_wrong_key())
                    completion(.failure(error))
                    break
                }
            }
        }
        else{
            makeToast(strMessage: R.string.errorMessage.no_internet_key())
        }
    }*/
}

extension URLResponse
{
    func getStatusCode() -> Int? {
        if let httpResponse = self as? HTTPURLResponse {
            return httpResponse.statusCode
        }
        return nil
    }
}

extension NSDictionary
{
    func prettyPrint()
    {
        for (key,value) in self {
            print("\(key) = \(value)")
        }
    }
}

extension Parameters
{
    func prettyPrint()
    {
        for (key,value) in self {
            print("\(key) = \(value)")
        }
    }
    
    func postmanPrint()
    {
        for (key,value) in self {
            print("\(key):\(value)")
        }
    }

    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }

    func printJson() {
        print(json)
    }
}

extension NSDictionary
{
    func getJsonString()
    {
        let jsonData: NSData = try! JSONSerialization.data(withJSONObject: self, options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
        print(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)
    }
}
