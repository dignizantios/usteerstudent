//
//  RiskService.swift
//  USteerStudent
//
//  Created by Usteer on 1/19/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct RiskService
{
    func RiskStatementService(relationId : String,cname : String,factor : String ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
        
//        if(isCheckAppVersion() == 1)
//        {
//            kURL = kApplicationBaseURL + R.string.webURL.kGetRiskStatementsURL() + "?uid=\(getUserDetail("user_id"))&rid="
//        }else{
            kURL = kApplicationBaseURL + R.string.webURL.kGetRiskStatementsURL()
                
//                + "?uid=\(getUserDetail("user_id"))&rid=\(relationId)&factor=\(factor)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        //}
        
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id")) 
        dictParam["rid"] = relationId
        dictParam["factor"] = factor
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("url - ",kURL)
        
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.RiskStatementService(relationId : relationId,cname : cname,factor : factor,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
