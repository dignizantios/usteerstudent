//
//  MyJourney.swift
//  USteerStudent
//
//  Created by Usteer on 5/4/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
struct MyJourney
{
    func MyJourneyService(year : Int,isFromAssessmentAfterTarget : Bool,rid : String ,completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = ""
        
        let value = isFromAssessmentAfterTarget ? "1" : "0"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["timezone"] = getCurrentTimeZone()
        dictParam["from_screen"] = value
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["rid"] = rid
        
        if(year == 0)
        {
            kURL = kApplicationBaseURL + R.string.webURL.kGetTimeLineURL()
//                + "?uid=\(getUserDetail("user_id"))&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)&from_screen=\(value)&rid=\(rid)"

        }else
        {
            kURL = kApplicationBaseURL + R.string.webURL.kGetTimeLineURL()
                
//                + "?uid=\(getUserDetail("user_id"))&year=\(year)&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&device_type=\(deviceType)&from_screen=\(value)&rid=\(rid)"
            
            dictParam["year"] = encryptString(string: "\(year)") 

        }
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        print("dictparam:\(dictParam)")
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.MyJourneyService(year : year,isFromAssessmentAfterTarget:isFromAssessmentAfterTarget,rid : rid,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
