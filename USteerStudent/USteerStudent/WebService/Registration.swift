//
//  Registration.swift
//  USteerStudent
//
//  Created by Usteer on 1/11/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

struct Registration
{
    func RegisterUser(param:[String:String],userType:String,completion: @escaping (Result<JSON>) -> ())
    {
//        var kURL = kApplicationBaseURL + R.string.webURL.kRegistrationURL() + "?name=\(param["name"] ?? "")&email=\(param["email"] ?? "")&dob=\(param["dob"] ?? "")&username=\(param["username"] ?? "")&password=\(param["password"] ?? "")&user_type=\(userType)&consent=\(param["consent"] ?? "")&timestamp=\(param["timestamp"] ?? "")&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&chart_icon=\(param["chart_icon"] ?? "")&device_type=\(deviceType)"
        
        if(getFirebaseToken() == "")
        {
            print("FCM not found")
            appDelegate.refreshFCMConnection()
            showToast(message: "Server not connecting properly try again to submit")
            return

        }
        
        var kURL = kApplicationBaseURL + R.string.webURL.kRegistrationURL()
            
//            + "?name=\(param["name"] ?? "")&lastname=\(param["lastname"] ?? "")&dob=\(param["dob"] ?? "")&username=\(param["username"] ?? "")&password=\(param["password"] ?? "")&user_type=\(userType)&consent=\(param["consent"] ?? "")&timestamp=\(param["timestamp"] ?? "")&timezone=\(getCurrentTimeZone())&device_id=\(getFirebaseToken())&chart_icon=\(param["chart_icon"] ?? "")&device_type=\(deviceType)&gender=\(param["gender"] ?? "")&user_role=\(param["user_role"] ?? "1")&version=\(GetAppversion())"
        
        
        var paramDict = [String:String]()
        paramDict = param
        paramDict["user_type"] = userType
        paramDict["timezone"] = getCurrentTimeZone()
        paramDict["device_id"] = getFirebaseToken()
        paramDict["device_type"] = deviceType
        paramDict["version"] = VersionCheck.shared.appVersion ?? "0.0"
        paramDict["user_role"] = param["user_role"] ?? "1"
        
        if(isCheckUserLocalChina())
        {
            paramDict["territory_name"] = "2"
        }
        
        kURL = kURL.encodedURLString()
        
        print("paramDict:\(paramDict)")
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: paramDict, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.RegisterUser(param:param,userType:userType,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func checkEmailRequest(param: [String : String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kCheckEmailExistURL()
//            + "?username=\(param["username"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        kURL = kURL.encodedURLString()
        
        
        var paramDict = [String:String]()
        paramDict = param
        paramDict["device_id"] = getFirebaseToken()
        paramDict["device_type"] = deviceType
        
        print("url - ",kURL)
        
        print("paramDict:\(paramDict)")
        
        //-----
        Alamofire.request(kURL, method: .post, parameters: paramDict, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.checkEmailRequest(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func getUserRole(param: [String : String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetUserRole()
        kURL = kURL.encodedURLString()
        
        print("url - ",kURL)
        
        print("param :\(param)")
        
        //-----
        Alamofire.request(kURL, method: .post, parameters: param, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getUserRole(param: param, completion:completion)
                        //self.checkEmailRequest(param:param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
