//
//  LoginService.swift
//  USteerStudent
//
//  Created by Usteer on 1/11/18.
//  Copyright © 2018 Usteer. All rights        reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster


struct LoginService
{
    func loginRequest(username:String,password:String, consent : String, completion: @escaping (Result<JSON>) -> ())
    {
        if(getFirebaseToken() == "")
        {
            print("FCM not found")
            appDelegate.refreshFCMConnection()
            showToast(message: "Server not connecting properly try again to submit")
            return
        }
        var kURL = kApplicationBaseURL + R.string.webURL.kLoginURL()
        var paramDict = [String:String]()
        paramDict["username"] = encryptString(string: username)
        paramDict["password"] = encryptString(string: password)
        paramDict["device_id"] = getFirebaseToken()
        paramDict["consent"] = consent
        paramDict["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
        paramDict["device_type"] = deviceType
        paramDict["user_type"] = userType
        paramDict["version"] = VersionCheck.shared.appVersion ?? "0.0"

        if(isCheckUserLocalChina())
        {
            paramDict["territory_name"] = "2"
        }
        
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        print("paramDict : \(paramDict)")
        Alamofire.request(kURL, method: .post, parameters: paramDict, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
        {
            if $0.result.isSuccess
            {
                completion($0.result)
            }
            else if $0.result.isFailure
            {
                let statusCode = $0.response?.statusCode
                if(statusCode == 500)
                {
                    self.loginRequest(username:username,password:password,consent:consent,completion: completion)
                }else if(statusCode != nil)
                {
                    APIResponseHandle(statusCode: statusCode!)
                    completion($0.result)
                }else{
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
            }else
            {
                showToast(message: R.string.errorMessage.something_went_wrong_key())
                completion($0.result)
            }
        })
        
    }
    func resendRequest(username:String,password:String,completion: @escaping (Result<JSON>) -> ())
    {
        if(getFirebaseToken() == "")
        {
            appDelegate.refreshFCMConnection()
        }
        
        var kURL = kApplicationBaseURL + R.string.webURL.kResendToeknURL()
//            + "?username=\(username)&password=\(password)&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["username"] =  encryptString(string: username)
        dictParam["password"] = password
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        kURL = kURL.encodedURLString()
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.resendRequest(username:username,password:password,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
        
    }
}
