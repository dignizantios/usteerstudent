//
//  ProfileService.swift
//  USteerStudent
//
//  Created by Usteer on 1/12/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

struct ProfileService
{
    func getProfile(userId:String, completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetProfileURL()
        
//            + "?uid=\(userId)&action=getprofile&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: userId)
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["action"] = encryptString(string: "getprofile")
        
        kURL = kURL.encodedURLString()

        print("url - ",kURL)
        print("dictPraam : \(dictParam)")
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getProfile(userId:userId,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func updateProfile(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kUpdateProfileURL()
            
//            + "?uid=\(param["uid"] ?? "")&action=updateprofile&name=\(param["name"] ?? "")&lastname=\(param["lastname"] ?? "")&username=\(param["username"] ?? "")&email=\(param["email"] ?? "")&birthdate=\(param["birthdate"] ?? "")&chart_icon=\(param["chart_icon"] ?? "1")&gender=\(param["gender"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)&user_role=\(param["user_role"] ?? "1")"
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam = param
        dictParam["action"] = encryptString(string: "updateprofile")
        dictParam["chart_icon"] = param["chart_icon"] ?? "1"
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["user_role"] = param["user_role"] ?? "1"
        
        print("url - ",kURL)
        print("dcitparam:\(dictParam)")
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.updateProfile(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })

    }
    func changePassword(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kChangePasswordStep1URL()
            
//            + "?uid=\(param["uid"] ?? "")&op=\(param["op"] ?? "")&np=\(param["np"] ?? "")&cp=\(param["cp"] ?? "")&device_id=\(getFirebaseToken())&date=\(getFormatedDate(date: Date()))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam = param
        dictParam["device_id"] = getFirebaseToken()
        dictParam["date"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["device_type"] = deviceType
        
        if(isCheckUserLocalChina())
        {
//            kURL = kURL + "&territory_name=2"
            dictParam["territory_name"] = "2"
        }
        
        
        kURL = kURL.encodedURLString()

        print("dictParam:\(dictParam)")
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.changePassword(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func changePasswordStep2(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        
        var kURL = kApplicationBaseURL + R.string.webURL.kChangePasswordStep2URL()
            
//            + "?uid=\(param["uid"] ?? "")&op=\(param["op"] ?? "")&np=\(param["np"] ?? "")&cp=\(param["cp"] ?? "")&otp=\(param["otp"] ?? "")&device_id=\(getFirebaseToken())&date=\(getFormatedDate(date: Date()))&device_type=\(deviceType)"
        
        var dictParam = [String:String]()
        dictParam = param
        dictParam["device_id"] = getFirebaseToken()
        dictParam["date"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["device_type"] = deviceType
        
        
        
        if(isCheckUserLocalChina())
        {
//            kURL = kURL + "&territory_name=2"
            dictParam["territory_name"] = "2"
        }
        kURL = kURL.encodedURLString()
        
        print("dictParam:\(dictParam)")
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.changePasswordStep2(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    func userLogout(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kLogoutURL()
        kURL = kURL.encodedURLString()

        var dictParam = [String:String]()
        dictParam["user_id"] = param["uid"]
        dictParam["session"] = param["session"]
        dictParam["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType

        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.changePassword(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
        
    }
    //
    func userAccountDelete(param:[String:String], completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kDeleteAccountURL()
            
//            + "?uid=\(param["uid"] ?? "")&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        kURL = kURL.encodedURLString()
        
        var dictParam =  [String:String]()
        dictParam["uid"] = encryptString(string: param["uid"] ?? "")
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.userAccountDelete(param:param,completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
        
    }
    func getCharacteristics(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetCharacteristicsURL()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getCharacteristics(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func getCar(completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetCar()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)"

        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.getCar(completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
    
    func changeCar(param:[String:String],completion: @escaping (Result<JSON>) -> ())
    {
        var kURL = kApplicationBaseURL + R.string.webURL.kGetCar()
            
//            + "?uid=\(getUserDetail("user_id"))&device_id=\(getFirebaseToken())&device_type=\(deviceType)&chart_icon=\(param["chart_icon"] ?? "")&action=update"
        
        kURL = kURL.encodedURLString()
        
        var dictParam = [String:String]()
        dictParam["uid"] = encryptString(string: getUserDetail("user_id")) 
        dictParam["device_id"] = getFirebaseToken()
        dictParam["device_type"] = deviceType
        dictParam["chart_icon"] = param["chart_icon"] ?? ""
        dictParam["action"] = "update"
        
        
        print("url - ",kURL)
        
        Alamofire.request(kURL, method: .post, parameters: dictParam, encoding: URLEncoding.httpBody, headers: nil).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword()).responseSwiftyJSON(completionHandler:
            {
                if $0.result.isSuccess
                {
                    completion($0.result)
                }
                else if $0.result.isFailure
                {
                    let statusCode = $0.response?.statusCode
                    if(statusCode == 500)
                    {
                        self.changeCar(param: param, completion: completion)
                    }else if(statusCode != nil)
                    {
                        APIResponseHandle(statusCode: statusCode!)
                        completion($0.result)
                    }else{
                        showToast(message: R.string.errorMessage.something_went_wrong_key())
                        completion($0.result)
                    }
                }else
                {
                    showToast(message: R.string.errorMessage.something_went_wrong_key())
                    completion($0.result)
                }
        })
    }
}
