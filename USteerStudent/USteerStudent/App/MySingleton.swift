//
//  MySingleton.swift
//  SwiftMVC
//
//  Created by Usteer on 05/04/17.
//  Copyright © 2017 Usteer. All rights reserved.
//

import Foundation
import UIKit

class MySingleton
{
    // Declare class instance property
    static let sharedManager = MySingleton()
    
    //MARK: - Application color theme declaration
    var themeYellowColor = UIColor()
    var themeLightGrayColor = UIColor()
    var themeChatGrayColor = UIColor()
    var themeHeaderGrayColor = UIColor()
    var themeHeaderTextGrayColor = UIColor()

    var themeSideMenuColor = UIColor()
    
    var themeDarkGrayColor = UIColor()
    var themeRedColor = UIColor()
    var themeGreenColor = UIColor()
    var themeBlueColor = UIColor()
    var themeGreenTextColor = UIColor()

    var themeChartLinesGrayColor = UIColor()

    //MARK: - UIScreen Bounds
    var screenRect = CGRect()
    
    
    //MARK: - Initializer of Class
    // @desc Declare an initializer
    // Because this class is singleton only one instance of this class can be created
    /// Initialization of variables with default value
    init()
    {
        //Shared object created of Datamanager
        screenRect = UIScreen.main.bounds

        themeYellowColor = UIColor.init(red: 253/255, green: 204/255, blue: 14/255, alpha: 1.0)
        themeLightGrayColor = UIColor.init(red: 170/255, green: 170/255, blue: 170/255, alpha: 1.0)
        themeChatGrayColor = UIColor.init(red: 217/255, green: 217/255, blue: 217/255, alpha: 1.0)
        themeHeaderGrayColor = UIColor.init(red: 246/255, green: 246/255, blue: 246/255, alpha: 1.0)
        themeHeaderTextGrayColor = UIColor.init(red: 153/255, green: 153/255, blue: 153/255, alpha: 1.0)

        themeChartLinesGrayColor = UIColor.init(red: 237/255, green: 237/255, blue: 237/255, alpha: 1.0)
        
        themeSideMenuColor = UIColor.init(red: 65/255, green: 64/255, blue: 66/255, alpha: 1.0)

        themeDarkGrayColor = UIColor.init(red: 70/255, green: 70/255, blue: 70/255, alpha: 1.0)
        themeRedColor = UIColor.init(red: 255/255, green: 102/255, blue: 102/255, alpha: 1.0)
        themeGreenColor = UIColor.init(red: 39/255, green: 173/255, blue: 39/255, alpha: 1.0)
        themeBlueColor = UIColor.init(red: 0/255, green: 112/255, blue: 192/255, alpha: 1.0)
        themeGreenTextColor = UIColor.init(red: 132/255, green: 197/255, blue: 86/255, alpha: 1.0)

    }
    
    
    //MARK: - Get Custom Fonts With size
    
    
    /// Get custom Bold type font with size
    ///
    /// - Parameter fontSize: font size in Int dataType
    /// - Returns: UIFont with size
    func getFontForTypeRoundedBoldWithSize(fontSize : Int) -> UIFont
    {
        if(screenRect.height >= 667)
        {
            return UIFont(name: "Gotham-Bold", size: CGFloat(fontSize + 2))!

        }
        else
        {
            return UIFont(name: "Gotham-Bold", size: CGFloat(fontSize-1))!

        }
    }
    
    /// Get custom Medium type font with size
    ///
    /// - Parameter fontSize: font size in Int dataType
    /// - Returns: UIFont with size
    func getFontForTypeRoundedLightWithSize(fontSize : Int) -> UIFont
    {
        if(screenRect.height >= 667)
        {
            return UIFont(name: "Gotham-Book", size: CGFloat(fontSize+2))!
        }
        else
        {
            return UIFont(name: "Gotham-Book", size: CGFloat(fontSize))!
        }
    }
    func currentScreenBoundsDependOnOrientation() -> CGRect
    {
        let reqSysVer = "8.0"
        let currSysVer: String = UIDevice.current.systemVersion
        if currSysVer.compare(reqSysVer, options: .numeric, range: nil, locale: .current) != .orderedAscending {
            return UIScreen.main.bounds
        }
        var screenBounds: CGRect = UIScreen.main.bounds
        let width: CGFloat = screenBounds.width
        let height: CGFloat = screenBounds.height
        let interfaceOrientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if UIInterfaceOrientationIsPortrait(interfaceOrientation) {
            screenBounds.size = CGSize(width: width, height: height)
            print("Portrait Height: \(screenBounds.size.height)")
        }
        else if UIInterfaceOrientationIsLandscape(interfaceOrientation) {
            screenBounds.size = CGSize(width: height, height: width)
            print("Landscape Height: \(screenBounds.size.height)")
        }
        
        return screenBounds
    }
    
}

