//
//  AppDelegate.swift
//  USteerStudent
//
//  Created by Usteer on 1/4/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
//import SplunkMint
import AVFoundation
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

import FirebaseCrash
import FirebaseMessaging
import ZendeskSDK
import ZendeskCoreSDK

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,SWRevealViewControllerDelegate,UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var orientationLock = UIInterfaceOrientationMask.portrait
    var navigationController : UINavigationController?
    var previousDeviceOrientation: UIDeviceOrientation = UIDevice.current.orientation

    struct AppUtility
    {
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
        static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation:UIInterfaceOrientation) {
            self.lockOrientation(orientation)
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        FirebaseApp.configure()
        
        try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        try? AVAudioSession.sharedInstance().setActive(true)
        
        registerForRemoteNotification()
        copyDBIfNotExist()

        if Defaults.value(forKey: "ZenDeskAppID") as? String != nil && Defaults.value(forKey: "ZenDeskClientId") as? String != nil && Defaults.value(forKey: "ZenDeskSupportURL") as? String != nil
        {
            if let appID = Defaults.value(forKey: "ZenDeskAppID") as? String,let clientID = Defaults.value(forKey: "ZenDeskClientId") as? String,let supportURL = Defaults.value(forKey: "ZenDeskSupportURL") as? String
            {
                Zendesk.initialize(appId: appID, clientId: clientID, zendeskUrl: supportURL)
                Support.initialize(withZendesk: Zendesk.instance)
            }
        }
        
        //--Firebase FCM token --//
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        return true
    }
    
    @objc func UserFlowScreen()
    {
        if getUserDetail("user_id") != ""
        {
            if(getUserDetail("is_private") == "")
            {
                navigateToUserSelect()
            }
            else
            {
                if(Defaults.value(forKey: R.string.keys.isLaunchFirstTime()) != nil)
                {
                    if(Defaults.value(forKey: R.string.keys.isLaunchFirstTime()) as! Bool == true)
                    {
                        let frontViewController = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
                        frontViewController.getAppVersion()
                        navigateToController(frontViewController : frontViewController)
                    }
                    else
                    {
                        let frontViewController = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
                        frontViewController.selectedVC = .AppLaunch
                        navigateToController(frontViewController : frontViewController)
                    }
                }
                else
                {
                    let frontViewController = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
                    frontViewController.getAppVersion()
                    navigateToController(frontViewController : frontViewController)
                }
            }
        }
        else
        {
            let frontViewController = objStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
            self.navigationController = UINavigationController.init(rootViewController: frontViewController)
            self.window?.rootViewController = self.navigationController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func navigateToUserSelect()
    {
        let frontViewController = objStoryboard.instantiateViewController(withIdentifier: "UserSelectVC") as! UserSelectVC
        let window = UIApplication.shared.delegate?.window as? UIWindow
        window?.rootViewController = frontViewController
        window?.makeKeyAndVisible()
        self.checkForVersionUpdate()
    }
    
    func navigateToController(frontViewController : UIViewController)
    {
        let rearViewController = objStoryboard.instantiateViewController(withIdentifier: "SideMenuVC")
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        revealController?.navigationController?.isNavigationBarHidden = true
        revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width)*0.75
        revealController?.view.alpha = 1
        revealController?.delegate = self
        self.navigationController = UINavigationController()
        let window = UIApplication.shared.delegate?.window as? UIWindow
        window?.rootViewController = revealController
        window?.makeKeyAndVisible()
        self.checkForVersionUpdate()
    }
    
    func checkForVersionUpdate()
    {
        VersionCheck.shared.checkAppStore() { isNew, version in
            let VersionValue = Float(version ?? "0.0")
            let currentVersion = Float(VersionCheck.shared.appVersion ?? "0.0")
            if isNew == true && currentVersion! < VersionValue!{
                let obj = VersionUpdatePopUpVC()
                obj.modalPresentationStyle = .overCurrentContext
                obj.modalTransitionStyle = .coverVertical
                UIApplication.shared.topMostViewController()?.present(obj, animated: true, completion: nil)
            }
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication)
    {
        connectToFcm()
        if(getUserDetail("user_id") != "")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "refreshView"), object: nil)
        }
    }
   
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask
    {
        if(isOnTreeView)
        {
            //TODO: Uncomment below line
            print("Orientation change call")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ReloadTreeMapView"), object: nil)
        }
        return self.orientationLock
    }
}

//MARK: - Database setup
extension AppDelegate
{
    func copyDBIfNotExist()
    {
        //3 place name change if db name change --- DAL.m,this function
        let storedDB: String = "USteerStudent.sqlite".pathInDocumentDirectory()
        let manager: FileManager = FileManager.default
        
        let dbPath: String = Bundle.main.path(forResource: "USteerStudent", ofType: "sqlite")!
        print("database path - \(dbPath)")

        if manager.fileExists(atPath: storedDB)
        {
            return
        }
        
        do {
            try manager.copyItem(atPath: dbPath, toPath: storedDB)
            print("database path - \(dbPath)")
            print("dbPath copy success")
        }
        catch let error as NSError
        {
            print("error - ",error.localizedDescription)
        }
    }
    //MARK: - Other methods
    func convertToDictionary(text: String) -> NSMutableDictionary
    {
        var dict = NSMutableDictionary()
        if let data = text.data(using: .utf8)
        {
            do
            {
                dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSMutableDictionary
                return dict
            } catch
            {
                print(error.localizedDescription)
            }
        }
        return dict
    }
}
extension AppDelegate
{
    // While App on Foreground mode......
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        print("Push for ios 10 version")
        print("User Info = ",notification.request.content.userInfo)
        
        let dict = notification.request.content.userInfo
        print("JSON DICt - ",JSON(dict))
        let dictData = JSON(dict)
        print("dictNotificationData \(dictData)" )
        
        if(getUserDetail("user_id") != "")
        {
            let strRelData = dictData["rel_data"].stringValue
            let relDataDict = JSON(convertToDictionary(text: strRelData))
            
            let dictNotificationCountData = relDataDict["notification"]
            dictNotificationData = dictNotificationCountData
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotificationCountData)
            
            if(notification.request.content.categoryIdentifier == "signpost")
            {
                if(dictData["user_id"].stringValue == getUserDetail("user_id"))
                {
                    //Navigate to Signpost
                    if(isOnSteering == 0)
                    {
                        completionHandler([.badge,.alert,.sound])
                    }else if(isOnSteering == 1)
                    {
                        completionHandler([.badge,.alert,.sound])
                    }else if(isOnSteering == 2)
                    {
                        
                    }
                }else{
                    completionHandler([.badge,.alert,.sound])
                }
            }else{
                if(dictData["page_type"].stringValue == "1") //1 for request handling for coach list section
                {
                    if(isOnCoachListScreen)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCoachList"), object: nil)
                        
                    }else{
                        completionHandler([.badge,.alert,.sound])
                    }
                }
                else if(dictData["page_type"].stringValue == "2") {   // 2 for chat notification
                    
                    print("Converted JSON - ",relDataDict)
                    print("dictChatUserData:\(dictChatUserData)")
                    
                    if relDataDict["is_group"].boolValue{
                        dictChatUserData["group_id"].stringValue = encryptString(string: dictChatUserData["group_id"].stringValue)
                    }
                    
                    if(isOnChatScreen)
                    {
                        if(dictChatUserData["other_id"].intValue == relDataDict["other_id"].intValue && dictChatUserData["group_id"].intValue == relDataDict["group_id"].intValue)
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatData"), object: relDataDict)
                            
                        }else{
                            completionHandler([.badge,.alert,.sound])
                        }
                        
                    }else if(isOnChatListScreen)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatList"), object: relDataDict)
                        
                    }else{
                        completionHandler([.badge,.alert,.sound])
                    }
                }
                else if(dictData["page_type"].stringValue == "3")
                {   // 3 for social assistant
                    
                    completionHandler([.badge,.alert,.sound])
                }
                else if(dictData["page_type"].stringValue == "4"){   // 4 for deleted chat message
                    
                    if(isOnChatListScreen)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatList"), object: relDataDict)
                        
                    }else if(isOnChatScreen)
                    {
                        if(dictChatUserData["other_id"].intValue == relDataDict["other_id"].intValue && dictChatUserData["group_id"].intValue == relDataDict["group_id"].intValue)
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatData"), object: relDataDict)
                        }
                    }
                }
                else
                {
                    completionHandler([.badge,.alert,.sound])
                }
            }
        }else{
            completionHandler([.badge,.alert,.sound])
        }
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void)
    {
        print("Tap on Banner Push for ios 10 version")
        
        print("User Info = ",response.notification.request.content.userInfo)
        let dict = response.notification.request.content.userInfo
        
        print("JSON DICt - ",JSON(dict))
        let userDict = JSON(dict)
        
        let strRelData = userDict["rel_data"].stringValue
        let relDataDict = JSON(convertToDictionary(text: strRelData))
        
        let dictNotificationCountData = relDataDict["notification"]
        dictNotificationData = dictNotificationCountData
        
        print("dictNotificationData From push \(dictNotificationCountData)" )
        print("categoryIdentifier - ",response.notification.request.content.categoryIdentifier)
        print("dictNotificationData From push did recieve \(dictNotificationData)" )

        if(getUserDetail("user_id") != "")
        {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotificationCountData)
            if(response.notification.request.content.categoryIdentifier == "signpost")
            {
                if(userDict["user_id"].stringValue == getUserDetail("user_id"))
                {
                    let strJsonCategory = userDict["categoryData"].stringValue
                    let dataToConvert = strJsonCategory.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    
                    //TODO: - Uncomment for bigger than ios10
                    var dictCategory = JSON()
                    if let data = try? JSON(data:dataToConvert!){
                        dictCategory = data
                    }
                    
                    if(isOnSteering == 0)
                    {
                        navigateToSignpost(categoryDict: dictCategory)
                    }else if(isOnSteering == 1)
                    {
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "navigateToMyTargets"), object: nil)
                        //
                    }else if(isOnSteering == 2)
                    {
                        
                    }
                }else{
                    completionHandler()
                }
            }
            else
            {
                if(userDict["page_type"].stringValue == "1") //1 for request handling for coach list section
                {
                    self.navigateToCoachList()
                    completionHandler()

                }
                else if(userDict["page_type"].stringValue == "2"){   // 2 for chat notification
                    
                    if(isOnChatScreen)
                    {
                        if(dictChatUserData["other_id"].intValue == relDataDict["other_id"].intValue && dictChatUserData["group_id"].intValue == relDataDict["group_id"].intValue)
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatData"), object: relDataDict)
                            
                        }else{
                            self.navigateToChat(relDataDict : relDataDict)
                        }
                    }
                    else{
                        self.navigateToChat(relDataDict : relDataDict)
                    }
                    completionHandler()

                }
                else if(userDict["page_type"].stringValue == "3"){   // 3 for social assistant
                    self.navigateToSocialAssistant(relDataDict : relDataDict)
                    completionHandler()
                }
                else if(userDict["page_type"].stringValue == "4"){   // 4 for Deleted chat message
                    
                }
            }
        
        }else{
            completionHandler()
        }
    }
    
    //    MARK:- Remote Notification Methods
    func registerForRemoteNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                Messaging.messaging().delegate = self
                print("granted:==\(granted)")
                if granted {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
                else {
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
            
        }
    }
    func registerForRemoteWithoutSoundNotification() {
        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.alert, .badge]) { (granted, error) in
                Messaging.messaging().delegate = self
                print("without notification granted:==\(granted)")
                if granted {
                    DispatchQueue.main.async(execute: {
                        UIApplication.shared.registerForRemoteNotifications()
                    })
                }
                else {
                }
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data)
    {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        
        print("Device Token = ", token)
        UserDefaults.standard.setValue(token, forKey: "device_token")
        UserDefaults.standard.synchronize()
        Messaging.messaging().apnsToken = deviceToken
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error)
    {
        print("Error = ",error.localizedDescription)
    }
}
//MARK: - Firebase Messeging delegate methods

extension AppDelegate : MessagingDelegate
{
    func refreshFCMConnection()
    {
        connectToFcm()
        let token = Messaging.messaging().fcmToken
        print("FCM After refresh token: \(token ?? "")")
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        guard InstanceID.instanceID().token() != nil else {
            return
        }
        
        // Disconnect previous FCM connection if it exists.
        //Messaging.messaging().disconnect() //Deprecated
        Messaging.messaging().shouldEstablishDirectChannel = false
        Messaging.messaging().shouldEstablishDirectChannel = true
        
        Messaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error?.localizedDescription ?? "")")
            } else {
                print("Connected to FCM.")
                let token = Messaging.messaging().fcmToken
                print("FCM token: \(token ?? "")")
            }
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String)
    {
        print("Firebase registration token: \(fcmToken)")
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
}
//MARK: - Navigation from Push
extension AppDelegate
{
    func navigateToSignpost(categoryDict : JSON)
    {
        let frontViewController = objReportStoryboard.instantiateViewController(withIdentifier: "MySteeringVC") as! MySteeringVC
        frontViewController.isFromReminder = true
        frontViewController.categoryDict = categoryDict
        navigateToController(obj : frontViewController)
    }
    func navigateToChat(relDataDict : JSON)
    {
        if(isOnChatScreen)
        {
            //Trigger post notification for reload chat data
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatData"), object: relDataDict)
        }else{
            let frontViewController = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            frontViewController.isFromPush = true
            frontViewController.dictChatDetails = relDataDict
            navigateToController(obj : frontViewController)
        }
    }
    func navigateToSocialAssistant(relDataDict : JSON)
    {
        let frontViewController = objNotificationStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        navigateToController(obj : frontViewController)
    }
    func navigateToCoachList()
    {
        //navigate to Coach list screen
        if(isOnCoachListScreen)
        {
            //Trigger post notification for reload coach list data
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadCoachList"), object: nil)
        }else{
            
            let frontViewController = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            frontViewController.isNavigateToCoach = true
            navigateToController(obj : frontViewController)
        }
    }
    func navigateToController(obj : UIViewController)
    {
        let frontViewController = obj
        let rearViewController = objStoryboard.instantiateViewController(withIdentifier: "SideMenuVC")
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        
        revealController?.navigationController?.isNavigationBarHidden = true
        revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width)*0.75
        revealController?.view.alpha = 1
        revealController?.delegate = self
        
        self.navigationController = UINavigationController()
        
        let window = UIApplication.shared.delegate?.window as? UIWindow
        window?.rootViewController = revealController
        window?.makeKeyAndVisible()
    }
}

//MARK:- Local Notification Reminder
extension AppDelegate
{
    func locallyNotificationSignpostReminder(year:Int , month: Int , day: Int,message : String,title:String,relationId:String,categoryData:JSON,identifier:String)
    {
        //"Simon, just a reminder to retrack your steering with Richard"
        let calendar = Calendar.current
        var timeInterval = DateComponents()
        timeInterval.minute = 2
        let date = Calendar.current.date(byAdding: timeInterval, to: Date())
        let currenthour = calendar.component(.hour, from: date ?? Date())
        let currentminute = calendar.component(.minute, from: date ?? Date())
        
        var datepicker = DateComponents()
        datepicker.year = year
        datepicker.month = month
        datepicker.day = day
        datepicker.hour = currenthour
        datepicker.minute = currentminute
        print("datepicker :\(datepicker)")
    
        //TODO: Uncomment for bigger than ios10
        let trigger = UNCalendarNotificationTrigger(dateMatching: datepicker, repeats: false)
        
        print("------------- Converted JSON to string ---------")
        let strJson = jsonToString(json: categoryData)
        
        let json = NSMutableDictionary()
        json.setValue(getUserDetail("user_id"), forKey: "user_id")
        json.setValue(relationId, forKey: "rid")
        json.setValue(strJson, forKey: "categoryData")

        let content = UNMutableNotificationContent()
        content.title = title
        content.body = message
        content.categoryIdentifier = "signpost"
        content.sound = UNNotificationSound.default()
        content.userInfo = json as! [AnyHashable : Any]
        
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().add(request){(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
        
    }
}

//MARK: - Service
extension AppDelegate
{
    func userLogoutService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            let param : [String:String] = ["uid" : encryptString(string: getUserDetail("user_id")),
                                           "session" : getUserDetail("session")
            ]
            
            ProfileService().userLogout(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.window?.rootViewController?.RemoveAllNotifications()
                        self.window?.rootViewController?.navigateToLogin()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.window?.rootViewController?.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
