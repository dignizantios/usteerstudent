//
//  Global.swift
//  USteerTeacher
//
//  Created by Usteer on 30/12/17.
//  Copyright © 2017 Usteer. All rights reserved.
//

import Foundation
import UIKit
import Toaster
import SwiftyJSON
import Firebase
//import Contacts
import MaterialComponents
import FirebaseMessaging
import Alamofire
import AlamofireSwiftyJSON
import AVFoundation
import CryptoSwift
import CommonCrypto
/*
 User 6
 username=johnyoung
 password=youngjohn
 
 User 7
 username=developer
 password=developer
 
 */


/*
 
    changes are made in  suggestion, addrelation ,edit relation,  API and other new API i build to add usteer user and contact as a relation and than send invitation, accept, declin.
 */

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let Defaults = UserDefaults.standard

//
var LoaderString:String = getCommonString(key: "Loading_key")
var LoaderType:Int = 29
let LoaderSize = CGSize(width: 30, height: 30)
//
let StringFilePath = Bundle.main.path(forResource: "CommonStrings", ofType: "plist")
let dictStrings = NSDictionary(contentsOfFile: StringFilePath!)
let screenRect = UIScreen.main.bounds
let myDal: DAL = DAL()
var arrayNotification : [JSON] = []
var deviceType = "1" ///For ios 1 and 0 for Android
var userType = "1" //for pupil 1 and 4 for coach
var isOnChatScreen = false
var isOnCoachListScreen = false
var dictChatUserData = JSON()
var isFromTargetBack = false
var strChinaRegionCode = "CN"
var GlobalAssessmentDict = JSON()
var startYear:Int = 1965

//TODO:-Check for live china
var kApplicationBaseURL = "https://steer.global/usteer_with_encryption/apps/"

struct GlobalVariables
{
    static let SDLineColor = "CC6600"
    static let TOSLineColor = "660066"
    static let TOOLineColor = "808080"
    static let SCLineColor = "CC0066"
    static let arrayCarsGlobal : [String] =  ["car_1","car_2","car_3","car_4","car_5","car_6","wheel_gray_one","wheel_gray_two","wheel_gray_three","wheel_gray_four"]
    static var isOnSeetingScreen = false
}

//
func getCommonString(key:String) -> String
{
    return dictStrings?.object(forKey: key) as? String ?? ""
}

let kAppName = getCommonString(key: "App_name_key")
let kCommonAlertTitle = ""

//
//var assessmentDict : JSON?
var currentAssessment = 0
var targetDict = JSON()
var treeZoomLevel : CGFloat = 0.6
var treeConsetOffset : CGPoint = CGPoint(x:0,y:0)
var dateSignpostValue = Date()
var isLoadFirstTime = true
var appLoadingFirstTime = true
var isLoadNotificationfirstTime = true
var isLinkSchoolAccount = false
var isSideMenuLoadFirstTime = true
var dictNotificationData = JSON()
var vwBlack = UIView(frame: UIScreen.main.bounds)
var isOnChatListScreen = false
var isOnTreeView = false

//If value is 0 then default 1 for on steering screen 2 for on target screen
var isOnSteering = 0
//

let objStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
let objMyTargetsStoryboard : UIStoryboard = UIStoryboard(name: "MyTarget", bundle: nil)
let objChatsStoryboard : UIStoryboard = UIStoryboard(name: "Chats", bundle: nil)
let objReportStoryboard : UIStoryboard = UIStoryboard(name: "Report", bundle: nil)
let objRouteMapStoryboard : UIStoryboard = UIStoryboard(name: "RouteMap", bundle: nil)
let objAssesmentStoryboard : UIStoryboard = UIStoryboard(name: "Assesment", bundle: nil)
let objProfileStoryboard : UIStoryboard = UIStoryboard(name: "Profile", bundle: nil)
let objSteerMindStoryboard : UIStoryboard = UIStoryboard(name: "SteerYourMind", bundle: nil)
let objLinkAccountStoryboard : UIStoryboard = UIStoryboard(name: "LinkAccount", bundle: nil)
let objRelationStoryboard : UIStoryboard = UIStoryboard(name: "ChooseRelation", bundle: nil)
let objCoachStoryboard : UIStoryboard = UIStoryboard(name: "Coach", bundle: nil)
let objNotificationStoryboard : UIStoryboard = UIStoryboard(name: "Notification", bundle: nil)
let objLearnStoryboard : UIStoryboard = UIStoryboard(name: "Learn", bundle: nil)
let objSubscriptionStoryboard : UIStoryboard = UIStoryboard(name: "Subscription", bundle: nil)

var selectedMenuIndex = 1

func getCurrentMonth() -> Int
{
    let date = Date()
    let calendar = Calendar.current
    
//    let year = calendar.component(.year, from: date)
    let month = calendar.component(.month, from: date)
//    let day = calendar.component(.day, from: date)
    
    return month
}
func getCurrentYear() -> String
{
    let date = Date()
    let strYear = DateToString(Formatter: "yyyy", date: date)
    return strYear
}
func getFirebaseToken() -> String
{
    let deviceToken = UserDefaults.standard.value(forKey: "device_token") as? String ?? ""
    return encryptString(string: Messaging.messaging().fcmToken ?? deviceToken)
}
func printFonts()
{
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName)
        print("Font Names = [\(names)]")
    }
}

func GetAppBuildNumber() -> String
{
    let dictionary = Bundle.main.infoDictionary!
    let build = dictionary["CFBundleVersion"] as! String
    return "\(build)"
}

func getDeviceLocal() -> String
{
    let locale = Locale.current
    return locale.regionCode ?? ""
}

func isCheckUserLocalChina() -> Bool
{
    //TODO:- change value according to uploading
    return false
}
/// Convert string to attributed string
///
/// - Returns: return attributed string
func attributedString(string1 : String , string2 : String) -> NSAttributedString?
{
    let attributes1 = [
        NSAttributedStringKey.font : MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        ]
    
    let attributes2 = [
        NSAttributedStringKey.font : MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        ]
    
    let attributedString1 = NSAttributedString(string: " \(string1)", attributes: attributes1)
    let attributedString2 = NSAttributedString(string: " \(string2)", attributes: attributes2)
    
    let FormatedString = NSMutableAttributedString()
    
    FormatedString.append(attributedString1)
    FormatedString.append(attributedString2)
 
    ///
    
    return FormatedString
}

func getRandomColor() -> UIColor
{
    let randomRed:CGFloat = CGFloat(drand48())
    
    let randomGreen:CGFloat = CGFloat(drand48())
    
    let randomBlue:CGFloat = CGFloat(drand48())
    
    return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 0.45)
    
}

//MARK: - Toast
func showToast(message:String)
{
    Toast(text: message, delay: 0, duration: 1.5).show()
    
    /*let messageSnack = MDCSnackbarMessage()
    messageSnack.text = message
    MDCSnackbarManager.show(messageSnack)*/
}
func showToastWithDelay(message:String)
{
    Toast(text: message, delay: 0, duration: 5.0).show()
}
func showToastWithCustomDelay(message:String,duration:TimeInterval)
{
    Toast(text: message, delay: 0, duration: duration).show()
}
func APIResponseHandle(statusCode : Int)
{
    switch statusCode
    {
        case 200:
            print("--------------------Check json parsing methods or server response is not in correct format.--------------------")
            break
        case 404:
            showToast(message: R.string.errorMessage.requested_page_not_found_key())
        case 401:
            showToast(message: R.string.errorMessage.unauthorized_request_key())
        default:
            showToast(message: R.string.errorMessage.something_went_wrong_key())
    }
}
//MARK:- Get user detail saved in UserDefaults
/// Get particular user detail saved in UserDefaults
///
/// - Parameter forKey: key in json reponse which value need.
/// - Returns:  string value after finding from response according to forKey
func getUserDetail(_ forKey: String) -> String
{
    guard let userDetail = UserDefaults.standard.value(forKey: R.string.keys.userDetail()) as? Data else { return "" }
    let dictData = JSON(userDetail)
    return dictData[forKey].stringValue
}

func getPhaseDetail(_ forKey: String) -> JSON
{
    guard let userDetail = UserDefaults.standard.value(forKey: R.string.keys.appVersionDetail()) as? Data else { return JSON() }
    let dictData = JSON(userDetail)
    return dictData[forKey]
}

func getVersionDetail(_ forKey: String) -> String
{
    guard let versionDetail = UserDefaults.standard.value(forKey: R.string.keys.kVersionUpdateDetail()) as? Data else { return "" }
    let dictData = JSON(versionDetail)
    return dictData[forKey].stringValue
}

func updateUserDetails(dataDict : JSON)
{
    var dict = dataDict
    dict["session"].stringValue = getUserDetail("session")
    dict["user_id"].stringValue = getUserDetail("user_id")
    dict["password"].stringValue = getUserDetail("password")
    dict["is_first_time"] = JSON(getUserDetail("is_first_time"))
    dict["is_private"].stringValue = getUserDetail("is_private")

    guard let rowdata = try? dict.rawData() else {return}
    
    Defaults.removeObject(forKey: R.string.keys.userDetail())
    Defaults.setValue(rowdata, forKey: R.string.keys.userDetail())
    Defaults.synchronize()
}

//MARK:- Dates
let gregorian_calendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)! as Calendar
let server_dt_format = "yyyyMMddHHmmss"
func DateToString(Formatter:String, date:Date) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.dateFormat = Formatter
    dateformatter.locale = Locale(identifier: "en_US")
    dateformatter.calendar = gregorian_calendar
    let convertedString = dateformatter.string(from: date)
    return convertedString
}
func getCurrentTimeStamp() -> Date
{
    return Date()
}
func StringToConvertedStringDate(strDate:String, strDateFormat:String, strRequiredFormat:String) -> String
{
    let dateformatter = DateFormatter()
    dateformatter.calendar = gregorian_calendar
    dateformatter.locale = Locale(identifier: "en_US")
    dateformatter.dateFormat = strDateFormat
    guard let convertedDate = dateformatter.date(from: strDate) else {
        return ""
    }
    dateformatter.dateFormat = strRequiredFormat
    let convertedString = dateformatter.string(from: convertedDate)
    return convertedString
}
func StringToDate(Formatter : String,strDate : String) -> Date
{
    let dateformatter = DateFormatter()
    dateformatter.calendar = gregorian_calendar
    dateformatter.locale = Locale(identifier: "en_US")
    dateformatter.dateFormat = Formatter
    guard let convertedDate = dateformatter.date(from: strDate) else {
        let str = dateformatter.string(from: Date())
        return dateformatter.date(from: str)!
    }
    return convertedDate
}
func getCurrentTimeZone() -> String
{
    return TimeZone.current.identifier
}

//MARK: -
func jsonToString(json: JSON) -> String
{
    do {
        
        let strFinalTime = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!

//        let data1 =  try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted) // first of all convert json to the data
//        let convertedString = String(data: data1, encoding: String.Encoding.utf8) // the data will be converted to the string
        print("strFinalTime  - ",strFinalTime) // <-- here is ur string
        return strFinalTime
        
    } catch let myJSONError {
        print("myJSONError - ",myJSONError)
        return ""
    }
    
}
//MARK:-
func isCheckAppVersion() -> Int
{
    if(Defaults.value(forKey: "appVersion") != nil)
    {
        return Defaults.value(forKey: "appVersion") as? Int ?? 1
    }
    return 1
}
func isGetLinkType() -> Int
{
    if(Defaults.value(forKey: "link_type") != nil)
    {
        return Defaults.value(forKey: "link_type") as? Int ?? 0
    }
    return 0
}
func isPrivateUser() -> Int
{
    if(getUserDetail("is_private") == "1")
    {
        return 1
    }
    return 0
}
//MARK: - Get Device contact

/*var contacts: [CNContact] = {
    let contactStore = CNContactStore()
    let keysToFetch = [
        CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
        CNContactPhoneNumbersKey,
        CNPostalAddressCityKey,
        CNContactEmailAddressesKey,
        CNContactImageDataKey,
        CNPostalAddressStateKey,
        CNPostalAddressCountryKey,
        CNContactImageDataAvailableKey,
        CNContactThumbnailImageDataKey
        ] as [Any]
    
    // Get all the containers
    var allContainers: [CNContainer] = []
    do {
        allContainers = try contactStore.containers(matching: nil)
    } catch {
        print("Error fetching containers")
    }
    
    var results: [CNContact] = []
    
    // Iterate all containers and append their contacts to our results array
    for container in allContainers {
        let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
        
        do {
            let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
            results.append(contentsOf: containerResults)
        } catch {
            print("Error fetching results for container")
        }
    }
    
    return results
}()
*/

//MARK:- service


//MARK: - Database
func executeQuery(strQuery : String) -> Bool
{
    myDal.executeBeginTransaction()
    print("strQuery - ",strQuery)
    let isSuccess = myDal.executeScalar(strQuery)
    myDal.executeCommiteTrasaction()
    return isSuccess
}
func executeSelectQuery(strQuery : String) -> NSMutableArray
{
    myDal.executeBeginTransaction()
    //print("strQuery - ",strQuery)
    let arrData:NSMutableArray = myDal.executeArraySet(strQuery)
    myDal.executeCommiteTrasaction()
    return arrData
}

//MARK: -
func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

func getAttributedString(strPrefixText:String,imgAttachment:UIImage,strPostText:String) -> NSAttributedString
{
    let fullString = NSMutableAttributedString(string: strPrefixText)
    let image1Attachment = NSTextAttachment()
    image1Attachment.image = imgAttachment
    let image1String = NSAttributedString(attachment: image1Attachment)
    fullString.append(image1String)
    fullString.append(NSAttributedString(string: strPostText))
    return fullString
}


func getThumbnailImage(url: URL) -> UIImage? {
    let asset = AVAsset(url: url)
    let assetImgGenerate = AVAssetImageGenerator(asset: asset)
    assetImgGenerate.appliesPreferredTrackTransform = true
    //Can set this to improve performance if target size is known before hand
    //assetImgGenerate.maximumSize = CGSize(width,height)
    let time = CMTimeMakeWithSeconds(1.0, 600)
    do {
        let img = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        let thumbnail = UIImage(cgImage: img)
        return thumbnail
    } catch {
        print(error.localizedDescription)
        return nil
    }
}

//MARK: String to encrpyData for Base32
func encryptString(string: String) -> String {
    
    var keyConverter256 = String()
    var ivConverter256 = String()
    
    var key = ""
    var iv = ""
    if Defaults.value(forKey: "secretKey") as? String == nil || Defaults.value(forKey: "secretivKey") as? String == nil{
        key = ""
        iv = ""
    }else{
        if let keyValue = Defaults.value(forKey: "secretKey") as? String,let ivValue = Defaults.value(forKey: "secretivKey") as? String{
            key = keyValue
            iv = ivValue
        }
        
    }
    
    if let keySha256 = key.hashed(.sha256) {
        print("keySha256: \(keySha256)")
        keyConverter256 = String(keySha256.prefix(32))
        print("keyConverter256: \(keyConverter256)")
    }
    
    if let ivSha256 = iv.hashed(.sha256) {
        print("ivSha256: \(ivSha256)")
        ivConverter256 = String(ivSha256.prefix(16))
        print("ivConverter256: \(ivConverter256)")
    }
    
    let encrypt = try! string.aesEncrypt(key: keyConverter256, iv: ivConverter256)
    print("encrypt:", encrypt)
    //"abcd1234".data(using: .utf8)?.base64EncodedString()
    let base64 = encrypt.data(using: .utf8)?.base64EncodedString()
    print("Base64:", base64 ?? "")
    
    return base64 ?? ""
}

func decreptedString(string: String) -> String {
    
    if string == ""{
        return ""
    }
    
    var keyConverter256 = String()
    var ivConverter256 = String()
    
     var key = ""
     var iv = ""
    
    if Defaults.value(forKey: "secretKey") as? String == nil || Defaults.value(forKey: "secretivKey") as? String == nil{
        key = ""
        iv = ""
    }else{
        if let keyValue = Defaults.value(forKey: "secretKey") as? String,let ivValue = Defaults.value(forKey: "secretivKey") as? String{
            key = keyValue
            iv = ivValue
        }
    }
    
    if let keySha256 = key.hashed(.sha256) {
        keyConverter256 = String(keySha256.prefix(32))
    }
    
    if let ivSha256 = iv.hashed(.sha256) {
        ivConverter256 = String(ivSha256.prefix(16))
    }
    
    let decodedData = Data(base64Encoded: string)!
    let decodedString = String(data: decodedData, encoding: .utf8)!
    
    let decrept = try! decodedString.aesDecrypt(key: keyConverter256, iv: ivConverter256)
    
    return decrept
}
