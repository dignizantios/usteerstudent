//
//  CustomView.swift
//  Pocket
//
//  Created by Usteer  on 8/8/17.
//  Copyright © 2017 Usteer. All rights reserved.
//

import UIKit

@IBDesignable class CustomView: UIView
{
    var isAutoHeight = false
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
    
    @IBInspectable
    var bgColor: UIColor? {
        get {
            return self.backgroundColor
        }
        set {
            if let color = newValue {
                self.backgroundColor = color
            } else {
                self.backgroundColor = UIColor.white
            }
        }
    }
    
    @IBInspectable
    var autoHeightRoundCorner: Bool {
        get {
            return isAutoHeight
        }
        set {
            isAutoHeight = newValue
        }
    }
    
    override func layoutSubviews()
    {
        if isAutoHeight == true {
            self.layer.cornerRadius = self.frame.height / 2.0
        }
    }
}
