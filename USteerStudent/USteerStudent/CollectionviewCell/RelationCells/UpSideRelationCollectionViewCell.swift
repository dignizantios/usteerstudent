//
//  UpSideRelationCollectionViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/10/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class UpSideRelationCollectionViewCell: UICollectionViewCell {

    @IBOutlet var imgvwCurve : UIImageView!
    @IBOutlet var btnRelationName : UIButton!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnRelationName.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15)
    }

    override func layoutSubviews() {
     
        self.clipsToBounds = false
        self.contentView.clipsToBounds = false
        imgvwCurve.clipsToBounds = false

    }
}
