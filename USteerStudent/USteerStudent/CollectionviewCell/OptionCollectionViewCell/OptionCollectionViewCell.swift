//
//  OptionCollectionViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 9/24/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class OptionCollectionViewCell: UICollectionViewCell {

    
    //MARK: - Outlets
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        lblOption.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblOption.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        
    }

}
