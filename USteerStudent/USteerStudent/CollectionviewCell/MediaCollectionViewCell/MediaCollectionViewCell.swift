//
//  MediaCollectionViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 9/25/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class MediaCollectionViewCell: UICollectionViewCell {

    //MARK: - Outlets
    @IBOutlet var vwVideoPlayer: WKYTPlayerView!
    
    //MARK: - Variables
    var strStreamingURL = ""
    
    override func awakeFromNib() {
        self.vwVideoPlayer.backgroundColor = UIColor.clear
        
    }

}
