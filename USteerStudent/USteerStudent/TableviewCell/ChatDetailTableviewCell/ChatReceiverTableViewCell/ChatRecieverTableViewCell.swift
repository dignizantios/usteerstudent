//
//  ChatRecieverTableViewCell.swift
//  Networker
//
//  Created by Usteer on 02/06/17.
//  Copyright © 2017 Usteer. All rights reserved.
//

import UIKit

class ChatRecieverTableViewCell: UITableViewCell {

    //MARK: - Outlets

    @IBOutlet var viewMessageBack: UIView!
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var viewSideBack: UIView!
    @IBOutlet var imgvwReciever: UIImageView!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var vwHighlight: UIView!

    //MARK: -
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblMessage.textColor = UIColor.white
        
        lblUsername.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 6)
        lblUsername.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        viewMessageBack.backgroundColor = MySingleton.sharedManager.themeYellowColor
        //viewMessageBack.roundCorners([.topLeft, .bottomRight, .topRight], radius: 10)
        viewSideBack.backgroundColor = MySingleton.sharedManager.themeYellowColor

        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //MARK: - SetUp chat Labels
    
    

}
