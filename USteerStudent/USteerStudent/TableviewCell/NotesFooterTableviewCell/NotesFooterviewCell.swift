//
//  NotesFooterviewCell.swift
//  USteerStudent
//
//  Created by Usteer on 4/11/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
import XCDYouTubeKit
import AVKit

class NotesFooterviewCell: UITableViewCell
{
    //MARK: - Outlets
    @IBOutlet var btnSubmit : UIButton!
    @IBOutlet var lblVideoHeader : UILabel!
    @IBOutlet var imgvwFactorThumbnail : UIImageView!
    @IBOutlet var btnPlayVideo : UIButton!
    var strVideoLink:String!
    @IBOutlet var vwVideoContainer: UIView!
    @IBOutlet var vwPlayer: UIView!
    @IBOutlet var vwHeader: UIView!
    let smallVideoPlayerViewController = AVPlayerViewController()
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        lblVideoHeader.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblVideoHeader.textColor = MySingleton.sharedManager.themeLightGrayColor
        
        //
        btnSubmit.setTitleColor(UIColor.white, for: .normal)
        btnSubmit.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        btnSubmit.layer.cornerRadius = btnSubmit.frame.size.height / 2
        btnSubmit.layer.masksToBounds = true
        btnSubmit.backgroundColor = MySingleton.sharedManager.themeYellowColor
        btnSubmit.setTitle("Got it!", for: .normal)
        
        vwVideoContainer.layer.borderColor = MySingleton.sharedManager.themeLightGrayColor.cgColor
        vwVideoContainer.layer.borderWidth = 2.0
        vwVideoContainer.layer.cornerRadius = 5.0
        
        vwPlayer.isHidden = true
    }

    @IBAction func clk_PlayVideo()
    {
        if vwPlayer.isHidden == false
        {
            smallVideoPlayerViewController.player?.pause()
            vwPlayer.isHidden = true
            return
        }
        vwPlayer.isHidden = false
        self.playVideoInView(strVideoURL:self.strVideoLink.youtubeID ?? "", view: self.vwPlayer)
    }
}

//MARK:- Setup Video UI

extension NotesFooterviewCell
{
    func playVideoInView(strVideoURL:String,view:UIView)
    {
        view.addSubview(smallVideoPlayerViewController.view)
        smallVideoPlayerViewController.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height)
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL) { (video, error) in
            if video != nil
            {
                let streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    self.smallVideoPlayerViewController.player = AVPlayer(url: streamURL)
                }
                self.smallVideoPlayerViewController.player?.play()
            }
        }
    }
}

//MARK: - Player delegate
extension NotesFooterviewCell : WKYTPlayerViewDelegate
{
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState)
    {
        if(state.rawValue == 1)
        {
            self.vwPlayer.isHidden = true
        }
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
}
