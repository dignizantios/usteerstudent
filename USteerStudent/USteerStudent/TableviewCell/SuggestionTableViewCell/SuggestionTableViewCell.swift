//
//  SuggestionTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 7/28/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import DropDown

class SuggestionTableViewCell: DropDownCell {

    @IBOutlet weak var lblEmail : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
//        lblName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
//        lblName.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        lblEmail.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
        lblEmail.textColor = MySingleton.sharedManager.themeLightGrayColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
