//
//  ColorDetailsTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class ColorDetailsTableViewCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet var vwPolarRed : UIView!
    @IBOutlet var vwAmber : UIView!
    @IBOutlet var vwBlue : UIView!
    @IBOutlet var vwGreen : UIView!

    @IBOutlet var lblPolarRed : UILabel!
    @IBOutlet var lblAmber : UILabel!
    @IBOutlet var lblBlue : UILabel!
    @IBOutlet var lblGreen : UILabel!
    
    @IBOutlet var lblPolarRedContent : UILabel!
    @IBOutlet var lblAmberContent : UILabel!
    @IBOutlet var lblBlueContent : UILabel!
    @IBOutlet var lblGreenContent : UILabel!

    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        setColors()
        
        [lblPolarRed,lblAmber,lblBlue,lblGreen].forEach({
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.textColor = MySingleton.sharedManager.themeLightGrayColor
            setLabelText(searchText : ($0?.text)!,label : ($0)!,word1:"LOW",word2:"HIGH")

        })
        
        [lblPolarRedContent,lblAmberContent,lblBlueContent,lblGreenContent].forEach({
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.textColor = MySingleton.sharedManager.themeLightGrayColor
            setLabelText(searchText : ($0?.text)!,label : ($0)!,word1:"LOW",word2:"HIGH")
        })
        
        

    }
    func setColors()
    {
        vwPolarRed.backgroundColor = MySingleton.sharedManager.themeRedColor
        vwAmber.backgroundColor = MySingleton.sharedManager.themeYellowColor
        vwBlue.backgroundColor = MySingleton.sharedManager.themeBlueColor
        vwGreen.backgroundColor = MySingleton.sharedManager.themeGreenColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setLabelText(searchText : String,label : UILabel,word1:String,word2:String)
    {
        /* Find the position of the search string. Cast to NSString as we want
         range to be of type NSRange, not Swift's Range<Index> */
        let range1 = (searchText as NSString).range(of: word1)
        let range2 = (searchText as NSString).range(of: word2)

        /* Make the text at the given range bold. Rather than hard-coding a text size,
         Use the text size configured in Interface Builder. */
        let attributedString = NSMutableAttributedString(string: searchText)
        attributedString.addAttribute(NSAttributedStringKey.font, value: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14), range: range1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14), range: range2)

        /* Put the text in a label */
        label.attributedText = attributedString

    }
}
