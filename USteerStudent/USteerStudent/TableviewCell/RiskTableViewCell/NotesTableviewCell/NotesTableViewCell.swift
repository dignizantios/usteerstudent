//
//  NotesTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class NotesTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet var lblHeading : UILabel!
    @IBOutlet var lblContent : UILabel!

    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblHeading.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        lblContent.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblContent.textColor = MySingleton.sharedManager.themeLightGrayColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
