//
//  RiskHeadingTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class RiskHeadingTableViewCell: UITableViewCell {

    //MARK: - outlets
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var btnStatus : UIButton!
    @IBOutlet var btnShowInfo : UIButton!

    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
        
        btnStatus.setTitleColor(UIColor.white, for: .normal)
        btnStatus.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
