//
//  Risk_TableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class Risk_TableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet var btnSelect : UIButton!
    @IBOutlet var lblTitle : UILabel!

    
    //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblTitle.textColor = MySingleton.sharedManager.themeLightGrayColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
