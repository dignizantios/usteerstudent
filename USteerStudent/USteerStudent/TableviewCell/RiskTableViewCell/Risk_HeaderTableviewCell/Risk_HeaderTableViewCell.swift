//
//  Risk_HeaderTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class Risk_HeaderTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet var imgvw : UIImageView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var vwBack : UIView!

     //MARK: -
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblTitle.textColor = MySingleton.sharedManager.themeHeaderTextGrayColor
        
        vwBack.backgroundColor = MySingleton.sharedManager.themeHeaderGrayColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
