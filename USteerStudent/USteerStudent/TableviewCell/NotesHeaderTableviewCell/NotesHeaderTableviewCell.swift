//
//  NotesHeaderTableviewCell.swift
//  USteerStudent
//
//  Created by Usteer on 4/11/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Charts
class NotesHeaderTableviewCell: UITableViewCell
{
    @IBOutlet var chartView : LineChartView!
    @IBOutlet var lblInfo : UILabel!
    @IBOutlet var lblChartHeadingFactor : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
                
        lblChartHeadingFactor.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblChartHeadingFactor.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

