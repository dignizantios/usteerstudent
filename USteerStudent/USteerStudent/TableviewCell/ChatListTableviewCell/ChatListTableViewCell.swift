//
//  ChatListTableViewCell.swift
//  USteerTeacher
//
//  Created by om on 1/2/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import UIKit

class ChatListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblpupilName: UILabel!
    @IBOutlet weak var lblMessageCount: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [lblpupilName,lblMessageCount].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        
        lblMessageCount.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12)
        lblMessageCount.backgroundColor = MySingleton.sharedManager.themeYellowColor
        lblMessageCount.textColor = UIColor.white

    }
    func unreadCell()
    {
        lblpupilName.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)

        lblMessageCount.isHidden = false
    }
    func defaultCell()
    {
        lblpupilName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)

        lblMessageCount.isHidden = true
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
