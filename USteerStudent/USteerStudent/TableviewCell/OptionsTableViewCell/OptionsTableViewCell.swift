//
//  OptionsTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 9/24/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class OptionsTableViewCell: UITableViewCell {

    //MARK: - Outlets
    @IBOutlet weak var lblOption: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    //MARK: -
    override func awakeFromNib() {
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
