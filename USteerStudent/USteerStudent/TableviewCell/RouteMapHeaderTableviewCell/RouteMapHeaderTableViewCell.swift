//
//  RouteMapHeaderTableViewCell.swift
//  USteerStudent
//
//  Created by Usteer on 1/8/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit

class RouteMapHeaderTableViewCell: UITableViewCell {

    //MARK: - Outlet
    @IBOutlet var lblHeading: UILabel!
    @IBOutlet var btnAdd:UIButton!
    @IBOutlet var vwBack:UIView!
    
    //MARK: -
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        vwBack.backgroundColor = MySingleton.sharedManager.themeHeaderGrayColor
        
        lblHeading.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblHeading.textColor = MySingleton.sharedManager.themeHeaderTextGrayColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
