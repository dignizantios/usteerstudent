//
//  RegisterVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/4/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

class RegisterVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var vwNameSeperator: UIView!
    @IBOutlet weak var vwLastNameSeperator: UIView!
    @IBOutlet weak var vwEmailSeperator: UIView!
    @IBOutlet weak var vwBirthdateSeperator: UIView!
    @IBOutlet weak var vwPasswordSeperator: UIView!
    @IBOutlet weak var vwGenderSeperator: UIView!

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblBirthdate: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var lblOther: UILabel!

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtBirthdate: UITextField!
    @IBOutlet weak var txtPassword: UITextField!

    @IBOutlet weak var lblRegisterHeading: UILabel!
    
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    @IBOutlet weak var btnOther: UIButton!

    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnViewPolicy: UIButton!
    @IBOutlet weak var btnPolicy: UIButton!
    
    @IBOutlet weak var collectionViewOptions: UICollectionView!
    @IBOutlet weak var constraintCollectionviewOptionHeight: NSLayoutConstraint!

    @IBOutlet weak var lblIAgree: UILabel!

    @IBOutlet weak var lblOptionHeading1: UILabel!
    @IBOutlet weak var lblOptionHeading2: UILabel!
    
    //MARK: - Varibales
    var strBirthdate = ""
    var dateBirthValue : Date?
    var strGender = ""
    
    /*
     Tutor
     - Parent
     - Partner
     - Student
     - Worker
     - Workplace
     - Coach
    */
    
    var arrayOptions : [JSON] = []
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewOptions.register(UINib(nibName: "OptionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        collectionViewOptions.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLayoutSubviews()
    {
        btnRegister.layer.cornerRadius = btnRegister.bounds.size.height / 2
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        collectionViewOptions.removeObserver(self, forKeyPath: "contentSize")
    }
    //MARK:- Overide Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if object is UICollectionView
        {
            self.constraintCollectionviewOptionHeight.constant = collectionViewOptions.contentSize.height
        }
    }
}
//MARK:- UI Setup
extension RegisterVC
{
    func setUpUI()
    {
        lblRegisterHeading.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 26)
        lblRegisterHeading.text = getCommonString(key: "Register_key").uppercased()
        lblRegisterHeading.textColor = MySingleton.sharedManager.themeYellowColor

        [lblName,lblLastName,lblEmail,lblBirthdate,lblPassword,lblGender].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        [lblMale,lblFemale,lblOther].forEach({
            $0?.textColor = MySingleton.sharedManager.themeLightGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        [lblOptionHeading1,lblOptionHeading2].forEach { (label) in
            label?.textColor = MySingleton.sharedManager.themeYellowColor
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        }
        [vwNameSeperator,vwLastNameSeperator,vwEmailSeperator,vwBirthdateSeperator,vwPasswordSeperator,vwGenderSeperator].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        [txtName,txtLastName,txtEmail,txtBirthdate,txtPassword].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
            $0?.delegate = self
        })
        
        btnRegister.setTitle(getCommonString(key: "Register_key").uppercased(), for: .normal)
        buttonUISetup(btnRegister, textColor: MySingleton.sharedManager.themeDarkGrayColor)

        btnViewPolicy.setTitleColor(MySingleton.sharedManager.themeYellowColor, for: .normal)
        btnViewPolicy.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        btnLogin.setTitleColor(.white, for: .normal)
        btnLogin.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 13)
        
        let strAttributedRegister = attributedString(string1:getCommonString(key:"Already_have_account_key"),string2:getCommonString(key: "Login_key"))
        btnLogin.setAttributedTitle(strAttributedRegister, for: .normal)
        
        btnViewPolicy.setTitle(getCommonString(key: "Privacy_policy_key"), for: .normal)
        
        lblIAgree.text =  getCommonString(key:"I_agree_to_the_key")
        lblIAgree.textColor = MySingleton.sharedManager.themeYellowColor
        lblIAgree.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblName.text = getCommonString(key: "Name_key").uppercased()
        lblLastName.text = getCommonString(key: "Lastname_key").uppercased()
        lblEmail.text = getCommonString(key: "Email_key").uppercased()
        lblBirthdate.text = getCommonString(key: "Birthdate_key").uppercased()
        lblPassword.text = getCommonString(key: "Password_key").uppercased()
        lblGender.text = getCommonString(key: "Gender_key").uppercased()
        lblMale.text = getCommonString(key: "Male_key")
        lblFemale.text = getCommonString(key: "Female_key")
        lblOther.text = getCommonString(key: "Neither_Key")
        
        lblOptionHeading1.text = "In what roles are you downloading USTEER?".uppercased()
        lblOptionHeading2.text = "Tick as many as you want".uppercased()

        getRole()
    }
    
    func setUpData(array:[JSON])
    {
        arrayOptions = array
        for i in 0..<arrayOptions.count
        {
            var dict = arrayOptions[i]
            dict["isSelected"] = "0"
            arrayOptions[i] = dict
        }
        collectionViewOptions.reloadData()
    }
}
//MARK:- UITextfield Setup
extension RegisterVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        txtBirthdate.resignFirstResponder()
        if(textField == txtBirthdate)
        {
            self.view.endEditing(true)
            let obj = MonthYearVC()
            obj.monthYearDelegate = self
            obj.strSelectedYear = String(Int(getCurrentYear())! - 15)
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
}
//MARK: - Pickerview Delegate
extension RegisterVC : SelectedMonthYearDelegate
{
    func SetSelectedMonthYear(strMonth: String, strYear: String,month : Int) {
        strBirthdate = "\(strYear)\(String(format: "%02d", month))"
        self.txtBirthdate.text = "\(String(format: "%02d", month)) - \(strYear)"
    }
}
//MARK: - SetDate Picker Value
extension RegisterVC : datePickerDelegate
{
    func setDateValue(dateValue: Date)
    {
        self.txtBirthdate.text = DateToString(Formatter: "dd-MM-yyyy", date: dateValue)
        strBirthdate = DateToString(Formatter: "yyyyMMdd", date: dateValue)
        dateBirthValue = dateValue
    }
}

//MARK:- Collectionview Methds
extension RegisterVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayOptions.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! OptionCollectionViewCell
        let dict = arrayOptions[indexPath.row]
        cell.lblOption.text = dict["role_name"].stringValue
        if(dict["isSelected"].stringValue == "1")
        {
            cell.btnSelect.isSelected = true
        }else{
            cell.btnSelect.isSelected = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var dict = arrayOptions[indexPath.row]
        if(dict["isSelected"].stringValue == "1")
        {
            dict["isSelected"].stringValue = "0"
        }else{
            dict["isSelected"].stringValue = "1"
        }
        arrayOptions[indexPath.row] = dict
        collectionViewOptions.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellWidth  = (collectionView.frame.size.width)/2
        return CGSize(width: cellWidth , height:35)
    }
}
//MARK:- Button Action Zone
extension RegisterVC
{
    @IBAction func btnLoginAction(_ sender: UIButton)
    {
        hideKeyboardDismiss()
        let loginVC = objStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    @IBAction func btnPolicyAction(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnViewAgreePolicyAction(_ sender: UIButton)
    {
        hideKeyboardDismiss()
        let obj = objStoryboard.instantiateViewController(withIdentifier: "PrivacyPoilcyVC")
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnRegisterAction(_ sender: UIButton)
    {
        var strSelectedRole = [String]()
        var isCheckSelected = false
        
        for i in 0..<arrayOptions.count
        {
            let dict = arrayOptions[i]
            if dict["isSelected"].stringValue == "1"
            {
                isCheckSelected = true
                strSelectedRole.append(dict["role_id"].stringValue)
            }
        }
        hideKeyboardDismiss()
        let objUserLocal = User()
        objUserLocal.strName = txtName.text?.trimmed() ?? ""
        objUserLocal.strLastName = txtLastName.text?.trimmed() ?? ""
        objUserLocal.strUsername = txtEmail.text?.trimmed() ?? ""
        objUserLocal.strEmail = txtEmail.text?.trimmed() ?? ""
        objUserLocal.strBirthdate = strBirthdate.trimmed()
        objUserLocal.strPassword = txtPassword.text?.trimmed().removingWhitespacesAndNewlines ?? ""
        objUserLocal.strConsent = btnPolicy.isSelected ? "y" : "n"
        objUserLocal.birthDateValue = dateBirthValue
        objUserLocal.strGender = strGender
        objUserLocal.strSelectedCar = "\(GlobalVariables.arrayCarsGlobal.count)"
        objUserLocal.strSelectedUserRole = strSelectedRole.joined(separator: ",")
        
        if(!objUserLocal.isValidForRegistration())
        {
            showToast(message: objUserLocal.strValidationMessage)
        }
        else if(!isCheckSelected)
        {
            showToast(message: "Please select at least one role")
        }
        else
        {
            RegisterUserService(objUserData : objUserLocal)
        }
    }
    @IBAction func btnMaleAction(_ sender: UIButton)
    {
        resetSelectedGender()
        btnMale.isSelected = true
        strGender = "m"
    }
    @IBAction func btnFemaleAction(_ sender: UIButton)
    {
        resetSelectedGender()
        btnFemale.isSelected = true
        strGender = "f"
    }
    
    @IBAction func btnOtherAction(_ sender: UIButton)
    {
        resetSelectedGender()
        btnOther.isSelected = true
        strGender = "n"
    }
    
    @IBAction func btnShowHidePassword(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = !sender.isSelected
    }

    func resetSelectedGender()
    {
        [btnMale,btnFemale,btnOther].forEach { (button) in
            button?.isSelected = false
        }
    }
    //MARK: - Other Methods
    /**/
    func  navigateToLoginScreen()
    {
        hideKeyboardDismiss()
        let Message = "We've sent you an email with a verification link. Once you have verified your information, log in with your details below."
        let attributedString = NSMutableAttributedString(string:Message)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: NSRange(location: 0, length: Message.length))
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16), range: NSRange(location: 0, length: Message.length))
        let obj = InfoPopUpVC()
        obj.attributed = attributedString
        obj.colorPopUpContent = UIColor.white
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        obj.popUpDelegate = self
        self.present(obj, animated: false, completion: nil)
    }
}
//MARK:- InfoPopUpDelegate
extension RegisterVC:InfoPopUpDelegate
{
    func dismissPopUp()
    {
        let loginVC = objStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
}


//MARK:- Service call
extension RegisterVC 
{
    func checkEmailExistService(objUser:User)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["username" : encryptString(string: txtEmail.text ?? "")]
            Registration().checkEmailRequest(param: param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func RegisterUserService(objUserData : User)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["name" : encryptString(string: objUserData.strName),
                                           "lastname" : encryptString(string: objUserData.strLastName),
                                           "username" : encryptString(string: objUserData.strUsername),
                                           "password" : encryptString(string: objUserData.strPassword),
                "dob" : encryptString(string: objUserData.strBirthdate),
                "consent" : objUserData.strConsent,
                "timestamp" : DateToString(Formatter: server_dt_format, date: Date()),
                "chart_icon" : objUserData.strSelectedCar,
                "gender" : encryptString(string: objUserData.strGender),
                "device_type" : deviceType,
                "user_role": encryptString(string: objUserData.strSelectedUserRole)
            ]
            print("param - ",param)
            
            Registration().RegisterUser(param:param, userType: "pupil", completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLoginScreen()
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLoginScreen()
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getRole()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            Registration().getUserRole(param: [:], completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        var arr = json["data"].arrayValue
                        for i in 0..<arr.count{
                            var dict = arr[i]
                            dict["role_id"].stringValue = decreptedString(string: dict["role_id"].stringValue)
                            dict["role_name"].stringValue = decreptedString(string: dict["role_name"].stringValue)
                            arr[i] = dict
                        }
                        self.setUpData(array: arr)
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

