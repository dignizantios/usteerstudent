//
//  LearnVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 04/05/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import CarbonKit

class LearnVC: UIViewController
{
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = ["INSIGHT","INFLUENCE","TARGET"]
    var icons = [UIImage.init(named: "ic_Insight"),
                 UIImage.init(named: "ic_Influence"),
                 UIImage.init(named: "ic_Target")]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateTo(_:)), name: NSNotification.Name(rawValue: "LearnNavigate"), object: nil)
        setUpUI()
    }
    
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle : getCommonString(key: "Learn_key"))
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: icons as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self)
        // Disable swipe
        for subview in carbonTabSwipeNavigation.pageViewController.view.subviews {
            if let subView = subview as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
        applyCarbonKitStyle()
    }
    
    @objc func navigateTo(_ notification: Notification)
    {
        let index = ((notification.userInfo?["userInfo"])! as! NSDictionary).value(forKey: "tab") as? Int ?? 0
        carbonTabSwipeNavigation.currentTabIndex = UInt(index)
    }
}

extension LearnVC: CarbonTabSwipeNavigationDelegate
{
    func applyCarbonKitStyle()
    {
        let tabWidth = (UIScreen.main.bounds.size.width / CGFloat(icons.count))
        carbonTabSwipeNavigation.toolbarHeight.constant = 75
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = .white
        
        carbonTabSwipeNavigation.setIndicatorColor(MySingleton.sharedManager.themeYellowColor)
        for index in 0..<icons.count {
            carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: index)
        }
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        
        carbonTabSwipeNavigation.setNormalColor(.black, font: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10))
        carbonTabSwipeNavigation.setSelectedColor(MySingleton.sharedManager.themeYellowColor, font: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10))
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        switch index
        {
        case 0:
            let insightVC = objLearnStoryboard.instantiateViewController(withIdentifier: "InsightVC") as! InsightVC
            return insightVC
        case 1:
            let vc = objLearnStoryboard.instantiateViewController(withIdentifier: "InfluenceVC") as! InfluenceVC
            return vc
        case 2:
            let vc = objLearnStoryboard.instantiateViewController(withIdentifier: "ProgressVC") as! ProgressVC
            vc.isInTab = true
            return vc
        default:
            return UIViewController()
        }
    }
    
    
}
