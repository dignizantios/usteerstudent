//
//  InsightVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 05/05/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import HGCircularSlider

class InsightVC: UIViewController
{
    @IBOutlet var PersonalityCV:UIView!
    @IBOutlet var RelationshipCV:UIView!
    @IBOutlet var leadingSpacePersonalityCV:NSLayoutConstraint!
    @IBOutlet var segmentedControl:TTSegmentedControl!
    @IBOutlet var circularSlider: CircularSlider!

    lazy var objRelationVC: ChooseRelationVC = {
        let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
        obj.selectedVC = .Sorting
        obj.isFromSideMenuSteering = false
        return obj
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.embed(objRelationVC, inView: self.RelationshipCV)
        
        // Segmented Slider
        segmentedControl.layer.cornerRadius = 17.5
        segmentedControl.hasBounceAnimation = true
        segmentedControl.itemTitles = ["Personality","Relationships"]
        segmentedControl.didSelectItemWith = { (index, title) -> () in
            UIView.animate(withDuration: 0.3) {
                self.leadingSpacePersonalityCV.constant = (index == 0) ? 0.0 : -UIScreen.main.bounds.width
                self.view.layoutIfNeeded()
            }
        }
        
        // Filter Slider
        circularSlider.endPointValue = 0
        circularSlider.minimumValue = 0
        circularSlider.maximumValue = 4
        circularSlider.addTarget(self, action: #selector(adjustFilter), for: .editingDidEnd)
    }
    
    @objc func adjustFilter() {
        let selectedHour = round(circularSlider.endPointValue)
        circularSlider.endPointValue = selectedHour
        updateFilter()
    }
    
    @objc func updateFilter() {
        var selectedValue = Int(circularSlider.endPointValue)
        selectedValue = (selectedValue == 0 ? 4 : selectedValue)
        self.sortTreegraph(index: selectedValue)
    }
    
    @IBAction func clk_Sort(btn:UIButton)
    {
        self.sortTreegraph(index: btn.tag)
        circularSlider.endPointValue  = CGFloat((btn.tag == 0 ? 4 : btn.tag))
    }
    
    func sortTreegraph(index:Int)
    {
        switch index
        {
        case 1:
            objRelationVC.selectedFilter = .ByTrustOfOthers
            break
        case 2:
            objRelationVC.selectedFilter = .BySeekingChange
            break
        case 3:
            objRelationVC.selectedFilter = .BySelfDisclosure
            break
        case 4:
            objRelationVC.selectedFilter = .ByTrustOfSelf
            break
        default:
            break
        }
        objRelationVC.getRelationService()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.RelationshipCV.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.RelationshipCV.isHidden = false
    }
}
