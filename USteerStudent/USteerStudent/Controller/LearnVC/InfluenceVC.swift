//
//  InfluenceVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 06/05/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RelationListCell: UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib()
    {
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
    }
}

class InfluenceVC: UIViewController
{
    // 8 Strategies Graph
    @IBOutlet var InfluenceCV:UIView!
    @IBOutlet var leadingContentView: NSLayoutConstraint!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var bottomStackView:UIStackView!
    @IBOutlet weak var progressGroup: RingProgressGroupView!
    @IBOutlet weak var widthProgressGroupView: NSLayoutConstraint!
    @IBOutlet weak var btn_Legend1,btn_Legend2,btn_Legend3,btn_Legend4,btn_Legend5,btn_Legend6,btn_Legend7,btn_Legend8:UIButton!
    @IBOutlet weak var lbl_Legend1,lbl_Legend2,lbl_Legend3,lbl_Legend4,lbl_Legend5,lbl_Legend6,lbl_Legend7,lbl_Legend8:UILabel!

    // Bottom Detail
    @IBOutlet var lblTitle:UILabel!
    @IBOutlet var tblList:UITableView!
    @IBOutlet var txtDetailMessage:UITextView!
    @IBOutlet var lblNoLegendMessage:UILabel!

    var arrList = NSMutableArray()
    var arrRelation_name = NSMutableArray()

    // All Strategies Slider
    @IBOutlet var tapDismissView:UIView!
    @IBOutlet var widthTapDismissView: NSLayoutConstraint!
    @IBOutlet var StrategiesCV:UIView!
    @IBOutlet var strategiesBtnView:UIStackView!
    lazy var objStrategiesListVC: StrategiesListVC = {
        let obj = objLearnStoryboard.instantiateViewController(withIdentifier: "StrategiesListVC") as! StrategiesListVC
        return obj
    }()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.embed(objStrategiesListVC, inView: self.StrategiesCV)
        self.widthProgressGroupView.constant = screenRect.width
        
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
        self.txtDetailMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        self.lblNoLegendMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        self.tapDismissView.addTapGesture {
            UIView.animate(withDuration: 0.3)
            {
                self.leadingContentView.constant = 10
                self.widthTapDismissView.constant = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.showDotView(self, stMessage: "We've analysed your relationships and you use the following influencing strategies")
        self.getStrategiesList()
        
        strategiesBtnView.alpha = 1.0
        strategiesBtnView.startButtonViewBlink()
    }
    
    @objc func hide()
    {
        self.hideDotView()
    }
    
    @IBAction func clk_Strategies()
    {
        UIView.animate(withDuration: 0.3)
        {
            if self.leadingContentView.constant == -(80 - screenRect.size.width)
            {
                self.leadingContentView.constant = 10
                self.widthTapDismissView.constant = 0
            }
            else
            {
                self.leadingContentView.constant = -(80 - screenRect.size.width)
                self.widthTapDismissView.constant = 80
            }
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func clk_Legend(_ btn:UIButton)
    {
        print(btn.titleLabel?.text ?? "")
        let obj = self.arrList[btn.tag-1] as! JSON
        lblTitle.textColor = UIColor.init(hex:obj["colorcode"].string ?? "000000")
        self.getStrategiesDetail(StrategyID: obj["id"].stringValue)
        self.lblNoLegendMessage.isHidden = true
    }
    
    func getGraphValue(val:Double) -> Double
    {
        return (val*75.0)/10000
    }
}

//MARK: - Update Data

extension InfluenceVC
{
    func updateChartData()
    {
        for (index, element) in self.arrList.enumerated()
        {
            let obj = element as! JSON
            let colorCode =  UIColor.init(hex:obj["colorcode"].string ?? "000000")
            let progress = self.getGraphValue(val: obj["percentage"].doubleValue)
            let percentage = "(\(obj["percentage"].intValue)%)"
            
            switch index+1
            {
            case 1:
                [self.lbl_Legend1].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend1].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring1].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 2:
                [self.lbl_Legend2].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend2].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring2].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 3:
                [self.lbl_Legend3].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend3].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring3].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 4:
                [self.lbl_Legend4].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend4].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring4].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 5:
                [self.lbl_Legend5].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend5].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring5].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 6:
                [self.lbl_Legend6].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend6].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring6].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 7:
                [self.lbl_Legend7].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend7].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring7].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            case 8:
                [self.lbl_Legend8].forEach({
                    $0!.textColor = colorCode
                    $0!.text = percentage
                })
                [self.btn_Legend8].forEach({
                    $0!.setTitle(obj["title"].stringValue, for: .normal)
                    $0!.setTitleColor(colorCode, for: .normal)
                })
                UIView.animate(withDuration: 0.2) {
                    [self.progressGroup.ring8].forEach ({
                        $0.progress = progress
                        $0.startColor = colorCode
                        $0.endColor = colorCode
                        $0.isHidden = (progress == 0.0) ? true : false
                    })
                }
                break
            default: break
                
            }
        }
    }
}

//MARK: - Service
extension InfluenceVC
{
    func getStrategiesList()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            let dictParam = ["uid": encryptString(string:getUserDetail("user_id")),
                             "device_id":getFirebaseToken()] as Parameters
            let kURL = kApplicationBaseURL + R.string.webURL.kGetStratigesURL()
            CommonService().PostService(url: kURL, param: dictParam) { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.arrList = NSMutableArray(array: json["data"].array ?? [])
                        self.updateChartData()
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.perform(#selector(self.hide), with: nil, afterDelay: 3.0)
            }
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getStrategiesDetail(StrategyID:String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let dictParam = ["uid": encryptString(string:getUserDetail("user_id")),
                             "device_id":getFirebaseToken(),
                             "strategies_id":encryptString(string:StrategyID)] as Parameters
            let kURL = kApplicationBaseURL + R.string.webURL.kGetStratigyDetailURL()
            CommonService().PostService(url: kURL, param: dictParam) { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        let att_strategies = NSMutableAttributedString(attributedString: data["strategies"].stringValue.htmlToAttributedString!)
                        att_strategies.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 13), range: NSRange(location: 0, length: att_strategies.string.length))
                        att_strategies.addAttribute(NSAttributedStringKey.foregroundColor, value:UIColor.init(hex: "F5C243"), range: NSRange(location: 0, length: att_strategies.string.length))
                        self.txtDetailMessage.attributedText = att_strategies
                        self.lblTitle.text = data["title"].stringValue
                        self.arrRelation_name = NSMutableArray(array: data["relation_name"].array ?? [])
                        self.bottomStackView.isHidden = false
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.tblList.reloadData()
                self.hideLoader()
            }
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}


//MARK: - UITableview Delegate
extension InfluenceVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrRelation_name.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:RelationListCell = tableView.dequeueReusableCell(withIdentifier: "RelationListCell") as! RelationListCell
        cell.selectionStyle = .none
        let obj = self.arrRelation_name[indexPath.row] as! JSON
        cell.lblTitle.text = "\(obj["name"].stringValue)"
        cell.lblTitle.textColor = .black
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
