//
//  StrategiesListVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 08/05/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class StrategiesListCell: UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func awakeFromNib()
    {
        self.lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
    }
}

class StrategiesListVC: UIViewController
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblList: UITableView!

    var arrList = NSMutableArray()
    var closureBack: (() -> Void)?

    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.getStrategiesList()
    }
        
    func onBackView(completion: @escaping () -> Void)
    {
        self.closureBack = completion
    }
    
    @IBAction func clk_Back()
    {
        self.closureBack!()
    }
}

//MARK: - UITableview Delegate
extension StrategiesListVC : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:StrategiesListCell = tableView.dequeueReusableCell(withIdentifier: "StrategiesListCell") as! StrategiesListCell
        cell.selectionStyle = .none
        let dict = self.arrList[indexPath.row] as! JSON
        cell.lblTitle.text = dict["title"].stringValue
        let att_strategies = NSMutableAttributedString(attributedString: dict["strategies"].stringValue.htmlToAttributedString!)
        att_strategies.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15), range: NSRange(location: 0, length: att_strategies.string.length))
        cell.lblMessage.attributedText = att_strategies
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: - Service
extension StrategiesListVC
{
    func getStrategiesList()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            let dictParam = ["uid": encryptString(string:getUserDetail("user_id")),
                             "device_id":getFirebaseToken()] as Parameters
            let kURL = kApplicationBaseURL + R.string.webURL.kGetAllStratigesURL()
            CommonService().PostService(url: kURL, param: dictParam) { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.arrList = NSMutableArray(array: json["data"].array ?? [])
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.tblList.reloadData()
                //self.hideLoader()
            }
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
