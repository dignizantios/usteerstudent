//
//  ProgressVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 09/05/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ProgressVC: UIViewController
{
    @IBOutlet var ScoreCV:UIView!
    @IBOutlet var SignpostCV:UIView!
    @IBOutlet var leadingScoreCV:NSLayoutConstraint!
    @IBOutlet var segmentedControl:TTSegmentedControl!
    
    @IBOutlet var lbl_Message:UILabel!
    @IBOutlet var lbl_Total_score:UILabel!
    @IBOutlet var lbl_Leadership_agility:UILabel!
    @IBOutlet var lbl_Emotional_resilience:UILabel!
    @IBOutlet var lbl_Self_efficacy:UILabel!
    @IBOutlet var lbl_Self_evalution:UILabel!

    var data = JSON()
    var isInTab = false

    lazy var objJournyVC: MyJourneyVC = {
        let vc = objStoryboard.instantiateViewController(withIdentifier: "MyJourneyVC") as! MyJourneyVC
        vc.isFromAssessmentAfterTarget = false
        return vc
    }()
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.embed(objJournyVC, inView: self.SignpostCV)

        // Segmented Slider
        segmentedControl.layer.cornerRadius = 17.5
        segmentedControl.hasBounceAnimation = true
        segmentedControl.itemTitles = ["Scores","Signposts"]
        segmentedControl.didSelectItemWith = { (index, title) -> () in
            UIView.animate(withDuration: 0.3) {
                self.leadingScoreCV.constant = (index == 0) ? 0.0 : -UIScreen.main.bounds.width
                self.view.layoutIfNeeded()
            }
        }
        
        if(isInTab != true)
        {
            setUpNavigationBarWithTitleAndBack(strTitle : "Progress")
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.lbl_Total_score.text = "0"
        self.lbl_Leadership_agility.text = "0"
        self.lbl_Emotional_resilience.text = "0"
        self.lbl_Self_efficacy.text = "0"
        self.lbl_Self_evalution.text = "0"
        
        self.showDotView(self, stMessage: "Your current USTEER metaskills score is...")
        self.getScoreList()
    }
    
    @objc func hide()
    {
        self.hideDotView()
        self.loadDataWithAnimation()
    }
    
    func loadDataWithAnimation()
    {
        let total_score = data["total_score"]
        self.lbl_Total_score.countAnimation(upto: Double(total_score["value"].intValue))
        
        let leadership_agility = data["leadership_agility"]
        self.lbl_Leadership_agility.countAnimation(upto: Double(leadership_agility["value"].intValue))
        
        let emotional_resilience = data["emotional_resilience"]
        self.lbl_Emotional_resilience.countAnimation(upto: Double(emotional_resilience["value"].intValue))
        
        let self_efficacy = data["self_efficacy"]
        self.lbl_Self_efficacy.countAnimation(upto: Double(self_efficacy["value"].intValue))
        
        let self_evalution = data["self_evalution"]
        self.lbl_Self_evalution.countAnimation(upto: Double(self_evalution["value"].intValue))
    }
    
    @IBAction func clk_leadership_agility()
    {
        let leadership_agility = data["leadership_agility"]
        self.updateMessageTitle(message: leadership_agility["message"].stringValue)
    }
    
    @IBAction func clk_emotional_resilience()
    {
        let emotional_resilience = data["emotional_resilience"]
        self.updateMessageTitle(message: emotional_resilience["message"].stringValue)
    }
    
    @IBAction func clk_self_efficacy()
    {
        let self_efficacy = data["self_efficacy"]
        self.updateMessageTitle(message: self_efficacy["message"].stringValue)
    }
    
    @IBAction func clk_self_evalution()
    {
        let self_evalution = data["self_evalution"]
        self.updateMessageTitle(message: self_evalution["message"].stringValue)
    }
    
    @IBAction func clk_Center()
    {
        let total_score = data["total_score"]
        self.updateMessageTitle(message: total_score["message"].stringValue)
    }
    
    func updateMessageTitle(message:String)
    {
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        let att_strategies = NSMutableAttributedString(attributedString: message.htmlToAttributedString!)
        att_strategies.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 13), range: NSRange(location: 0, length: att_strategies.string.length))
        att_strategies.addAttribute(NSAttributedStringKey.foregroundColor, value:MySingleton.sharedManager.themeYellowColor, range: NSRange(location: 0, length: att_strategies.string.length))
        att_strategies.addAttribute(NSAttributedStringKey.paragraphStyle, value:style, range: NSRange(location: 0, length: att_strategies.string.length))
        lbl_Message.attributedText = att_strategies
    }
}

//MARK: - Service
extension ProgressVC
{
    func getScoreList()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            let dictParam = ["uid": encryptString(string:getUserDetail("user_id")),
                             "device_id":getFirebaseToken(),
                             "timezone":getCurrentTimeZone()] as Parameters
            let kURL = kApplicationBaseURL + R.string.webURL.kGetScoreURL()
            CommonService().PostService(url: kURL, param: dictParam) { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.data = json["data"]
                        let total_score = self.data["total_score"]
                        self.updateMessageTitle(message: total_score["message"].stringValue)
                        self.perform(#selector(self.hide), with: nil, afterDelay: 2.0)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            }
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
