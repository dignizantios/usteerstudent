//
//  LinkAccountVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/9/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
import ZendeskSDK
import ZendeskCoreSDK

enum selectedLinkAs
{
    case Student
    case Teacher
    case Parent
    case TP
    case Workplace
    case College
}

class LinkAccountVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet weak var btnLinkAccount: UIButton!
    @IBOutlet weak var vwPasswordSeperator: UIView!
    @IBOutlet weak var vwUsernameSeperator: UIView!
    @IBOutlet weak var vwSchoolCodeSeperator: UIView!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblSchoolCode: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtSchoolCode: UITextField!

    @IBOutlet weak var vwUserNameHolder: UIView!
    @IBOutlet weak var vwPasswordHolder: UIView!
    @IBOutlet weak var vwSchoolCodeHolder: UIView!
    
    @IBOutlet weak var vwSuccessCodeAlert: UIView!
    @IBOutlet weak var lblSuccessCodeMessage: UILabel!

    //MAARK: - Variables
    var objUserData = User()
    var linkAs: selectedLinkAs!
    var isAccountLinked = false
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews()
    {
        [btnLinkAccount].forEach({
            $0?.layer.cornerRadius = ($0?.bounds.size.height)! / 2
            $0?.layer.masksToBounds = true
        })
    }
}
//MARK:- UI Setup
extension LinkAccountVC
{
    func setUpUI()
    {
        [lblPassword,lblUserName,lblSchoolCode].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        
        [vwPasswordSeperator,vwUsernameSeperator,vwSchoolCodeSeperator].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        
        [txtPassword,txtUsername,txtSchoolCode].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
            $0?.delegate = self
        })
        
        [vwSchoolCodeHolder,vwUserNameHolder,vwPasswordHolder].forEach({
            $0?.isHidden = true
        })
        
        btnLinkAccount.setTitle(getCommonString(key: "Link_key").uppercased(), for: .normal)
        buttonUISetup(btnLinkAccount, textColor: MySingleton.sharedManager.themeDarkGrayColor)

        btnLinkAccount.layoutIfNeeded()
        self.view.layoutIfNeeded()
        //
        lblPassword.text = getCommonString(key: "Password_key").uppercased()
        lblUserName.text = getCommonString(key: "Username_key").uppercased()
                
        switch linkAs
        {
        case .Student:
            setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Link_school_account_key").capitalized)
            vwPasswordHolder.isHidden = false
            vwUserNameHolder.isHidden = false
            let strMessage = "To link you to your school or university we need you to insert the ID/password you have been given. If you are at an AS Tracking school then this ID will be your AS Tracking username/ password. YOU NEED TO COMPLETE THIS LINK STEP EVEN IF YOU USED THIS ID TO REGISTER FOR USTEER IN THE FIRST PLACE."
            presentInfoWindow(strMessage : strMessage)
            break
        case .Teacher:
            setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Link_school_account_key").capitalized)
            vwPasswordHolder.isHidden = false
            vwUserNameHolder.isHidden = false
            let strMessage = "Please insert your school email address and AS Tracking password."
            presentInfoWindow(strMessage : strMessage)
            break
        case .Parent:
            setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Link_school_account_key").capitalized)
            vwSchoolCodeHolder.isHidden = false
            let strMessage = "Enter the code sent to you by your child’s school"
            presentInfoWindow(strMessage : strMessage)
            break
        default:break
        }
        vwSuccessCodeAlert.isHidden = true
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwSuccessCodeAlert.addGestureRecognizer(tapOptions)
    }
    
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        self.vwSuccessCodeAlert.fadeOut()
        self.view.endEditing(true)
    }
    
    func presentInfoWindow(strMessage : String)
    {
        let obj = InfoPopUpVC()
        obj.popUpDelegate = self
        obj.strMessage = strMessage
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
    
    func navigateToHome()
    {
        isLinkSchoolAccount = true
        let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
        obj.selectedVC = .LinkSchool
        navigateToController(obj : obj)
    }
    func navigateToUserGuidance()
    {
        isLinkSchoolAccount = true
        let obj = objStoryboard.instantiateViewController(withIdentifier: "UserGuidanceVC") as! UserGuidanceVC
        navigateToController(obj : obj)
    }
    func navigateToController(obj : UIViewController)
    {
        selectedMenuIndex = 1
        let rearNavigation = UINavigationController(rootViewController: obj)
        rearNavigation.isNavigationBarHidden = false
        self.revealViewController().setFront(rearNavigation, animated: true)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadSideMenuTable"), object: nil)
    }
}
//MARK: - Pop up delegate
extension LinkAccountVC : InfoPopUpDelegate
{
    func dismissPopUp()
    {
        if(isAccountLinked)
        {
            navigateToUserGuidance()
        }
    }
}
//MARK:- UITextfield Setup
extension LinkAccountVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
//MARK:- Button Actions
extension LinkAccountVC
{
    @IBAction func btnSubmitAction(_ sender : UIButton)
    {
        self.view.endEditing(true)
        switch linkAs
        {
        case .Student, .Teacher:
            if(txtUsername.text?.length == 0 || txtPassword.text?.length == 0)
            {
                if(txtUsername.text?.length == 0)
                {
                    showToast(message: R.string.validationMessage.enter_username_key())
                }
                else
                {
                    showToast(message: R.string.validationMessage.enter_password_key())
                }
                return
            }
            LinkSchoolAccount()
            break
        case .Parent:
            if(txtSchoolCode.text?.length == 0)
            {
                showToast(message: "Please enter code.")
                return
            }
            LinkCodeAccount()
            break
        default: break
        }
    }
    
    @IBAction func btnShowHidePassword(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        txtPassword.isSecureTextEntry = !sender.isSelected
    }
}
//MARK: - Other methods
extension LinkAccountVC
{
    func navigateToLaunch()
    {
        self.navigationController?.isNavigationBarHidden = true
        let frontViewController = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
        let rearViewController = objStoryboard.instantiateViewController(withIdentifier: "SideMenuVC")
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        revealController?.navigationController?.isNavigationBarHidden = true
        revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width)*0.75
        revealController?.delegate = self
        self.navigationController?.pushViewController(revealController!, animated: true)
    }
}

//MARK:- Service
extension LinkAccountVC 
{
    func LinkSchoolAccount()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            var param : [String:String] = ["timestamp" : DateToString(Formatter: server_dt_format, date: Date()),
                                           "username" : txtUsername.text ?? "",
                                           "password" : txtPassword.text ?? "",
                                           "link_type" : "sc"]
            switch linkAs
            {
            case .Student:
                param["user_type"] = "p"
                break
            case .Teacher:
                param["user_type"] = "t"
                break
            default: break
            }
            
            print("param - ",param)
            LinkSchoolService().LinkSchoolAccount(param:param, userType: "pupil", completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.txtUsername.text = ""
                        self.txtPassword.text = ""
                        Defaults.setValue(true, forKey: "isShowTutorFirstTime")
                        let message = "You're now linked up! Remember to use your REGISTRATION ID (\(getUserDetail("username")) when you log in to USTEER future - your link ID won't be needed again"
                        self.presentInfoWindow(strMessage:message)
                        self.isAccountLinked = true
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func LinkCodeAccount()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["timestamp" : DateToString(Formatter: server_dt_format, date: Date()),
                                           "code" : txtSchoolCode.text ?? ""]
            print("param - ",param)
            LinkSchoolService().LinkAccountWithCode(param:param, completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.txtSchoolCode.text = ""
                        //Show success custome popup
                        let name = "Thank you \(getUserDetail("fullname"))"
                        let freeText = "We’ve applied 6 months free use to your USTEER account from today"
                        let Message = "\(name) \n\nAs a benefit for parents with children at  schools who use STEER, we are giving you free use of the USTEER app for the next 6 months\n\n\(freeText)"
                        let attributedString = NSMutableAttributedString(string:Message)
                        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: NSRange(location: 0, length: Message.length))
                        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15), range: NSRange(location: 0, length: Message.length))
                        var range = (Message as NSString).range(of:name)
                        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: MySingleton.sharedManager.themeYellowColor , range: range)
                        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
                        range = (Message as NSString).range(of:freeText)
                        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
                        self.lblSuccessCodeMessage.attributedText = attributedString
                        self.vwSuccessCodeAlert.fadeIn()
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        self.showResponseMessage(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func showResponseMessage(message:String)
    {
        let name = "Hi \(getUserDetail("fullname"))"
        let Message = "\(name) \n\n\(message)"
        let attributedString = NSMutableAttributedString(string:Message)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: NSRange(location: 0, length: Message.length))
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15), range: NSRange(location: 0, length: Message.length))
        let range = (Message as NSString).range(of:name)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: MySingleton.sharedManager.themeYellowColor , range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
        let obj = InfoPopUpVC()
        obj.attributed = attributedString
        obj.colorPopUpContent = UIColor.white
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
}
