//
//  LinkZendeskViewController.swift
//  USteerStudent
//
//  Created by HARSHIT on 27/04/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import ZendeskSDK
import ZendeskCoreSDK

class LinkZendeskViewController: UIViewController
{
    //MARK: - Outlets
    @IBOutlet weak var vwSeperator1: UIView!
    @IBOutlet weak var vwSeperator2: UIView!
    @IBOutlet weak var vwSeperator3: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblHowHelp: UILabel!
    
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtQuery: UITextView!
    
    @IBOutlet weak var btnSend: UIButton!
    
    var linkAs: selectedLinkAs!
    
    let provider = ZDKRequestProvider()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        Zendesk.instance?.setIdentity(Identity.createAnonymous(name: getUserDetail("fullname"), email: getUserDetail("username")))
        txtName.text = getUserDetail("fullname")
        txtEmail.text = getUserDetail("username")
        setUpNavigationBarWithTitleAndBack(strTitle: "Leave us a message".capitalized)
        self.setUpUI()
    }

    func setUpUI()
    {
        [lblName,lblEmail,lblHowHelp].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        [vwSeperator1,vwSeperator2,vwSeperator3].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        [txtName,txtEmail].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
        })
        txtQuery.textColor = MySingleton.sharedManager.themeDarkGrayColor
        txtQuery.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        btnSend.setTitle("Send".uppercased(), for: .normal)
        buttonUISetup(btnSend, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        btnSend.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }

    @IBAction func clk_Send()
    {
        if(txtName.text == "" || txtEmail.text == "")
        {
            if(txtName.text == "")
            {
                showToast(message: "Please enter yout full name.")
            }
            else
            {
                showToast(message: "Please enter your email address.")
            }
            return
        }
        let request = ZDKCreateRequest()
        request.subject = "New inquiry from link account."
        switch linkAs
        {
        case .College:
            request.requestDescription = "FromCollege"
            break
        case .TP:
            request.requestDescription = "FromTrainingProvider"
            break
        case .Workplace:
            request.requestDescription = "FromWorkplace"
            break
        default: break
        }
        let customFieldOne = CustomField(fieldId: 360030654952, value: txtName.text)
        let customFieldTwo = CustomField(fieldId: 360030741591, value: txtEmail.text)
        let customFieldThree = CustomField(fieldId: 360030654972, value: txtQuery.text)
        request.customFields = [customFieldOne, customFieldTwo, customFieldThree]
        showLoader()
        provider.createRequest(request) { result, error in
            if (error != nil)
            {
                print(error?.localizedDescription ?? "")
            }
            else
            {
                print(result as! ZDKDispatcherResponse)
                let response = result as! ZDKDispatcherResponse
                if response.response.statusCode == 201
                {
                    let str = String(decoding: response.data, as: UTF8.self)
                    print(str)
                    let alert = UIAlertController(title: "", message:"Thanks for your enquiry which we have received. We'll be in touch with you shortly.", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title:"OK", style: .default) { (_) -> Void in
                        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountOrgViewController") as! LinkAccountOrgViewController
                        let rearNavigation = UINavigationController(rootViewController: obj)
                        rearNavigation.isNavigationBarHidden = false
                        self.revealViewController().setFront(rearNavigation, animated: true)
                    })
                    self.present(alert, animated: true)
                }
                else
                {
                    let str = String(decoding: response.data, as: UTF8.self)
                    print(str)
                }
                // Handle the success
            }
            self.hideLoader()
        }
    }
}
