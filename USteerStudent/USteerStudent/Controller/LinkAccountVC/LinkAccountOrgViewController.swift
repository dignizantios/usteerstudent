//
//  LinkAccountOrgViewController.swift
//  USteerStudent
//
//  Created by HARSHIT on 03/04/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit

class LinkAccountOrgViewController: UIViewController
{
    @IBOutlet weak var btnLinkWorkplace: UIButton!
    @IBOutlet weak var btnLinkTP: UIButton!
    @IBOutlet weak var btnLinkCollege: UIButton!
    @IBOutlet weak var btnLinkSchool: UIButton!
    
    //MARK: - School Link Alert
    @IBOutlet var vwSchoolLink: UIView!
    var objUserData = User()

    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
    }

    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Link_to_organisation_key").capitalized)
        buttonUISetup(btnLinkWorkplace, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        buttonUISetup(btnLinkTP, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        buttonUISetup(btnLinkCollege, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        buttonUISetup(btnLinkSchool, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        vwSchoolLink.isHidden = true
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwSchoolLink.addGestureRecognizer(tapOptions)
    }
    
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        self.vwSchoolLink.fadeOut()
        self.view.endEditing(true)
    }
    
    @IBAction func clk_Workplace()
    {
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkCodeAccountVC") as! LinkCodeAccountVC
        obj.linkAs = .Workplace
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func clk_TP()
    {
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkCodeAccountVC") as! LinkCodeAccountVC
        obj.linkAs = .TP
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func clk_College()
    {
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkCodeAccountVC") as! LinkCodeAccountVC
        obj.linkAs = .College
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func clk_School()
    {
        vwSchoolLink.isHidden = false
        vwSchoolLink.fadeIn()
    }
}

// MARK: School Alert Button Action
extension LinkAccountOrgViewController
{
    @IBAction func clk_Student()
    {
        self.vwSchoolLink.fadeOut()
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountVC") as! LinkAccountVC
        obj.linkAs = .Student
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func clk_Teacher()
    {
        self.vwSchoolLink.fadeOut()
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountVC") as! LinkAccountVC
        obj.linkAs = .Teacher
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func clk_Parent()
    {
        self.vwSchoolLink.fadeOut()
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountVC") as! LinkAccountVC
        obj.linkAs = .Parent
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
