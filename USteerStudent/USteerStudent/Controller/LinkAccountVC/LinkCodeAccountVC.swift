//
//  LinkCodeAccountVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 28/04/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit

class LinkCodeAccountVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet weak var btnLinkAccount: UIButton!
    @IBOutlet weak var vwCodeSeperator: UIView!
    @IBOutlet weak var lblCode: UILabel!
    @IBOutlet weak var txtCode: UITextField!
    
    //MARK: - Alert Outlets
    @IBOutlet weak var vwAlert: UIView!
    @IBOutlet weak var lblButtonTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    //MAARK: - Variables
    var linkAs: selectedLinkAs!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        vwAlert.alpha = 0
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        switch linkAs
        {
        case .College:
            self.updateAlertMessage(Message:"Not got a code?\n\nWe're currently offering discounted USTEER membership for colleges\n\nPlease leave us a message and we’ll get in touch", ButtonTitle: "ENTER THE CODE SENT BY YOUR COLLEGE")
            setUpNavigationBarWithTitleAndBack(strTitle: "Link to My College".capitalized)
            lblCode.text = "College code".uppercased()
            break
        case .Workplace:
            self.updateAlertMessage(Message: "Not got a code?\n\nWe're currently offering discounted USTEER membership for Workplace\n\nPlease leave us a message and we’ll get in touch", ButtonTitle: "ENTER THE CODE SENT BY YOUR WORKPLACE")
            setUpNavigationBarWithTitleAndBack(strTitle: "Link to Workplace".capitalized)
            lblCode.text = "Workplace code".uppercased()
            break
        case .TP:
            self.updateAlertMessage(Message: "Not got a code?\n\nWe're currently offering discounted USTEER membership for training and apprentice providers\n\nPlease leave us a message and we’ll get in touch", ButtonTitle: "ENTER THE CODE SENT BY YOUR PROVIDER")
            setUpNavigationBarWithTitleAndBack(strTitle: "Link to Training Provider".capitalized)
            lblCode.text = "Training Provider code".uppercased()
            break
        case .none,.Student,.Teacher,.Parent: break
        }
    }
    
    func setUpUI()
    {
        lblCode.textColor = MySingleton.sharedManager.themeYellowColor
        lblCode.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        
        vwCodeSeperator.backgroundColor = MySingleton.sharedManager.themeYellowColor
        
        txtCode.textColor = MySingleton.sharedManager.themeDarkGrayColor
        txtCode.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        txtCode.placeholder = getCommonString(key: "Enter_here_key")
        
        btnLinkAccount.setTitle(getCommonString(key: "Link_key").uppercased(), for: .normal)
        buttonUISetup(btnLinkAccount, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        btnLinkAccount.layoutIfNeeded()
        self.view.layoutIfNeeded()
    }
    
    func updateAlertMessage(Message:String, ButtonTitle:String)
    {
        let attributedString = NSMutableAttributedString(string:Message)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: NSRange(location: 0, length: Message.length))
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15), range: NSRange(location: 0, length: Message.length))
        var range = (Message as NSString).range(of: "Please leave us a message and we’ll get in touch")
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
        range = (Message as NSString).range(of: "Not got a code?")
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: MySingleton.sharedManager.themeYellowColor , range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
        lblButtonTitle.text = ButtonTitle
        lblMessage.attributedText = attributedString
        vwAlert.fadeIn()
    }
    
    @IBAction func btnSubmitAction(_ sender : UIButton)
    {
        if(txtCode.text == "" && txtCode.text?.length == 0)
        {
            showToast(message: "Please enter code.")
            return
        }
        switch linkAs
        {
        case .TP:
            break
        case .Workplace:
            break
        case .College:
            break
        case .none,.Student,.Teacher,.Parent:
            break
        }
    }
    
    @IBAction func clk_DismissAlert()
    {
        vwAlert.fadeOut()
    }
    
    @IBAction func clk_NoCode()
    {
        vwAlert.fadeOut()
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkZendeskViewController") as! LinkZendeskViewController
        obj.linkAs = self.linkAs
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}
