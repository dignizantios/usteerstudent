//
//  SelectCarVC.swift
//  USteerStudent
//
//  Created by Usteer on 4/19/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster


class carCell: UICollectionViewCell {
    
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var imgSelected: UIImageView!
    @IBOutlet weak var btnSelect: UIButton!
    
    
    //MARK:- ViewLife Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
}
class SelectCarVC: UIViewController
{

    //MARK: - Outlets
    @IBOutlet var lblInfo : UILabel!
    @IBOutlet var btnSelectCar : UIButton!
    @IBOutlet var collectionvwCars : UICollectionView!

    //MARK: - Variables
    var objUserData = User()
    var arrayCars : [JSON] = []
    var dictProfileData : JSON?
    var handlerChangeCar:() -> Void = { }
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
//MARK: - Setup UI
extension SelectCarVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBackPolicy(strTitle: getCommonString(key: "Chart_icon_key"))
        
        lblInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        btnSelectCar.backgroundColor = MySingleton.sharedManager.themeYellowColor
        btnSelectCar.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        btnSelectCar.setTitleColor(UIColor.white, for: .normal)
        btnSelectCar.layer.cornerRadius = btnSelectCar.frame.size.height / 2
        btnSelectCar.layer.masksToBounds = true
        btnSelectCar.setTitle(getCommonString(key: "Submit_key").uppercased(), for: .normal)
        
//        setUpArray()
        
        getCar()
    }
    func setUpArray()
    {
        let arrayImages = GlobalVariables.arrayCarsGlobal

        /*for i in 0..<arrayImages.count
        {
            var dict = JSON()
            dict["carImage"].stringValue = arrayImages[i]
            dict["isSelected"] = JSON("0")
            dict["selectedImage"].stringValue = "ic_radio_checked"
            dict["unSelectedImage"].stringValue = "ic_radio_unchecked"

            arrayCars.append(dict)
        }*/
        
        for i in 0..<arrayImages.count
        {
            var dict = JSON()
            if(dictProfileData?["chart_icon"].stringValue ?? "0" == "\(i+1)")
            {
                dict["isSelected"] = JSON("1")
            }else{
                dict["isSelected"] = JSON("0")
                
            }
            
            dict["carImage"].stringValue = arrayImages[i]
            dict["selectedImage"].stringValue = "ic_radio_checked"
            dict["unSelectedImage"].stringValue = "ic_radio_unchecked"
            
            arrayCars.append(dict)
        }
        
        collectionvwCars.reloadData()
    }
}
//MARK: - Collectionview methods
extension SelectCarVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayCars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carCell", for: indexPath) as! carCell
        
        let dict = arrayCars[indexPath.row]
        cell.imgCar.image = UIImage(named:dict["carImage"].stringValue)
        if(dict["isSelected"].stringValue == "0")
        {
            cell.imgSelected.image = UIImage(named: dict["unSelectedImage"].stringValue)

        }else{
            cell.imgSelected.image = UIImage(named: dict["selectedImage"].stringValue)
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(btnSelectCarClickAction), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let width = (collectionView.frame.size.width / 2)
//        let height = (collectionView.frame.size.height / 3) - 0.5
        
        return CGSize(width: width, height: width)
    }
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
}
//MARK: - Button Action
extension SelectCarVC
{
    @objc func btnSelectCarClickAction(_ sender : UIButton)
    {
        objUserData.strSelectedCar = "\(sender.tag+1)"
        showToast(message: getCommonString(key: "Good_choice_key"))
        for i in 0..<arrayCars.count
        {
            var dataDict = arrayCars[i]
            if(i == sender.tag)
            {
                dataDict["isSelected"].stringValue = "1"
            }else{
                dataDict["isSelected"].stringValue = "0"
            }
            arrayCars[i] = dataDict
        }
        collectionvwCars.reloadData()
    }
    
    @IBAction func btnSelectCarSubmitAction(_ sender : UIButton)
    {
        if(objUserData.strSelectedCar.isEmpty)
        {
            showToast(message:R.string.validationMessage.chart_icon_key())
        }else{
            changeCar()
//            RegisterUser()
        }
    }
}
//MARK:- Other methods
extension SelectCarVC
{
    func  navigateToLoginScreen()
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ViewController
            {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    
    func naviagteToLinkSchoolAccount()
    {
        let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountOrgViewController") as! LinkAccountOrgViewController
        obj.objUserData = objUserData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func navigateToLaunch()
    {
        self.navigationController?.isNavigationBarHidden = true
        let frontViewController = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
        let rearViewController = objStoryboard.instantiateViewController(withIdentifier: "SideMenuVC")
        
        let frontNavigationController = UINavigationController(rootViewController: frontViewController)
        let rearNavigationController = UINavigationController(rootViewController: rearViewController)
        
        let revealController = SWRevealViewController(rearViewController: rearNavigationController, frontViewController: frontNavigationController)
        
        revealController?.navigationController?.isNavigationBarHidden = true
        revealController?.rearViewRevealWidth = (frontViewController.view.frame.size.width)*0.75
        revealController?.delegate = self
        self.navigationController?.pushViewController(revealController!, animated: true)
    }
}
//MARK: - Service
extension SelectCarVC 
{
    func RegisterUser()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["name" : objUserData.strName,
                                           "username" : objUserData.strUsername,
                                           "password" : objUserData.strPassword,
//                                           "email" : objUserData.strEmail,
                                           "dob" : objUserData.strBirthdate,
                                           "consent" : objUserData.strConsent,
                                           "timestamp" : DateToString(Formatter: server_dt_format, date: Date()),
                                           "chart_icon" : objUserData.strSelectedCar,
                                           "gender" : objUserData.strGender,
                                           "device_type" : deviceType
                                           ]
            print("param - ",param)
            
            Registration().RegisterUser(param:param, userType: "pupil", completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        
                        showToast(message: json["message"].stringValue)
                        
                        /*let dict = json["data"]
                        guard let rowdata = try? dict.rawData() else {return}
                        
                        Defaults.removeObject(forKey: R.string.keys.userDetail())
                        Defaults.setValue(rowdata, forKey: R.string.keys.userDetail())
                        Defaults.setValue(true, forKey: "isFromRegistration")
                        Defaults.synchronize()
                        
                        selectedMenuIndex = 0*/
                        
                        self.navigateToLoginScreen()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func changeCar()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()            
            
            let param : [String:String] = ["chart_icon" : objUserData.strSelectedCar]
            print("param - ",param)
            
            ProfileService().changeCar(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
//                        showToast(message: json["message"].stringValue)
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getCar()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            ProfileService().getCar(completion: {(result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.objUserData.strSelectedCar = json["data"]["chart_icon"].stringValue
                        self.dictProfileData?["chart_icon"].stringValue = json["data"]["chart_icon"].stringValue
                        self.setUpArray()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

