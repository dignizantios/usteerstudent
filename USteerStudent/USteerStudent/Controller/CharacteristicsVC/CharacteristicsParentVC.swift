//
//  CharacteristicsParentVC.swift
//  USteerStudent
//
//  Created by Usteer on 4/30/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import CarbonKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

enum JournyParentVC
{
    case assessment
    case sideMenu
}


class CharacteristicsParentVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var mainView: UIView!
    
    //MARK: - Varibales
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = NSArray()
    var isBackFromAssessment = false
    var isFromAssessmentAfterTarget = false
    var selectedJourneyParentVC = JournyParentVC.sideMenu
    var categorydict = JSON()
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        items = [getCommonString(key: "Me_key").uppercased(),
                 getCommonString(key: "My_journey_key").uppercased()
        ]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: mainView)
        
        setUpUI()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        style()
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - UI Setup
extension CharacteristicsParentVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle : getCommonString(key: "My_journey_me_key").capitalized)
    }
}
//MARK: - Carbon kit
extension CharacteristicsParentVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        let width = UIScreen.main.bounds.size.width
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        
        let tabWidth = (width / CGFloat(items.count))
        let indicatorcolor: UIColor = MySingleton.sharedManager.themeYellowColor
        let color: UIColor = .white
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = color
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        
        
        carbonTabSwipeNavigation.setNormalColor(MySingleton.sharedManager.themeLightGrayColor, font: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12))
        carbonTabSwipeNavigation.setSelectedColor(MySingleton.sharedManager.themeYellowColor, font: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12))
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        
        
        if(Defaults.value(forKey: "FirstTimeLoadCharateristic") == nil)
        {
            Defaults.value(forKey: "FirstTimeLoadCharateristic")
            Defaults.setValue(true, forKey: "FirstTimeLoadCharateristic")
            carbonTabSwipeNavigation.currentTabIndex = 0
        }else{
            
            if(Defaults.value(forKey: "FirstTimeLoadCharateristic") as! Bool == true)
            {
                Defaults.setValue(false, forKey: "FirstTimeLoadCharateristic")
                carbonTabSwipeNavigation.currentTabIndex = 0
            }
            else{
                carbonTabSwipeNavigation.currentTabIndex = 1
            }
        }
        
        if(isFromAssessmentAfterTarget)
        {
            carbonTabSwipeNavigation.currentTabIndex = 1
        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        switch index
        {
            case 0:
                let vc = objStoryboard.instantiateViewController(withIdentifier: "CharacteristicsVC") as! CharacteristicsVC
                return vc
            case 1 :
                let vc = objStoryboard.instantiateViewController(withIdentifier: "MyJourneyVC") as! MyJourneyVC
                vc.isFromAssessmentAfterTarget = isFromAssessmentAfterTarget
                
                if(selectedJourneyParentVC == .assessment)
                {
                    vc.selectedJourneyParent = .assessment
                    vc.categorydict = categorydict
                }
                
//                vc.isBackFromAssessment = isBackFromAssessment
                return vc
            default:
                let vc = objStoryboard.instantiateViewController(withIdentifier: "CharacteristicsVC") as! CharacteristicsVC
                return vc
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt)
    {
//        if(isLoadFirstTime)
//        {
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "animateDots"), object: 1)
//        }else{
//            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "animateDots"), object: 0)
//        }
    }
}
