//
//  CharacteristicsVC.swift
//  USteerStudent
//
//  Created by Usteer on 2/21/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

//MARK: - CharacteristicsHeader
class CharacteristicsHeaderTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblStatement: UILabel!
    
    override func awakeFromNib()
    {
        lblStatement.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblStatement.textColor = MySingleton.sharedManager.themeDarkGrayColor
    }
}

//MARK: - CharacteristicsCell
class CharacteristicsTableViewCell : UITableViewCell
{
    @IBOutlet var vwHeader : UIView!
    @IBOutlet var lblContent : UILabel!
    @IBOutlet var lblHeading : UILabel!

    override func awakeFromNib()
    {
        lblContent.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblContent.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblHeading.textColor = UIColor.black
    }
}
class CharacteristicsVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var tblCharacteristics : UITableView!
    @IBOutlet var vwContent : UIView!
    @IBOutlet var imgvwBubble : UIImageView!

    //MARK: - Variables
    var arrayCharacteristics : [JSON] = []
    var strMessage = ""
    var strOpenStatement = ""
    var strCloseStatement = ""
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getCharacteristics()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblCharacteristics.addSubview(self.refreshControl)
        self.tblCharacteristics.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        getCharacteristics()
    }
}

//MARK: - UI setup
extension CharacteristicsVC
{
    @objc func showContent()
    {
        self.hideDotView()
    }
}

//MARK: - UITableview Delegate
extension CharacteristicsVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayCharacteristics.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.numberOfLines = 5
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        return arrayCharacteristics.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0 || (indexPath.row == arrayCharacteristics.count + 1))
        {
            let cell:CharacteristicsHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "headercell") as! CharacteristicsHeaderTableViewCell
            if(indexPath.row == 0)
            {
                cell.lblStatement.text = self.strOpenStatement
            }else{
                cell.lblStatement.text = self.strCloseStatement
            }
            return cell
        }
        let cell:CharacteristicsTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CharacteristicsTableViewCell
        cell.selectionStyle = .none
        let dict = arrayCharacteristics[indexPath.row-1]
        cell.lblHeading.text = dict["title"].stringValue
        cell.lblContent.text = dict["description"].stringValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: - Service
extension CharacteristicsVC 
{
    func getCharacteristics()
    {
        self.showDotView(self, stMessage: "Considering")
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            ProfileService().getCharacteristics(completion:{ (result) in
    
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.strOpenStatement = decreptedString(string: data["opener"].stringValue)
                        self.strCloseStatement = decreptedString(string: data["closer"].stringValue)
                        var arrayChar = data["charateristic"].arrayValue
                        for i in 0..<arrayChar.count
                        {
                            var dict = arrayChar[i]
                            dict["description"].stringValue = decreptedString(string: dict["description"].stringValue)
                            arrayChar[i] = dict
                        }
                        self.arrayCharacteristics = arrayChar
                        self.imgvwBubble.isHidden = false
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        self.vwContent.isHidden = false
                        self.strMessage = json["message"].stringValue
                        self.imgvwBubble.isHidden = true
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblCharacteristics.reloadData()
                }
                self.perform(#selector(self.showContent), with: nil, afterDelay: 4.0)
            })
        }
        else
        {
            self.hideLoader()
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
