//
//  MyJourneyVC.swift
//  USteerStudent
//
//  Created by Usteer on 4/30/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Toaster

import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

class MyJourneyTableViewCell: UITableViewCell
{
    //MARK: - Outlets
    @IBOutlet weak var vwContainer: UIView!

    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var vwLine: UIView!
    @IBOutlet weak var vwHideBottomLine: UIView!
    @IBOutlet weak var imgvwCar: UIImageView!
    
    @IBOutlet weak var lblRelation: UILabel!
    @IBOutlet weak var lblScore: UILabel!
    @IBOutlet weak var imgvwEmoji: UIImageView!

    @IBOutlet weak var constraintTop: NSLayoutConstraint!
    @IBOutlet weak var constraintIconWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintIconHeight: NSLayoutConstraint!

    override func awakeFromNib()
    {
        [lblMessage,lblScore,lblRelation].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        lblRelation.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 8)
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 8)

        vwLine.backgroundColor = MySingleton.sharedManager.themeYellowColor
    }
    
    func UIForNotes()
    {
        [lblMessage,lblScore,lblRelation].forEach { (label) in
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        vwContainer.backgroundColor = MySingleton.sharedManager.themeChatGrayColor
        vwHideBottomLine.backgroundColor = MySingleton.sharedManager.themeChatGrayColor


    }
    func UIForSignpost()
    {
        [lblMessage,lblScore,lblRelation].forEach { (label) in
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        vwContainer.backgroundColor = UIColor.white
        vwHideBottomLine.backgroundColor = UIColor.white


    }
}
class MyTimeLineTableViewCell: UITableViewCell
{
    //MARK: - Outlets
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var vwContentBack: UIView!
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var vwLine: UIView!

    
    override func awakeFromNib()
    {
        vwLine.backgroundColor = UIColor.white
        vwContentBack.backgroundColor = MySingleton.sharedManager.themeYellowColor
        
        lblTime.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblTime.textColor = UIColor.white
        
    }
}
class MyJourneyVC: UIViewController
{

    //MARK: - Outlets
    @IBOutlet var tblMyJourney : UITableView!

    @IBOutlet var pickerView: AKPickerView!

    @IBOutlet var vwReward: UIView!
    @IBOutlet var vwRewardBack: UIView!

    @IBOutlet var imgvwReward: UIImageView!
    @IBOutlet var lblRewardMessage: UILabel!

    //MARK: - Variables
    var arrayTimeline : [JSON] = []
//    var arrayTime : [String] = ["2018","2017","2016","2015","2014"]
    var arrayMonths : [String] = ["JAN","FEB","MAR","APR","MAY","JUN","JULY","AUG","SEP","OCT","NOV","DEC"]
    var arrayPickerTitles : [Any] = []

    var carIcon = 1
    var strMessage = ""
    var selectedYear = 0
    var isBackFromAssessment = false
    var isFromAssessmentAfterTarget = false
    var selectedJourneyParent = JournyParentVC.sideMenu
    var categorydict = JSON()
    var isInitialLoad = true

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getMyJourney()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tblMyJourney.tableFooterView = UIView()
        self.tblMyJourney.addSubview(self.refreshControl)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadChartData), name: NSNotification.Name(rawValue: "reloadChartData"), object: nil)
        
        setUpUI()
        getMyJourney()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//MARK: - SetupUI
extension MyJourneyVC
{
    func setUpUI()
    {
        //setUpNavigationBarWithTitleAndSideMenu(strTitle: "Characteristics_key")
        
        self.pickerView.isHidden = false
        self.vwRewardBack.isHidden = true

        [vwReward].forEach{
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor.withAlphaComponent(0.95);
        }
        [lblRewardMessage].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        
        let Tap = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwReward.addGestureRecognizer(Tap)
        
        
        let vwRewardBackTap = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwRewardBack.addGestureRecognizer(vwRewardBackTap)
        
        configurePicker()
        
    }
    @objc func hideView()
    {
        vwRewardBack.isHidden = true
    }
    
    func configurePicker()
    {
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
        
        self.pickerView.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 20)
        self.pickerView.highlightedFont = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 20)
        
        self.pickerView.interitemSpacing = 20
        self.pickerView.highlightedTextColor = MySingleton.sharedManager.themeYellowColor
        self.pickerView.pickerViewStyle = .wheel
        
        self.pickerView.maskDisabled = false
        self.pickerView.reloadData()
    }
    @objc func reloadChartData()
    {
        getMyJourney()
    }
}
//MARK: - Geture method
extension MyJourneyVC
{
    
}
//MARK: - UITableview Methods
extension MyJourneyVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        /*if arrayTimeline.count == 0
        {
            return 1
        }
        
        if(tableView == tblMyJourney)
        {
            return arrayMonths.count
        }*/
        return 1
    }
    /*func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if arrayTimeline.count == 0
        {
            return nil
        }
        if(tableView == tblMyJourney)
        {
            let vwHeader = UIView(frame:CGRect(x:0,y:0,width:tableView.frame.size.width,height:25))
            vwHeader.backgroundColor = UIColor.clear
            
            let lblHeader = UILabel(frame:CGRect(x:(vwHeader.frame.size.width/2 - 38),y:0,width:76,height:25))
            lblHeader.text = arrayMonths[section]
            lblHeader.backgroundColor = MySingleton.sharedManager.themeYellowColor
            lblHeader.layer.cornerRadius = lblHeader.frame.size.height / 2
            lblHeader.layer.masksToBounds = true
            lblHeader.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12)
            lblHeader.textColor = UIColor.white
            lblHeader.layer.borderColor = UIColor.white.cgColor
            lblHeader.layer.borderWidth = 1
            lblHeader.textAlignment = .center

            vwHeader.addSubview(lblHeader)
            
            return vwHeader
        }
        
        return nil
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        
        if(tableView == tblMyJourney)
        {
            return 50
        }
        return 0
    }*/
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
            if arrayTimeline.count == 0
            {
                let lbl = UILabel()
                lbl.text = strMessage
                lbl.textAlignment = NSTextAlignment.center
                lbl.numberOfLines = 3
                lbl.textColor = MySingleton.sharedManager.themeYellowColor
                lbl.center = tableView.center
                tableView.backgroundView = lbl
                return 0
            }
            tableView.backgroundView = nil
            return arrayTimeline.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
            let cell:MyJourneyTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MyJourneyTableViewCell
            cell.selectionStyle = .none
            cell.contentView.backgroundColor = UIColor.clear
            cell.backgroundColor = UIColor.clear
            
            if(indexPath.row == 0)
            {
                cell.constraintTop.constant = 30
            }
            else{
                cell.constraintTop.constant = 0
            }
            let dict = arrayTimeline[indexPath.row]

            if(indexPath.row == arrayTimeline.count-1)
            {
                cell.vwHideBottomLine.isHidden = false
            }
            else{
                cell.vwHideBottomLine.isHidden = true
            }
            
            ///
            if(dict["type"].stringValue == "target")
            {
//                cell.lblScore.text =  "Signpost score - " + dict["target_score"].stringValue
                cell.lblMessage.text = dict["targetName"].stringValue.uppercased()
                
                cell.lblScore.addImageToLabel(imageName: #imageLiteral(resourceName: "ic_add_target_logo"), afterLabel: false, strText: "  Signposted score - " + dict["target_score"].stringValue,width:15,height:15)
                cell.UIForSignpost()
            }else{

                cell.lblMessage.text = "NOTE"
//                cell.lblScore.text =  dict["factor"].stringValue + " - " + dict["notes"].stringValue
                
                cell.lblScore.addImageToLabel(imageName: #imageLiteral(resourceName: "ic_notes_small"), afterLabel: false, strText: " " + dict["notes"].stringValue,width:12,height:15)
                cell.UIForNotes()

            }
            cell.lblRelation.text =  dict["rname"].stringValue.uppercased()
            
            if(carIcon > 6)
            {
                cell.constraintIconWidth.constant = 30
                cell.constraintIconHeight.constant = 30
            }else{
                cell.constraintIconWidth.constant = 37
                cell.constraintIconHeight.constant = 20
            }
            
            let tintedImage = getSelectedCar(chartIcon: carIcon).mask(with: getColorForCar(color:dict["icon_color"].stringValue))
            cell.imgvwCar.image = tintedImage
            
            
            if(dict["reward"].intValue == 0)
            {
                cell.imgvwEmoji.isHidden = true
            }else{
                cell.imgvwEmoji.isHidden = false
                cell.imgvwEmoji.image = rewardEmoji(reward: dict["reward"].intValue).mask(with: MySingleton.sharedManager.themeYellowColor)
            }
            
            return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var dict = arrayTimeline[indexPath.row]
        print("dict:\(dict)")
        if(dict["type"].stringValue == "target")
        {
            let obj = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "StatementVC") as! StatementVC
            obj.strTargetId = dict["targetId"].stringValue
            obj.targetFactor = getFactors(str: dict["targetName"].stringValue)
            obj.checkStatementParentVC = .myjourney
       //     dict["other_id"].stringValue = decreptedString(string: dict["other_id"].stringValue)
            obj.dictDetails = dict
            self.navigationController?.pushViewController(obj, animated: true)
        }else{
            let obj = objReportStoryboard.instantiateViewController(withIdentifier: "NotesReportVC") as! NotesReportVC
            
            var dictCategory = JSON()
            dictCategory["rid"].stringValue = dict["rid"].stringValue
            dictCategory["rname"].stringValue = dict["rname"].stringValue
            dictCategory["is_private"].stringValue = dict["is_private"].stringValue
            dictCategory["other_id"].stringValue = dict["other_id"].stringValue
            dictCategory["default_name"].stringValue = dict["rname"].stringValue
            print("dictCategory - ",dictCategory)
            
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            dict["factor"].stringValue = decreptedString(string: dict["factor"].stringValue)
             dict["is_historic"].stringValue = decreptedString(string: dict["is_historic"].stringValue)

            print("dict:\(dict)")
            obj.dictData = dict
            obj.currentScore = dict["score"].stringValue
            obj.selectedFactorValue = getSelectedFactors(str:dict["factor"].stringValue)
            obj.selectedNotesParentVC = .myjourney
            obj.carIcon = carIcon
            obj.categoryDict = dictCategory
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
//MARK: - Picker methods
extension MyJourneyVC : AKPickerViewDataSource, AKPickerViewDelegate
{
    // MARK: - AKPickerViewDataSource
    
    func numberOfItemsInPickerView(_ pickerView: AKPickerView) -> Int {
        return self.arrayPickerTitles.count
    }
    
    func pickerView(_ pickerView: AKPickerView, titleForItem item: Int) -> String {
        return "\(self.arrayPickerTitles[item])"
    }
    
    /*func pickerView(_ pickerView: AKPickerView, imageForItem item: Int) -> UIImage {
        return UIImage(named: self.arrayPickerTitles[item])!
    }*/
    
    // MARK: - AKPickerViewDelegate
    
    func pickerView(_ pickerView: AKPickerView, didSelectItem item: Int) {
        
        selectedYear = self.arrayPickerTitles[item] as! Int
        print("\(self.arrayPickerTitles[item])")
        self.arrayTimeline = []
        getMyJourney()
    }
}
//MARK: - Other methods
extension MyJourneyVC
{
    func setUpEmojiToView(reward:Int,rewardMessage:String)
    {
        imgvwReward.image = rewardEmoji(reward: reward)
//        lblRewardMessage.text = rewardText(reward: reward)
        lblRewardMessage.text = rewardMessage
    }
    func getAttachmentString(image:UIImage,title:String) -> NSAttributedString
    {
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image = image
//        attachment.setImageHeight(height: 20)
        let attachmentString:NSAttributedString = NSAttributedString(attachment: attachment)
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(string:title)
//        attributedString.append(axtractedImageAttribute)
        attributedString.append(attachmentString)
        return attributedString
    }
}
//MARK: - Service
extension MyJourneyVC 
{
    func getMyJourney()
    {
//        self.arrayTimeline = []
//        self.tblMyJourney.reloadData()
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            var rid = ""
            if(selectedJourneyParent == .assessment)
            {
                rid = categorydict["rid"].stringValue
            }
            MyJourney().MyJourneyService(year:selectedYear,isFromAssessmentAfterTarget:isFromAssessmentAfterTarget,rid : rid,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        
                        let data = json["data"]
                        
                        self.carIcon = Int(decreptedString(string: data["chart_icon"].stringValue)) ?? 1
                        var arrayTimeLineLocal = data["timeline_detail"].arrayValue
                        
                        for i in 0..<arrayTimeLineLocal.count{
                            var dict = arrayTimeLineLocal[i]
                            dict["your_score"].stringValue = decreptedString(string: dict["your_score"].stringValue)
                            dict["rname"].stringValue = decreptedString(string: dict["rname"].stringValue)
                            dict["target_score"].stringValue = decreptedString(string: dict["target_score"].stringValue)
                            dict["targetName"].stringValue = decreptedString(string: dict["targetName"].stringValue)
                            dict["notes"].stringValue = decreptedString(string: dict["notes"].stringValue)
                            dict["reward"].stringValue = decreptedString(string: dict["reward"].stringValue)
                            
                            arrayTimeLineLocal[i] = dict
                        }
                        
                        self.arrayTimeline = arrayTimeLineLocal

                        let arrayYears = data["year_list"].arrayObject
//                        print("arrayYears - ",arrayYears)
                        self.arrayPickerTitles = arrayYears!
                        self.pickerView.isHidden = false
                        self.pickerView.reloadData()
                        /*if((arrayYears?.count)! > 0)
                        {
//                            self.pickerView.scrollToItem((arrayYears?.count)! - 1)
                            self.pickerView.selectItem((arrayYears?.count)! - 1, animated: true)

                        }*/
                        
                        if(self.isInitialLoad)
                        {
                            self.isInitialLoad = false
                            self.pickerView.selectItem((arrayYears?.count)! - 1, animated: true)
                        }
                        
//                        if(self.isFromAssessmentAfterTarget)
//                        {
                            if(data["assessment_reward"].intValue > 0)
                            {
                                self.setUpEmojiToView(reward: data["assessment_reward"].intValue,rewardMessage : decreptedString(string: data["reward_message"].stringValue))
                                self.vwRewardBack.isHidden = false
                                
                            }else{
                                self.vwRewardBack.isHidden = true
//                                self.setUpEmojiToView(reward: 1)
                            }
//                        }
//                        self.vwRewardBack.isHidden = false
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        self.strMessage = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    
                    self.tblMyJourney.reloadData()
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
