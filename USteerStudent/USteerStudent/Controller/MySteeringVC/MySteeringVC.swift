//
//  MySteeringVC.swift
//  USteerStudent
//
//  Created by Usteer on 2/15/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class MySteeringVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var mainView: UIView!
    
    //MARK: - Varibales
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = NSArray()
    var categoryDict : JSON?
    var isSelectedFromVC = selectedReportController.ChooseRelationVC
    var isShowLatestButton = false
    var isFromReminder = false
    var dictDetails = JSON()

    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
        
        // Do any additional setup after loading the view.
        items = [getCommonString(key: "See_my_steering_key").uppercased(),
                 getCommonString(key: "My_target_key").uppercased(),
                 getCommonString(key: "Messages_key").uppercased()]
        
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: mainView)
        
        if(isFromReminder)
        {
            navigateToMyTargets()
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        isOnSteering = 1
        NotificationCenter.default.addObserver(self, selector: #selector(self.navigateToMyTargets), name: NSNotification.Name(rawValue: "navigateToMyTargets"), object: nil)

        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        style()
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isOnSteering = 0
    }
    override func viewDidLayoutSubviews() {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
}
//MARK: - setUP Notifications
extension MySteeringVC
{
    @objc func navigateToMyTargets()
    {
        carbonTabSwipeNavigation.currentTabIndex = 1
    }
}
//MARK: - UI Setup
extension MySteeringVC
{
    func setUpUI()
    {
        if(isFromReminder)
        {
            setUpNavigationBarWithTitleAndSideMenu(strTitle: "")
        }
        else
        {
            setUpNavigationBarWithTitleAndBackAndSideMenuLocal(strTitle: "")
        }
    }
    func setUpNavigationBarWithTitleAndBackAndSideMenuLocal(strTitle : String)
    {
        //rearViewRevealWidth
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        // create the button
        let leftImage  = UIImage(named: "ic_menubar_white_header")!.withRenderingMode(.alwaysOriginal)
        let leftButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 20))
        leftButton.setBackgroundImage(leftImage, for: .normal)
        leftButton.addTarget(SWRevealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)), for:.touchUpInside)
        
        let leftBackImage  = UIImage(named: "ic_back_arrow_white_header")!.withRenderingMode(.alwaysOriginal)
        let leftBackButton = UIButton(frame: CGRect(x:0,y: 0,width: 20,height: 20))
        leftBackButton.setBackgroundImage(leftBackImage, for: .normal)
        leftBackButton.addTarget(self, action: #selector(backBtnAction), for:.touchUpInside)
        
        let menuButton = UIBarButtonItem(customView: leftButton)
        let backButton = UIBarButtonItem(customView: leftBackButton)
        
        self.navigationItem.hidesBackButton = false
        self.navigationItem.setLeftBarButtonItems([menuButton,backButton], animated: true)
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.navigationItem.titleView = HeaderLabel
    }
    
    //MARK: - Button Action
    @objc func backBtnAction(_ sender : UIButton)
    {
        switch isSelectedFromVC
        {
            case .QuestionsVC:
                backToRelationTree()
            case .ChooseRelationVC:
                if(categoryDict?["tree_noti"].intValue ?? 0 > 0)
                {
                    backToRelationTree()
                }
                else
                {
                    back()
                }
            case .myjourney:
                back()
            case .myjourneyAssessment:
                navigateToJourney()
        }
    }
    func back()
    {
        self.navigationController?.popViewController(animated: true)
    }
    func backToRelationTree()
    {
        var isFound = false
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ChooseRelationVC {
                currentAssessment = 0
                isFound = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTreeData"), object: nil)
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
        if(!isFound)
        {
            let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            self.revealViewController().setFront(rearNavigation, animated: true)
        }
    }
    func navigateToJourney()
    {
        selectedMenuIndex = 3
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CharacteristicsParentVC") as! CharacteristicsParentVC
        let rearNavigation = UINavigationController(rootViewController: obj)
        rearNavigation.isNavigationBarHidden = false
        self.revealViewController().setFront(rearNavigation, animated: true)
    }
}
//MARK: - Carbon kit
extension MySteeringVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        let tabWidth = (UIScreen.main.bounds.size.width / CGFloat(items.count))
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = .white
        carbonTabSwipeNavigation.setIndicatorColor(MySingleton.sharedManager.themeYellowColor)
        for index in 0..<items.count {
            carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: index)
        }
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(MySingleton.sharedManager.themeLightGrayColor, font: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10))
        carbonTabSwipeNavigation.setSelectedColor(MySingleton.sharedManager.themeYellowColor, font: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10))
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        switch index {
        case 0:
            let vc = objReportStoryboard.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
            vc.categoryDict = self.categoryDict
            vc.dictDetails = dictDetails
            if(isSelectedFromVC == .myjourney)
            {
                vc.selectedParentVC = .myjourney
            }
            else if(isSelectedFromVC == .QuestionsVC)
            {
                vc.selectedParentVC = .question
            }
            else if isSelectedFromVC == .ChooseRelationVC{
                vc.selectedParentVC = .chooseRelation
            }
            if(categoryDict?["tree_noti"].intValue ?? 0 > 0)
            {
                vc.isShowLatestButton = isShowLatestButton
            }
            else
            {
                vc.isShowLatestButton = isShowLatestButton
            }
            return vc
        case 1 :
            let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "TargetVC") as! TargetVC
            vc.categoryDict = self.categoryDict
            return vc
        case 2 :
            let vc = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
            vc.categoryDict = categoryDict!
            vc.checkChatParentVC = .relation
            return vc
        default:
            let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "TargetVC") as! TargetVC
            vc.categoryDict = self.categoryDict
            return vc
        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt)
    {
        if(carbonTabSwipeNavigation.currentTabIndex == 0)
        {
            setNavigationTitle(strHeading : getCommonString(key: "See_my_steering_key").capitalized)
        }
        else if(carbonTabSwipeNavigation.currentTabIndex == 1)
        {
            setNavigationTitle(strHeading : getCommonString(key: "My_target_key").capitalized)
        }
        else{
            setNavigationTitle(strHeading : getCommonString(key: "Messages_key").capitalized)
        }
    }
}
//MARK: - Button Action
extension MySteeringVC
{
    func setNavigationTitle(strHeading : String)
    {
        if(isFromReminder)
        {
            setUpNavigationBarWithTitleAndSideMenu(strTitle: strHeading + " - \(self.categoryDict?["rname"].stringValue ?? "")")
            
        }else{
            setUpNavigationBarWithTitleAndBackAndSideMenuLocal(strTitle: strHeading + " - \(self.categoryDict?["rname"].stringValue ?? "")")
        }
    }
}
