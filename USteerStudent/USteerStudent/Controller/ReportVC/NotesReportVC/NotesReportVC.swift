//
//  NotesReportVC.swift
//  USteerStudent
//
//  Created by Usteer on 4/11/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

import Charts
import AVFoundation

import MediaPlayer
import AVKit
import AudioToolbox
import JTMaterialTransition
import WebKit

enum checkNotesParentVC
{
    case myjourney
    case steering
}

class NotesReportVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var tblNotes : UITableView!
    @IBOutlet var vwFactorDetail : UIView!
    @IBOutlet var vwNoData : UIView!
    private let webviewFactorDetails = WKWebView(frame: .zero)

    //MARK: - Variables
    var selectedNotesParentVC = checkNotesParentVC.steering
    var arrayStatements : [JSON] = []
    var strErrorMessage = ""
    var dictData = JSON()
    var dictPointDetailsData = JSON()
    var categoryDict = JSON()
    var currentScore = ""
    var selectedFactorValue = TargetFactor.SD

    var isLoadFirstTimeScreen = true
    var strNoteText = ""
    var strVideoLink = ""

    var player = AVPlayer()
    var playerController = AVPlayerViewController()
    var carIcon = 1
    var isLoadingVideo = true
    
    var activityIndicator = UIActivityIndicatorView()
    var transitionButton : UIButton?

    var transition: JTMaterialTransition?
        
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tblNotes.register(UINib(nibName: "Risk_TableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblNotes.register(UINib(nibName: "NotesFooterviewCell", bundle: nil), forCellReuseIdentifier: "NotesFooterviewCell")
        tblNotes.register(UINib(nibName: "NotesHeaderTableviewCell", bundle: nil), forCellReuseIdentifier: "NotesHeadercell")
        
        webviewFactorDetails.translatesAutoresizingMaskIntoConstraints = false
        self.vwFactorDetail.addSubview(self.webviewFactorDetails)
        NSLayoutConstraint.activate([
            self.webviewFactorDetails.leftAnchor.constraint(equalTo: self.vwFactorDetail.leftAnchor),
            self.webviewFactorDetails.bottomAnchor.constraint(equalTo: self.vwFactorDetail.bottomAnchor),
            self.webviewFactorDetails.rightAnchor.constraint(equalTo: self.vwFactorDetail.rightAnchor),
            self.webviewFactorDetails.topAnchor.constraint(equalTo: self.vwFactorDetail.topAnchor),
            ])
        self.vwFactorDetail.setNeedsLayout()
        UISetup()
        getRiskStatement()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
    
    override func viewDidLayoutSubviews()
    {
        self.view.layoutIfNeeded()
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
}
//MARK: - UI Set up
extension NotesReportVC
{
    func UISetup()
    {
        setUpNavigationBarWithTitleAndBack(strTitle: "")
        vwFactorDetail.isHidden = true
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
    }
    
    func getFactor() -> String
    {
        return decreptedString(string: dictPointDetailsData["title"].stringValue)
    }
}
//MARK: - Webview methods
extension NotesReportVC
{
    func loadWebView()
    {
        let strURL = dictPointDetailsData["article_detail"].stringValue
        if(!strURL.isEmpty)
        {
            if let url = URL (string: strURL)
            {
                let requestObj = URLRequest(url: url)
                webviewFactorDetails.load(requestObj)
            }
        }
    }
}

//MARK: - Geture method
extension NotesReportVC
{
    /// Tap reconizer on table to hide keyboard
    ///
    /// - Parameter sender: geture reconizer
    @objc func hideView(sender: UITapGestureRecognizer) {
        // handling code
        self.vwFactorDetail.isHidden = true
        self.view.endEditing(true)
    }
}
//MARK: - UITableview Delegate
extension NotesReportVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        tableView.backgroundView = nil
        return arrayStatements.count + 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.row == 0)
        {
            let cell:NotesHeaderTableviewCell = tableView.dequeueReusableCell(withIdentifier: "NotesHeadercell") as! NotesHeaderTableviewCell
            cell.chartView.delegate = self
            configureChart(chartview: cell.chartView)
            cell.lblChartHeadingFactor.text = decreptedString(string: (dictPointDetailsData["title"].stringValue)).uppercased()
            cell.lblInfo.textAlignment = .justified
            let scoreDict = dictPointDetailsData["score_statement"]
            cell.lblInfo.text = decreptedString(string: scoreDict["statement"].stringValue)
            setUpChartData(chart: cell.chartView)
            return cell
        }
        else if(arrayStatements.count + 1 == indexPath.row)
        {
            let cell:NotesFooterviewCell = tableView.dequeueReusableCell(withIdentifier: "NotesFooterviewCell") as! NotesFooterviewCell
            cell.imgvwFactorThumbnail.image = #imageLiteral(resourceName: "Factor_thumbnail")
            cell.strVideoLink = self.strVideoLink
            cell.btnSubmit.addTarget(self, action: #selector(btnDoneClickAction), for: .touchUpInside)
            cell.vwHeader.alpha = 1.0
            cell.vwHeader.startViewBlink()
            return cell
        }

        let cell:Risk_TableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! Risk_TableViewCell
        cell.selectionStyle = .none
        let dict = arrayStatements[indexPath.row - 1]
        cell.lblTitle.text = dict["statement"].stringValue
        cell.btnSelect.tag = indexPath.row - 1
        cell.btnSelect.addTarget(self, action: #selector(btnSelectAction), for: .touchUpInside)
        if(dict["isSelected"].stringValue == "1")
        {
            cell.btnSelect.isSelected = true
        }else{
            cell.btnSelect.isSelected = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(arrayStatements.count + 1 == indexPath.row)
        {
            return 300
        }
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
//MARK: - SetUp Data
extension NotesReportVC
{
    func getDataLineColor(factor : String) -> String
    {
        if(factor == "sd")
        {
            return GlobalVariables.SDLineColor
        }else if(factor == "tos")
        {
            return GlobalVariables.TOSLineColor
        }else if(factor == "too")
        {
            return GlobalVariables.TOOLineColor
        }else if(factor == "sc")
        {
            return GlobalVariables.SCLineColor
        }else{
            return "000000"
        }
    }
    func setUpChartData(chart : LineChartView)
    {
        let dataDict = dictPointDetailsData["chart_detail"]
        
        //----SD----//
        var dictgen = dataDict["gen"]
        var dictCon = dataDict["con"]
    
        var category = ""
        var arrayDataPoints : [JSON] = []
        if(dictgen["rname"].stringValue == dictPointDetailsData["rname"].stringValue)
        {
            arrayDataPoints = dictgen["detail"].arrayValue
            
            for i in 0..<arrayDataPoints.count{
                var dict = arrayDataPoints[i]
                dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
                arrayDataPoints[i] = dict
            }
            dictgen["detail"] = JSON(arrayDataPoints)
            category = "gen"
        }else{
            arrayDataPoints = dictCon["detail"].arrayValue
            
            for i in 0..<arrayDataPoints.count{
                var dict = arrayDataPoints[i]
                dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
                arrayDataPoints[i] = dict
            }
            dictCon["detail"] = JSON(arrayDataPoints)
            category = "con"
        }
        print("arrayDataPoints - ",arrayDataPoints)

        if(arrayDataPoints.count == 0)
        {
            return
        }
       
        //*****
        ///--end reward set
        
        setUpChartDataWithPoints(arrayDataSet : arrayDataPoints,chart:chart,category:category)
    }
    
    func setUpChartDataWithPoints(arrayDataSet : [JSON],chart:LineChartView,category:String)
    {
        var array = arrayDataSet.sorted {$0["dateTobedisplay"].intValue < $1["dateTobedisplay"].intValue}
        let arrayXlabels = xLabelsArray(mergeXLabelsArray: array)
        chart.xAxis.valueFormatter = DayAxisValueFormatter(chart:chart,array:arrayXlabels)
        chart.xAxis.wordWrapEnabled = false
        chart.marker = XYMarkerView(color: UIColor.lightGray,
                                    font: MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 6),
                                    textColor: .white,
                                    insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                    xAxisValueFormatter: DayAxisValueFormatter(chart:chart,array:arrayXlabels))
        
        //-------Dataset 1-------//
        var arrayEntryDataSet1 : [ChartDataEntry] = []
        for i in 0..<array.count
        {
            var dict = array[i]
            
            let strDateLocal = dict["dateTobedisplay"].stringValue
            let strDateValueLocal = strDateLocal.prefix(8)
            
            //d MMM,yyyy
            let strDateValue = StringToConvertedStringDate(strDate: String(strDateValueLocal), strDateFormat: "yyyyMMdd", strRequiredFormat: "d MMM,yyyy")
            
            let XIndex = arrayXlabels.index(of: strDateValue)
            print("chart_icon - ",dictPointDetailsData["chart_icon"].stringValue)
            
            let dataEntry = ChartDataEntry(x: Double(XIndex!), y: dict["score"].doubleValue)
            dict["markLabel"].stringValue = dictPointDetailsData["title"].stringValue
            dict["category"].stringValue = category
            dict["chart_icon"].intValue = carIcon
            dict["dataSetArray"] = JSON(arrayDataSet)
            dict["rid"].stringValue = dictPointDetailsData["rid"].stringValue
            
            let img = getSelectedCar(chartIcon : carIcon) //selected car choosen by user
            
            var color = ""
            if(dict["score_id"].intValue == dictData["score_id"].intValue)
            {
                color = dict["icon_color"].stringValue
            }else{
                color = "grey"
            }
            let tintedImage = img.mask(with: getColorForCar(color:color))
            let score = dict["score"].doubleValue
            
            if(dict["have_notes"].boolValue)
            {
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : true,showRight:true,chartIcon: carIcon).convertViewToImage(carIcon : carIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : true,showRight:false,chartIcon: carIcon).convertViewToImage(carIcon : carIcon)
                    dataEntry.icon = mergeImage
                }
                
            }else{
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : false,showRight:true,chartIcon: carIcon).convertViewToImage(carIcon : carIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : false,showRight:false,chartIcon: carIcon).convertViewToImage(carIcon : carIcon)
                    dataEntry.icon = mergeImage
                }
            }
            
            dataEntry.data = dict as AnyObject
            array[i] = dict
            arrayEntryDataSet1.append(dataEntry)
        }
        print("DataSet 1 ------ ",arrayEntryDataSet1)
        
        var pointsJson = JSON()
        pointsJson["markLabel"].stringValue = dictPointDetailsData["title"].stringValue
        pointsJson["dataSet1"].arrayObject = arrayEntryDataSet1
        pointsJson["dataSetHeading1"].stringValue = dictPointDetailsData["title"].stringValue
        pointsJson["xLabels"].arrayObject = arrayXlabels
        
        //---data set on chart-
        let lineColor1 =  hexStringToUIColor(hex: getDataLineColor(factor: dictData["factor"].stringValue))
        let dataSet = dataSetCreate(values : arrayEntryDataSet1 , dataSetHeading : dictPointDetailsData["title"].stringValue , lineColor : lineColor1)
        dataSet.lineDashLengths = [5, 2.5]
        dataSet.highlightLineDashLengths = [5, 2.5]
        //---- end ---//
        let data = LineChartData(dataSets: [dataSet])
        data.setValueTextColor(.clear)
        data.setValueFont(.systemFont(ofSize: 9))
        chart.data = data
        chart.notifyDataSetChanged()
        chart.animate(xAxisDuration: 2.0)
    }
}

//MARK: - Other methods
extension NotesReportVC
{
    func setUpNavigationBarWithTitleAndHide(strTitle: String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(hideBackButton))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        self.navigationItem.titleView = HeaderLabel
    }
    @objc func hideBackButton()
    {
        self.vwFactorDetail.isHidden = true
        navigateToBack()
    }
    func navigateForFactorDetail()
    {
        let title = "Factor Details"
        setUpNavigationBarWithTitleAndHide(strTitle: title)
    }
    func navigateToBack()
    {
        let title = "My notes for \(getFactor()) WITH \(decreptedString(string: dictPointDetailsData["rname"].stringValue))"
        setUpNavigationBarWithTitleAndBack(strTitle: title)
    }
    
    func navigateToReport()
    {
        self.navigationController?.popViewController(animated: true)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChartData"), object: nil)
    }
    
    func setUpNotesData(data : JSON)
    {
        self.dictPointDetailsData = data
        let dict = data["point_detail"]
        self.strNoteText = decreptedString(string: dict["notes"].stringValue)
        
        if(isCheckUserLocalChina())
        {
            strVideoLink = data["server_link"].stringValue
        }else{
            strVideoLink = data["video"].stringValue
        }
        
        var array = data["risk_statement"].arrayValue
        for i in 0..<array.count
        {
            var dict = array[i]
            dict["id"].stringValue = decreptedString(string: dict["id"].stringValue)
            dict["statement"].stringValue = decreptedString(string: dict["statement"].stringValue)
            if(dict["is_selected"].boolValue == true)
            {
                dict["isSelected"].stringValue = "1"
            }else{
                dict["isSelected"].stringValue = "0"
            }
            array[i] = dict
        }
        
        self.arrayStatements = array
        
        navigateToBack()
        self.tblNotes.reloadData()
    }
    func dismissController()
    {
        if(isLoadFirstTimeScreen)
        {
            isLoadFirstTimeScreen = false
            tblNotes.reloadData()
        }
    }
}

//MARK: - Button Action
extension NotesReportVC
{
    @objc func btnSelectAction(_ sender : UIButton)
    {
        var dict = arrayStatements[sender.tag]
        if(dict["isSelected"].stringValue == "0")
        {
            dict["isSelected"].stringValue = "1"
        }else{
            dict["isSelected"].stringValue = "0"
        }
        arrayStatements[sender.tag] = dict
        tblNotes.reloadData()
    }
    
    @objc func btnDoneClickAction(_ sender : UIButton)
    {
        if(isCheckForSubmit())
        {
            self.submitNotesService()
        }
    }
    
    func navigateToScreen()
    {
        let alert = UIAlertController(title: "Set a new signpost to steer this?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { action in
            let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "AddTargetVC") as! AddTargetVC
            vc.selectedFactor = self.selectedFactorValue
            vc.categoryDict = self.categoryDict
            vc.currentScore = self.currentScore
            targetDict["factor"].stringValue = self.dictData["factor"].stringValue
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "Not right now", style: .cancel, handler: { action in
            self.navigateToReport()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func isCheckForSubmit() -> Bool
    {
        var arraySelectedRisk : [String] = []
        arrayStatements.forEach({ (json) in
            
            if(json["isSelected"].stringValue == "1")
            {
                arraySelectedRisk.append(json["id"].stringValue)
            }
        })
        let strId = arraySelectedRisk.joined(separator: ",")
        if(strId == "")
        {
            showToast(message: R.string.validationMessage.select_statement_key())
            return false
        }
        return true
    }
}
//MARK:- Chartview methods
extension NotesReportVC
{
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight)
    {
        print("chart \(chartView.tag) selected value \(entry)")
    }
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        NSLog("chartValueNothingSelected");
    }
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat){
    }
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
    }
    func setVisibleXRange(minXRange: CGFloat, maxXRange: CGFloat){
    }
}

//MARK: - Video Playing
extension NotesReportVC : AVPlayerViewControllerDelegate
{
    @objc func btnFactorLinkAction(_ sender : UIButton)
    {
        vwFactorDetail.isHidden = false
        navigateForFactorDetail()
    }
}
//MARK: - Player Delegate methods
extension NotesReportVC : DismissPlayerViewDelegate
{
    func dismissVideoPlayer()
    {
        print("Player dismissed")
        dismissController()
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
    }
}

//MARK: - Service
extension NotesReportVC 
{
    func getRiskStatement()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "rid" : dictData["rid"].stringValue,
                                           "score_id" : dictData["score_id"].stringValue,
                                           "score" : dictData["score"].stringValue,
                                           "is_historic" : dictData["is_historic"].stringValue,
                                           "factor" : dictData["factor"].stringValue
                
            ]
            ReportService().GetPointData(param : param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        var data = json["data"]
                        data["score"].stringValue = decreptedString(string: data["score"].stringValue)
                        
                        print("data:\(data)")
                        self.setUpNotesData(data : data)
                        self.loadWebView()
                        self.vwNoData.isHidden = true
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func submitNotesService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            var param : [String:Any] = [:]
            
            var arraySelectedRisk : [String] = []
            arrayStatements.forEach({ (json) in
                
                if(json["isSelected"].stringValue == "1")
                {
                    arraySelectedRisk.append(json["id"].stringValue)
                }
            })
            
            let strId = arraySelectedRisk.joined(separator: ",")
            var json = JSON()
            json["uid"].stringValue = encryptString(string: getUserDetail("user_id"))
            json["rid"].stringValue = dictPointDetailsData["rid"].stringValue
            json["score_id"].stringValue = dictData["score_id"].stringValue
            json["factor"].stringValue = encryptString(string: dictData["factor"].stringValue.lowercased())
            json["is_historic"].stringValue = dictData["is_historic"].stringValue
            json["notes"].stringValue = encryptString(string: strNoteText) 
            json["risk_ids"].stringValue = encryptString(string: strId)
            json["created"].stringValue = DateToString(Formatter: server_dt_format, date: Date())
            json["modified"].stringValue = DateToString(Formatter: server_dt_format, date: Date())
            json["timezone"].stringValue = getCurrentTimeZone()
            json["score"].stringValue = encryptString(string: dictPointDetailsData["score"].stringValue) 
            json["device_id"].stringValue = "\(getFirebaseToken())"
            json["device_type"].stringValue = "\(deviceType)"
            param["notes"] = json
            print("param - ",param)
            
            ReportService().AddPointNotes(param: param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToScreen()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

