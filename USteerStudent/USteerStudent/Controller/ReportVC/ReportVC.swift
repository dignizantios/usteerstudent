//
//  ReportVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/6/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Charts
import SwiftyJSON
import AlamofireSwiftyJSON

import Alamofire


enum checkReportParentController
{
    case treeview
    case myjourney
    case question
    case chooseRelation
}
class ReportVC: UIViewController
{
    //MARK:- Outlets
    @IBOutlet var vwNoData: UIView!
    @IBOutlet var lblErrorMessage: UILabel!

    //MARK:- self/school chart outlets
    @IBOutlet var chartViewScore: LineChartView!

    @IBOutlet var vwSelfSteering: UIView!

    @IBOutlet var btnShowLatest: UIButton!
    @IBOutlet var btnShowSelf: UIButton!
    
    //MARK:- Info outlets
    @IBOutlet var lblMessage: UILabel!
    @IBOutlet var imgInfoCar: UIImageView!
    @IBOutlet var vwInfo: UIView!
    
    //MARK:- other relation chart outlets
    @IBOutlet var chartViewScore1: LineChartView!
    @IBOutlet var chartViewScore2: LineChartView!
    @IBOutlet var chartViewScore3: LineChartView!
    @IBOutlet var chartViewScore4: LineChartView!
    
    @IBOutlet var vwCategorySteering: UIView!
    @IBOutlet var btnShowRelationSteering: UIButton!
    
    @IBOutlet var imgSelfCarDisplay: UIImageView!
    //Chart relation data outlets
    @IBOutlet var btnSD: UIButton!
    @IBOutlet var btnTOS: UIButton!
    @IBOutlet var btnTOO: UIButton!
    @IBOutlet var btnSC: UIButton!
    
    @IBOutlet var btnSDAddTargetHidden: UIButton!
    @IBOutlet var btnTOSAddTargetHidden: UIButton!
    @IBOutlet var btnTOOAddTargetHidden: UIButton!
    @IBOutlet var btnSCAddTargetHidden: UIButton!
    
    @IBOutlet var btnSDAddTargetRelationHidden: UIButton!
    @IBOutlet var btnTOSAddTargetRelationHidden: UIButton!
    @IBOutlet var btnTOOAddTargetRelationHidden: UIButton!
    @IBOutlet var btnSCAddTargetRelationHidden: UIButton!
    
    @IBOutlet var lblSDChartHeading: UILabel!
    @IBOutlet var lblTOSChartHeading: UILabel!
    @IBOutlet var lblTOOChartHeading: UILabel!
    @IBOutlet var lblSCChartHeading: UILabel!

    @IBOutlet var lblSD: UILabel!
    @IBOutlet var lblTOS: UILabel!
    @IBOutlet var lblTOO: UILabel!
    @IBOutlet var lblSC: UILabel!
    
    //Add target
    
    @IBOutlet var vwSDContainer: UIView!
    @IBOutlet var vwTOSContainer: UIView!
    @IBOutlet var vwTOOContainer: UIView!
    @IBOutlet var vwSCContainer: UIView!
    
    @IBOutlet var btnSDAddTarget: UIButton!
    @IBOutlet var btnTOSAddTarget: UIButton!
    @IBOutlet var btnTOOAddTarget: UIButton!
    @IBOutlet var btnSCAddTarget: UIButton!
    
    @IBOutlet var btnRelationSDAddTarget: UIButton!
    @IBOutlet var btnRelationTOSAddTarget: UIButton!
    @IBOutlet var btnRelationTOOAddTarget: UIButton!
    @IBOutlet var btnRelationSCAddTarget: UIButton!
    
    @IBOutlet var lblSDAddTarget: UILabel!
    @IBOutlet var lblTOSAddTarget: UILabel!
    @IBOutlet var lblTOOAddTarget: UILabel!
    @IBOutlet var lblSCAddTarget: UILabel!
    
    @IBOutlet var lblSDHeading: UILabel!
    @IBOutlet var lblTOSHeading: UILabel!
    @IBOutlet var lblTOOHeading: UILabel!
    @IBOutlet var lblSCHeading: UILabel!
    
    //For relation charts
    
    @IBOutlet var vwSDRelationContainer: UIView!
    @IBOutlet var vwTOSRelationContainer: UIView!
    @IBOutlet var vwTOORelationContainer: UIView!
    @IBOutlet var vwSCRelationContainer: UIView!
    
    @IBOutlet var vwSDReward: UIView!
    @IBOutlet var vwTOSReward: UIView!
    @IBOutlet var vwTOOReward: UIView!
    @IBOutlet var vwSCReward: UIView!

    @IBOutlet var lblSDReward: UILabel!
    @IBOutlet var lblTOSReward: UILabel!
    @IBOutlet var lblTOOReward: UILabel!
    @IBOutlet var lblSCReward: UILabel!

    @IBOutlet var imgSDReward: UIImageView!
    @IBOutlet var imgTOSReward: UIImageView!
    @IBOutlet var imgTOOReward: UIImageView!
    @IBOutlet var imgSCReward: UIImageView!
    
    //For self relation chart
    @IBOutlet var vwSelfReward: UIView!
    @IBOutlet var lblSelfReward: UILabel!
    @IBOutlet var imgSelfReward: UIImageView!
    
    //For Relation grey pop up
    @IBOutlet var vwSDRelationPhase2Popup: UIView!
    @IBOutlet var vwTOSRelationPhase2Popup: UIView!
    @IBOutlet var vwTOORelationPhase2Popup: UIView!
    @IBOutlet var vwSCRelationPhase2Popup: UIView!
    
    @IBOutlet var vwDefaultPhase2PopUp: UIView!

    //For Info Alert On Relation Assessment fnish adding
    @IBOutlet var vwInfoAlertPopup: UIView!
    @IBOutlet var vwSteerThisInInfoAlertPopup: UIView!

    //MARK:- variables
    //
    var dictDetails : JSON?
    var selectedParentVC = checkReportParentController.treeview
    var selectedFactor = TargetFactor.SD
    
    var arrayScore : [JSON] = []
    var arrayMainScore : [JSON] = []
    var arrayMainScoreWithoutLastest : [JSON] = []
    var categoryDict : JSON?
    var arrayXLabels : [String] = []
    var isShowLatestButton = false
    var dictRelationScores : JSON?
    var dictDataStoreForRelationScores = JSON()
    var reportData = JSON()
    var dictPointsData : JSON?
    var isAddNotesReload = false
    var chartIcon = 1
    var isAllowToAddSignpost = true
    
    ///
    var isclickedFirstTime = true
    var lastOpenNotesDataDict = JSON()
    var lastChartViewSelected = LineChartView()
    //
    
    var arrayRandomDataPoints : [ChartDataEntry] = []
    var lastClickedDataEntry = ChartDataEntry()
    var timer = Timer()
    
    
    var arrayDefaultSelfSD : [JSON] = []
    var arrayDefaultSelfTOS : [JSON] = []
    var arrayDefaultSelfTOO : [JSON] = []
    var arrayDefaultSelfSC : [JSON] = []
    
    var arrayDefaultSD : [JSON] = []
    var arrayDefaultTOS : [JSON] = []
    var arrayDefaultTOO : [JSON] = []
    var arrayDefaultSC : [JSON] = []

    var arrayRelationSD : [JSON] = []
    var arrayRelationTOS : [JSON] = []
    var arrayRelationTOO : [JSON] = []
    var arrayRelationSC : [JSON] = []

    var arraySelectedFactor : [String] = ["sd"]
    var grayCleredInitialCount = 0
    
    //MARK:- Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        isFromTargetBack = false
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadChartData), name: NSNotification.Name(rawValue: "reloadChartData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.viewWillAppear(_:)), name: NSNotification.Name(rawValue: "refreshView"), object: nil)
       
        let carIcon = getUserDetail("chart_icon")
        imgInfoCar.image = getSelectedCar(chartIcon: Int(carIcon) ?? 1)
        self.vwInfo.isHidden = true
        self.vwInfoAlertPopup.isHidden = true
        
        //-----
        setUpUI()
        //getReportsService()
        if(selectedParentVC == .myjourney)
        {
            setSelectedFactor()
            highLightFactor()
        }
        else if(selectedParentVC == .question) || (selectedParentVC == .chooseRelation)
        {
            [vwDefaultPhase2PopUp,vwSDRelationPhase2Popup,vwTOSRelationPhase2Popup,vwTOORelationPhase2Popup,vwSCRelationPhase2Popup].forEach({ (view) in
                view?.alpha = 1
            })
        }
        else{
            [vwDefaultPhase2PopUp,vwSDRelationPhase2Popup,vwTOSRelationPhase2Popup,vwTOORelationPhase2Popup,vwSCRelationPhase2Popup].forEach({ (view) in
                view?.isHidden = true
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        getReportsService()
        
        chartViewScore.highlightValue(nil, callDelegate: false)
        chartViewScore1.highlightValue(nil, callDelegate: false)
        chartViewScore2.highlightValue(nil, callDelegate: false)
        chartViewScore3.highlightValue(nil, callDelegate: false)
        chartViewScore4.highlightValue(nil, callDelegate: false)
        
        if(isFromTargetBack)
        {
            setRandomHighlightedValue()
        }
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        setBlinkButton()
        if(selectedParentVC == .question) || (selectedParentVC == .chooseRelation)
        {
            if(isShowLatestButton)
            {
                [vwDefaultPhase2PopUp,vwSDRelationPhase2Popup,vwTOSRelationPhase2Popup,vwTOORelationPhase2Popup,vwSCRelationPhase2Popup].forEach({ (view) in
                    view?.alpha = 1
                })
                if(arraySelectedFactor.count > 0)
                {
                    vwDefaultPhase2PopUp.isHidden = false
                }else{
                    vwDefaultPhase2PopUp.isHidden = true
                }
            }
        }
        else
        {
            if(isShowLatestButton)
            {
                [vwDefaultPhase2PopUp,vwSDRelationPhase2Popup,vwTOSRelationPhase2Popup,vwTOORelationPhase2Popup,vwSCRelationPhase2Popup].forEach({ (view) in
                    view?.alpha = 1
                    view?.isHidden = false
                })
                if(arraySelectedFactor.count > 0)
                {
                    vwDefaultPhase2PopUp.isHidden = false
                }else{
                    vwDefaultPhase2PopUp.isHidden = true
                }
            }
        }
        
        setBlinkView()
        
        let isShowReportAlert = Defaults.bool(forKey: "isShowReportAlert")
        let count = Defaults.integer(forKey: "ReportAlertPopUp_Count")
        if count <= 2 && isShowReportAlert == true
        {
            Defaults.set(count+1, forKey: "ReportAlertPopUp_Count")
            Defaults.set(false, forKey: "isShowReportAlert")
            self.vwInfoAlertPopup.isHidden = false
        }
        else
        {
            self.vwInfoAlertPopup.isHidden = true
        }
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        timer.invalidate()
        isFromTargetBack = false
    }
    
    //MARK: -
    func setSelectedFactor()
    {
        selectedFactor = TargetFactor(rawValue: getFactors(str :decreptedString(string: (dictDetails?["factor"].stringValue)!)))!
        print("selectedFactor - ",selectedFactor.rawValue)
    }
    func highLightFactor()
    {
        print("selectedFactor ---------- ",selectedFactor)
        if(categoryDict?["rname"].stringValue.lowercased() == "self" || categoryDict?["rname"].stringValue.lowercased() == "school")
        {
            if(selectedFactor == TargetFactor.SD)
            {
                btnSDAddTarget.isSelected = true
            }
            else if(selectedFactor == TargetFactor.TOS)
            {
                btnTOSAddTarget.isSelected = true
            }
            else if(selectedFactor == TargetFactor.TOO)
            {
                btnTOOAddTarget.isSelected = true
            }
            else if(selectedFactor == TargetFactor.SC)
            {
                btnSCAddTarget.isSelected = true
            }
        }
        else{
            
            if(selectedFactor == TargetFactor.SD)
            {
                btnRelationSDAddTarget.isSelected = true
            }
            else if(selectedFactor == TargetFactor.TOS)
            {
                btnRelationTOSAddTarget.isSelected = true
            }
            else if(selectedFactor == TargetFactor.TOO)
            {
                btnRelationTOOAddTarget.isSelected = true
            }
            else if(selectedFactor == TargetFactor.SC)
            {
                btnRelationSCAddTarget.isSelected = true
            }
        }
        showHighLightedButton()
    }
    func highLightView(view : UIView)
    {
        view.layer.cornerRadius = 5
        view.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
        view.layer.borderWidth = 1
    }
    func showHighLightedButton()
    {
        if(btnSDAddTarget.isSelected)
        {
            highLightView(view: vwSDContainer)
        }
        if(btnTOSAddTarget.isSelected)
        {
            highLightView(view: vwTOSContainer)
        }
        if(btnTOOAddTarget.isSelected)
        {
            highLightView(view: vwTOOContainer)
        }
        if(btnSCAddTarget.isSelected)
        {
            highLightView(view: vwSCContainer)
        }
        
        if(btnRelationSDAddTarget.isSelected)
        {
            highLightView(view: vwSDRelationContainer)
        }
        if(btnRelationTOSAddTarget.isSelected)
        {
            highLightView(view: vwTOSRelationContainer)
        }
        if(btnRelationTOOAddTarget.isSelected)
        {
            highLightView(view: vwTOORelationContainer)
        }
        if(btnRelationSCAddTarget.isSelected)
        {
            highLightView(view: vwSCRelationContainer)
        }
    }
    //MARK: - Chart random highlight values
    func setRandomHighlightedValue()
    {
        if(lastClickedDataEntry != nil)
        {
            timer = Timer.scheduledTimer(timeInterval: 3.0, target: self,   selector: (#selector(highLightRandomDataEntry)), userInfo: nil, repeats: true)
        }
    }
    @objc func highLightRandomDataEntry()
    {
        print("Tiemr called")
        var chart = LineChartView()
        if(lastChartViewSelected == chartViewScore)
        {
            chart = chartViewScore
        }
        else if(lastChartViewSelected == chartViewScore1)
        {
            chart = chartViewScore1
        }
        else if(lastChartViewSelected == chartViewScore2)
        {
            chart = chartViewScore2
        }
        else if(lastChartViewSelected == chartViewScore3)
        {
            chart = chartViewScore3
        }
        else if(lastChartViewSelected == chartViewScore4)
        {
            chart = chartViewScore4
        }
        
        let dataSet = chart.data?.getDataSetForEntry(lastClickedDataEntry)
        if(dataSet?.entryCount ?? 0 > 2)
        {
            let randomIndex =  arc4random_uniform(UInt32((dataSet?.entryCount)!-1))
            let randomChartDataEntry = dataSet?.entryForIndex(Int(randomIndex))
            chart.highlightValue(Highlight(x: (randomChartDataEntry?.x)!, y: (randomChartDataEntry?.y)!, dataSetIndex: 0), callDelegate: false)
            chart.setNeedsDisplay()
            
        }else{
            timer.invalidate()
        }
    }
}
//MARK:- UI Setup
extension ReportVC
{
    @objc func reloadChartData()
    {
        isAddNotesReload = true
        getReportsService()
    }
    func setUpUI()
    {
        vwNoData.isHidden = false
        
        [vwSDReward,vwTOSReward,vwTOOReward,vwSCReward].forEach({ (view) in
            view?.isHidden = true
        })
        [lblSD,lblTOO,lblTOS,lblSC,lblSDHeading,lblTOOHeading,lblTOSHeading,lblSCHeading].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor

        }
        [lblSDHeading,lblTOOHeading,lblTOSHeading,lblSCHeading].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        
        [lblSDAddTarget,lblTOOAddTarget,lblTOSAddTarget,lblSCAddTarget].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            label?.text = "STEER THIS"
        }
        [lblSDChartHeading,lblTOOChartHeading,lblTOSChartHeading,lblSCChartHeading].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor.withAlphaComponent(0.75)
        }
        
        [lblSDReward,lblTOSReward,lblTOOReward,lblSCReward,lblSelfReward].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        
        [btnShowLatest,btnShowSelf,btnShowRelationSteering].forEach { (button) in
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        }
        //
        btnShowRelationSteering.isSelected = false
        btnShowRelationSteering.setTitle(getCommonString(key: "Show_again_key").uppercased(), for: .normal)
        btnShowRelationSteering.setTitle(getCommonString(key: "Show_again_key").uppercased(), for: .selected)
        //
        btnShowRelationSteering.setTitleColor(MySingleton.sharedManager.themeBlueColor, for: .normal)
        btnShowRelationSteering.backgroundColor = MySingleton.sharedManager.themeYellowColor
        btnShowRelationSteering?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)

        //
        btnShowLatest.setTitleColor(MySingleton.sharedManager.themeGreenColor, for: .normal)
        btnShowLatest.setTitleColor(MySingleton.sharedManager.themeGreenColor, for: .selected)
        btnShowLatest.setTitle(getCommonString(key: "Show_latest_key").uppercased(), for: .normal)
        btnShowLatest.setTitle(getCommonString(key: "Show_latest_key").uppercased(), for: .selected)
        btnShowLatest.layer.cornerRadius = btnShowSelf.frame.size.height / 2
        btnShowLatest.layer.masksToBounds = true

        //
        btnShowSelf.setTitleColor(MySingleton.sharedManager.themeBlueColor, for: .normal)
        btnShowSelf.setTitleColor(MySingleton.sharedManager.themeBlueColor, for: .selected)
        btnShowSelf.setTitle(getCommonString(key: "Show_self_key").uppercased(), for: .normal)
        btnShowSelf.setTitle(getCommonString(key: "Hide_self_key").uppercased(), for: .selected)
        btnShowSelf.layer.cornerRadius = btnShowSelf.frame.size.height / 2
        btnShowSelf.layer.masksToBounds = true

        lblSDChartHeading.text = "Self - Disclosure".uppercased()
        lblTOSChartHeading.text = "Trust of self".uppercased()
        lblTOOChartHeading.text = "Trust of others".uppercased()
        lblSCChartHeading.text = "Seeking change".uppercased()

        //
        [btnRelationSDAddTarget,btnRelationSCAddTarget,btnRelationTOOAddTarget,btnRelationTOSAddTarget,btnSDAddTarget,btnTOSAddTarget,btnTOOAddTarget,btnSCAddTarget].forEach { (button) in
            button?.isSelected = false
        }
        
        [btnShowLatest,btnShowSelf].forEach { (button) in
            button?.backgroundColor = MySingleton.sharedManager.themeYellowColor
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
        }

        [vwSDReward,vwTOSReward,vwSCReward,vwTOOReward,vwSelfReward].forEach{
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor.withAlphaComponent(0.75);
        }
        let tapOptions1 = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwSDReward.addGestureRecognizer(tapOptions1)
        
        let tapOptions2 = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwTOSReward.addGestureRecognizer(tapOptions2)
        
        let tapOptions3 = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwSCReward.addGestureRecognizer(tapOptions3)
        
        let tapOptions4 = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwTOOReward.addGestureRecognizer(tapOptions4)
        
        let tapOptions5 = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwSelfReward.addGestureRecognizer(tapOptions5)
        
        let tapOptions6 = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwInfo.addGestureRecognizer(tapOptions6)
        
        //Relation chart pop up info getures
        let SDInfoPhase2 = UITapGestureRecognizer(target: self, action: #selector(hidePhase2View))
        vwSDRelationPhase2Popup.addGestureRecognizer(SDInfoPhase2)
        
        let TOSInfoPhase2 = UITapGestureRecognizer(target: self, action: #selector(hidePhase2View))
        vwTOSRelationPhase2Popup.addGestureRecognizer(TOSInfoPhase2)
        
        let TOOInfoPhase2 = UITapGestureRecognizer(target: self, action: #selector(hidePhase2View))
        vwTOORelationPhase2Popup.addGestureRecognizer(TOOInfoPhase2)
        
        let SCInfoPhase2 = UITapGestureRecognizer(target: self, action: #selector(hidePhase2View))
        vwSCRelationPhase2Popup.addGestureRecognizer(SCInfoPhase2)
        
        
        let DefaultInfoPhase2 = UITapGestureRecognizer(target: self, action: #selector(hideDefaultPhase2View))
        vwDefaultPhase2PopUp.addGestureRecognizer(DefaultInfoPhase2)
        
        let hideGesture = UITapGestureRecognizer(target: self, action: #selector(hideView))
        vwInfoAlertPopup.addGestureRecognizer(hideGesture)
        
        vwSteerThisInInfoAlertPopup.layer.cornerRadius = 10.0
        vwSteerThisInInfoAlertPopup.layer.borderWidth = 1.0
        vwSteerThisInInfoAlertPopup.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
        
        //setup for self-school chart
        btnShowLatest.isHidden = !isShowLatestButton
        
        let strCategory = categoryDict?["default_name"].stringValue.lowercased()
        if(strCategory == "self")
        {
            btnShowSelf.isHidden = true
            btnShowSelf.isSelected = true

            vwSelfSteering.isHidden = false
            vwCategorySteering.isHidden = true

        }else if(strCategory == "school")
        {
            btnShowSelf.isHidden = false
            btnShowSelf.isSelected = false
            vwSelfSteering.isHidden = false
            vwCategorySteering.isHidden = true
        }
        else{
            vwSelfSteering.isHidden = true
            vwCategorySteering.isHidden = false
        }
        
        //Selected all change
        [btnSD,btnTOS,btnTOO,btnSC].forEach({
            $0?.isSelected = false
        })
        btnSD.isSelected = true
        
        lblErrorMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblErrorMessage.textColor = MySingleton.sharedManager.themeYellowColor
        //---
        
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblMessage.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblMessage.text = "TOUCH DIFFERENT CARS TO OPEN YOUR JOURNEY \n\n The grey icons are your biases for SELF. The coloured icons are your biases in this relationship"
        imgInfoCar.image = imgInfoCar.image?.withHorizontallyFlippedOrientation()
        [chartViewScore,chartViewScore1,chartViewScore2,chartViewScore3,chartViewScore4].forEach { (chartview) in
            configureChart(chartview : chartview)
        }
    }
    
    private func initialize() {
        self.edgesForExtendedLayout = []
    }
}
//MARK: - Geture method
extension ReportVC
{
    /// Tap reconizer on table to hide keyboard
    ///
    /// - Parameter sender: geture reconizer
    @objc func hideView(sender: UITapGestureRecognizer) {
        // handling code
        
        if(sender.view == vwSDReward)
        {
            vwSDReward.isHidden = true
        }else if(sender.view == vwSCReward)
        {
            vwSCReward.isHidden = true
        }
        else if(sender.view == vwTOOReward)
        {
            vwTOOReward.isHidden = true
        }else if(sender.view == vwTOSReward)
        {
            vwTOSReward.isHidden = true
        }else if(sender.view == vwSelfReward)
        {
            vwSelfReward.isHidden = true
        }else if(sender.view == vwInfo)
        {
            vwInfo.isHidden = true
        }
        else if(sender.view == vwInfoAlertPopup)
        {
            vwInfoAlertPopup.isHidden = true
        }
    }
    
//
    @objc func hidePhase2View(sender: UITapGestureRecognizer) {
        // handling code
        
        if(sender.view == vwSDRelationPhase2Popup)
        {
            vwSDRelationPhase2Popup.isHidden = true
        }else if(sender.view == vwTOSRelationPhase2Popup)
        {
            vwTOSRelationPhase2Popup.isHidden = true
        }
        else if(sender.view == vwTOORelationPhase2Popup)
        {
            vwTOORelationPhase2Popup.isHidden = true
        }else
        {
            vwSCRelationPhase2Popup.isHidden = true
        }
        
        redirectWithLatestData(sender : sender)
    }
    
    @objc func hideDefaultPhase2View(sender: UITapGestureRecognizer) {
        vwDefaultPhase2PopUp.isHidden = true
        redirectWithDefaultLatestData(sender : sender)
    }
    
    func selectedFactorArray() -> [JSON]
    {
        
        let strFactor = arraySelectedFactor.last
        
        var array : [JSON] = []
        
        if(strFactor == "sd")
        {
            if(categoryDict?["default_name"].stringValue.lowercased() == "self")
            {
                array = arrayDefaultSelfSD
            }else{
                array = arrayDefaultSD
            }
        }
        else if(strFactor == "tos")
        {
            if(categoryDict?["default_name"].stringValue.lowercased() == "self")
            {
                array = arrayDefaultSelfTOS
            }else{
                array = arrayDefaultTOS
            }
        }
        else if(strFactor == "too")
        {
            if(categoryDict?["default_name"].stringValue.lowercased() == "self")
            {
                array = arrayDefaultSelfTOO
            }else{
                array = arrayDefaultTOO
            }
        }
        else
        {
            if(categoryDict?["default_name"].stringValue.lowercased() == "self")
            {
                array = arrayDefaultSelfSC
            }else{
                array = arrayDefaultSC
            }
        }
        return array
    }
    func redirectWithDefaultLatestData(sender : UITapGestureRecognizer)
    {
        if arraySelectedFactor.count == 0{
            return
        }
        
        arraySelectedFactor.removeLast()
        timer.invalidate()
        isFromTargetBack = false
        
        let arrayPointJSON = selectedFactorArray()
        //TODO: - Manage if array zero
        var dict = arrayPointJSON.last
        print("Last dict = ",dict ?? [])
        if(dict!["markLabel"].stringValue == getCommonString(key: "Self_disclosure_key"))
        {
            dict!["factor"].stringValue = "sd"
        }else if(dict!["markLabel"].stringValue == getCommonString(key: "Trust_of_self_key"))
        {
            dict!["factor"].stringValue = "tos"
        }else if(dict!["markLabel"].stringValue == getCommonString(key: "Trust_of_others_key"))
        {
            dict!["factor"].stringValue = "too"
        }else if(dict!["markLabel"].stringValue == getCommonString(key: "Seeking_changes_key"))
        {
            dict!["factor"].stringValue = "sc"
        }
        dictPointsData = dict
        if(isclickedFirstTime)
        {
            isclickedFirstTime = false
        }
        //-----
        let vc = objReportStoryboard.instantiateViewController(withIdentifier: "NotesReportVC") as! NotesReportVC
        targetDict = JSON()
        targetDict["factor"].stringValue = dictPointsData!["factor"].stringValue
        
        var currentScore = ""
        if(categoryDict?["default_name"].stringValue.lowercased() == "self")
        {
            currentScore = getCurrentScore(factor: dictPointsData!["factor"].stringValue, category: "gen")
        }else{
            currentScore = getCurrentScore(factor: dictPointsData!["factor"].stringValue, category: "con")
        }
        targetDict["currentScore"].stringValue = currentScore
        
        vc.dictData = dictPointsData ?? JSON()
        vc.carIcon = (dictPointsData?["chart_icon"].intValue)!
        vc.categoryDict = categoryDict ?? JSON()
        vc.currentScore = currentScore
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func redirectWithLatestData(sender : UITapGestureRecognizer)
    {
        if grayCleredInitialCount == 1
        {
            Defaults.set(true, forKey: "isShowReportAlert")
        }
        else
        {
            Defaults.set(false, forKey: "isShowReportAlert")
        }
        
        timer.invalidate()
        isFromTargetBack = false
        
        var arrayPointJSON : [JSON] = []
        if(sender.view == vwSDRelationPhase2Popup)
        {
            arrayPointJSON = arrayRelationSD
        }else if(sender.view == vwTOSRelationPhase2Popup)
        {
            arrayPointJSON = arrayRelationTOS
        }
        else if(sender.view == vwTOORelationPhase2Popup)
        {
            arrayPointJSON = arrayRelationTOO
        }else if(sender.view == vwSCRelationPhase2Popup)
        {
            arrayPointJSON = arrayRelationSC
        }
        //TODO: - Manage if array zero
        var dict = arrayPointJSON.last
        if(dict!["markLabel"].stringValue == getCommonString(key: "Self_disclosure_key"))
        {
            dict!["factor"].stringValue = "sd"
        }else if(dict!["markLabel"].stringValue == getCommonString(key: "Trust_of_self_key"))
        {
            dict!["factor"].stringValue = "tos"
        }else if(dict!["markLabel"].stringValue == getCommonString(key: "Trust_of_others_key"))
        {
            dict!["factor"].stringValue = "too"
        }else if(dict!["markLabel"].stringValue == getCommonString(key: "Seeking_changes_key"))
        {
            dict!["factor"].stringValue = "sc"
        }
        
        dictPointsData = dict
        if(isclickedFirstTime)
        {
            isclickedFirstTime = false
        }

        //-----
        let vc = objReportStoryboard.instantiateViewController(withIdentifier: "NotesReportVC") as! NotesReportVC
        targetDict = JSON()
        targetDict["factor"].stringValue = dictPointsData!["factor"].stringValue
        var currentScore = ""
        if(categoryDict?["default_name"].stringValue.lowercased() == "self")
        {
            currentScore = getCurrentScore(factor: dictPointsData!["factor"].stringValue, category: "gen")
        }
        else
        {
            currentScore = getCurrentScore(factor: dictPointsData!["factor"].stringValue, category: "con")
        }
        targetDict["currentScore"].stringValue = currentScore
        vc.dictData = dictPointsData ?? JSON()
        vc.carIcon = (dictPointsData?["chart_icon"].intValue)!
        vc.categoryDict = categoryDict ?? JSON()
        vc.currentScore = currentScore
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - Blink Setup
extension ReportVC
{
    func setBlinkView()
    {
        [vwDefaultPhase2PopUp,vwSDRelationPhase2Popup,vwTOSRelationPhase2Popup,vwTOORelationPhase2Popup,vwSCRelationPhase2Popup].forEach({ (view) in
            view?.alpha = 1
        })
    }

    func setBlinkButton()
    {
        [btnSDAddTarget,btnTOSAddTarget,btnTOOAddTarget,btnSCAddTarget].forEach { (button) in
            button?.alpha = 1
        }
        
        [lblSDAddTarget,lblTOSAddTarget,lblTOOAddTarget,lblSCAddTarget].forEach { (label) in
            label?.alpha = 1
        }
        if(btnSDAddTarget.isSelected)
        {
            btnSDAddTarget.startBlink()
            lblSDAddTarget.startBlink()
        }
        if(btnTOSAddTarget.isSelected)
        {
            btnTOSAddTarget.startBlink()
            lblTOSAddTarget.startBlink()
        }
        if(btnTOOAddTarget.isSelected)
        {
            btnTOOAddTarget.startBlink()
            lblTOOAddTarget.startBlink()
        }
        if(btnSCAddTarget.isSelected)
        {
            btnSCAddTarget.startBlink()
            lblSCAddTarget.startBlink()
        }
        ///----
        [btnRelationSDAddTarget,btnRelationTOSAddTarget,btnRelationTOOAddTarget,btnRelationSCAddTarget].forEach { (button) in
            button?.alpha = 1
        }
        [lblSDHeading,lblTOSHeading,lblTOOHeading,lblSCHeading].forEach { (label) in
            label?.alpha = 1
        }
        if(btnRelationSDAddTarget.isSelected)
        {
            btnRelationSDAddTarget.startBlink()
            lblSDHeading.startBlink()
        }
        if(btnRelationTOSAddTarget.isSelected)
        {
            btnRelationTOSAddTarget.startBlink()
            lblTOSHeading.startBlink()
        }
        if(btnRelationTOOAddTarget.isSelected)
        {
            btnRelationTOOAddTarget.startBlink()
            lblTOOHeading.startBlink()
        }
        if(btnRelationSCAddTarget.isSelected)
        {
            btnRelationSCAddTarget.startBlink()
            lblSCHeading.startBlink()
        }
    }
}

//MARK: - Data setup
extension ReportVC
{
    func showHideEmojiView(view : UIView,reward:Int)
    {
        if(isShowLatestButton)
        {
            if(reward == 0)
            {
                view.isHidden = true
            }
            else{
                view.isHidden = false
                setUpEmojiToView(view : view,reward:reward)
            }
        }else{
            view.isHidden = true
        }
        view.isHidden = true
    }
    
    func setUpEmojiToView(view : UIView,reward:Int)
    {
        if(view == vwSelfReward)
        {
            imgSelfReward.image = rewardEmoji(reward: reward)
            lblSelfReward.text = rewardText(reward: reward)
        }else if(view == vwSDReward)
        {
            lblSDReward.text = rewardText(reward: reward)
            imgSDReward.image = rewardEmoji(reward: reward)
        }
        else if(view == vwTOSReward)
        {
            lblTOSReward.text = rewardText(reward: reward)
            imgTOSReward.image = rewardEmoji(reward: reward)
        }
        else if(view == vwTOOReward)
        {
            lblTOOReward.text = rewardText(reward: reward)
            imgTOOReward.image = rewardEmoji(reward: reward)
        }
        else //if(view == vwSCReward)
        {
            lblSCReward.text = rewardText(reward: reward)
            imgSCReward.image = rewardEmoji(reward: reward)
        }
    }
    
    //MARK: - Set Data for four diffrent charts
    func setUpCategoryData(dataDict:JSON)
    {
        print("dataDict:\(dataDict)")
        var mainData = dataDict
        
        //----SD----//
        var dictSD = mainData["SD"]
        var dictgenSD = dictSD["gen"]
        var dictConSD = dictSD["con"]
        
        let strSDdataSetHeading = getCommonString(key: "Self_disclosure_key")
        var arraySDDataSet1 = dictgenSD["detail"].arrayValue
        for i in 0..<arraySDDataSet1.count{
            var dict = arraySDDataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySDDataSet1[i] = dict
        }
        dictgenSD["detail"] = JSON(arraySDDataSet1)
        var arraySDDataSet2 = dictConSD["detail"].arrayValue
        for i in 0..<arraySDDataSet2.count{
            var dict = arraySDDataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySDDataSet2[i] = dict
        }
        dictConSD["detail"] = JSON(arraySDDataSet2)
        dictSD["con"] = dictConSD
        dictSD["gen"] = dictgenSD
        mainData["SD"] = dictSD
        print("dictSD :\(dictSD)")
        
        //----TOS----//
        var dictTOS = mainData["TOS"]
        var dictgenTOS = dictTOS["gen"]
        var dictConTOS = dictTOS["con"]
        
        let strTOSdataSetHeading = getCommonString(key: "Trust_of_self_key")

        var arrayTOSDataSet1 = dictgenTOS["detail"].arrayValue
        for i in 0..<arrayTOSDataSet1.count{
            var dict = arrayTOSDataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOSDataSet1[i] = dict
        }
        dictgenTOS["detail"] = JSON(arrayTOSDataSet1)
        
        var arrayTOSDataSet2 = dictConTOS["detail"].arrayValue
        for i in 0..<arrayTOSDataSet2.count{
            var dict = arrayTOSDataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOSDataSet2[i] = dict
        }
        
        dictConTOS["detail"] = JSON(arrayTOSDataSet2)
        dictTOS["con"] = dictConTOS
        dictTOS["gen"] = dictgenTOS
        mainData["TOS"] = dictTOS
        
        //----TOO----//
        var dictTOO = mainData["TOO"]
        var dictgenTOO = dictTOO["gen"]
        var dictConTOO = dictTOO["con"]
        
        let strTOOdataSetHeading = getCommonString(key: "Trust_of_others_key")
        var arrayTOODataSet1 = dictgenTOO["detail"].arrayValue
        for i in 0..<arrayTOODataSet1.count{
            var dict = arrayTOODataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOODataSet1[i] = dict
        }
        dictgenTOO["detail"] = JSON(arrayTOODataSet1)
        
        var arrayTOODataSet2 = dictConTOO["detail"].arrayValue
        for i in 0..<arrayTOODataSet2.count{
            var dict = arrayTOODataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOODataSet2[i] = dict
        }
        
        dictConTOO["detail"] = JSON(arrayTOODataSet2)
        dictTOO["con"] = dictConTOO
        dictTOO["gen"] = dictgenTOO
        mainData["TOO"] = dictTOO
        
        //----SC----//
        var dictSC = mainData["SC"]
        var dictgenSC = dictSC["gen"]
        var dictConSC = dictSC["con"]
        
        let strSCdataSetHeading = getCommonString(key: "Seeking_changes_key")

        var arraySCDataSet1 = dictgenSC["detail"].arrayValue
        for i in 0..<arraySCDataSet1.count{
            var dict = arraySCDataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySCDataSet1[i] = dict
        }
        dictgenSC["detail"] = JSON(arraySCDataSet1)
        
        var arraySCDataSet2 = dictConSC["detail"].arrayValue
        for i in 0..<arraySCDataSet2.count{
            var dict = arraySCDataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySCDataSet2[i] = dict
        }
        
        dictConSC["detail"] = JSON(arraySCDataSet2)
        dictSC["con"] = dictConSC
        dictSC["gen"] = dictgenSC
        mainData["SC"] = dictSC
        
        //TODO: - Relation scroes
        self.dictRelationScores = mainData
        
        //----Setup Label Glow
        //*****
        if(dictSD["is_glow"].boolValue)
        {
            btnRelationSDAddTarget.isSelected = true
        }
        if(dictTOS["is_glow"].boolValue)
        {
            btnRelationTOSAddTarget.isSelected = true
        }
        if(dictTOO["is_glow"].boolValue)
        {
            btnRelationTOOAddTarget.isSelected = true
        }
        if(dictSC["is_glow"].boolValue)
        {
            btnRelationSCAddTarget.isSelected = true
        }
        setBlinkButton()

        //----Setup reward view
        //*****
        showHideEmojiView(view: vwSDReward, reward: dictConSD["reward"].intValue )
        showHideEmojiView(view: vwTOSReward, reward: dictConTOS["reward"].intValue)
        showHideEmojiView(view: vwTOOReward, reward: dictConTOO["reward"].intValue)
        showHideEmojiView(view: vwSCReward, reward: dictConSC["reward"].intValue)
        //*****
        ///--end reward set
        
        setUpChartDataWithPoints(strMarkHeading:strSDdataSetHeading,arrayDataSet1 : arraySDDataSet1,arrayDataSet2 : arraySDDataSet2,strDataSetHeading1:strSDdataSetHeading + " - \(dictgenSD["rname"].stringValue)",strDataSetHeading2:strSDdataSetHeading + " - \(dictConSD["rname"].stringValue )",dataSetLine1Color : "000000",dataSetLine2Color : GlobalVariables.SDLineColor,chart:chartViewScore1,rid1:dictgenSD["rid"].stringValue ,rid2:dictConSD["rid"].stringValue ,factor:TargetFactor.SD.rawValue)
        
        setUpChartDataWithPoints(strMarkHeading:strTOSdataSetHeading,arrayDataSet1 : arrayTOSDataSet1,arrayDataSet2 : arrayTOSDataSet2,strDataSetHeading1:strTOSdataSetHeading + " - \(dictgenTOS["rname"].stringValue )",strDataSetHeading2:strTOSdataSetHeading + " - \(dictConTOS["rname"].stringValue )",dataSetLine1Color : "000000",dataSetLine2Color : GlobalVariables.TOSLineColor,chart:chartViewScore2,rid1:dictgenTOS["rid"].stringValue ,rid2:dictConTOS["rid"].stringValue ,factor:TargetFactor.TOS.rawValue)
        
        setUpChartDataWithPoints(strMarkHeading:strTOOdataSetHeading,arrayDataSet1 : arrayTOODataSet1,arrayDataSet2 : arrayTOODataSet2,strDataSetHeading1:strTOOdataSetHeading + " - \(dictgenTOO["rname"].stringValue )",strDataSetHeading2:strTOOdataSetHeading + " - \(dictConTOO["rname"].stringValue )",dataSetLine1Color : "000000",dataSetLine2Color : GlobalVariables.TOOLineColor,chart:chartViewScore3,rid1:dictgenTOO["rid"].stringValue ,rid2:dictConTOO["rid"].stringValue ,factor:TargetFactor.TOO.rawValue)
        
        setUpChartDataWithPoints(strMarkHeading:strSCdataSetHeading,arrayDataSet1 : arraySCDataSet1,arrayDataSet2 : arraySCDataSet2,strDataSetHeading1:strSCdataSetHeading + " - \(dictgenSC["rname"].stringValue )",strDataSetHeading2:strSCdataSetHeading + " - \(dictConSC["rname"].stringValue )",dataSetLine1Color : "000000",dataSetLine2Color : GlobalVariables.SCLineColor,chart:chartViewScore4,rid1:dictgenSC["rid"].stringValue ,rid2:dictConSC["rid"].stringValue ,factor:TargetFactor.SC.rawValue)
        
        //--------///
    }
   
    func setUpChartDataWithPoints(strMarkHeading:String,arrayDataSet1 : [JSON],arrayDataSet2 : [JSON],strDataSetHeading1:String,strDataSetHeading2:String,dataSetLine1Color : String,dataSetLine2Color : String,chart:LineChartView,rid1:String,rid2:String,factor:String)
    {
        var array1 = arrayDataSet1.sorted {$0["dateTobedisplay"].intValue < $1["dateTobedisplay"].intValue}
        var array2 = arrayDataSet2.sorted {$0["dateTobedisplay"].intValue < $1["dateTobedisplay"].intValue}
        
        let mergeArray = arrayDataSet1 + arrayDataSet2
        
        let arrayXlabels = xLabelsArray(mergeXLabelsArray: mergeArray)
        
        [chart].forEach { (chartview) in
            chartview.xAxis.valueFormatter = DayAxisValueFormatter(chart:chartview,array:arrayXlabels)
            chartview.xAxis.wordWrapEnabled = false
            chartview.marker = XYMarkerView(color: UIColor.lightGray,
                                        font: MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 6),
                                        textColor: .white,
                                        insets: UIEdgeInsets(top: 8, left: 8, bottom: 20, right: 8),
                                        xAxisValueFormatter: DayAxisValueFormatter(chart:chartview,array:arrayXlabels))
        }
        
        print("--------- \(strMarkHeading) --------------- ")


        //-------Dataset 1-------//
        
        
        var arrayEntryDataSet1 : [ChartDataEntry] = []
        for i in 0..<array1.count
        {
            var dict = array1[i]
            
            let strDateLocal = dict["dateTobedisplay"].stringValue
            let strDateValueLocal = strDateLocal.prefix(8)
            
            //d MMM,yyyy
            let strDateValue = StringToConvertedStringDate(strDate: String(strDateValueLocal), strDateFormat: "yyyyMMdd", strRequiredFormat: "d MMM,yyyy")
            
            let XIndex = arrayXlabels.index(of: strDateValue)
            //---set dash line data
            
            let dataEntry = ChartDataEntry(x: Double(XIndex!), y: dict["score"].doubleValue)
            dict["markLabel"].stringValue = strMarkHeading
            dict["lineColor"].stringValue = dataSetLine1Color
            dict["category"].stringValue = "gen"
            dict["chart_icon"].intValue = chartIcon
            dict["dataSetArray"] = JSON(arrayDataSet1)
            dict["rid"].stringValue = rid1
            dict["factor"].stringValue = factor

            let img = getSelectedCar(chartIcon : chartIcon) //selected car choosen by user
            //For self graph car color will be grey
            let tintedImage = img.mask(with: getColorForCar(color:dict["icon_color"].stringValue))
            imgSelfCarDisplay.image = tintedImage
            let score = dict["score"].doubleValue
            if(dict["have_notes"].boolValue)
            {
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : true,showRight:true,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : true,showRight:false,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }
            }else{
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : false,showRight:true,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : false,showRight:false,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }
            }
            
            dataEntry.data = dict as AnyObject
            array1[i] = dict
            arrayEntryDataSet1.append(dataEntry)
        }
        print("DataSet 1 ------ ",arrayEntryDataSet1)
        
        //-------Dataset 2-------//
        var arrayEntryDataSet2 : [ChartDataEntry] = []
        for i in 0..<array2.count
        {
            var dict = array2[i]
            let strDateLocal = dict["dateTobedisplay"].stringValue
            let strDateValueLocal = strDateLocal.prefix(8)
            //d MMM,yyyy
            let strDateValue = StringToConvertedStringDate(strDate: String(strDateValueLocal), strDateFormat: "yyyyMMdd", strRequiredFormat: "d MMM,yyyy")
            let XIndex = arrayXlabels.index(of: strDateValue)
            let dataEntry = ChartDataEntry(x: Double(XIndex ?? 0), y: dict["score"].doubleValue)
            dict["rid"].stringValue = rid2
            dict["markLabel"].stringValue = strMarkHeading
            dict["lineColor"].stringValue = dataSetLine2Color
            dict["dataSetArray"] = JSON(arrayDataSet2)
            dict["category"].stringValue = "con"
            dict["chart_icon"].intValue = chartIcon
            dict["factor"].stringValue = factor

            let img = getSelectedCar(chartIcon : chartIcon)
            let tintedImage = img.mask(with: getColorForCar(color:dict["icon_color"].stringValue))
            let score = dict["score"].doubleValue

            if(dict["have_notes"].boolValue)
            {
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : true,showRight:true,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : true,showRight:false,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }
            }else{
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : false,showRight:true,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage,showLeft : false,showRight:false,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }
            }
            dataEntry.data = dict as AnyObject
            array2[i] = dict
            arrayEntryDataSet2.append(dataEntry)
        }
        if(factor == TargetFactor.SD.rawValue)
        {
            arrayRelationSD = array2
        }
        else if(factor == TargetFactor.TOS.rawValue)
        {
            arrayRelationTOS = array2
        }
        else if(factor == TargetFactor.TOO.rawValue)
        {
            arrayRelationTOO = array2
        }
        else{
            arrayRelationSC = array2
        }
        
        var pointsJson = JSON()
        pointsJson["markLabel"].stringValue = strMarkHeading
        pointsJson["dataSet1"].arrayObject = arrayEntryDataSet1
        pointsJson["dataSet2"].arrayObject = arrayEntryDataSet2
        pointsJson["lineColor1"].stringValue = dataSetLine1Color
        pointsJson["lineColor2"].stringValue = dataSetLine2Color
        pointsJson["dataSetHeading1"].stringValue = strDataSetHeading1
        pointsJson["dataSetHeading2"].stringValue = strDataSetHeading2
        pointsJson["xLabels"].arrayObject = arrayXlabels
        
        //---data set on chart-
        let lineColor1 =  hexStringToUIColor(hex: dataSetLine1Color)
        let lineColor2 =  hexStringToUIColor(hex: dataSetLine2Color)
        
        let dataGenSet = dataSetCreate(values : arrayEntryDataSet1 , dataSetHeading : strDataSetHeading1 , lineColor : lineColor1)
        let dataConSet = dataSetCreate(values : arrayEntryDataSet2 , dataSetHeading : strDataSetHeading2 , lineColor : lineColor2)
        //---- end ---//
        let data = LineChartData(dataSets: [dataGenSet,dataConSet])
        data.setValueTextColor(.clear)
        data.setValueFont(.systemFont(ofSize: 9))
        chart.data = data
        chart.notifyDataSetChanged()
        if(!isAddNotesReload)
        {
            chart.animate(xAxisDuration: 2.0)
        }
    }
    
    //MARK:- self/school chart data set
    func setUpData(dataDict : JSON)
    {
        //----SD----//
        
        var mainData = dataDict
        
        var dictSD = mainData["SD"]
        print("dictSD:\(dictSD)")
        
        var dictgenSD = dictSD["gen"]
        dictgenSD["rname"].stringValue = decreptedString(string: dictgenSD["rname"].stringValue)
        
        var dictConSD = dictSD["con"]
         dictConSD["rname"].stringValue = decreptedString(string: dictConSD["rname"].stringValue)
        
        let strSDdataSetHeading = getCommonString(key: "Self_disclosure_key")

        var arraySDDataSet1 = dictgenSD["detail"].arrayValue
        for i in 0..<arraySDDataSet1.count{
            var dict = arraySDDataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySDDataSet1[i] = dict
        }
        dictgenSD["detail"] = JSON(arraySDDataSet1)
        
        var arraySDDataSet2 = dictConSD["detail"].arrayValue
        for i in 0..<arraySDDataSet2.count{
            var dict = arraySDDataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySDDataSet2[i] = dict
        }
       dictConSD["detail"] = JSON(arraySDDataSet2)
        
        dictSD["con"] = dictConSD
        dictSD["gen"] = dictgenSD
        mainData["SD"] = dictSD
        print("dictSD :\(dictSD)")
        
        //----TOS----//
        var dictTOS = mainData["TOS"]
        var dictgenTOS = dictTOS["gen"]
         dictgenTOS["rname"].stringValue = decreptedString(string: dictgenTOS["rname"].stringValue)
        
        var dictConTOS = dictTOS["con"]
         dictConTOS["rname"].stringValue = decreptedString(string: dictConTOS["rname"].stringValue)
        
        let strTOSdataSetHeading = getCommonString(key: "Trust_of_self_key")
        
        var arrayTOSDataSet1 = dictgenTOS["detail"].arrayValue
        for i in 0..<arrayTOSDataSet1.count{
            var dict = arrayTOSDataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOSDataSet1[i] = dict
        }
        dictgenTOS["detail"] = JSON(arrayTOSDataSet1)
        
        var arrayTOSDataSet2 = dictConTOS["detail"].arrayValue
        for i in 0..<arrayTOSDataSet2.count{
            var dict = arrayTOSDataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOSDataSet2[i] = dict
        }
        
        dictConTOS["detail"] = JSON(arrayTOSDataSet2)
        dictTOS["con"] = dictConTOS
        dictTOS["gen"] = dictgenTOS
        mainData["TOS"] = dictTOS
        
        //----TOO----//
        var dictTOO = mainData["TOO"]
        var dictgenTOO = dictTOO["gen"]
        dictgenTOO["rname"].stringValue = decreptedString(string: dictgenTOO["rname"].stringValue)
        var dictConTOO = dictTOO["con"]
        dictConTOO["rname"].stringValue = decreptedString(string: dictConTOO["rname"].stringValue)

        let strTOOdataSetHeading = getCommonString(key: "Trust_of_others_key")
        
        var arrayTOODataSet1 = dictgenTOO["detail"].arrayValue
        for i in 0..<arrayTOODataSet1.count{
            var dict = arrayTOODataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOODataSet1[i] = dict
        }
        
        dictgenTOO["detail"] = JSON(arrayTOODataSet1)
        
        var arrayTOODataSet2 = dictConTOO["detail"].arrayValue
        for i in 0..<arrayTOODataSet2.count{
            var dict = arrayTOODataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arrayTOODataSet2[i] = dict
        }
        
         dictConTOO["detail"] = JSON(arrayTOODataSet2)
         dictTOO["con"] = dictConTOO
         dictTOO["gen"] = dictgenTOO
         mainData["TOO"] = dictTOO
        
        
        //----SC----//
        var dictSC = mainData["SC"]
        var dictgenSC = dictSC["gen"]
        dictgenSC["rname"].stringValue = decreptedString(string: dictgenSC["rname"].stringValue)
        var dictConSC = dictSC["con"]
        dictConSC["rname"].stringValue = decreptedString(string: dictConSC["rname"].stringValue)
        
        let strSCdataSetHeading = getCommonString(key: "Seeking_changes_key")
        
        var arraySCDataSet1 = dictgenSC["detail"].arrayValue
        for i in 0..<arraySCDataSet1.count{
            var dict = arraySCDataSet1[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySCDataSet1[i] = dict
        }
        dictgenSC["detail"] = JSON(arraySCDataSet1)
        
        var arraySCDataSet2 = dictConSC["detail"].arrayValue
        for i in 0..<arraySCDataSet2.count{
            var dict = arraySCDataSet2[i]
            dict["score"].stringValue = decreptedString(string: dict["score"].stringValue)
            arraySCDataSet2[i] = dict
        }
        
        dictConSC["detail"] = JSON(arraySCDataSet2)
        dictSC["con"] = dictConSC
        dictSC["gen"] = dictgenSC
        mainData["SC"] = dictSC
        
        //TODO: - Relation scroes
        self.dictRelationScores = mainData
        
        //---Setup glowing
        
        if(dictSD["is_glow"].boolValue)
        {
            btnSDAddTarget.isSelected = true
        }
        if(dictTOS["is_glow"].boolValue)
        {
            btnTOSAddTarget.isSelected = true
        }
        if(dictTOO["is_glow"].boolValue)
        {
            btnTOOAddTarget.isSelected = true
        }
        if(dictSC["is_glow"].boolValue)
        {
            btnSCAddTarget.isSelected = true
        }
        setBlinkButton()
        ///
        ///
        //----Setup reward view
        //TODO: - static name check for self
        //--start reward text
        //*****
        if(dictgenSD["rname"].stringValue.lowercased() == "self")
        {
            showHideEmojiView(view: vwSelfReward, reward: dictgenSD["reward"].intValue)
        }else{
            showHideEmojiView(view: vwSelfReward, reward: dictConSD["reward"].intValue)
        }
        
        //---Data point setup
        let mergeXLabelsArray = arraySDDataSet1 + arraySDDataSet2 + arrayTOSDataSet1 + arrayTOSDataSet2 + arrayTOODataSet1 + arrayTOODataSet2 + arraySCDataSet1 + arraySCDataSet2
        
        arrayXLabels = []
        arrayXLabels = xLabelsArray(mergeXLabelsArray: mergeXLabelsArray)
        print("arrayXLabels count - ",arrayXLabels.count)
        
        chartViewScore.xAxis.valueFormatter = DayAxisValueFormatter(chart:chartViewScore,array:arrayXLabels)
        chartViewScore.xAxis.wordWrapEnabled = false
        let marker = XYMarkerView(color: UIColor.lightGray,
                                  font: MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 6),
                                  textColor: .white,
                                  insets: UIEdgeInsets(top: 8, left: 0, bottom: 20, right: 8),
                                  xAxisValueFormatter: DayAxisValueFormatter(chart:chartViewScore,array:arrayXLabels))

        chartViewScore.marker = marker
        
        ////Set Data set array
        //----SD--//
        appendSingleDataSet(strMarkHeading:strSDdataSetHeading,arrayDataSet : arraySDDataSet1,strDataSetHeading:strSDdataSetHeading + " - \(dictgenSD["rname"].stringValue)",dataSetLineColor : GlobalVariables.SDLineColor,category:"gen",dict:dictgenSD,factor:TargetFactor.SD.rawValue)
        
        appendSingleDataSet(strMarkHeading:strSDdataSetHeading,arrayDataSet : arraySDDataSet2,strDataSetHeading:strSDdataSetHeading + " - \(dictConSD["rname"].stringValue)",dataSetLineColor : GlobalVariables.SDLineColor,category:"con",dict:dictConSD,factor:TargetFactor.SD.rawValue)

        //----TOS--//
        appendSingleDataSet(strMarkHeading:strTOSdataSetHeading,arrayDataSet : arrayTOSDataSet1,strDataSetHeading:strTOSdataSetHeading + " - \(dictgenTOS["rname"].stringValue)",dataSetLineColor : GlobalVariables.TOSLineColor ,category:"gen",dict:dictgenTOS,factor:TargetFactor.TOS.rawValue)
        
        appendSingleDataSet(strMarkHeading:strTOSdataSetHeading,arrayDataSet : arrayTOSDataSet2,strDataSetHeading:strTOSdataSetHeading + " - \(dictConTOS["rname"].stringValue)",dataSetLineColor : GlobalVariables.TOSLineColor,category:"con",dict:dictConTOS,factor:TargetFactor.TOS.rawValue)
        
        //----TOO--//
        appendSingleDataSet(strMarkHeading:strTOOdataSetHeading,arrayDataSet : arrayTOODataSet1,strDataSetHeading:strTOOdataSetHeading + " - \(dictgenTOO["rname"].stringValue)",dataSetLineColor : GlobalVariables.TOOLineColor,category:"gen",dict:dictgenTOO,factor:TargetFactor.TOO.rawValue)
        
        appendSingleDataSet(strMarkHeading:strTOOdataSetHeading,arrayDataSet : arrayTOODataSet2,strDataSetHeading:strTOOdataSetHeading + " - \(dictConTOO["rname"].stringValue)",dataSetLineColor : GlobalVariables.TOOLineColor,category:"con",dict:dictConTOO,factor:TargetFactor.TOO.rawValue)
        
        //----SC--//
        appendSingleDataSet(strMarkHeading:strSCdataSetHeading,arrayDataSet : arraySCDataSet1,strDataSetHeading:strSCdataSetHeading + " - \(dictgenSC["rname"].stringValue)",dataSetLineColor : GlobalVariables.SCLineColor,category:"gen",dict:dictgenSC,factor:TargetFactor.SC.rawValue)
        
        appendSingleDataSet(strMarkHeading:strSCdataSetHeading,arrayDataSet : arraySCDataSet2,strDataSetHeading:strSCdataSetHeading + " - \(dictConSC["rname"].stringValue)",dataSetLineColor : GlobalVariables.SCLineColor,category:"con",dict:dictConSC,factor:TargetFactor.SC.rawValue)
      
        setSelectedDataSetArray()
    }
 
   
    func appendSingleDataSet(strMarkHeading:String,arrayDataSet : [JSON],strDataSetHeading:String,dataSetLineColor : String,category : String,dict:JSON,factor:String)
    {
        var array = arrayDataSet.sorted {$0["dateTobedisplay"].intValue < $1["dateTobedisplay"].intValue}
        var arrayEntryDataSet : [ChartDataEntry] = []
        var arrayEntryDataSetWithoutLatest : [ChartDataEntry] = []
        let relationID = dict["rid"].stringValue
        print("---------  \(strMarkHeading)  ---------")

        for i in 0..<array.count
        {
            var dict = array[i]
            
            let strDateLocal = dict["dateTobedisplay"].stringValue
            let strDateValueLocal = strDateLocal.prefix(8)
            
            //d MMM,yyyy
            let strDateValue = StringToConvertedStringDate(strDate: String(strDateValueLocal), strDateFormat: "yyyyMMdd", strRequiredFormat: "d MMM,yyyy")
            
            let XIndex = arrayXLabels.index(of: strDateValue)
            let dataEntry = ChartDataEntry(x: Double(XIndex!), y: dict["score"].doubleValue)
            
            let score = dict["score"].doubleValue
            dict["markLabel"].stringValue = strMarkHeading
            dict["rid"].stringValue = relationID
            dict["lineColor"].stringValue = dataSetLineColor
            dict["dataSetArray"] = JSON(arrayDataSet)
            dict["category"].stringValue = category
            dict["chart_icon"].intValue = chartIcon
            dict["factor"].stringValue = factor
            dataEntry.data = dict as AnyObject

            let img = getSelectedCar(chartIcon: chartIcon)
            var tintedImage : UIImage!
            tintedImage = img.mask(with: getColorForCar(color:dict["icon_color"].stringValue))

            if(dict["have_notes"].boolValue)
            {
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage!,showLeft : true,showRight:true,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage!,showLeft : true,showRight:false,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }
                
            }else{
                if(score >= 12 || score <= 3)
                {
                    let mergeImage = createViewForPoint(tintedImage:tintedImage!,showLeft : false,showRight:true,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }else{
                    let mergeImage = createViewForPoint(tintedImage:tintedImage!,showLeft : false,showRight:false,chartIcon: chartIcon).convertViewToImage(carIcon : chartIcon)
                    dataEntry.icon = mergeImage
                }
            }
            
            arrayEntryDataSet.append(dataEntry)
            if(dict["is_seen"].boolValue == true)
            {
                arrayEntryDataSetWithoutLatest.append(dataEntry)
            }
            array[i] = dict
        }
        
        var pointsJson = JSON()
        pointsJson["markLabel"].stringValue = strMarkHeading
        pointsJson["category"].stringValue = category
        pointsJson["dataSet"].arrayObject = arrayEntryDataSet
        pointsJson["lineColor"].stringValue = dataSetLineColor
        pointsJson["dataSetHeading"].stringValue = strDataSetHeading
        pointsJson["xLabels"].arrayObject = arrayXLabels
        pointsJson["dataDict"] = dict
        pointsJson["dataArray"] = JSON(array)
        arrayMainScore.append(pointsJson)
        pointsJson["dataSet"].arrayObject = arrayEntryDataSetWithoutLatest
        arrayMainScoreWithoutLastest.append(pointsJson)
        if(category == "gen")
        {
            if(factor == TargetFactor.SD.rawValue)
            {
                arrayDefaultSelfSD = array
            }
            else if(factor == TargetFactor.TOS.rawValue)
            {
                arrayDefaultSelfTOS = array
            }
            else if(factor == TargetFactor.TOO.rawValue)
            {
                arrayDefaultSelfTOO = array
            }
            else{
                arrayDefaultSelfSC = array
            }
        }
        else
        {
            if(factor == TargetFactor.SD.rawValue)
            {
                arrayDefaultSD = array
            }
            else if(factor == TargetFactor.TOS.rawValue)
            {
                arrayDefaultTOS = array
            }
            else if(factor == TargetFactor.TOO.rawValue)
            {
                arrayDefaultTOO = array
            }
            else{
                arrayDefaultSC = array
            }
        }
    }
    
    //MARK: - Selected Data Set setup
    func setSelectedDataSetArray()
    {
        arrayScore = []
        var arrayShowScore : [JSON] = []
        if(!isShowLatestButton)
        {
            print("arrayMainScore selected count - ",arrayMainScoreWithoutLastest.count)
            arrayShowScore = arrayMainScore
        }else{
            print("arrayMainScoreWithoutLastest selected count - " ,arrayMainScoreWithoutLastest.count)
            arrayShowScore = arrayMainScoreWithoutLastest
        }
        
        for i in 0..<arrayShowScore.count
        {
            let dict = arrayShowScore[i]
            if(btnSD.isSelected)
            {
                if(dict["markLabel"].stringValue == getCommonString(key: "Self_disclosure_key"))
                {
                    if(dict["category"].stringValue == "gen")
                    {
                        if(btnShowSelf.isSelected)
                        {
                            arrayScore.append(dict)
                        }
                    }else{
                        arrayScore.append(dict)
                    }
                }
            }
            if(btnTOS.isSelected)
            {
                if(dict["markLabel"].stringValue == getCommonString(key: "Trust_of_self_key"))
                {
                    if(dict["category"].stringValue == "gen")
                    {
                        if(btnShowSelf.isSelected)
                        {
                            arrayScore.append(dict)
                        }
                    }else{
                        arrayScore.append(dict)
                    }
                }
            }
            if(btnTOO.isSelected)
            {
                if(dict["markLabel"].stringValue == getCommonString(key: "Trust_of_others_key"))
                {
                    if(dict["category"].stringValue == "gen")
                    {
                        if(btnShowSelf.isSelected)
                        {
                            arrayScore.append(dict)
                        }
                    }else{
                        arrayScore.append(dict)
                    }
                }
            }
            if(btnSC.isSelected)
            {
                if(dict["markLabel"].stringValue == getCommonString(key: "Seeking_changes_key"))
                {
                    if(dict["category"].stringValue == "gen")
                    {
                        if(btnShowSelf.isSelected)
                        {
                            arrayScore.append(dict)
                        }
                    }else{
                        arrayScore.append(dict)
                    }
                }
            }
        }
        if(arrayScore.count == 0)
        {
            chartViewScore.data = nil
            chartViewScore.clear()
        }else{
            chartDataSet()
        }
    }
    
    func chartDataSet()
    {
        print("Score array count - ",arrayScore.count)
        var arrayDataSet : [LineChartDataSet] = []
        for i in 0..<arrayScore.count
        {
            let dict = arrayScore[i]
            let dictData = dict["dataDict"]
            //TODO: - Emoji view set up chceck if all factor selected
            if(dictData["rname"].stringValue == categoryDict?["rname"].stringValue)
            {
                showHideEmojiView(view: vwSelfReward, reward: dictData["reward"].intValue)
            }
            print("*****************")
            let lineColor1 =  hexStringToUIColor(hex: dict["lineColor"].stringValue)
            let strDataSetHeading1 =  dict["dataSetHeading"].stringValue
            let dataSet1 =  dict["dataSet"].arrayObject as? [ChartDataEntry]
            let dataGenSet = dataSetCreate(values : dataSet1! , dataSetHeading : strDataSetHeading1 , lineColor : lineColor1.withAlphaComponent(1.0))
            if((dict["category"].stringValue ) != "gen")
            {
                dataGenSet.lineDashLengths = [5, 2.5]
                dataGenSet.highlightLineDashLengths = [5, 2.5]
            }
            dataGenSet.circleRadius = 0
            arrayDataSet.append(dataGenSet)
        }
        
        let data = LineChartData(dataSets: arrayDataSet)
        data.setValueTextColor(.clear)
        data.setValueFont(.systemFont(ofSize: 6))
        chartViewScore.clearValues()
        chartViewScore.data = data
        
        chartViewScore.notifyDataSetChanged()
        if(!isAddNotesReload)
        {
            chartViewScore.animate(xAxisDuration: 2.0)
        }
    }
}

//MARK: - Chart selection button action
extension ReportVC
{
    @IBAction func SDSelectAction(_ sender : UIButton)
    {
        if(btnSD.isSelected)
        {
            if(arraySelectedFactor.contains("sd"))
            {
                arraySelectedFactor.remove(at: arraySelectedFactor.index(of: "sd")!)
            }
        }
        else
        {
            arraySelectedFactor.append("sd")
        }
        resetSelected()
        btnSD.isSelected = !btnSD.isSelected
        setSelected(button : btnSD)
    }
    
    @IBAction func TOSSelectAction(_ sender : UIButton)
    {
        if(btnTOS.isSelected)
        {
            if(arraySelectedFactor.contains("tos"))
            {
                arraySelectedFactor.remove(at: arraySelectedFactor.index(of: "tos")!)
            }
        }
        else
        {
            arraySelectedFactor.append("tos")
        }
        resetSelected()
        btnTOS.isSelected = !btnTOS.isSelected
        setSelected(button : btnTOS)
    }
    
    @IBAction func TOOSelectAction(_ sender : UIButton)
    {
        if(btnTOO.isSelected)
        {
            if(arraySelectedFactor.contains("too"))
            {
                arraySelectedFactor.remove(at: arraySelectedFactor.index(of: "too")!)
            }
        }
        else
        {
            arraySelectedFactor.append("too")
        }
        resetSelected()
        btnTOO.isSelected = !btnTOO.isSelected
        setSelected(button : btnTOO)
    }
    
    @IBAction func SCSelectAction(_ sender : UIButton)
    {
        if(btnSC.isSelected)
        {
            if(arraySelectedFactor.contains("sc"))
            {
                arraySelectedFactor.remove(at: arraySelectedFactor.index(of: "sc")!)
            }
        }
        else
        {
            arraySelectedFactor.append("sc")
        }
        resetSelected()
        btnSC.isSelected = !btnSC.isSelected
        setSelected(button : btnSC)
    }
    
    func resetSelected()
    {
        isAddNotesReload = false
        chartViewScore.fitScreen()
    }
    
    func setSelected(button : UIButton)
    {
        setSelectedDataSetArray()
    }
}

//MARK:- Chartview methods
extension ReportVC 
{
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight)
    {
        timer.invalidate()
        
        isFromTargetBack = false
        lastClickedDataEntry = entry
        print("chart \(chartView.tag) selected value \(entry)")
       /* chartView.centerViewToAnimated(xValue: entry.x, yValue: entry.y,
                                            axis: chartView.data!.getDataSetByIndex(highlight.dataSetIndex).axisDependency,
                                            duration: 1)*/
        
        print("entry value - ",entry.data as Any)
        var dict = entry.data as? JSON ?? JSON()
        
        if(dict["markLabel"].stringValue == getCommonString(key: "Self_disclosure_key"))
        {
            dict["factor"].stringValue = "sd"
        }else if(dict["markLabel"].stringValue == getCommonString(key: "Trust_of_self_key"))
        {
            dict["factor"].stringValue = "tos"
        }else if(dict["markLabel"].stringValue == getCommonString(key: "Trust_of_others_key"))
        {
            dict["factor"].stringValue = "too"
        }else if(dict["markLabel"].stringValue == getCommonString(key: "Seeking_changes_key"))
        {
            dict["factor"].stringValue = "sc"
        }
        
        print("lastOpenNotesDataDict - ",lastOpenNotesDataDict["score_id"].intValue)
        print("dict - ",dict["score_id"].intValue)
        
        if(lastOpenNotesDataDict["score_id"].intValue != dict["score_id"].intValue)
        {
            isclickedFirstTime = true
        }
        else if(lastOpenNotesDataDict["score_id"].intValue == dict["score_id"].intValue && lastChartViewSelected != chartView)
        {
            isclickedFirstTime = true
        }
        
        /*let vc = objReportStoryboard.instantiateViewController(withIdentifier: "NotesReportVC") as! NotesReportVC
        print("point data - ",dict)
        vc.dictData = dict*/
//        vc.categoryData = categoryDict ?? JSON()
        
        dictPointsData = dict
        lastOpenNotesDataDict = dict
        lastChartViewSelected = chartView as! LineChartView
        
//        arrayRandomDataPoints = dict[""]
        
        if(!isclickedFirstTime && lastChartViewSelected == chartView)
        {
            //self.navigationController?.pushViewController(vc, animated: true)
        }
        if(isclickedFirstTime)
        {
            isclickedFirstTime = false
        }

        //--------
    }
    func chartValueNothingSelected(_ chartView: ChartViewBase)
    {
        NSLog("chartValueNothingSelected");
        let vc = objReportStoryboard.instantiateViewController(withIdentifier: "NotesReportVC") as! NotesReportVC
        print("point data - ",dictPointsData as Any)
        
        targetDict = JSON()
        targetDict["factor"].stringValue = dictPointsData!["factor"].stringValue
        
        var currentScore = ""
        if(categoryDict?["default_name"].stringValue.lowercased() == "self")
        {
            currentScore = getCurrentScore(factor: dictPointsData!["factor"].stringValue, category: "gen")
        }else{
            currentScore = getCurrentScore(factor: dictPointsData!["factor"].stringValue, category: "con")
        }
        targetDict["currentScore"].stringValue = currentScore

        vc.dictData = dictPointsData ?? JSON()
        vc.carIcon = (dictPointsData?["chart_icon"].intValue)!
        vc.categoryDict = categoryDict ?? JSON()
        vc.currentScore = currentScore
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func chartScaled(_ chartView: ChartViewBase, scaleX: CGFloat, scaleY: CGFloat)
    {
//        print("*********")
//        print("scaleX - ",scaleX)
//        print("-------")
//        print("scaleY - ",scaleY)
    }
    func chartTranslated(_ chartView: ChartViewBase, dX: CGFloat, dY: CGFloat) {
    }
    func setVisibleXRange(minXRange: CGFloat, maxXRange: CGFloat)
    {
    }
}
//MARK: - Get Current Score
extension ReportVC
{
    func getCurrentScore(factor:String,category:String) -> String
    {
        print("dictRelationScores - ",dictRelationScores as Any)
        //Factor Dict
        var factorDict = JSON()
        if(factor.uppercased() == "SD")
        {
            factorDict = dictRelationScores!["SD"]
        }else if(factor.uppercased() == "TOS")
        {
            factorDict = dictRelationScores!["TOS"]
        }else if(factor.uppercased() == "TOO")
        {
            factorDict = dictRelationScores!["TOO"]
        }else{
            factorDict = dictRelationScores!["SC"]
        }
        
        //Category Dict
        var categoryDict = JSON()
        if(category == "gen")
        {
            categoryDict = factorDict["gen"]
        }else{  //con category
            categoryDict = factorDict["con"]
        }
        
        //Get Array
        var arrayPoints = categoryDict["detail"].arrayValue
        arrayPoints = arrayPoints.sorted {$0["dateTobedisplay"].intValue < $1["dateTobedisplay"].intValue}
        
        if(arrayPoints.count > 0)
        {
            let lastPointDict = arrayPoints[arrayPoints.count - 1]
            return lastPointDict["score"].stringValue
        }
        return ""
    }
}

//MARK: - Button action
extension ReportVC
{
    @IBAction func btnShowRelationShipSteeringAction(_ sender : UIButton)
    {
        btnShowRelationSteering.isSelected = true
        setUpCategoryData(dataDict: self.dictDataStoreForRelationScores)
    }
    @IBAction func btnShowSelfAction(_ sender : UIButton)
    {
        chartViewScore.fitScreen()
        isAddNotesReload = false
        sender.isSelected = !sender.isSelected
        
        setSelectedDataSetArray()
    }
    @IBAction func btnShowLatestAction(_ sender : UIButton)
    {
        //--------
        chartViewScore.fitScreen()
        isAddNotesReload = false
        self.chartViewScore.clear()
        isShowLatestButton = false
        setSelectedDataSetArray()
    }
    @objc func btnAddTargetAction(_ sender : UIButton)
    {
        if(!isAllowToAddSignpost)
        {
            showAlert(strMessage : "Please get premium version to use this functionality")
        }else{
            let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "AddTargetVC") as! AddTargetVC
            if(sender.tag == 0)
            {
                vc.selectedFactor = TargetFactor.SD
            }else if(sender.tag == 1)
            {
                vc.selectedFactor = TargetFactor.TOS
            }else if(sender.tag == 2)
            {
                vc.selectedFactor = TargetFactor.TOO
            }else if(sender.tag == 3)
            {
                vc.selectedFactor = TargetFactor.SC
            }
            targetDict = JSON()
            targetDict["factor"].stringValue = vc.selectedFactor.rawValue
            vc.categoryDict = categoryDict
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func btnTargetAddAction(_ sender : UIButton)  //for UI button action
    {
        if(!isAllowToAddSignpost)
        {
            showAlert(strMessage : "Please get premium version to use this functionality")
            
        }else{
            let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "AddTargetVC") as! AddTargetVC
            if(sender == btnSDAddTarget || sender == btnRelationSDAddTarget || sender == btnSDAddTargetHidden || sender == btnSDAddTargetRelationHidden)
            {
                vc.selectedFactor = TargetFactor.SD
            }else if(sender == btnTOSAddTarget || sender == btnRelationTOSAddTarget || sender == btnTOSAddTargetHidden || sender == btnTOSAddTargetRelationHidden)
            {
                vc.selectedFactor = TargetFactor.TOS
            }else if(sender == btnTOOAddTarget || sender == btnRelationTOOAddTarget || sender == btnTOOAddTargetHidden || sender == btnTOOAddTargetRelationHidden)
            {
                vc.selectedFactor = TargetFactor.TOO
            }else if(sender == btnSCAddTarget || sender == btnRelationSCAddTarget || sender == btnSCAddTargetHidden || sender == btnSCAddTargetRelationHidden)
            {
                vc.selectedFactor = TargetFactor.SC
            }
            targetDict = JSON()
            targetDict["factor"].stringValue = vc.selectedFactor.rawValue
            
            var currentScore = ""
            if(categoryDict?["default_name"].stringValue.lowercased() == "self")
            {
                currentScore = getCurrentScore(factor: vc.selectedFactor.rawValue, category: "gen")
            }else{
                currentScore = getCurrentScore(factor: vc.selectedFactor.rawValue, category: "con")
            }
            
            print("currentScore  report - ",currentScore)
            //        let currentScore = getCurrentScore(factor: vc.selectedFactor.rawValue, category: "con")
            //        print("categoryDict - ",categoryDict)
            vc.categoryDict = categoryDict
            vc.currentScore = currentScore
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func showAlert(strMessage : String)
    {
        let alert = UIAlertController(title: kCommonAlertTitle, message: strMessage, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
        }))
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
//MARK: - textview methods
extension ReportVC : UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView)
    {
        
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool
    {
        
        if(text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}
//MARK:- Service
extension ReportVC 
{
    func getReportsService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            if(!isAddNotesReload)
            {
                self.vwNoData.isHidden = false
            }
            print("categoryDict - ",categoryDict ?? JSON())
            
            let relationId = categoryDict?["rid"].stringValue ?? ""
            let relationName = categoryDict?["rname"].stringValue ?? ""
            
            let param : [String:String] = ["rid" : relationId,
                                           "rname" : relationName
            ]
            
            ReportService().GetReportData(param : param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        if(!self.isAddNotesReload)
                        {
                            self.vwNoData.isHidden = true
                        }
                        //TODO:- Add defaults name
                        if(self.categoryDict?["default_name"].stringValue.lowercased() == "self" || self.categoryDict?["default_name"].stringValue.lowercased() == "school")
                        {
                            self.arrayMainScore = []
                            self.arrayMainScoreWithoutLastest = []
                            self.chartViewScore.clear()
                            self.showGreyBoxforSelfSchool(dict:json["data"])
                            self.chartIcon = json["data"]["chart_icon"].intValue
                            self.isAllowToAddSignpost = json["data"]["is_set_signpost"].boolValue
                            self.setUpData(dataDict : json["data"]["score_detail"])
                        }else{
                            self.arrayMainScore = []
                            self.chartViewScore1.clear()
                            self.chartViewScore2.clear()
                            self.chartViewScore3.clear()
                            self.chartViewScore4.clear()
                            self.showGreyBoxfor4Charts(dict:json["data"])
                            self.dictDataStoreForRelationScores = json["data"]["score_detail"]
                            self.chartIcon = json["data"]["chart_icon"].intValue
                            self.isAllowToAddSignpost = json["data"]["is_set_signpost"].boolValue
                            self.setUpCategoryData(dataDict : json["data"]["score_detail"])
                        }
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        self.lblErrorMessage.text = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func showGreyBoxfor4Charts(dict:JSON)
    {
        grayCleredInitialCount = 0
        if dict["sd_tree_noti"].intValue == 0{
            self.vwSDRelationPhase2Popup.isHidden = false
            grayCleredInitialCount = grayCleredInitialCount + 1
        }else{
            self.vwSDRelationPhase2Popup.isHidden = true
        }
        if dict["sc_tree_noti"].intValue == 0{
            self.vwSCRelationPhase2Popup.isHidden = false
            grayCleredInitialCount = grayCleredInitialCount + 1
        }else{
            self.vwSCRelationPhase2Popup.isHidden = true
        }
        if dict["too_tree_noti"].intValue == 0{
            self.vwTOORelationPhase2Popup.isHidden = false
            grayCleredInitialCount = grayCleredInitialCount + 1
        }else{
            self.vwTOORelationPhase2Popup.isHidden = true
        }
        if dict["tos_tree_noti"].intValue == 0{
            self.vwTOSRelationPhase2Popup.isHidden = false
            grayCleredInitialCount = grayCleredInitialCount + 1
        }else{
             self.vwTOSRelationPhase2Popup.isHidden = true
        }
    }
    
    func showGreyBoxforSelfSchool(dict:JSON)
    {
        if dict["rel_tree_noti"].intValue == 0{
            self.vwDefaultPhase2PopUp.isHidden = false
        }else{
            self.vwDefaultPhase2PopUp.isHidden = true
        }
    }
}
