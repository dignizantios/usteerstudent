//
//  CoachVC.swift
//  USteerStudent
//
//  Created by Usteer on 2/14/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

enum checkCoachParentVC
{
    case sideMenu
    case chat
}

class CoachTableviewCell : UITableViewCell
{
    //MARK: - Outlets
    @IBOutlet weak var lblCoachName: UILabel!
    @IBOutlet weak var btnSendRequest: UIButton!
    @IBOutlet weak var btnTerminateRequest: UIButton!

    //MARK: -
    override func awakeFromNib() {
        
        lblCoachName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblCoachName.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        [btnSendRequest,btnTerminateRequest].forEach { (button) in
            button?.backgroundColor = MySingleton.sharedManager.themeGreenColor
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 8)
            button?.setTitleColor(.white, for: .normal)
            button?.layer.cornerRadius = (btnSendRequest.bounds.size.height) / 2
            button?.layer.masksToBounds = true
        }
    }
    
    func infoButtonUI(color : UIColor)
    {
        btnSendRequest.layer.cornerRadius = (btnSendRequest.bounds.size.height) / 2
        btnSendRequest.layer.masksToBounds = true
        btnSendRequest.layer.borderWidth = 1
        btnSendRequest.layer.borderColor = color.cgColor
    }
}

class CoachVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tblCoach: UITableView!
    
    //MARK: - Variables
    var arrayCoach : [JSON] = []
    var strErrorMessage = ""
    var selectedCoachParentVC = checkCoachParentVC.chat
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:#selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
       self.getCoachList(isFromPushReload: false)
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblCoach.addSubview(self.refreshControl)
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        isOnCoachListScreen = true
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadCoachList), name: NSNotification.Name(rawValue: "reloadCoachList"), object: nil)

        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isOnCoachListScreen = false
    }
}
//MARK: - Notification trigger methods
extension CoachVC
{
    @objc func reloadCoachList()
    {
        print("Coach list table relaod")
        getCoachList(isFromPushReload : true)
    }
}
//MARK: - UI Setup
extension CoachVC
{
    func setUpUI()
    {
        if(selectedCoachParentVC == .sideMenu)
        {
            setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Tutor_list_key"))
        }else{
            setUpNavigationBarWithTitleAndBack(strTitle : getCommonString(key: "Tutor_list_key"))
        }
        
        tblCoach.tableFooterView = UIView()
        
        
        if(Defaults.value(forKey: "isLoadInfoFirstTime") == nil)
        {
            presentInfoWindow()
            Defaults.setValue(false, forKey: "isLoadInfoFirstTime")
        }
        
        getCoachList(isFromPushReload: false)
    }
    func presentInfoWindow()
    {
        var strMessage = "Select your own tutor from the list and send them a request to link to you. You can only link to ONE tutor."
        
        if(getUserDetail("is_private") == "1")
        {
            strMessage = "You have to be a member of a school or university which is using USTEER to link up"
        }
        
        let obj = InfoPopUpVC()
        obj.popUpDelegate = self
        obj.strMessage = strMessage
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
    
}
//MARK: - Pop up delegate
extension CoachVC : InfoPopUpDelegate
{
    func dismissPopUp()
    {
        
    }
}
//MARK: - UITableview Delegate
extension CoachVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayCoach.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            lbl.numberOfLines = 5
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayCoach.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:CoachTableviewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CoachTableviewCell
        cell.selectionStyle = .none
        
        let dict = arrayCoach[indexPath.row]
        cell.lblCoachName.text = dict["fullname"].stringValue
        cell.btnSendRequest.tag = indexPath.row
        cell.btnTerminateRequest.tag = indexPath.row
        
        cell.btnSendRequest.addTarget(self, action: #selector(btnSendRequestAction), for: .touchUpInside)
        cell.btnTerminateRequest.addTarget(self, action: #selector(btnTerminateRequestAction), for: .touchUpInside)

        /*
         "status_detail" : {
         "3" : "Request decline",
         "1" : "Request sent",
         "4" : "Request terminated",
         "2" : "Request accepted"
         },
        */
        
        cell.btnTerminateRequest.isHidden = true
        cell.btnSendRequest.isUserInteractionEnabled = false
        if(dict["status"].stringValue == "0")
        {
            cell.btnSendRequest.backgroundColor = .clear
            cell.btnSendRequest.setTitle("Unread".uppercased(), for: .normal)
            cell.btnSendRequest.setTitleColor(MySingleton.sharedManager.themeBlueColor, for: .normal)
            

        }else if(dict["status"].stringValue == "1")
        {
            cell.btnSendRequest.backgroundColor = .clear
            cell.btnSendRequest.setTitle("WAITING TO BE ACCEPTED", for: .normal)
            cell.btnSendRequest.setTitleColor(MySingleton.sharedManager.themeBlueColor, for: .normal)
            cell.btnSendRequest.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 6)
            cell.infoButtonUI(color: MySingleton.sharedManager.themeBlueColor)
            cell.btnTerminateRequest.isHidden = false
            cell.btnTerminateRequest.setTitle("Disconnect".uppercased(), for: .normal)
            cell.btnTerminateRequest.backgroundColor = MySingleton.sharedManager.themeLightGrayColor
        }else if(dict["status"].stringValue == "2")
        {
            cell.btnSendRequest.backgroundColor = .clear
            cell.btnSendRequest.setTitle("Accepted".uppercased(), for: .normal)
            cell.btnSendRequest.setTitleColor(MySingleton.sharedManager.themeYellowColor, for: .normal)
            cell.btnSendRequest.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 8)

            cell.btnTerminateRequest.isHidden = false
            cell.btnTerminateRequest.setTitle("Disconnect".uppercased(), for: .normal)
            cell.btnTerminateRequest.backgroundColor = MySingleton.sharedManager.themeLightGrayColor
            cell.infoButtonUI(color: MySingleton.sharedManager.themeYellowColor)
            
        }else if(dict["status"].stringValue == "3")
        {
            cell.btnSendRequest.backgroundColor = .clear
            cell.btnSendRequest.setTitleColor(MySingleton.sharedManager.themeRedColor, for: .normal)
            cell.btnSendRequest.setTitle("declined".uppercased(), for: .normal)
            cell.infoButtonUI(color: MySingleton.sharedManager.themeRedColor)
        }
        else if(dict["status"].stringValue == "4")//terminated
        {
            cell.btnSendRequest.backgroundColor = .clear
            cell.btnSendRequest.setTitleColor(.red, for: .normal)
            cell.btnSendRequest.setTitle("terminated".uppercased(), for: .normal)
            cell.infoButtonUI(color: .red)
        }
        else
        {
            cell.btnSendRequest.backgroundColor = MySingleton.sharedManager.themeGreenColor
            cell.btnSendRequest.setTitle("send request".uppercased(), for: .normal)
            cell.btnSendRequest.setTitleColor(UIColor.white, for: .normal)
            cell.infoButtonUI(color: MySingleton.sharedManager.themeGreenColor)
            cell.btnSendRequest.isUserInteractionEnabled = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let dict = arrayCoach[indexPath.row]

        var json = JSON()
        json["user_id"].stringValue = getUserDetail("user_id")
        json["group_id"].stringValue = "0"
        json["name"].stringValue = dict["name"].stringValue
        json["fullname"].stringValue = dict["fullname"].stringValue
        json["other_id"].stringValue = dict["coach_id"].stringValue
        json["is_group"] = JSON(false)
        json["unread_msg"].stringValue = "0"

        if(dict["status"].stringValue == "2")
        {
            let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
            obj.dictChatData = json
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
}
//MARK: - Button action
extension CoachVC
{
    @objc func btnSendRequestAction(_ sender : UIButton)
    {
        let dict = arrayCoach[sender.tag]
        sendRequestToCoach(dict : dict,status:1)
    }
    @objc func btnTerminateRequestAction(_ sender : UIButton)
    {
        let dict = arrayCoach[sender.tag]
        sendRequestToCoach(dict : dict,status:4)
    }
    func setTutorScreen()
    {

        if arrayCoach.contains(where: { (json) -> Bool in
            if(json["status"].stringValue == "2")
            {
                return true
            }
            return false
        }){
            Defaults.setValue(false, forKey: "isShowTutorFirstTime")

        }else{
            Defaults.setValue(true, forKey: "isShowTutorFirstTime")

        }
        
    }
    
}

//MARK: - Service
extension CoachVC
{
    func getCoachList(isFromPushReload : Bool)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            if(!isFromPushReload)
            {
                showLoader()

            }
            
            LinkSchoolService().getCoachList(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        
                        var arrayDetail = json["data"]["user_detail"].arrayValue
                        
                        for i in 0..<arrayDetail.count{
                            
                            var dict = arrayDetail[i]
                            dict["fullname"].stringValue = decreptedString(string: dict["fullname"].stringValue)
                            dict["lastname"].stringValue = decreptedString(string: dict["lastname"].stringValue)
                            dict["name"].stringValue = decreptedString(string: dict["name"].stringValue)
                            arrayDetail[i] = dict
                            
                        }
                        
                        self.arrayCoach = arrayDetail
                        self.setTutorScreen()
                        self.tblCoach.reloadData()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        self.strErrorMessage = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        self.strErrorMessage = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblCoach.reloadData()
                }
                if(!isFromPushReload)
                {
                    self.hideLoader()
                }
                
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func sendRequestToCoach(dict : JSON,status:Int)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            LinkSchoolService().SendRequest(coach_id : dict["coach_id"].stringValue ,school_id : dict["school_id"].stringValue ,status : status,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.getCoachList(isFromPushReload: false)

                    }else if(json["status"].stringValue == "0")
                    {
                        self.strErrorMessage = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblCoach.reloadData()
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
