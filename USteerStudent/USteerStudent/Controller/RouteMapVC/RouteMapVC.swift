//
//  RouteMapVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/8/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

class RouteMapTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    //MARK: -
    override func awakeFromNib() {
        
        lblName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
        lblName.textColor = MySingleton.sharedManager.themeDarkGrayColor
    }
}
class RouteMapVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var tblRelation: UITableView!
    @IBOutlet var btnSubmit: UIButton!

    //Popup
    @IBOutlet var vwAddPopup: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblRelation: UILabel!
    @IBOutlet var txtRelation: UITextField!
    @IBOutlet var btnAdd: UIButton!
    @IBOutlet var vwRelationSeperator: UIView!
    
    //MARK: - Variables
//    var arrayRelation : [JSON] = []
    var strErrorMessage = ""
    var dictRouteMapData : JSON?
    var categoryDict : JSON?
    
    //MARK: - Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tblRelation.register(UINib(nibName: "RouteMapHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "RouteMapHeaderTableViewCell")
        tblRelation.tableFooterView = UIView()
        tblRelation.contentInset = UIEdgeInsetsMake(0, 0, 100, 0)

        setUpUI()
        //
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func viewDidLayoutSubviews()
    {
        btnAdd.layer.cornerRadius = btnAdd.bounds.size.height / 2
        btnAdd.layer.masksToBounds = true
        
        [btnAdd,btnSubmit].forEach({
            $0?.layer.cornerRadius = btnAdd.bounds.size.height / 2
            $0?.layer.masksToBounds = true
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI Setup
extension RouteMapVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Route_map_key").capitalized)
        
        lblTitle.backgroundColor = MySingleton.sharedManager.themeYellowColor
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 20)
        lblTitle.text = getCommonString(key: "Add_relation_key").capitalized
        lblTitle.textColor = .white
        
        lblRelation.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblRelation.textColor = MySingleton.sharedManager.themeYellowColor
        lblRelation.text = getCommonString(key: "Relation_key").uppercased()

        txtRelation.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        txtRelation.textColor = MySingleton.sharedManager.themeDarkGrayColor
        txtRelation.placeholder = getCommonString(key: "Enter_here_key")
        
        vwRelationSeperator.backgroundColor = MySingleton.sharedManager.themeYellowColor

        btnAdd.setTitle(getCommonString(key: "Add_key").uppercased(), for: .normal)
        buttonUISetup(btnAdd, textColor: MySingleton.sharedManager.themeDarkGrayColor)

        btnSubmit.setTitle(getCommonString(key: "Submit_key").uppercased(), for: .normal)
        buttonUISetup(btnSubmit, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        vwAddPopup.isHidden = true
        getRelationService()
    }
}
//MARK: - UITableview Delegate
extension RouteMapVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        return 52
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        let cell:RouteMapHeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RouteMapHeaderTableViewCell") as! RouteMapHeaderTableViewCell
        cell.btnAdd.tag = section
        
        if(section == 0)
        {
            cell.lblHeading.text = getCommonString(key: "Individual_key").uppercased()
        }
        else
        {
            cell.lblHeading.text = getCommonString(key: "Groups_key").uppercased()
        }
        cell.btnAdd.addTarget(self, action: #selector(btnAddAction), for: .touchUpInside)
        return cell.contentView
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
//        if arrayRelation.count == 0
//        {
//
//            let lbl = UILabel()
//            //lbl.text = strErrorMessage
//            lbl.textAlignment = NSTextAlignment.center
//            lbl.textColor = MySingleton.sharedManager.themeYellowColor
//            lbl.center = tableView.center
//            tableView.backgroundView = lbl
//            return 0
//        }

        if(section == 0)
        {
            return dictRouteMapData?["ind"].arrayValue.count ?? 0
        }
        return dictRouteMapData?["grp"].arrayValue.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:RouteMapTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! RouteMapTableViewCell
        
        cell.selectionStyle = .none
        
        cell.btnSelect.setTitle("\(indexPath.section)", for: .disabled)
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(btnSelectAction), for: .touchUpInside)
        
        if(indexPath.section == 0)
        {
            let aryLocal = dictRouteMapData?["ind"].arrayValue
            let dictData = aryLocal![indexPath.row]
            
            cell.lblName.text = dictData["rname"].stringValue
            if(dictData["is_check"].intValue == 1)
            {
                cell.btnSelect.isSelected = true
            }
            else{
                cell.btnSelect.isSelected = false
            }
            
        }else{
            let arrayLocal = dictRouteMapData?["grp"].arrayValue
            let dictData = arrayLocal![indexPath.row]
            
            
            cell.lblName.text = dictData["rname"].stringValue
            if(dictData["is_check"].intValue == 1)
            {
                cell.btnSelect.isSelected = true
            }
            else{
                cell.btnSelect.isSelected = false
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let obj = objRouteMapStoryboard.instantiateViewController(withIdentifier: "PlayAudioVC")
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK: - Button Action
extension RouteMapVC
{
    @objc func btnAddAction(_ sender : UIButton)
    {
        txtRelation.text = ""
        vwAddPopup.isHidden = false
        vwAddPopup.alpha = 0
        vwAddPopup.fadeIn()
        vwAddPopup.tag = sender.tag
    }
    @objc func btnSelectAction(_ sender : UIButton)
    {
        let section = Int(sender.title(for: .disabled) ?? "0")
        if(section == 0)
        {
            var ary = dictRouteMapData!["ind"].arrayValue
            var dictData = ary[sender.tag]
            
            if(dictData["is_check"].intValue == 1){
                dictData["is_check"].intValue = 0
            }else{
                dictData["is_check"].intValue = 1
            }
            ary[sender.tag] = dictData
            dictRouteMapData!["ind"] = JSON(ary)
        }else{
            var ary = dictRouteMapData!["grp"].arrayValue
            var dictData = ary[sender.tag]
            if(dictData["is_check"].intValue == 1){
                dictData["is_check"].intValue = 0
            }else{
                dictData["is_check"].intValue = 1
            }
            ary[sender.tag] = dictData
            dictRouteMapData!["grp"] = JSON(ary)
        }
        tblRelation.reloadData()
    }
    @IBAction func btnSubmitAction(_ sender : UIButton)
    {
        submitRouteMap()
    }
    @IBAction func btnAddDoneAction(_ sender : UIButton)
    {
        if(txtRelation.text?.length == 0)
        {
            showToast(message: R.string.validationMessage.enter_relation_key())
            
        }else{
            vwAddPopup.fadeOut()
            if(vwAddPopup.tag == 0)
            {
                addRelationService(type:"ind")
            }else{
                addRelationService(type:"grp")
            }
        }
    }
    @IBAction func btnAddCloseAction(_ sender : UIButton)
    {
        vwAddPopup.fadeOut()
    }
}
//MARK:- UITextfield Setup
extension RouteMapVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
//MARK: - Other Methods
extension RouteMapVC
{
    /*func setUpArray(data : JSON)
    {
        self.arrayRelation = []
        var arrayIndividualData = data["ind"].arrayValue
        var arrayGroupData = data["grp"].arrayValue
        var dict = JSON()
        dict["individual"] = JSON(arrayIndividualData)
        self.arrayRelation.append(dict)
        
        dict = JSON()
        dict["group"] = JSON(arrayGroupData)
        self.arrayRelation.append(dict)
        
        print("New Relation array :::::: ",arrayRelation)
        
        self.tblRelation.reloadData()
    }*/
}

//MARK:- Service
extension RouteMapVC 
{
    func addRelationService(type:String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            RouteMapService().AddRelationService(relation : txtRelation.text ?? "",type : type, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.vwAddPopup.fadeOut()
                        self.getRelationService()
                        self.tblRelation.reloadData()
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            RouteMapService().GetRelationService(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.dictRouteMapData = json["data"]
                        self.tblRelation.reloadData()
                        //self.setUpArray(data : json["data"])
                    }else if(json["status"].stringValue == "0")
                    {
                        self.strErrorMessage = json["message"].stringValue
                        self.tblRelation.reloadData()
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func submitRouteMap()
    {
        let arrayIndividual = dictRouteMapData?["ind"].arrayValue
        let arrayGroup = dictRouteMapData?["grp"].arrayValue

        var arrayIndividualSelected : [String] = []
        var arrayGroupSelected : [String] = []

        arrayIndividual?.forEach({ (json) in
            if(json["is_check"].intValue == 1)
            {
                arrayIndividualSelected.append(json["rid"].stringValue)
            }
        })
        arrayGroup?.forEach({ (json) in
            if(json["is_check"].intValue == 1)
            {
                arrayGroupSelected.append(json["rid"].stringValue)
            }
        })
        let strIndividualId = arrayIndividualSelected.joined(separator: ",")
        let strGroupId = arrayGroupSelected.joined(separator: ",")

        print("selected individual ::: ",strIndividualId)
        print("selected group ::: ",strGroupId)
        
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            RouteMapService().AddRouteMapService(strIndividual:strIndividualId,strGroup: strGroupId,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTreeData"), object: "DONE")
                        self.navigationController?.popViewController(animated: true)

                    }else if(json["status"].stringValue == "0")
                    {
                        self.strErrorMessage = json["message"].stringValue
                        self.tblRelation.reloadData()
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

