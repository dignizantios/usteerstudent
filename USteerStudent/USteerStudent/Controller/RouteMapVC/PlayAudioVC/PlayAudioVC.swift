//
//  PlayAudioVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/9/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import AVFoundation
import AlamofireSwiftyJSON
import Alamofire

import SwiftyJSON

import AVFoundation
import MediaPlayer
import AVKit
import AudioToolbox

class PlayAudioVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var btnPlayPause: UIButton!
    @IBOutlet var lblInfo: UILabel!
    

    //MARK: - Variables
    var isPlaying = false
    //var audioPlayer: AVAudioPlayer?
    var audioPlayer: AVPlayer?
    var player : AVAudioPlayer?
    var dictData : JSON?
    var isAudioFinished = false
    
    //MARK: - Life cycle method
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }
    override func viewDidLayoutSubviews()
    {
        btnPlayPause.layer.cornerRadius = btnPlayPause.bounds.size.height / 2
        btnPlayPause.layer.masksToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK: - UI Setup
extension PlayAudioVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle : getCommonString(key: "Playing_audio_key").capitalized)
        
        lblInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        btnPlayPause.setTitle(getCommonString(key: "Play_audio_key").uppercased(), for: .normal)
        buttonUISetup(btnPlayPause, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        setUpAudio()

    }
    func setPlayPauseTitle()
    {
        if(isPlaying){
            btnPlayPause.setTitle(getCommonString(key: "Pause_audio_key").uppercased(), for: .normal)
        }else{
            btnPlayPause.setTitle(getCommonString(key: "Play_audio_key").uppercased(), for: .normal)
        }
    }
    
}
//MARK: - Button Action
extension PlayAudioVC
{
    
    @IBAction func btnPlayPauseAction(_ sender : UIButton)
    {
        if(!isPlaying)
        {
            if(isAudioFinished)
            {
                playSoundWithURL()
                audioPlayer?.play()
            }else{
                audioPlayer?.play()
            }
        }else
        {
            audioPlayer?.pause()
        }
        
        isPlaying = !isPlaying
        setPlayPauseTitle()
    }
}

//MARK: - Audio Play Method
extension PlayAudioVC
{
    func setUpAudio()
    {
//        playSound() //Audio file from bundle
        playSoundWithURL()  //Audio file from URL
    }
    func playSound()
    {
        isAudioFinished = false
        let sound = Bundle.main.url(forResource: "SampleAudio", withExtension: "mp3")
        do {
            let playerItem = AVPlayerItem(url: sound!)
            audioPlayer = AVPlayer(playerItem: playerItem)
            audioPlayer?.pause()
            //guard let player = audioPlayer else { return }
//            player.prepareToPlay()
//            audioPlayer?.play()
        }
    }
    func playSoundWithURL()
    {
        
        isAudioFinished = false
        
        let strUrl = dictData?["audio_file"].stringValue ?? ""
        if(strUrl != "")
        {

            
            let url = URL(string: strUrl)!
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                
                documentsURL.appendPathComponent(url.lastPathComponent)
                return (documentsURL, [.removePreviousFile])
            }
            
            showLoader()
            
            let request = download(url, to: destination).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword())
            
            request.responseData(completionHandler: { (response) in
                
                self.hideLoader()
                
                if response.result.isSuccess, let data = response.result.value {
                    print(data)
                    
                    print(FileManagerHelper.getAllFiles())
                    
                    if let fileURL = FileManagerHelper.searchFile(with: url.lastPathComponent) {
                        let playerItem = AVPlayerItem(url: fileURL)
                        self.audioPlayer = AVPlayer(playerItem:   playerItem)
                        self.audioPlayer?.rate = 1.0
                        self.audioPlayer?.volume = 1.0
                        self.audioPlayer?.pause()
//                        self.audioPlayer?.play()
                        self.setPlayPauseTitle()
                        
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.audioPlayer?.currentItem)
                    }
                }
            })
        } else {
            isPlaying = false
            setPlayPauseTitle()
            showToast(message: R.string.staticMessages.audio_not_play_key())
        }
    }
    
    @objc func playerItemDidReachEnd(){
        print("Finished")
        isAudioFinished = true
        isPlaying = false
        setPlayPauseTitle()
        
        showToast(message: R.string.staticMessages.audio_finished())
    }
    
}

