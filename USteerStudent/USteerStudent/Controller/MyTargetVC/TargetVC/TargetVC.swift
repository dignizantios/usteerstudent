//
//  TargetVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/3/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


class TargetTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var lblFactor: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var btnDeleteTarget: UIButton!
    @IBOutlet weak var lblCurrentScore: UILabel!
    @IBOutlet weak var lblTargetScore: UILabel!
    @IBOutlet weak var imgvwReward: UIImageView!
    @IBOutlet weak var btnContent: UIButton!
    @IBOutlet weak var btnDropdown: UIButton!

    //MARK: -
    override func awakeFromNib()
    {
        lblFactor.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblFactor.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        lblDate.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblDate.textColor = MySingleton.sharedManager.themeLightGrayColor
        
        [lblCurrentScore,lblTargetScore].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 10)
            label?.textColor = MySingleton.sharedManager.themeLightGrayColor
        }
    }
}

class TargetVC: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet var tblTargets: UITableView!
    
    //MARK: - Variables
    var arrayTargets : [JSON] = []
    var strErrorMessage = ""
    var categoryDict : JSON?
    var isFromMyTargets = false
    //MARK: - Variables
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getTargets()
        refreshControl.endRefreshing()
    }
    //MARK: - Life cycle methods

    override func viewDidLoad()
    {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        self.tblTargets.addSubview(self.refreshControl)
        
        tblTargets.tableFooterView = UIView()
        setupUI()
        getTargets()
    }

    override func viewDidAppear(_ animated: Bool) {

    }
    override func viewWillAppear(_ animated: Bool)
    {
        
        super.viewWillAppear(true)
        isOnSteering = 2 //For check on same screen
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadMyTarget), name: NSNotification.Name(rawValue: "reloadMyTarget"), object: nil)
        
        
//        self.tblTargets.register(StatementTableViewCell(), forCellReuseIdentifier: "statementCell")
        //self.tblTargets.register(StatementTableViewCell.self, forCellReuseIdentifier: "statementCell")

        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - Notification Methods
    @objc func reloadMyTarget()
    {
        getTargets()
    }
}
//MARK: - UI setup
extension TargetVC
{
    func setupUI()
    {
        if(isFromMyTargets)
        {
            setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "My_target_key"))
        }
    }
}
//MARK: - UITableview Delegate
extension TargetVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int {
        if arrayTargets.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            lbl.numberOfLines = 3
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayTargets.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell:TargetTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TargetTableViewCell

        
        let dict = arrayTargets[section]
        
        if(isFromMyTargets)
        {
            cell.lblFactor.text = dict["rname"].stringValue + " - " + dict["factor"].stringValue
        }else{
            cell.lblFactor.text = dict["targetName"].stringValue
        }
        let strDate = dict["targetDate"].stringValue
        
        if(!strDate.contains("0000"))
        {
            cell.lblDate.text = StringToConvertedStringDate(strDate: strDate, strDateFormat: "yyyyMMdd", strRequiredFormat: "dd-MM-yyyy")
        }
        cell.lblDate.isHidden = false
        
        cell.lblCurrentScore.text = getCommonString(key: "Current_score_key") + " - " + dict["your_score"].stringValue
        cell.lblTargetScore.text = getCommonString(key: "Target_score_key") + " - " + dict["target_score"].stringValue
        
        //-------
        cell.btnDeleteTarget.tag = section
        cell.btnContent.tag = section
        cell.btnDropdown.tag = section

        cell.btnDeleteTarget.addTarget(self, action: #selector(deleteTargetAction), for: .touchUpInside)
        cell.btnContent.addTarget(self, action: #selector(btnContentAction), for: .touchUpInside)
        cell.btnDropdown.addTarget(self, action: #selector(btnDropdownAction), for: .touchUpInside)

        if(dict["reward"].intValue == 0)
        {
            cell.imgvwReward.isHidden = true
        }else{
            cell.imgvwReward.isHidden = false
            cell.imgvwReward.image = rewardEmoji(reward: dict["reward"].intValue).mask(with: MySingleton.sharedManager.themeYellowColor)
        }
        
        if(dict["isSelected"].stringValue == "0")
        {
            cell.btnDropdown.isSelected = false
        }else{
            cell.btnDropdown.isSelected = true
        }
        
        
        return cell.contentView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 75
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        /*if arrayTargets.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            lbl.numberOfLines = 3
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayTargets.count*/
        
        let dict = arrayTargets[section]
        print("isSelected - ",dict["isSelected"].stringValue)
        if(dict["isSelected"].stringValue == "0")
        {
            return 0
        }
        return dict["arrayStatements"].arrayValue.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:StatementTableViewCell = tableView.dequeueReusableCell(withIdentifier: "statementCell") as! StatementTableViewCell
        cell.selectionStyle = .none
        
        
        let dict = arrayTargets[indexPath.section]
        let array = dict["arrayStatements"].arrayValue
        print("array - ",array)
        print("indexPath.row - ",indexPath.row)
        let dictStatement = array[indexPath.row]
        print("dictStatement - ",dictStatement)
        
        
        if(dictStatement["isNotesStatement"].stringValue == "1")
        {
            cell.lblStatementName.text = dictStatement["risk_bias"].stringValue
            if(dictStatement["isInitialStatement"].stringValue == "1")
            {
                cell.constrainsLeading.constant = 45
                cell.vwSeperator.isHidden = true
                cell.lblStatementName.textColor = MySingleton.sharedManager.themeDarkGrayColor
                cell.lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)

            }else{
                cell.constrainsLeading.constant = 65
                cell.lblStatementName.textColor = MySingleton.sharedManager.themeYellowColor
                cell.vwSeperator.isHidden = false
                cell.constrainsLeadingSeperator.constant = 65
                cell.lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)


            }
        }else
        {
            cell.lblStatementName.text = dictStatement["statement"].stringValue

            if(dictStatement["isInitialStatement"].stringValue == "1")
            {
                cell.constrainsLeading.constant = 45
                cell.vwSeperator.isHidden = true
                cell.lblStatementName.textColor = MySingleton.sharedManager.themeDarkGrayColor
                cell.lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)

            }
            else{
                cell.constrainsLeading.constant = 65
                cell.lblStatementName.textColor = MySingleton.sharedManager.themeDarkGrayColor
                cell.vwSeperator.isHidden = false
                cell.constrainsLeadingSeperator.constant = 65
                cell.lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)


            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        /*let targetDict = arrayTargets[indexPath.row]
        let targetId = targetDict["targetId"].stringValue
        
        let obj = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "StatementVC") as! StatementVC
        obj.strTargetId = targetId
        obj.dictDetails = targetDict
        self.navigationController?.pushViewController(obj, animated: true)*/
        
        
    }
    
    
    
}
//MARK: - Button action
extension TargetVC
{
    @objc func btnContentAction(_ sender : UIButton)
    {
        getSelectedStatements(sender)
    }
    @objc func btnDropdownAction(_ sender : UIButton)
    {
        getSelectedStatements(sender)
    }
    func getSelectedStatements(_ sender : UIButton)
    {
        var targetDict = arrayTargets[sender.tag]
        let arraystatements = targetDict["arrayStatements"].arrayValue
        
        
        for i in 0..<arrayTargets.count
        {
            var dict = arrayTargets[i]
            let selectedValue = dict["isSelected"].stringValue
            
            if(sender.tag == i)
            {
                if (selectedValue == "1")
                {
                    dict["isSelected"].stringValue="0"
                }else{
                    dict["isSelected"].stringValue="1"
                }
            }else{
                dict["isSelected"].stringValue="0"
            }
            arrayTargets[i] = dict
            
        }
        
        
        if(arraystatements.count == 0)
        {
            getTargetStatements(dict : targetDict,section:sender.tag)
        }
        else{
            tblTargets.reloadData()
        }
    }
    @objc func deleteTargetAction(_ sender : UIButton)
    {
        let alert = UIAlertController(title: "Are you sure you want to delete target?", message: "", preferredStyle: UIAlertControllerStyle.alert)

        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            self.deleteTargetService(index: sender.tag)
        }))
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK: - Other methods
extension TargetVC
{
    func setUpData(dataArray : [JSON])
    {
        arrayTargets = []
        var array = dataArray
        for i in 0..<array.count
        {
            var dict = array[i]
            dict["isSelected"].stringValue = "0"
            dict["targetName"].stringValue = decreptedString(string: dict["targetName"].stringValue)
            dict["your_score"].stringValue = decreptedString(string: dict["your_score"].stringValue)
            dict["target_score"].stringValue = decreptedString(string: dict["target_score"].stringValue)
            dict["arrayStatements"] = JSON([])
            array[i] = dict
        }
        
        arrayTargets = array
        tblTargets.reloadData()

        //------------------//
//        let array = dataArray.sorted {$0["reviewsAverage"].intValue < $1["reviewsAverage"].intValue}
//        print("Array - ",array)
    }
    func setUpStatementData(dataArray : [JSON],notesArray : [JSON],section:Int)
    {
        var ArrayNotes = notesArray
        var ArrayStatements = dataArray

        
        
        var json = JSON()
        json["statement"].stringValue = "Behaviours I want to develop"
        ArrayStatements.insert(json, at: 0)
        
        
        if(ArrayNotes.count > 0)
        {
            json = JSON()
            json["risk_bias"].stringValue = "Behaviours I recognise and want to address"
            ArrayNotes.insert(json, at: 0)
        }
        
        for i in 0..<ArrayNotes.count
        {
            var dict = ArrayNotes[i]
            dict["isNotesStatement"].stringValue = "1"
            if(i == 0)
            {
                dict["isInitialStatement"].stringValue = "1"
            }else{
                dict["isInitialStatement"].stringValue = "0"
            }
            
            ArrayNotes[i] = dict
        }
        
        print("arrayStatement : \(ArrayStatements)")
        
        for i in 0..<ArrayStatements.count
        {
            var dict = ArrayStatements[i]
            if i != 0{
                dict["statement"].stringValue = decreptedString(string: dict["statement"].stringValue)
            }

            dict["isNotesStatement"].stringValue = "0"
            if(i == 0)
            {
                dict["isInitialStatement"].stringValue = "1"
            }else{
                dict["isInitialStatement"].stringValue = "0"
            }
            ArrayStatements[i] = dict
        }
        
        print("arrayStatement : \(ArrayStatements)")
        
        let mergerArray = ArrayNotes + ArrayStatements
        
        var dict = arrayTargets[section]
        dict["arrayStatements"] = JSON(mergerArray)
        arrayTargets[section] = dict
//        self.arrayStatements = dataArray
        tblTargets.reloadData()
    }
}
//MARK:- Service
extension TargetVC
{
    func getTargets()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let relationId = self.categoryDict?["rid"].stringValue ?? ""
            
            MyTargetsService().GetMyTarget(relationID : relationId,isFromMyTargets : isFromMyTargets,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        let dictTargets = json["data"]
                        self.setUpData(dataArray : dictTargets["targets"].arrayValue)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        self.arrayTargets = []
                        self.strErrorMessage = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                        self.tblTargets.reloadData()
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func deleteTargetService(index : Int)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let targetDict = arrayTargets[index]
            let targetId = targetDict["targetId"].stringValue
            
            print("targetId - ",targetId)
            

            MyTargetsService().DeleteMyTarget(targetId : targetId,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.getTargets()

                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
            self.hideLoader()
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getTargetStatements(dict : JSON,section:Int)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            MyTargetsService().GetTargetStatements(targetId : dict["targetId"].stringValue,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        let dictTargets = json["data"]
                        self.setUpStatementData(dataArray : dictTargets["targetStatement"].arrayValue,notesArray : dictTargets["notesStatement"].arrayValue,section:section)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

