//
//  StatementVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


enum StatementParentVC
{
    case signpost
    case myjourney
}

class StatementTableViewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var lblStatementName: UILabel!
    @IBOutlet weak var constrainsLeading: NSLayoutConstraint!
    @IBOutlet weak var constrainsLeadingSeperator: NSLayoutConstraint!
    @IBOutlet weak var vwSeperator: UIView!

    //MARK: -
    override func awakeFromNib() {
        
        lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblStatementName.textColor = MySingleton.sharedManager.themeDarkGrayColor
        constrainsLeading.constant = 16
        vwSeperator.isHidden = true
    }
}
class StatementVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblStatements: UITableView!
    @IBOutlet var btnUpdateSignpost: UIButton!
    @IBOutlet var btnTestYourProgress: UIButton!

    @IBOutlet var constraintViewHeightButton: NSLayoutConstraint!
    @IBOutlet var vwButtons: UIView!

    //MARK: - Variables
    var arrayStatements : [JSON] = []
    var strErrorMessage = ""
    var strTargetId = ""
    var dictDetails = JSON()
    var targetFactor = ""
    var checkStatementParentVC = StatementParentVC.signpost

    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        print("dictDetails:\(dictDetails)")
        
        // Do any additional setup after loading the view.
        tblStatements.tableFooterView = UIView()
        
        setUpUI()
        getTargetStatements()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI setup
extension StatementVC
{
    func setUpUI()
    {
        let strSignPostDate = StringToConvertedStringDate(strDate: dictDetails["targetDate"].stringValue, strDateFormat: "yyyyMMdd", strRequiredFormat: "dd-MM-yyyy")
        
        
        [btnTestYourProgress,btnUpdateSignpost].forEach { (button) in
            button?.backgroundColor = MySingleton.sharedManager.themeYellowColor
            button?.setTitleColor(MySingleton.sharedManager.themeDarkGrayColor, for: .normal)
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
            button?.layer.cornerRadius = btnUpdateSignpost.frame.size.height / 2
            button?.layer.masksToBounds = true
        }
        
        setUpNavigationBarWithTitleAndBack(strTitle : "My signposts : \(strSignPostDate)")
        
        
        if(checkStatementParentVC == .myjourney)
        {
            hideShowButtonView(isHidden : false)
        }
        else{
            hideShowButtonView(isHidden : true)
        }
    }
    func hideShowButtonView(isHidden : Bool)
    {
        if(isHidden)
        {
            vwButtons.isHidden = isHidden
            constraintViewHeightButton.constant = vwButtons.frame.size.height
        }else{
            constraintViewHeightButton.constant = 0
            vwButtons.isHidden = isHidden
        }
    }
}
//MARK: - UITableview Delegate
extension StatementVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayStatements.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            lbl.numberOfLines = 3
            tableView.backgroundView = lbl
            
            return 0
        }
        tableView.backgroundView = nil
        return arrayStatements.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:StatementTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! StatementTableViewCell
        
        cell.selectionStyle = .none
        
        
        
        let dict = arrayStatements[indexPath.row]
        cell.lblStatementName.text = dict["statement"].stringValue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
    
}
//MARK:- Check assessment PopUp
extension StatementVC : AssessmentCheckDelegate
{
    func btnOption1Selected(selectedOption: CheckOption)
    {
        if(selectedOption == .link)
        {
            selectedMenuIndex = 6
            let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountOrgViewController") as! LinkAccountOrgViewController
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            self.revealViewController().setFront(rearNavigation, animated: true)
        }else{
            SaveShareChangesService(type_val : "1")
        }
    }
    
    func btnOption2Selected(selectedOption: CheckOption)
    {
        if(selectedOption == .link)
        {
            self.getAssessments(dict: getCategoryDict())
        }else{
            SaveShareChangesService(type_val : "0")
        }
    }
    
    func ShowCheckAssessmentOption(strMessage : String,isLink:Bool)
    {
        let obj = AssessmentCheckVC()
        obj.delegateAssessmentCheck = self
        obj.strMessage = strMessage
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        if(isLink)
        {
            obj.selectedOption = .link
        }else{
            obj.selectedOption = .share
        }
        
        self.present(obj, animated: false, completion: nil)
    }
}
//MARK: - Other methods
extension StatementVC
{
    func setUpData(dataArray : [JSON])
    {
            self.arrayStatements = dataArray
            tblStatements.reloadData()
    }
    
}
//MARK: - Button Actions
extension StatementVC
{
    @IBAction func btnUpdateSignpostAction(_ sender : UIButton)
    {
        
//        let obj = objReportStoryboard.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
//        obj.selectedParentVC = .myjourney
//        obj.dictDetails = dictDetails
//        self.navigationController?.pushViewController(obj, animated: true)
        
        let categoryDict = getCategoryDict()
        print("categoryDict:\(categoryDict)")
        let obj = objReportStoryboard.instantiateViewController(withIdentifier: "MySteeringVC") as! MySteeringVC
        obj.categoryDict = categoryDict
        obj.isSelectedFromVC = selectedReportController.myjourney
        obj.dictDetails = dictDetails
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    @IBAction func btnTestYourProgressAction(_ sender : UIButton)
    {
        let categoryDict = getCategoryDict()
        if(categoryDict["rname"].stringValue.lowercased() == "self")
        {
            CheckAssessments(dict: categoryDict)
        }else{
            getAssessments(dict:categoryDict)
        }
        
    }
    func getCategoryDict() -> JSON
    {
        var categoryDict = JSON()
        categoryDict["rid"].stringValue = dictDetails["rid"].stringValue
        categoryDict["rname"].stringValue = dictDetails["rname"].stringValue
        categoryDict["default_name"].stringValue = dictDetails["rname"].stringValue
        categoryDict["is_private"].stringValue = dictDetails["is_private"].stringValue
        categoryDict["other_id"].stringValue = dictDetails["other_id"].stringValue
        return categoryDict
    }
}
//MARK: - Service
extension StatementVC 
{
    func getAssessments(dict:JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "rid" : "\(dict["rid"].stringValue)"
            ]
            AssessmentService().getAssessmentQuestions(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        let obj = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentSliderVC") as! AssesmentSliderVC
                        obj.categoryDict = dict
                        obj.checkTreeParentVC = .journey
                        self.navigationController?.pushViewController(obj, animated: true)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func CheckAssessments(dict:JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "rid" : "\(dict["rid"].stringValue)"
            ]
            AssessmentService().checkAssessmentQuestions(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        if(data["type"].stringValue == "link") //link
                        {
                            self.ShowCheckAssessmentOption(strMessage: data["messageForLinkSchol"].stringValue,isLink:true)
                        }else if(data["type"].stringValue == "all_ok")
                        {
                            self.getAssessments(dict: dict)
                        }
                        else if(data["type"].stringValue == "share") //Share
                        {
                            self.ShowCheckAssessmentOption(strMessage: data["messageForSharingData"].stringValue,isLink:false)
                        }
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func SaveShareChangesService(type_val : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "type" : "share",
                                           "rid" : dictDetails["rid"].stringValue,
                                           "type_val" : type_val
            ]
            AssessmentService().SaveShareChangeService(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        //                        let data = json["data"]
                        self.getAssessments(dict: self.getCategoryDict())
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getTargetStatements()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            MyTargetsService().GetTargetStatements(targetId : self.strTargetId,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        let dictTargets = json["data"]
                        
                        var arrayTargetStatement = dictTargets["targetStatement"].arrayValue
                        
                        for i in 0..<arrayTargetStatement.count{
                            var dict = arrayTargetStatement[i]
                            dict["statement"].stringValue = decreptedString(string: dict["statement"].stringValue)
                            arrayTargetStatement[i] = dict
                        }
                        
                        self.setUpData(dataArray : arrayTargetStatement)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
