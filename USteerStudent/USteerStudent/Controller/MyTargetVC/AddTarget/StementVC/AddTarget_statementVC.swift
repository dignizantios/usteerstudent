//
//  AddTarget_statementVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


class AddTarget_StatementTableviewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var lblStatementName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!

    //MARK: -
    override func awakeFromNib() {
        lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblStatementName.textColor = MySingleton.sharedManager.themeDarkGrayColor
    }
}

class AddTarget_statementVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblvwStatements: UITableView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet weak var lblTargetFactor: UILabel!

    //MARK: - Alert
    @IBOutlet var vwAlert: UIView!
    
    //MARK: - Variables
    var checkSelectedAddTargetParent = SelectedAddTargetVC.steering

    var arrayStatements : [JSON] = []
    var strErrorMessage = ""
    var categoryDict : JSON?
    var currentScore = ""
    var isLoadFirstTimeScreen = true
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.getTargetStatements()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tblvwStatements.addSubview(self.refreshControl)
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        let tartgetDate = targetDict["targetDate"].stringValue
        let targetScore = targetDict["targetScore"].stringValue
        
        if(tartgetDate == "" || targetScore == "")
        {
            strErrorMessage = R.string.validationMessage.select_targetDate_Score_key()
            btnDone.isHidden = true
        }
        else
        {
            if(self.isLoadFirstTimeScreen)
            {
                getTargetStatements()
                btnDone.isHidden = false
            }
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        btnDone.layer.cornerRadius = btnDone.bounds.size.height / 2
        btnDone.layer.masksToBounds = true
    }
}

//MARK: - UI Setup
extension AddTarget_statementVC
{
    func setUpUI()
    {
        self.vwAlert.isHidden = true
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        self.vwAlert.addGestureRecognizer(tapOptions)
        
        btnDone.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        buttonUISetup(btnDone, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        lblTargetFactor.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblTargetFactor.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblTargetFactor.isHidden = true
        tblvwStatements.contentInset = UIEdgeInsetsMake(0, 0, 66, 0)
        
        let factor = targetDict["factor"].stringValue
        let strFactor = getFactorName(str: factor)
        lblTargetFactor.text = "\(strFactor.uppercased()): BEHAVIOURS I WANT TO DEVELOP."
    }
    
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        self.vwAlert.fadeOut()
        self.view.endEditing(true)
    }
}
//MARK: - UITableview Delegate
extension AddTarget_statementVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayStatements.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.numberOfLines = 3
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayStatements.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:AddTarget_StatementTableviewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AddTarget_StatementTableviewCell
        
        cell.selectionStyle = .none
        let dict = arrayStatements[indexPath.row]
        cell.lblStatementName.text = dict["statement"].stringValue
        if(dict["isSelected"].stringValue == "1")
        {
            cell.btnSelect.isSelected = true
        }else{
            cell.btnSelect.isSelected = false
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(btnSelectAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}
//MARK: - Button Action
extension AddTarget_statementVC
{
    @objc func btnSelectAction(_ sender : UIButton)
    {
        var dict = arrayStatements[sender.tag]
        if(dict["isSelected"].stringValue == "0")
        {
            dict["isSelected"].stringValue = "1"
        }else{
            dict["isSelected"].stringValue = "0"
        }
        arrayStatements[sender.tag] = dict
        tblvwStatements.reloadData()
        getSelectedIds()
    }
    
    @IBAction func btnDoneClickAction(_ sender : UIButton)
    {
        //targetStatementsIds
        if arrayStatements.contains(where: { (json) -> Bool in
            if(json["isSelected"].stringValue == "1")
            {
                return true
            }
            return false
        }){
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "btnNextAction"), object: "NEXT")

        }else{
            showToast(message: R.string.validationMessage.select_risk_statements_key())
        }
    }
    
    func isCheckForGetStatements() -> Bool
    {
        if(targetDict["targetScore"].stringValue != "" && targetDict["targetDate"].stringValue != "")
        {
            return true
        }else{
            if(targetDict["targetScore"].stringValue == "")
            {
                showToast(message: R.string.validationMessage.select_targetDate_Score_key())
            }
            else if(targetDict["targetDate"].stringValue != "")
            {
                showToast(message: R.string.validationMessage.enter_target_date_key())
            }
            return false
        }
    }
}
//MARK: - Other methods
extension AddTarget_statementVC
{
    func getSelectedIds()
    {
        var arraySelectedIds : [String] = []
        arrayStatements.forEach { (json) in
            
            if(json["isSelected"].stringValue == "1")
            {
                arraySelectedIds.append(json["id"].stringValue)
            }
        }
        
        targetDict["targetStatementsIds"].stringValue = arraySelectedIds.joined(separator: ",")
    }
}
//MARK:- Service
extension AddTarget_statementVC 
{
    
    func getTargetStatements()
    {
        if(!isCheckForGetStatements())
        {
            return
        }
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            var param : [String:Any] = [:]
            param["uid"] = getUserDetail("user_id")
            param["rid"] = categoryDict?["rid"].stringValue ?? ""
            param["factor"] = targetDict["factor"].stringValue
            param["cscore"] = currentScore
            param["tscore"] = targetDict["targetScore"].stringValue
            param["tdate"] = targetDict["targetDate"].stringValue
            param["timezone"] = getCurrentTimeZone()
            print("param - ",param)
            
            MyTargetsService().GetMyTargetStatements(param: param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.isLoadFirstTimeScreen = false
                        let datadict = json["data"]
                        var ary = datadict["signpost_statement"].arrayValue
                        for i in 0..<ary.count
                        {
                            var dict = ary[i]
                            dict["statement"].stringValue = decreptedString(string: dict["statement"].stringValue)
                            dict["isSelected"] = "0"
                            ary[i] = dict
                        }
                        self.lblTargetFactor.isHidden = false
                        self.arrayStatements = ary
                        self.tblvwStatements.reloadData()
                        self.vwAlert.isHidden = false
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}


