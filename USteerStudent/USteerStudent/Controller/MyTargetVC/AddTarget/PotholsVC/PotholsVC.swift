//
//  PotholsVC.swift
//  USteerStudent
//
//  Created by Usteer on 4/10/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class PotholesTableviewCell: UITableViewCell {
    
    //MARK: - Outlets
    @IBOutlet weak var lblStatementName: UILabel!
    @IBOutlet weak var btnSelect: UIButton!
    
    //MARK: -
    override func awakeFromNib() {
        lblStatementName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblStatementName.textColor = MySingleton.sharedManager.themeDarkGrayColor
    }
}
class PotholsVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblPotholes : UITableView!
    @IBOutlet var btnDone : UIButton!
    @IBOutlet weak var lblTargetFactor: UILabel!

    //MARK: - Alert
    @IBOutlet var vwAlert: UIView!
    
    //MARK: - Variables
    var checkSelectedAddTargetParent = SelectedAddTargetVC.steering

    var strErrorMessage = ""
    var arrayStatements : [JSON] = []
    var categoryDict : JSON?
    var currentScore = ""
    var isLoadFirstTimeScreen = true
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        self.getPotholsStatementsService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblPotholes.addSubview(self.refreshControl)
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let tartgetDate = targetDict["targetDate"].stringValue
        let targetScore = targetDict["targetScore"].stringValue
        
        if(tartgetDate == "" || targetScore == "")
        {
            strErrorMessage = R.string.validationMessage.select_targetDate_Score_key()
            btnDone.isHidden = true
        }else{
            if(isLoadFirstTimeScreen)
            {
                getPotholsStatementsService()
                btnDone.isHidden = false
            }
            
        }
    }
    override func viewDidLayoutSubviews()
    {
        btnDone.layer.cornerRadius = btnDone.bounds.size.height / 2
        btnDone.layer.masksToBounds = true
    }
}
//MARK: - Setup UI
extension PotholsVC
{
    func setUpUI()
    {
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwAlert.addGestureRecognizer(tapOptions)
        
        btnDone.setTitle(getCommonString(key: "Done_key").uppercased(), for: .normal)
        buttonUISetup(btnDone, textColor: MySingleton.sharedManager.themeDarkGrayColor)

        lblTargetFactor.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        lblTargetFactor.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblTargetFactor.isHidden = true
        
        tblPotholes.contentInset = UIEdgeInsetsMake(0, 0, 66, 0)
        
        let factor = targetDict["factor"].stringValue
        let strFactor = getFactorName(str: factor)
        lblTargetFactor.text = "\(strFactor.uppercased()): AND FINALLY, SOMETHING TO REMEMBER..."
    }
    
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        self.vwAlert.fadeOut()
        self.view.endEditing(true)
    }
}
//MARK: - UITableview Delegate
extension PotholsVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayStatements.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.numberOfLines = 3
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayStatements.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PotholesTableviewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PotholesTableviewCell
        
        cell.selectionStyle = .none
        let dict = arrayStatements[indexPath.row]
        cell.lblStatementName.text = dict["statement"].stringValue
        if(dict["isSelected"].stringValue == "1")
        {
            cell.btnSelect.isSelected = true
        }else{
            cell.btnSelect.isSelected = false
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(btnSelectAction), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
}
//MARK: - Button Action
extension PotholsVC
{
    @objc func btnSelectAction(_ sender : UIButton)
    {
        var dict = arrayStatements[sender.tag]
        if(dict["isSelected"].stringValue == "0")
        {
            dict["isSelected"].stringValue = "1"
        }else{
            dict["isSelected"].stringValue = "0"
        }
        arrayStatements[sender.tag] = dict
        tblPotholes.reloadData()
        
        getSelectedIds()
    }
    @IBAction func btnSubmitAction(_ sender:UIButton)
    {
        if(isCheckForSubmit())
        {
            submitTargetService()

        }else{
            
        }
        
        /*if arrayStatements.contains(where: { (json) -> Bool in
            if(json["isSelected"].stringValue == "1")
            {
                return true
            }
            return false
        }){
         
        }else{
            showToast(message: "Please select any one statement")

        }*/
    }
}
//MARK: - Other methods
extension PotholsVC
{
    func getSelectedIds()
    {
        var arraySelectedIds : [String] = []
        arrayStatements.forEach { (json) in
            
            if(json["isSelected"].stringValue == "1")
            {
                arraySelectedIds.append(json["id"].stringValue)
            }
        }
        
        targetDict["stumbleStatementsIds"].stringValue = arraySelectedIds.joined(separator: ",")
    }
    func navigateToMyTargets()
    {
        /*let viewControllers: [UIViewController] = self.navigationController!.viewControllers
         for aViewController in viewControllers
         {
             if aViewController is MyTargetParentVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
         }
        self.navigationController?.popViewController(animated: true)*/
    }
    func isCheckForGetStatements() -> Bool
    {
        if(targetDict["targetScore"].stringValue != "" && targetDict["targetDate"].stringValue != "")
        {
            return true
        }else{
            if(targetDict["targetScore"].stringValue == "")
            {
                showToast(message: R.string.validationMessage.select_targetDate_Score_key())
            }
            else if(targetDict["targetDate"].stringValue != "")
            {
                showToast(message: R.string.validationMessage.enter_target_date_key())
            }
            return false
        }
    }
    func isCheckForSubmit() -> Bool
    {
        
        if(targetDict["targetStatementsIds"].stringValue != "" && targetDict["targetScore"].stringValue != "" && targetDict["targetDate"].stringValue != "" && targetDict["stumbleStatementsIds"].stringValue != "")
        {
            return true
        }else{
            if(targetDict["targetStatementsIds"].stringValue == "")
            {
                showToast(message: R.string.validationMessage.select_risk_statements_key())
            }
            else if(targetDict["stumbleStatementsIds"].stringValue == "")
            {
                showToast(message: R.string.validationMessage.select_pothols_statements_key())
            }else if(targetDict["targetScore"].stringValue == "")
            {
                showToast(message: R.string.validationMessage.target_score_add_key())
            }
            else if(targetDict["targetDate"].stringValue != "")
            {
                showToast(message: R.string.validationMessage.enter_target_date_key())
            }
            return false
        }
    }
    func SetReminder()
    {
//        let strSignpostDate = targetDict["targetDate"].stringValue
//        let dateSignpost = StringToDate(Formatter: "yyyyMMdd", strDate: strSignpostDate)
        let dateSignpost = dateSignpostValue
        print("dateSignpost - ",dateSignpost)
        
        let dateCurrentValue = Date()
        let arrayDates = generateDatesArrayBetweenTwoDates(startDate: dateCurrentValue, endDate: dateSignpost)
        
        var middleDate = dateSignpost
        if(arrayDates.count>0)
        {
            let IndexMiddle = Int(arrayDates.count/2)
            middleDate = arrayDates[IndexMiddle]
        }
        
        
        let calendar = Calendar.current

        ////----set reminder after 14 days---///
//        var timeInterval = DateComponents()
//        timeInterval.day = 14
//        let date = Calendar.current.date(byAdding: timeInterval, to: dateSignpost)

        //----

        let year = calendar.component(.year, from: middleDate)
        let month = calendar.component(.month, from: middleDate)
        let day = calendar.component(.day, from: middleDate)
        
        print("***************")

        print("year - ",year)
        print("month - ",month)
        print("day - ",day)
        
        ////----set reminder before 1 hour ---///
        var beforeTimeInterval = DateComponents()
        beforeTimeInterval.day = -1
        let beforeDate = Calendar.current.date(byAdding: beforeTimeInterval, to: dateSignpost)
        
        print("beforeDate - ",beforeDate)

//        let dateDiffrence = beforeDate?.interval(ofComponent: .hour, fromDate: Date())
//        print("Diffrence - ",dateDiffrence)
//
//        if(dateDiffrence! > 1)
//        {
        
        let beforeYear = calendar.component(.year, from: beforeDate ?? Date())
        let beforeMonth = calendar.component(.month, from: beforeDate ?? Date())
        let beforeDay = calendar.component(.day, from: beforeDate ?? Date())
        
        print("***************")
        print("beforeYear - ",beforeYear)
        print("beforeMonth - ",beforeMonth)
        print("beforeDay - ",beforeDay)
        
//        print("Relation - ",categoryDict?["rname"].stringValue)
        
        let strIdentifierMiddleSignpost = "middleSignpost_" + DateToString(Formatter: server_dt_format, date: Date())
        let strIdentifierDayBeforeSignpost = "dayBeforeSignpost_" + DateToString(Formatter: server_dt_format, date: Date())
        
        appDelegate.locallyNotificationSignpostReminder(year: year, month: month, day: day, message: "how are you getting on steering your Seeking Change in relation to \(categoryDict?["rname"].stringValue ?? "")",title:"Steering Tracking",relationId:"\(categoryDict?["rid"].stringValue ?? "")",categoryData:categoryDict ?? JSON(),identifier:strIdentifierMiddleSignpost)
        
        appDelegate.locallyNotificationSignpostReminder(year: beforeYear, month: beforeMonth, day: beforeDay, message: "\(getUserDetail("username")), just a reminder to retrack your steering with \(categoryDict?["rname"].stringValue ?? "")",title:"Steering Reminder",relationId:"\(categoryDict?["rid"].stringValue ?? "")",categoryData:categoryDict ?? JSON(),identifier:strIdentifierDayBeforeSignpost)
//        }
        
        
    }
}
//MARK:- Service
extension PotholsVC 
{
    func getPotholsStatementsService()
    {
        if(!isCheckForGetStatements())
        {
            return
        }
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            var param : [String:Any] = [:]
            param["uid"] = getUserDetail("user_id")
            param["rid"] = categoryDict?["rid"].stringValue ?? ""
            param["factor"] = targetDict["factor"].stringValue
            param["cscore"] = currentScore
            param["tscore"] = targetDict["targetScore"].stringValue
            param["tdate"] = targetDict["targetDate"].stringValue
            param["timezone"] = getCurrentTimeZone()
            
            print("param - ",param)
            
            MyTargetsService().GetPotholsStatement(param: param, completion:{ (result) in
                
                if let json = result.value
                {
                    self.isLoadFirstTimeScreen = false
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let datadict = json["data"]
                        var ary = datadict["potholes_statement"].arrayValue
                        for i in 0..<ary.count
                        {
                            var dict = ary[i]
                            dict["statement"].stringValue = decreptedString(string: dict["statement"].stringValue)
                            dict["isSelected"] = "0"
                            ary[i] = dict
                        }
                        self.lblTargetFactor.isHidden = false
                        self.arrayStatements = ary
                        self.tblPotholes.reloadData()
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func submitTargetService()
    {
        self.SetReminder()
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            var param : [String:Any] = [:]
            
            var json = JSON()
            json["uid"].stringValue = encryptString(string: getUserDetail("user_id"))
            json["rid"].stringValue = categoryDict?["rid"].stringValue ?? ""
            json["factor"].stringValue = targetDict["factor"].stringValue
            json["currentScore"].stringValue = encryptString(string: currentScore)
            json["targetScore"].stringValue = encryptString(string: targetDict["targetScore"].stringValue)
            json["targetDate"].stringValue = targetDict["targetDate"].stringValue
            
            json["targetStatementsIds"].stringValue = targetDict["targetStatementsIds"].stringValue
            json["stumbleStatementsIds"].stringValue = targetDict["stumbleStatementsIds"].stringValue
            
            json["timezone"].stringValue = getCurrentTimeZone()
            json["created"].stringValue = DateToString(Formatter: server_dt_format, date: Date())
            json["device_id"].stringValue = "\(getFirebaseToken())"
            json["device_type"].stringValue = "\(deviceType)"
            
            param["target_data"] = json
            print("param - ",param)
            
            MyTargetsService().SaveMyTargetStatements(param: param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.SetReminder()
                        showToast(message: json["message"].stringValue)
                        targetDict["riskStatements"].arrayObject  = []
                        self.navigationController?.popViewController(animated: true)
                        isFromTargetBack = true
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadMyTarget"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "navigateToMyTargets"), object: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTreeData"), object: "DONE")
                        isFromTargetBack = true
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
