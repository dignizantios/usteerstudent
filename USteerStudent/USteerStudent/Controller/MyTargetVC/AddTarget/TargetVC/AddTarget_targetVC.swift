//
//  AddTarget_targetVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


enum TargetParentVC
{
    case myjourney
    case target
}


class AddTarget_targetVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var btnNext: UIButton!
    @IBOutlet var txtTargetDate: UITextField!
    @IBOutlet var vwSeperator: UIView!
    @IBOutlet var lblInfo: UILabel!
    @IBOutlet var lblTargetDateHeading: UILabel!
    @IBOutlet var lblSetTargetInfo: UILabel!
    @IBOutlet var lblSelectedTargetFactor: UILabel!

    //MARK: - Slider outlets
    @IBOutlet weak var slider: MSCircularSlider!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var viewGuage: UIImageView!
    @IBOutlet weak var imgHandle: UIImageView!
    
    //MARK: - Alert
    @IBOutlet var vwAlert: UIView!
    
    //MARK: - Varibales
    var strErrorMessage = ""
    var strTargetDate = ""
    var currentScore = "0"
    var categoryDict : JSON?
    var lastAngle = 0.0
    var checkSelectedAddTargetParent = SelectedAddTargetVC.steering
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
        
        let alertView = UIAlertController(title: kCommonAlertTitle, message: R.string.validationMessage.message_happy_existing_score_key(), preferredStyle: .alert)
        let OkAction = UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { (alert) in
            //Relation Delete action
            self.vwAlert.isHidden = false
        })
        alertView.addAction(OkAction)
        appDelegate.window?.rootViewController?.present(alertView, animated: true, completion:nil)
    }
    
    override func viewDidLayoutSubviews()
    {
        btnNext.layer.cornerRadius = btnNext.bounds.size.height / 2
        btnNext.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpData()
    }
}
//MARK: - UI Setup
extension AddTarget_targetVC
{
    func setUpUI()
    {
        vwAlert.isHidden = true
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwAlert.addGestureRecognizer(tapOptions)

        btnNext.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        buttonUISetup(btnNext, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        [lblInfo,lblTargetDateHeading,lblSelectedTargetFactor].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        })

        lblSelectedTargetFactor.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)

        txtTargetDate.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        txtTargetDate.textColor = MySingleton.sharedManager.themeLightGrayColor

        lblSetTargetInfo.textColor = UIColor.orange
        lblSetTargetInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblSetTargetInfo.text = getCommonString(key: "Set_new_target_Score_key").uppercased()
        
        lblTargetDateHeading.textColor = MySingleton.sharedManager.themeYellowColor
        vwSeperator.backgroundColor = MySingleton.sharedManager.themeYellowColor
        
        slider.delegate = self
        slider.maximumAngle = 180
        slider.minimumValue = 0
        slider.maximumValue = 180
        slider.handleImage = nil
        slider.filledColor = UIColor.clear
        slider.unfilledColor = UIColor.clear
        slider.handleColor = UIColor.clear
        slider.currentValue = Double(currentScore) ?? 0.0
        slider.handleType = .largeCircle
        spin(viewImg: imgHandle, angle: (Double(currentScore) ?? 0.0) * 12)
        valueLbl.text = String(format: "%.1f", slider.currentValue)
        
        let factor = targetDict["factor"].stringValue
        let strFactor = getFactorName(str: factor)
        print("selected strFactor - ",strFactor)
        lblSelectedTargetFactor.text = "Selected Target Factor : \(strFactor)"
    }
    
    func setUpData() {
        targetDict["targetScore"].stringValue = currentScore
    }
    
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        self.vwAlert.fadeOut()
        self.view.endEditing(true)
    }
}
//MARK:- UITextfield Setup
extension AddTarget_targetVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        txtTargetDate.resignFirstResponder()
        if(textField == txtTargetDate)
        {
            self.view.endEditing(true)
            let obj = DatePickerVC()
            obj.pickerDelegate = self
            obj.isSetMinimumDate = true
            obj.minimumDate = getCurrentTimeStamp()
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.datePickerCustomMode = datepickerMode(customPickerMode:UIDatePickerMode.date)
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
}
//MARK: - MSSlider Delegate
extension AddTarget_targetVC : MSCircularSliderDelegate
{
    func circularSlider(_ slider: MSCircularSlider, valueChangedTo value: Double, fromUser: Bool) {
        
        let finalValue = Int(value / 12)
        print("Slider value - ",value)
        valueLbl.text = String(format: "%.1d", finalValue)
        targetDict["targetScore"].stringValue = valueLbl.text ?? ""
        spin(viewImg: imgHandle, angle: value)
    }
    //MARK: - slider helper methods
    func spin(viewImg: UIImageView, angle: Double)
    {
        var angle = angle - 90
        viewImg.layer.anchorPoint = CGPoint.init(x: 0.5, y: 1.925)
        let animation = CABasicAnimation.init(keyPath: "transform.rotation")
        if angle >= 360.0 {
            angle = 360.0
        }
        animation.fromValue = lastAngle
        
        let temp = (angle) * (Double.pi/180)
        animation.toValue = temp
        lastAngle = temp
        
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        
        animation.timingFunction = CAMediaTimingFunction.init(name: kCAMediaTimingFunctionEaseInEaseOut)
        animation.autoreverses = false
        viewImg.layer.add(animation, forKey: "rotationAnimation")
    }
}
//MARK: - Button Action
extension AddTarget_targetVC
{
    @IBAction func btnNextClickAction(_ sender : UIButton)
    {
        if(txtTargetDate.text == "")
        {
            showToast(message: R.string.validationMessage.enter_target_date_key())
        }
        else if(self.currentScore == targetDict["targetScore"].stringValue)
        {
            showToast(message: R.string.validationMessage.set_new_signpost_goal_key())
        }
        else{
            targetDict["targetDate"].stringValue = strTargetDate
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "btnNextAction"), object: "NEXT")
        }
    }
}
//MARK: - SetDate Picker Value
extension AddTarget_targetVC : datePickerDelegate
{
    func setDateValue(dateValue: Date)
    {
        self.txtTargetDate.text = DateToString(Formatter: "dd-MM-yyyy", date: dateValue)
        strTargetDate = DateToString(Formatter: "yyyyMMdd", date: dateValue)
        targetDict["targetDate"].stringValue = strTargetDate
        dateSignpostValue = dateValue
    }
}
