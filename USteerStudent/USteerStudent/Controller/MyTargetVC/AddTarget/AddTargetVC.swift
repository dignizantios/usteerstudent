//
//  AddTargetVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import CarbonKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

enum SelectedAddTargetVC
{
    case steering
    case myjourney
}


class AddTargetVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var mainView: UIView!

    //MARK: - Varibales
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = NSArray()
    var categoryDict : JSON?
    var selectedFactor = TargetFactor.SD
    var lastSelectedTabIndex = 0
    var currentScore = ""
    var checkSelectedTargetVC = TargetParentVC.target
    var checkSelectedAddTargetVC = SelectedAddTargetVC.steering
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        items = [getCommonString(key: "Steer_key").uppercased(),
                 getCommonString(key: "Signpost_key").uppercased(),
                 getCommonString(key: "Potholes_key").uppercased()
        ]
        
        print("currentScore Add target - ",currentScore)
        
//        print("items - ",items)
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: mainView)
        
        setUpUI()
        setUpNotifications()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        style()
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
    }
    override func viewDidLayoutSubviews()
    {
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - setUP Notifications
extension AddTargetVC
{
    func setUpNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.btnNextAction), name: NSNotification.Name(rawValue: "btnNextAction"), object: nil)
    }
}
//MARK: - UI Setup
extension AddTargetVC
{
    func setUpUI()
    {
        let categoryName = categoryDict?["rname"].stringValue ?? ""
        //setUpNavigationBarWithTitleAndBack(strTitle: (getCommonString(key: "Add_target_key").capitalized +
             //          " -  " + categoryName))
        setUpNavigationBarWithTitleAndBackAndSideMenu(strTitle: (getCommonString(key: "Add_target_key").capitalized +
           " -  " + categoryName))
        
//        targetDict["factor"].stringValue = selectedFactor.rawValue
    }
}
//MARK: - Carbon kit
extension AddTargetVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        let width = UIScreen.main.bounds.size.width
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        
        let tabWidth = (width / CGFloat(items.count))
        //let tabWidth = (width / 2)

        let indicatorcolor: UIColor = MySingleton.sharedManager.themeYellowColor
        let color: UIColor = .white
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = color
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 2)
        
//        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
//        carbonTabSwipeNavigation.pagesScrollView?.isScrollEnabled = false
//        carbonTabSwipeNavigation.toolbar.isUserInteractionEnabled = false
        
        carbonTabSwipeNavigation.setNormalColor(MySingleton.sharedManager.themeLightGrayColor, font: UIFont.systemFont(ofSize: 12))
        carbonTabSwipeNavigation.setSelectedColor(MySingleton.sharedManager.themeYellowColor, font: UIFont.boldSystemFont(ofSize: 12))
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isDirectionalLockEnabled = true
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        switch index
        {
            /*case 0:
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "RiskVC") as! RiskVC
                vc.selectedFactor = self.selectedFactor
                vc.categoryDict = categoryDict
                return vc*/
            case 0 :
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "AddTarget_targetVC") as! AddTarget_targetVC
//                vc.selec = checkSelectedTargetVC
                vc.currentScore = currentScore
                vc.checkSelectedAddTargetParent = checkSelectedAddTargetVC
                vc.categoryDict = categoryDict

                return vc
            case 1 :
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "AddTarget_statementVC") as! AddTarget_statementVC
                vc.currentScore = currentScore
                vc.categoryDict = categoryDict
                vc.checkSelectedAddTargetParent = checkSelectedAddTargetVC

                return vc
            case 2 :
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "PotholsVC") as! PotholsVC
                vc.currentScore = currentScore
                vc.categoryDict = categoryDict
                vc.checkSelectedAddTargetParent = checkSelectedAddTargetVC

                return vc
            default:
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "AddTarget_targetVC") as! AddTarget_targetVC
                vc.checkSelectedAddTargetParent = checkSelectedAddTargetVC
                vc.currentScore = currentScore
                vc.categoryDict = categoryDict
                return vc
        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt)
    {
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willMoveAt index: UInt)
    {
//        if(carbonTabSwipeNavigation.currentTabIndex == 0 && index == 1)
//        {
//            carbonTabSwipeNavigation.currentTabIndex = 0
//        }else if(carbonTabSwipeNavigation.currentTabIndex == 1 && index == 2)
//        {
//            carbonTabSwipeNavigation.currentTabIndex = 1
//        }
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, willBeginTransitionFrom index: UInt)
    {
        
    }
    
}
//MARK: - Button Action
extension AddTargetVC
{
    @objc func btnNextAction(_ notification : NSNotification)
    {
       // let object = notification.object
        setUpNavigation()
    }
    
    func setUpNavigation()
    {
        if(carbonTabSwipeNavigation.currentTabIndex == 0)
        {
            print("Segement 1 Next")
            carbonTabSwipeNavigation.currentTabIndex = UInt(carbonTabSwipeNavigation.currentTabIndex+1)
        }
        else if(carbonTabSwipeNavigation.currentTabIndex == 1)
        {
            print("Segement 2 Next")
            carbonTabSwipeNavigation.currentTabIndex = UInt(carbonTabSwipeNavigation.currentTabIndex+1)
        }
        else
        {
            print("Done action")
        }
//        setButtonTitle()
    }
    /*func setButtonTitle()
    {
        if(carbonTabSwipeNavigation.currentTabIndex == 0)
        {
            btnNext.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        }
        else if(carbonTabSwipeNavigation.currentTabIndex == 1)
        {
            btnNext.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        }
        else
        {
            btnNext.setTitle(getCommonString(key: "Submit_key").uppercased(), for: .normal)
        }
    }*/
}
