//
//  RiskVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


class RiskVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var tblRisk: UITableView!
    @IBOutlet var btnNext: UIButton!

    //MARK: - Popup Outlet
    @IBOutlet var vwColorPopup : UIView!
    @IBOutlet var lblColorPopupHeading : UILabel!

    @IBOutlet var vwPolarRed : UIView!
    @IBOutlet var vwAmber : UIView!
    @IBOutlet var vwBlue : UIView!
    @IBOutlet var vwGreen : UIView!
    
    @IBOutlet var lblPolarRed : UILabel!
    @IBOutlet var lblAmber : UILabel!
    @IBOutlet var lblBlue : UILabel!
    @IBOutlet var lblGreen : UILabel!
    
    @IBOutlet var lblPolarRedContent : UILabel!
    @IBOutlet var lblAmberContent : UILabel!
    @IBOutlet var lblBlueContent : UILabel!
    @IBOutlet var lblGreenContent : UILabel!
    
    //MARK: - Variables
    var arrayRisk : [JSON] = []
    var strErrorMessage = ""
    var categoryDict : JSON?
    var selectedFactor = TargetFactor.SD
    var strFactorHeading = ""
    var strNotesDescription = ""
    var dictRiskData : JSON?
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getRiskStatements()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.tblRisk.addSubview(self.refreshControl)
        
        // Do any additional setup after loading the view.
        tblRisk.register(UINib(nibName: "RiskHeadingTableViewCell", bundle: nil), forCellReuseIdentifier: "RiskHeadingTableViewCell")
        tblRisk.register(UINib(nibName: "Risk_TableViewCell", bundle: nil), forCellReuseIdentifier: "Risk_TableViewCell")
        tblRisk.register(UINib(nibName: "Risk_HeaderTableViewCell", bundle: nil), forCellReuseIdentifier: "Risk_HeaderTableViewCell")
        tblRisk.register(UINib(nibName: "NotesTableViewCell", bundle: nil), forCellReuseIdentifier: "NotesTableViewCell")
        
        tblRisk.contentInset = UIEdgeInsetsMake(0, 0, 100, 0)
        tblRisk.isHidden = true
        setUpUI()
        getRiskStatements()
    }
    override func viewDidLayoutSubviews()
    {
        btnNext.layer.cornerRadius = btnNext.bounds.size.height / 2
        btnNext.layer.masksToBounds = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
//MARK: - UI Setup
extension RiskVC
{
    func setUpUI()
    {
        btnNext.setTitle(getCommonString(key: "Next_key").uppercased(), for: .normal)
        buttonUISetup(btnNext, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        vwColorPopup.isHidden = true
        
        lblColorPopupHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblColorPopupHeading.backgroundColor = MySingleton.sharedManager.themeYellowColor
        lblColorPopupHeading.textColor = .white

        lblPolarRed.text = "Polar red HIGH and LOW"
        lblAmber.text = "Amber HIGH and LOW"
        lblBlue.text = "Blue HIGH and LOW"
        lblGreen.text = "Green HIGH and LOW"
        
        [lblPolarRed,lblAmber,lblBlue,lblGreen].forEach({
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.textColor = MySingleton.sharedManager.themeLightGrayColor
            setLabelText(searchText : ($0?.text)!,label : ($0)!,word1:"LOW",word2:"HIGH")
        })
        
        [lblPolarRedContent,lblAmberContent,lblBlueContent,lblGreenContent].forEach({
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.textColor = MySingleton.sharedManager.themeLightGrayColor
            $0?.text = "Your steering shows a polar bias LOW and HIGH"
            setLabelText(searchText : ($0?.text)!,label : ($0)!,word1:"LOW",word2:"HIGH")
        })
        
        vwPolarRed.backgroundColor = MySingleton.sharedManager.themeRedColor
        vwAmber.backgroundColor = MySingleton.sharedManager.themeYellowColor
        vwBlue.backgroundColor = MySingleton.sharedManager.themeBlueColor
        vwGreen.backgroundColor = MySingleton.sharedManager.themeGreenColor
        
        
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwColorPopup.addGestureRecognizer(tapOptions)
        
        tblRisk.reloadData()
    }
    func checkSelectedFactor() -> String
    {
        return selectedFactor.rawValue
        /*switch selectedFactor
        {
            case .SD:
                return "sd"
            case .TOS:
                return "tos"
            case .TOO:
                return "too"
            case .SC:
                return "sc"
        }*/
    }
    func setLabelText(searchText : String,label : UILabel,word1:String,word2:String)
    {
        /* Find the position of the search string. Cast to NSString as we want
         range to be of type NSRange, not Swift's Range<Index> */
        let range1 = (searchText as NSString).range(of: word1)
        let range2 = (searchText as NSString).range(of: word2)
        
        /* Make the text at the given range bold. Rather than hard-coding a text size,
         Use the text size configured in Interface Builder. */
        let attributedString = NSMutableAttributedString(string: searchText)
        attributedString.addAttribute(NSAttributedStringKey.font, value: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14), range: range1)
        attributedString.addAttribute(NSAttributedStringKey.font, value: MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14), range: range2)
        
        /* Put the text in a label */
        label.attributedText = attributedString
    }
}
//MARK: - Geture method
extension RiskVC
{
    /// Tap reconizer on table to hide keyboard
    ///
    /// - Parameter sender: geture reconizer
    @objc func hideView(sender: UITapGestureRecognizer? = nil) {
        // handling code
        
        self.vwColorPopup.fadeOut()
        self.view.endEditing(true)
    }
}
//MARK: - UITableview Delegate
extension RiskVC : UITableViewDelegate,UITableViewDataSource
{
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if(section == 0)
        {
            let cell:RiskHeadingTableViewCell = tableView.dequeueReusableCell(withIdentifier: "RiskHeadingTableViewCell") as! RiskHeadingTableViewCell
            cell.lblTitle.text = dictRiskData?["title"].stringValue ?? ""
            
            cell.btnStatus.backgroundColor = MySingleton.sharedManager.themeGreenColor
            cell.btnStatus.setTitle(dictRiskData?["label"].stringValue ?? "", for: .normal)
            
            cell.btnStatus.isUserInteractionEnabled = false
            
            cell.btnShowInfo.addTarget(self, action: #selector(btnShowInfoAction), for: .touchUpInside)
            
            return cell.contentView
        }
        else // Notes header
        {
            let cell:Risk_HeaderTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Risk_HeaderTableViewCell") as! Risk_HeaderTableViewCell
            cell.lblTitle.text = getCommonString(key: "Notes_key").uppercased() + " :"
            cell.imgvw.image = UIImage(named:"ic_notes_header")
            
            return cell.contentView
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if(section == 0)
        {
            return 68
        }
        return 43
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if(arrayRisk.count > 0)
        {
            if(section == 0)
            {
                let dict = arrayRisk[section]
                return dict["statements"].arrayValue.count
            }
            return 1
        }
        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.section == 0)
        {
            let cell:Risk_TableViewCell = tableView.dequeueReusableCell(withIdentifier: "Risk_TableViewCell") as! Risk_TableViewCell
            
            cell.selectionStyle = .none
            cell.btnSelect.tag = indexPath.row
            cell.btnSelect.addTarget(self, action: #selector(btnSelectRiskAction), for: .touchUpInside)
            cell.btnSelect.setTitle("\(indexPath.section)", for: .disabled)
            
            let dict = arrayRisk[indexPath.section]
            let array = dict["statements"].arrayValue
            let dataDict = array[indexPath.row]
            cell.lblTitle.text = dataDict["statement"].stringValue
            
            
            if(dataDict["isSelected"].stringValue == "1")
            {
                cell.btnSelect.isSelected = true
            }else{
                cell.btnSelect.isSelected = false

            }
            return cell
        }
        else
        {
            let cell:NotesTableViewCell = tableView.dequeueReusableCell(withIdentifier: "NotesTableViewCell") as! NotesTableViewCell
            
            cell.selectionStyle = .none
            cell.lblHeading.text = self.strFactorHeading
            cell.lblContent.text = self.strNotesDescription
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
//MARK: - Button Actions
extension RiskVC
{
    @objc func btnShowInfoAction(_ sender : UIButton)
    {
        vwColorPopup.alpha = 0
        vwColorPopup.fadeIn()
    }
    @IBAction func btnCloseColorPopupAction(_ sender : UIButton)
    {
        vwColorPopup.fadeOut()
    }
    @objc func btnSelectRiskAction(_ sender : UIButton)
    {
//        sender.isSelected = !sender.isSelected
        let section = Int(sender.title(for: .disabled) ?? "0")
        var dictStatements = arrayRisk[section!]
        
        var array = dictStatements["statements"].arrayValue
        var dict = array[sender.tag]
        
        if dict["isSelected"].stringValue == "1"
        {
            dict["isSelected"].stringValue = "0"
            
        }else{
            dict["isSelected"].stringValue = "1"
        }
        array[sender.tag] = dict
        dictStatements["statements"] = JSON(array)
        arrayRisk[section!] = dictStatements
        tblRisk.reloadData()
        
        var arraySelectedRisk : [String] = []
        arrayRisk.forEach { (json) in
            let arrayStatements = json["statements"].arrayValue
            arrayStatements.forEach({ (statementJson) in
                if(statementJson["isSelected"].stringValue == "1")
                {
                    arraySelectedRisk.append(statementJson["id"].stringValue)
                }
            })
        }
        print("Selected statements ::: ",arraySelectedRisk)
        targetDict["riskStatementsIds"].stringValue = arraySelectedRisk.joined(separator: ",")

        print("targetDict - ",targetDict)

    }
    
    @IBAction func btnNextClickAction(_ sender : UIButton)
    {
        var arraySelectedRisk : [String] = []
        arrayRisk.forEach { (json) in
            let arrayStatements = json["statements"].arrayValue
            arrayStatements.forEach({ (statementJson) in
                if(statementJson["isSelected"].stringValue == "1")
                {
                    arraySelectedRisk.append(statementJson["id"].stringValue)
                }
            })
        }
        print("Selected statements ::: ",arraySelectedRisk)
        if(arraySelectedRisk.count > 0)
        {
            targetDict["riskStatements"].arrayObject = arraySelectedRisk
            targetDict["riskStatementsIds"].stringValue = arraySelectedRisk.joined(separator: ",")
            navigateToNext()
        }else{
            showToast(message:R.string.validationMessage.select_risk_statements_key())
        }
    }
}
//MARK: - Other Methods
extension RiskVC
{
    func navigateToNext()
    {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "btnNextAction"), object: "NEXT")
    }
    func setUpData(dataDict : JSON)
    {
        dictRiskData = dataDict
        print("dataDict - ",dataDict)
        ////-----------
        var dict = JSON()
        dict["title"].stringValue = dataDict["title"].stringValue
        dict["isSelected"].stringValue = "0"
        dict["status"].stringValue = dataDict["label"].stringValue.uppercased()
        targetDict["currentScore"].stringValue = dataDict["score"].stringValue
        targetDict["targetScore"].stringValue = dataDict["score"].stringValue

        var arrayStatements = dataDict["risk_statement"].arrayValue
        for i in 0..<arrayStatements.count
        {
            var dict = arrayStatements[i]
            dict["isSelected"].stringValue = "0"
            arrayStatements[i] = dict
        }
        
        dict["statements"] = JSON(arrayStatements)
        
        arrayRisk.append(dict)
        
        strFactorHeading = dataDict["title"].stringValue
        strNotesDescription = dataDict["notes"].stringValue
        
        print("Array Risk - ",arrayRisk)
        tblRisk.isHidden = false
        tblRisk.reloadData()
    }
}
//MARK:- Service
extension RiskVC
{
    func getRiskStatements()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            
            let relationId = categoryDict?["rid"].stringValue ?? ""
            let relationName = categoryDict?["rname"].stringValue ?? ""
            let factorValue = checkSelectedFactor()

            RiskService().RiskStatementService(relationId:relationId,cname : relationName,factor : factorValue, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.setUpData(dataDict : json["data"])
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
