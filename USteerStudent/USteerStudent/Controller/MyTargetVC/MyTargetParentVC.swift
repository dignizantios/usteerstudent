//
//  MyTargetParentVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import CarbonKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON


class MyTargetParentVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var mainView: UIView!
    
    //MARK: - Varibales
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = NSArray()
    var categoryDict = JSON()
    
    //MARK: - Life cycle methods

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        items = [getCommonString(key: "My_target_key").uppercased(),
                 getCommonString(key: "Messages_key").uppercased()
        ]
        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: mainView)
        setUpUI()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        style()
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: - UI Setup
extension MyTargetParentVC
{
    func setUpUI()
    {
        //setUpNavigationBarWithTitleAndSideMenu(strTitle : getCommonString(key: "My_target_key").capitalized)
    }
}
//MARK: - Carbon kit
extension MyTargetParentVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        let width = UIScreen.main.bounds.size.width
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        let tabWidth = (width / CGFloat(items.count))
        let indicatorcolor: UIColor = MySingleton.sharedManager.themeDarkGrayColor
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        carbonTabSwipeNavigation.toolbar.barTintColor = MySingleton.sharedManager.themeYellowColor
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.white, font: UIFont.systemFont(ofSize: 12))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.white, font: UIFont.boldSystemFont(ofSize: 12))
        
        //carbonTabSwipeNavigation.carbonTabSwipeScrollView.setContentOffset(<#T##contentOffset: CGPoint##CGPoint#>, animated: false)
    }
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        
        switch index
        {
            case 0:
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "TargetVC") as! TargetVC
                return vc
            case 1 :
                let vc = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
                return vc
            default:
                let vc = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "TargetVC") as! TargetVC
                return vc
        }
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, didMoveAt index: UInt)
    {
    }
}
//MARK: - Service
extension MyTargetParentVC 
{
}
