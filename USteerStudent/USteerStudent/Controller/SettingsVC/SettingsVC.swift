//
//  SettingsVC.swift
//  USteerStudent
//
//  Created by Usteer on 2/17/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
import UserNotifications


class SettingsVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var lblNotification : UILabel!
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var lblSoundNotification : UILabel!

    @IBOutlet var btnSwitchNotification : UIButton!
    @IBOutlet var btnSwitchEmail : UIButton!
    @IBOutlet var btnSoundNotification : UIButton!
    
    @IBOutlet var btnEditProfile : UIButton!
    @IBOutlet var btnChangePassword : UIButton!
    @IBOutlet var btnAccount : UIButton!
    @IBOutlet var btnAgreement : UIButton!
    @IBOutlet weak var btnChangeCarOutlet: UIButton!
    
    //MARK:- Variablde declaration
    var dictProfile : JSON?
    var isUpdateProfile = false
    var arrayOptions : [JSON] = []

    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUiSetUP()
        setUpNotifications()
        getUserProfile()
    }
    
    func setUpNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(getUpdatedProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
    }
    
    @objc func getUpdatedProfile()
    {
        getUserProfile()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
    override func viewDidLayoutSubviews()
    {
        [btnEditProfile,btnChangePassword,btnAgreement,btnAccount].forEach({
            $0?.layer.cornerRadius = ($0?.bounds.size.height)! / 2
            $0?.layer.masksToBounds = true
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        GlobalVariables.isOnSeetingScreen = false

    }
    
    @objc func defaultsChanged()
    {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.getNotificationSettings { (settings) in
            guard settings.authorizationStatus == .authorized else {return}
            if settings.soundSetting == .enabled
            {
                self.btnSoundNotification.isSelected = false
            }
            else {
                self.btnSoundNotification.isSelected = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI setup
extension SettingsVC
{
    func setUiSetUP()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Settings_key"))
        [lblNotification,lblEmail,lblSoundNotification].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        })
        
        fetchNotificationSettingService()
        
        [btnEditProfile,btnChangePassword,btnAccount,btnAgreement,btnChangeCarOutlet].forEach({
            buttonUISetup($0!, textColor: MySingleton.sharedManager.themeDarkGrayColor)
            $0?.layoutIfNeeded()
            self.view.layoutIfNeeded()
        })
        
        btnEditProfile.setTitle(getCommonString(key: "Edit_profile_key").uppercased(), for: .normal)
        btnChangePassword.setTitle(getCommonString(key: "Change_password_key").uppercased(), for: .normal)
//        btnDeleteAccount.setTitle(getCommonString(key: "Delete_account_key").uppercased(), for: .normal)
        btnAccount.setTitle(getCommonString(key: "Account_key").uppercased(), for: .normal)
        btnAgreement.setTitle(getCommonString(key: "Licence_agreement_key").uppercased(), for: .normal)
    }
}
//MARK: - Switch Methods
extension SettingsVC
{
    @IBAction func btnChangeCarAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "SelectCarVC") as! SelectCarVC
        obj.dictProfileData = dictProfile
        obj.handlerChangeCar = {[weak self] in
            self?.getUserProfile()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnNotificationAction(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        saveEmailSettingNotification(switchValue : "pn",isOnValue : sender.isSelected ? "1" : "0")
    }
    
    @IBAction func btnEmailAction(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        saveEmailSettingNotification(switchValue : "em",isOnValue : sender.isSelected ? "1" : "0")
    }
    
    @IBAction func btnSoundNotificationAction(_ sender: UIButton)
    {
        sender.isSelected = !sender.isSelected
        notificationsoundSettingService(isOnValue : sender.isSelected ? "0" : "1")
    }
    
    func openNotificationSetting()
    {
        let alert = UIAlertController(title: "", message: "For change sound setting redirect to app settings?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel))
        alert.addAction(UIAlertAction(title:  getCommonString(key: "Go_to_setting_key"), style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    UIApplication.shared.openURL(settingsUrl as URL)
                    // Fallback on earlier versions
                }
            }
        })
        present(alert, animated: true)
    }
}


//MARK: - Button Action
extension SettingsVC
{
    @IBAction func btnEditProfileAction(_ sender : UIButton)
    {
        let obj = objProfileStoryboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        obj.dictProfileData = dictProfile
        obj.arrayOptions = arrayOptions
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnChangePasswordAction(_ sender : UIButton)
    {
        let obj = objProfileStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnAgreementAction(_ sender : UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "PrivacyPoilcyVC") as! PrivacyPoilcyVC
        obj.selectedParentPrivacyVC = .agreement
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    @IBAction func btnAccountAction(_ sender : UIButton)
    {
        let obj = objSubscriptionStoryboard.instantiateViewController(withIdentifier: "AccountSubVC") as! AccountSubVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: - Service
extension SettingsVC
{
    func fetchNotificationSettingService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            SettingsService().fetchNotificationSettingService(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.btnSwitchNotification.isSelected = data["pn"].stringValue == "1" ? true : false
                        self.btnSwitchEmail.isSelected = data["em"].stringValue == "1" ? true : false
                        self.btnSoundNotification.isSelected = data["sn"].stringValue == "1" ? false : true
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func saveEmailSettingNotification(switchValue : String,isOnValue : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            SettingsService().saveSettingsService(isOn: isOnValue, switchValue: switchValue, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func notificationsoundSettingService(isOnValue : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            SettingsService().saveSoundNotificationService(isOn: isOnValue, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let strMessage = isOnValue=="0" ? "Notifications will now be silent" : "Notifications will have a sound"
                        showToast(message: strMessage)
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

//MARK: - Service
extension SettingsVC
{
    @objc func getUserProfile()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            ProfileService().getProfile(userId: getUserDetail("user_id"), completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        var dictDecrepted = JSON()
                        dictDecrepted = data
                        dictDecrepted["lastname"].stringValue = decreptedString(string: data["lastname"].stringValue)
                        dictDecrepted["user_role"].stringValue = decreptedString(string: data["user_role"].stringValue)
                        dictDecrepted["username"].stringValue = decreptedString(string: data["username"].stringValue)
                        dictDecrepted["short_name"].stringValue = decreptedString(string: data["short_name"].stringValue)
                        dictDecrepted["email"].stringValue = decreptedString(string: data["email"].stringValue)
                        dictDecrepted["birthdate"].stringValue = decreptedString(string: data["birthdate"].stringValue)
                        dictDecrepted["fullname"].stringValue = decreptedString(string: data["fullname"].stringValue)
                        dictDecrepted["name"].stringValue = decreptedString(string: data["name"].stringValue)
                        dictDecrepted["gender"].stringValue = decreptedString(string: data["gender"].stringValue)
                        print("Data Decripted:", dictDecrepted)
                        self.dictProfile = dictDecrepted
                        self.getRole()
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getRole()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            Registration().getUserRole(param: [:], completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        var strSelectedRole = [String]()
                        var arr = json["data"].arrayValue
                        
                        for i in 0..<arr.count{
                            var dict = arr[i]
                            dict["role_id"].stringValue = decreptedString(string: dict["role_id"].stringValue)
                            dict["role_name"].stringValue = decreptedString(string: dict["role_name"].stringValue)
                            arr[i] = dict
                        }
                        
                        self.arrayOptions = arr
                        print("self.arrayOptions : \(self.arrayOptions)")
                        let arrUserRole = (self.dictProfile!["user_role"].stringValue).components(separatedBy: ",")
                        for i in 0..<arrUserRole.count
                        {
                            let roleId = arrUserRole[i]
                            for j in 0..<self.arrayOptions.count
                            {
                                var dict = self.arrayOptions[j]
                                dict["isSelected"] = "0"
                                if roleId == dict["role_id"].stringValue
                                {
                                    strSelectedRole.append(dict["role_name"].stringValue)
                                }
                                self.arrayOptions[j] = dict
                            }
                        }
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func userLogoutService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["uid" : encryptString(string: getUserDetail("user_id")),
                                           "session" : getUserDetail("session")]
            ProfileService().userLogout(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

