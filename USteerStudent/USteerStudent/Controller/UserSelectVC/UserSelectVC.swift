//
//  UserSelectVC.swift
//  USteerStudent
//
//  Created by Usteer on 9/13/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
class UserSelectVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var lblTitle : UILabel!
    
    @IBOutlet var lblPrivateUser : UILabel!
    @IBOutlet var lblSchoolUniversityUser : UILabel!

    @IBOutlet var lblPrivateUserTitle : UILabel!
    @IBOutlet var lblSchoolUniversityUserTitle : UILabel!
    
    @IBOutlet var btnPrivateUser : UIButton!
    @IBOutlet var btnSchoolUniversityUser : UIButton!
    
    @IBOutlet var vwPrivateUser : UIView!
    @IBOutlet var vwSchoolUniversityUser : UIView!

    @IBOutlet var btnStart : UIButton!

    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK: - UI setup
extension UserSelectVC
{
    func setUpUI()
    {
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblTitle.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        [vwPrivateUser,vwSchoolUniversityUser].forEach { (view) in
            view?.layer.cornerRadius = 5
            view?.layer.masksToBounds = true
            view?.layer.borderWidth = 1
        }
        
        [lblPrivateUserTitle,lblSchoolUniversityUserTitle].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        }
        
        [lblPrivateUser,lblSchoolUniversityUser].forEach { (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        }
        
        btnStart.backgroundColor = MySingleton.sharedManager.themeYellowColor
        btnStart.layer.cornerRadius = btnStart.frame.size.height/2
        btnStart.layer.masksToBounds = true
        btnStart.setTitleColor(MySingleton.sharedManager.themeDarkGrayColor, for: .normal)
        btnStart.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblPrivateUser.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        setUnSelectedPrivateUser()
        setUnSelectedSchoolUniversity()
    }
    
    func setSelectedPrivateUser()
    {
        btnPrivateUser.isSelected = true
        btnSchoolUniversityUser.isSelected = false
        vwPrivateUser.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
        lblPrivateUserTitle.textColor = MySingleton.sharedManager.themeYellowColor
    }
    func setUnSelectedPrivateUser()
    {
        btnPrivateUser.isSelected = false
        btnSchoolUniversityUser.isSelected = false
        vwPrivateUser.layer.borderColor = MySingleton.sharedManager.themeLightGrayColor.cgColor
        lblPrivateUserTitle.textColor = MySingleton.sharedManager.themeLightGrayColor
    }
    func setSelectedSchoolUniversity()
    {
        btnSchoolUniversityUser.isSelected = true
        btnPrivateUser.isSelected = false

        vwSchoolUniversityUser.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
        lblSchoolUniversityUserTitle.textColor = MySingleton.sharedManager.themeYellowColor
    }
    func setUnSelectedSchoolUniversity()
    {
        btnSchoolUniversityUser.isSelected = false
        btnPrivateUser.isSelected = false
        vwSchoolUniversityUser.layer.borderColor = MySingleton.sharedManager.themeLightGrayColor.cgColor
        lblSchoolUniversityUserTitle.textColor = MySingleton.sharedManager.themeLightGrayColor
    }
}
//MARK: - Button Action
extension UserSelectVC
{
    @IBAction func btnPrivateUserAction(_ sender : UIButton)
    {
        setUnSelectedSchoolUniversity()
        setSelectedPrivateUser()
        btnPrivateUser.isSelected = true
    }
    @IBAction func btnSchoolUniversityUserAction(_ sender : UIButton)
    {
        setUnSelectedPrivateUser()
        setSelectedSchoolUniversity()
        btnSchoolUniversityUser.isSelected = true
    }
    @IBAction func btnStartAction(_ sender : UIButton)
    {
        if(btnPrivateUser.isSelected == false && btnSchoolUniversityUser.isSelected == false)
        {
            showToast(message: "Please select user private or not")
            return
        }
        saveUserPrivateService(is_private : btnPrivateUser.isSelected ? "1" : "0")
    }
}
//MARK: - Service
extension UserSelectVC
{
    //
    func saveUserPrivateService(is_private : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
             showLoader()
            BasicTaskService().saveUserPrivate(is_private : is_private,completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    let dict = json["data"]
                    if(json["status"].stringValue == "1")
                    {
                        Defaults.setValue(false, forKey: "isUserSelectFirstTime")
                        self.setPrivateUserValueToUserDetails(is_private:is_private)
                        let frontViewController = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
                        frontViewController.getAppVersion()
                        appDelegate.navigateToController(frontViewController : frontViewController)
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                        let dictNotification = dict["notification"]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
