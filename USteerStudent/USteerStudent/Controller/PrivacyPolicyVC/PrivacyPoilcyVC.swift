//
//  PrivacyPoilcyVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/4/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import WebKit

enum CheckParentPolicyNavigation
{
    case policy
    case sidemenu
}

enum selectedParentPolicyVC
{
    case privacy
    case agreement
}

class PrivacyPoilcyVC: UIViewController
{
    private let webView = WKWebView(frame: .zero)
    var selectedParentPrivacyVC = selectedParentPolicyVC.privacy
    var selectedParentPolicyNavigation = CheckParentPolicyNavigation.policy

    override func viewDidLoad()
    {
        super.viewDidLoad()
        webView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(self.webView)
        NSLayoutConstraint.activate([
            self.webView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.webView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.webView.topAnchor.constraint(equalTo: self.view.topAnchor),
        ])
        self.view.setNeedsLayout()
        setUpUI()
    }
}
//MARK: - UI Setup
extension PrivacyPoilcyVC
{
    func setUpUI()
    {
        if(selectedParentPolicyNavigation == .sidemenu)
        {
            setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Privacy_policy_key").capitalized)
        }
        else
        {
            setUpNavigationBarWithTitleAndBackPolicy(strTitle : getCommonString(key: "Privacy_policy_key").capitalized)
        }
        if(selectedParentPrivacyVC == .privacy)
        {
            getPrivacyPolicyService()
        }
        else
        {
            getLicenceAgreementService()
        }
    }
    
    func setHTMLText(data : JSON)
    {
        let strHTML =  data["body"].stringValue
        let strHeading =  data["Heading"].stringValue
        webView.loadHTMLString(strHeading + "<br/>" + strHTML, baseURL: nil)
    }
}
//MARK: - Service
extension PrivacyPoilcyVC 
{
    func getPrivacyPolicyService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            BasicTaskService().GetPrivacyPolicyService(isPrivacy : true,completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.setHTMLText(data: data)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getLicenceAgreementService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            BasicTaskService().GetPrivacyPolicyService( isPrivacy : false,completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.setHTMLText(data: data)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
