//
//  AssesmentSliderVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/8/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

import AVFoundation

enum AssessmentParentVC
{
    case tree
    case journey
}
class SliderInfoCollectionviewCell : UICollectionViewCell
{
    //MARK: - outlets
    @IBOutlet var lblInfo : UILabel!
    
}
class AssesmentSliderVC: UIViewController
{
    //MARK: - outlets
    @IBOutlet var lblInfo : UILabel!
    @IBOutlet var btnPlayAudio : UIButton!
    @IBOutlet var vwInfoSlider : UIView!
    @IBOutlet var collectionviewInfo : UICollectionView!
    @IBOutlet var pageControlInfo : UIPageControl!

    
    @IBOutlet var vwAudioSelect : UIView!
    @IBOutlet var btnMaleVoice : UIButton!
    @IBOutlet var btnFemaleVoice : UIButton!
    @IBOutlet var btnNoAudio : UIButton!
    @IBOutlet var lblChooseAudioHeading : UILabel!

    
    //MARK: - Variables
    var count = 0
    var arrayIntroStatements:[Any] = []
    var isPlaying = false
    var audioPlayer: AVPlayer?
    var lastContentOffset: CGPoint = .zero
    var isSelfAssessment = false
    var categoryDict = JSON()
    var isAudioFinished = false
    
    var dictAssessment = JSON()
    var MaleAudioURL = ""
    var FemaleAudioURL = ""
    
    var checkTreeParentVC = AssessmentParentVC.tree

    
    lazy var COLLECTION_VIEW_WIDTH: CGFloat = {
        return CGFloat(self.collectionviewInfo.frame.size.width / 2)
    }()
    
    fileprivate let TRANSFORM_CELL_VALUE = CGAffineTransform(scaleX: 1, y: 1) // Or define other transform effects here
    fileprivate let ANIMATION_SPEED = 0.5

    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        vwAudioSelect.isHidden = true
        // Do any additional setup after loading the view.
        setUpUI()
    }
    override func viewDidLayoutSubviews()
    {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)

        btnPlayAudio.layer.cornerRadius = btnPlayAudio.bounds.size.height / 2
        btnPlayAudio.layer.masksToBounds = true
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        count = 0
        
        if(checkTreeParentVC == .journey)
        {
            getAssessments(dict: categoryDict)
            getRelationService()
        }else{
//            setupData()
            setUpAssessmentData()
        }
//        self.collectionviewInfo.decelerationRate = UIScrollViewDecelerationRateNormal

    }
    override var prefersStatusBarHidden : Bool {
        return false
    }
}
//MARK: - UI Setup
extension AssesmentSliderVC
{
    func setUpUI()
    {
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)
        lblInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        btnPlayAudio.setTitle(getCommonString(key: "Play_audio_key").uppercased(), for: .normal)
        buttonUISetup(btnPlayAudio, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        pageControlInfo.pageIndicatorTintColor = MySingleton.sharedManager.themeChatGrayColor
        pageControlInfo.currentPageIndicatorTintColor = MySingleton.sharedManager.themeYellowColor
        
        lblChooseAudioHeading.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblChooseAudioHeading.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        
        [btnMaleVoice,btnFemaleVoice,btnNoAudio].forEach { (button) in
            button?.setTitleColor(MySingleton.sharedManager.themeDarkGrayColor, for: .normal)
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        }
    }
    func setUpAssessmentData()
    {
        count = 0
        collectionviewInfo.contentOffset.x = 0
        pageControlInfo.currentPage = 0
        pageControlInfo.numberOfPages = 0
        
        var arrayAssessmentLocal = GlobalAssessmentDict["assessment"].arrayValue
        if(arrayAssessmentLocal.count > 0)
        {
            var dictAssessmentLocal = arrayAssessmentLocal[currentAssessment]
            arrayIntroStatements = []
            arrayIntroStatements = dictAssessmentLocal["intros"].arrayObject ?? []
            let dictAudio = dictAssessmentLocal["gender_audio"] ?? JSON()
            let maleAudioURL = dictAudio["male"].stringValue
            let femaleAudioURL = dictAudio["female"].stringValue
            if(isSelfAssessment)
            {
                if(maleAudioURL != "")
                {
                    print("maleAudioURL - ",maleAudioURL)
                    print("femaleAudioURL - ",femaleAudioURL)
                    
                    vwInfoSlider.isHidden = true
                    vwAudioSelect.isHidden = false
                    self.MaleAudioURL = maleAudioURL
                    self.FemaleAudioURL = femaleAudioURL
                    
                }else{
                    vwAudioSelect.isHidden = true
                    if(arrayIntroStatements.count != 0)
                    {
                        vwInfoSlider.isHidden = false
                    }else{
                        self.navigateToQuestions()
                    }
                }
            }else{
                vwAudioSelect.isHidden = true
                if(arrayIntroStatements.count != 0) {
                    vwInfoSlider.isHidden = false
                } else {
                    self.navigateToQuestions()
                }
            }
            
            
            collectionviewInfo.reloadData()
            pageControlInfo.numberOfPages = arrayIntroStatements.count
            
            
            dictAssessmentLocal["start_time"] =  JSON(DateToString(Formatter: server_dt_format, date: Date()))
            arrayAssessmentLocal[currentAssessment] = dictAssessmentLocal
            GlobalAssessmentDict["assessment"] = JSON(arrayAssessmentLocal)
            setupLeftNavigationButton(strTitle : "\(dictAssessmentLocal["rname"].stringValue)")
        }
        else{
            
            setupLeftNavigationButton(strTitle : getCommonString(key: "Slider_key"))
        }
    }
    /*func setupData()
    {
        count = 0
        collectionviewInfo.contentOffset.x = 0
        pageControlInfo.currentPage = 0
        pageControlInfo.numberOfPages = 0
        let arrayAssessmentLocal = assessmentDict?["assessment"].arrayValue
        if(arrayAssessmentLocal?.count ?? 0 > 0)
        {
            let dictAssessmentLocal = arrayAssessmentLocal?[currentAssessment]
            arrayIntroStatements = []
            arrayIntroStatements = dictAssessmentLocal?["intros"].arrayObject ?? []
            
            let dictAudio = dictAssessmentLocal?["gender_audio"] ?? JSON()
            
            let maleAudioURL = dictAudio["male"].stringValue
            let femaleAudioURL = dictAudio["female"].stringValue
            
            if(isSelfAssessment)
            {
                if(maleAudioURL != "")
                {
                    print("maleAudioURL - ",maleAudioURL)
                    print("femaleAudioURL - ",femaleAudioURL)

                    vwInfoSlider.isHidden = true
                    vwAudioSelect.isHidden = false
                   // playAudio(audioURL : audioURL)
                    self.MaleAudioURL = maleAudioURL
                    self.FemaleAudioURL = femaleAudioURL

                }else{
                    vwAudioSelect.isHidden = true
                    if(arrayIntroStatements.count != 0)
                    {
                        vwInfoSlider.isHidden = false
                    }else{
                        self.navigateToQuestions()
                    }
                }
            }else{
                vwAudioSelect.isHidden = true
                if(arrayIntroStatements.count != 0) {
                    vwInfoSlider.isHidden = false
                } else {
                    self.navigateToQuestions()
                }
            }
            
            collectionviewInfo.reloadData()
            pageControlInfo.numberOfPages = arrayIntroStatements.count
            
            ////Update assessment start date
            let strStartDate = getFormatedDate(date: Date())
            let assessmentQuery = "update tbl_assessment set start_time = '\(strStartDate)' where assessment_id = \(dictAssessmentLocal?["aid"].intValue ?? 0) and is_resume=0"
            if(executeQuery(strQuery: assessmentQuery))
            {
                print("assessment start time updated")
            }
            setupLeftNavigationButton(strTitle : "\(dictAssessmentLocal?["rname"].stringValue ?? "")")

        }else{

            setupLeftNavigationButton(strTitle : getCommonString(key: "Slider_key"))
        }
        
    }*/
    func setupLeftNavigationButton(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(backAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        //        HeaderLabel.adjustsFontSizeToFitWidth = true
        //        HeaderLabel.minimumScaleFactor = 0.5
        //        HeaderLabel.lineBreakMode = .byTruncatingTail
        self.navigationItem.titleView = HeaderLabel
        
    }
    
    @objc func backAction() {
        let alert = UIAlertController(title:"", message: "\(R.string.validationMessage.quit_assessment_key())", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            self.deleteAllAssessments()
            self.navigationController?.popViewController(animated: true)

        }))
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    func playAudio(audioURL : String) {
        //playSoundWithURL(audioURL : audioURL)
    }
}
//MARK: - Button action
extension AssesmentSliderVC
{
    @IBAction func btnPlayAudioAction(_ sender:UIButton)
    {
        if(!isPlaying)
        {
            if(isAudioFinished)
            {
                self.navigateToQuestions()
            }else{
                audioPlayer?.play()
            }
        }else
        {
            audioPlayer?.pause()
        }
        
        isPlaying = !isPlaying
        setPlayPauseTitle()
    }
    @IBAction func btnAudioSelectAction(_ sender:UIButton)
    {
        [btnFemaleVoice,btnMaleVoice,btnNoAudio].forEach { (button) in
            button?.isSelected = false
        }
        if(sender == btnFemaleVoice)
        {
            btnFemaleVoice.isSelected = true
            playSoundWithURL(isPlayMale: false)
        }else if(sender == btnMaleVoice)
        {
            btnMaleVoice.isSelected = true
            playSoundWithURL(isPlayMale: true)
        }else{
            btnNoAudio.isSelected = true
            if(arrayIntroStatements.count != 0)
            {
                vwInfoSlider.isHidden = false
            }else{
                self.navigateToQuestions()
            }
        }
        vwAudioSelect.isHidden = true
    }
}
//MARK:- Scrollview Methods
extension AssesmentSliderVC
{
    /*func scrollViewWillBeginDragging(_ scrollView: UIScrollView)
    {
        if(scrollView == collectionviewInfo)
        {
            count = pageControlInfo.currentPage
            print("Count - ",count)
            
                // moved right
                print("Move right")
                UIView.animate(withDuration: ANIMATION_SPEED, animations: {
                    //                        cell?.transform = self.TRANSFORM_CELL_VALUE
                    //                cell?.setNonIdentityLayout()
                    self.collectionviewInfo.scrollToItem(at: IndexPath(item: self.count, section: 0), at: .centeredHorizontally, animated: true)
                })
        }
    }*/
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool)
    {
        if(scrollView == collectionviewInfo)
        {
            count = pageControlInfo.currentPage
            print("Count - ",count)
            if(count == arrayIntroStatements.count-1)
            {
                if (self.lastContentOffset.x < scrollView.contentOffset.x)
                {
                    // moved right
                    print("Move right")
                    navigateToQuestions()
                } else if (self.lastContentOffset.x > scrollView.contentOffset.x) {
                    // moved left
                    print("Move Left")
                }
                else{
                    print("Didn't move")
                }
            }
            
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if(scrollView == collectionviewInfo)
        {
            let pageNumber = Int(round(collectionviewInfo.contentOffset.x / collectionviewInfo.frame.size.width))
            pageControlInfo.currentPage = Int(pageNumber)
            self.lastContentOffset.x = scrollView.contentOffset.x
        }
    }
    
    //-----
    /*func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        // Do tableView data refresh according to the corresponding pages here
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>)
    {
        // Scroll to corresponding position
        let pageWidth: Float = Float(COLLECTION_VIEW_WIDTH)  // width + space
        let currentOffset: Float = Float(scrollView.contentOffset.x)
        let targetOffset: Float = Float(targetContentOffset.pointee.x)
        var newTargetOffset: Float = 0
        
        if targetOffset > currentOffset {
            newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth
        }
        else {
            newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth
        }
        if newTargetOffset < 0 {
            newTargetOffset = 0
        }
        else if (newTargetOffset > Float(scrollView.contentSize.width)){
            newTargetOffset = Float(Float(scrollView.contentSize.width))
        }
        
        targetContentOffset.pointee.x = CGFloat(currentOffset)
        scrollView.setContentOffset(CGPoint(x: CGFloat(newTargetOffset), y: scrollView.contentOffset.y), animated: true)
        
        // Set transforms
        let identityIndex: Int = Int(newTargetOffset / pageWidth)
        
        var cell = collectionviewInfo.cellForItem(at: IndexPath.init(row: identityIndex, section: 0)) as? SliderInfoCollectionviewCell
        
        UIView.animate(withDuration: ANIMATION_SPEED, animations: {
            cell?.transform = CGAffineTransform.identity
//            cell?.setIdentityLayout()
        })
        
        // right cell
        cell = collectionviewInfo.cellForItem(at: IndexPath.init(row: identityIndex + 1, section: 0)) as? SliderInfoCollectionviewCell
        
        UIView.animate(withDuration: ANIMATION_SPEED, animations: {
            cell?.transform = self.TRANSFORM_CELL_VALUE
//            cell?.setNonIdentityLayout()
        })
        
        // left cell, which is not necessary at index 0
        if (identityIndex != 0) {
            cell = collectionviewInfo.cellForItem(at: IndexPath.init(row: identityIndex - 1, section: 0)) as? SliderInfoCollectionviewCell
            
            UIView.animate(withDuration: ANIMATION_SPEED, animations: {
                cell?.transform = self.TRANSFORM_CELL_VALUE
//                cell?.setNonIdentityLayout()
            })
        }
    }*/
}
//MARK:- Collectionview Datasource Methods
// tell the collection view how many cells to make
extension AssesmentSliderVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayIntroStatements.count
    }
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SliderInfoCollectionviewCell", for: indexPath as IndexPath) as! SliderInfoCollectionviewCell
        
        cell.lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        cell.lblInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
        cell.lblInfo.text = arrayIntroStatements[indexPath.row] as? String ?? ""
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if(arrayIntroStatements.count == 1)
        {
            navigateToQuestions()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var cellWidth : CGFloat = 0.0
        var cellheight : CGFloat = 0.0
        
        if #available(iOS 11.0, *) {
            
            let guide = view.safeAreaLayoutGuide
            
            if(screenRect.size.width >= 812 && UI_USER_INTERFACE_IDIOM() == .phone)
            {
                print(" guide height - \(guide.layoutFrame.size.height) ------ guide width - \(guide.layoutFrame.size.width)")
                cellWidth = guide.layoutFrame.size.height
                cellheight = guide.layoutFrame.size.width
            }else{
                cellWidth  = screenRect.size.height-44
                cellheight  = screenRect.size.width
            }
        } else {
            // Fallback on earlier versions
            cellWidth  = screenRect.size.height-44
            cellheight  = screenRect.size.width
        }
        
        print("width - \(cellWidth) ------ height - \(cellheight)")
        return CGSize(width: cellheight , height:CGFloat(cellWidth))
    }

}
//MARK: - Other action
extension AssesmentSliderVC
{
    
    func navigateToQuestions()
    {
        let obj = objAssesmentStoryboard.instantiateViewController(withIdentifier: "QuestionsVC") as! QuestionsVC
        obj.categoryDict = self.categoryDict
        obj.checkTreeParentVC = checkTreeParentVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func setPlayPauseTitle()
    {
        
        if(isPlaying){
            btnPlayAudio.setTitle(getCommonString(key: "Pause_audio_key").uppercased(), for: .normal)
        }else{
            btnPlayAudio.setTitle(getCommonString(key: "Play_audio_key").uppercased(), for: .normal)
        }
    }
    func playSoundWithURL(isPlayMale : Bool)
    {
        isAudioFinished = false
        var strUrl = ""

        if(isPlayMale)
        {
            strUrl = self.MaleAudioURL
        }else{
            strUrl = self.FemaleAudioURL
        }
        print("Audio URL - ",strUrl)
        if(strUrl != "")
        {
            let url = URL(string: strUrl)!
            let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                print("documentsURL - ",documentsURL)

                documentsURL.appendPathComponent(url.lastPathComponent)
                return (documentsURL, [.removePreviousFile])
            }
            
            showLoader()
            
            let request = download(url, to: destination).authenticate(user: R.string.keys.basicAuthenticationUsername(), password: R.string.keys.basicAuthenticationPassword())
            
            request.responseData(completionHandler: { (response) in
                
                self.hideLoader()
                
                if response.result.isSuccess, let data = response.result.value {
                    print(data)
                    
                    print(FileManagerHelper.getAllFiles())
                    
                    if let fileURL = FileManagerHelper.searchFile(with: url.lastPathComponent) {
                        let playerItem = AVPlayerItem(url: fileURL)
                        self.audioPlayer = AVPlayer(playerItem:   playerItem)
                        self.audioPlayer?.rate = 1.0
                        self.audioPlayer?.volume = 1.0
                        self.isPlaying = false
                        self.audioPlayer?.pause()
                        self.setPlayPauseTitle()
                        
                        NotificationCenter.default.addObserver(self, selector: #selector(self.playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.audioPlayer?.currentItem)
                    }
                }
            })
        } else {
            isPlaying = false
            setPlayPauseTitle()
            showToast(message: R.string.staticMessages.audio_not_play_key())
        }
        
    }
    @objc func playerItemDidReachEnd(){
        print("Finished")
        isAudioFinished = true
        
        showToast(message: R.string.staticMessages.audio_finished())
        
        isPlaying = false
        setPlayPauseTitle()
        self.navigateToQuestions()
        /*if(arrayIntroStatements.count != 0)
        {
            vwInfoSlider.isHidden = false
        }else{
            self.navigateToQuestions()
        }*/
    }
}
//MARK: - Other methods
extension AssesmentSliderVC
{
    func setUpAssessmentQuestions(dict : JSON?,categoryDict:JSON)
    {
        /*assessmentDict = JSON()
        assessmentDict = dict
        saveAssessmentToDB(categoryDict:categoryDict)*/
        
        GlobalAssessmentDict = dict!
        setData()
        
    }
    /*func saveAssessmentToDB(categoryDict:JSON)
    {
        let userId = getUserDetail("user_id")
        
        let arrayAssessmentLocal = assessmentDict?["assessment"].arrayValue
        if(arrayAssessmentLocal?.count ?? 0 > 0)
        {
            deleteAllAssessments()
            
            for i in 0..<(arrayAssessmentLocal?.count)!
            {
                let dict = arrayAssessmentLocal?[i]
                let assessmentId = dict?["aid"].intValue ?? 0
                let AssessmentCategoryId = dict?["rid"].intValue ?? 0
                let AssessmentCname = dict?["rname"].stringValue ?? ""
                
                let dicAudio = dict?["gender_audio"] ?? JSON()
                let AssessmentMaleAudioURL = dicAudio["male"].stringValue
                let AssessmentFemaleAudioURL = dicAudio["female"].stringValue
                
                let Query = String(format: "insert into tbl_assessment(user_id,category_id,assessment_id,category_name,maleAudio,femaleAudio) values(\(userId),\(AssessmentCategoryId),\(assessmentId),'\(AssessmentCname)','\(AssessmentMaleAudioURL)','\(AssessmentFemaleAudioURL)')")
                
                if(executeQuery(strQuery: Query))
                {
                    print("assessment inserted")
                }
                
                let arrayIntros = dict?["intros"].arrayObject as? [String] ?? [""]
                arrayIntros.forEach({ (strStatement) in
                    
                    ///----------
                    let introQuery = String(format: "insert into tbl_intro_sentences(statement,assessment_id) values('\(strStatement)',\(assessmentId))")
                    if(executeQuery(strQuery: introQuery))
                    {
                        print("Intro statements inserted")
                    }
                    //------
                })
                
                let arrayClosers = dict?["closers"].arrayObject as? [String] ?? [""]
                arrayClosers.forEach({ (strStatement) in
                    
                    ///----------
                    let ClosersQuery = String(format: "insert into tbl_closer_statements(statement,assessment_id) values('\(strStatement)',\(assessmentId))")
                    if(executeQuery(strQuery: ClosersQuery))
                    {
                        print("Closer statements inserted")
                    }
                    //------
                })
                
                let arrayQuestions = dict?["que_n_ans"].arrayValue
                var questionId = 0
                var answerId = 0
                
                for j in 0..<(arrayQuestions?.count)!
                {
                    let questionDict = arrayQuestions?[j]
                    
                    let question = questionDict?["question"].stringValue ?? ""
                    
                    let selectIdQuery = "select max(id) as question_id from tbl_questions"
                    let ary = executeSelectQuery(strQuery: selectIdQuery)
                    
                    if(ary.count > 0)
                    {
                        let dictQuestion = ary.object(at: 0) as! NSDictionary
                        questionId = dictQuestion["question_id"] as? Int ?? 0
                        
                        if(questionId == 0)
                        {
                            questionId = 1
                        }else{
                            questionId = questionId + 1
                        }
                        
                    }
                    
                    let questionQuery = String(format: "insert into tbl_questions(assessment_id,question_id,question) values(\(assessmentId),\(questionId),'\(question)')")
                    if(executeQuery(strQuery: questionQuery))
                    {
                        print("question inserted")
                    }
                    
                    let arrayAnswers = questionDict?["options"].arrayValue
                    
                    for k in 0..<(arrayAnswers?.count)!
                    {
                        let answer = arrayAnswers?[k] ?? ""
                        
                        let selectIdQuery = "select max(id) as answer_id from tbl_answers"
                        let ary = executeSelectQuery(strQuery: selectIdQuery)
                        
                        if(ary.count > 0)
                        {
                            let dictQuestion = ary.object(at: 0) as! NSDictionary
                            answerId = dictQuestion["answer_id"] as? Int ?? 0
                            
                            if(answerId == 0)
                            {
                                answerId = 1
                            }else{
                                answerId = answerId + 1
                            }
                        }
                        
                        let selectMaxIdQuery = "select max(id) as question_id from tbl_questions"
                        let maxAry = executeSelectQuery(strQuery: selectMaxIdQuery)
                        
                        if(maxAry.count > 0)
                        {
                            let dictQuestion = maxAry.object(at: 0) as! NSDictionary
                            questionId = dictQuestion["question_id"] as? Int ?? 0
                            
                            if(questionId == 0)
                            {
                                questionId = 1
                            }
                        }
                        
                        let answerQuery = String(format: "insert into tbl_answers(question_id,answer_id,answer) values(\(questionId),\(answerId),'\(answer)')")
                        if(executeQuery(strQuery: answerQuery))
                        {
                            print("answer inserted")
                        }
                    }
                }
            }
            setData()
            
            //
        }else{
            showToast(message: R.string.validationMessage.no_assessment_available_key())
            
        }
    }*/
    func setData()
    {
        if(categoryDict["rname"].stringValue.lowercased() == "self")
        {
            self.isSelfAssessment  = true
        }else{
            self.isSelfAssessment  = false
        }
//        setupData()
        setUpAssessmentData()
    }
}
//MARK: - Audio Player methods
extension AssesmentSliderVC : AVAudioPlayerDelegate
{
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool)
    {
        
        print("finish plying")
        player.stop()
        isPlaying = false
        setPlayPauseTitle()
        self.navigateToQuestions()
        /*if(arrayIntroStatements.count != 0)
        {
            vwInfoSlider.isHidden = false
        }else{
            self.navigateToQuestions()
        }*/
    }
    
}
//MARK: - Service
extension AssesmentSliderVC 
{
    func getAssessments(dict:JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "rid" : "\(dict["rid"].stringValue)"
            ]
            AssessmentService().getAssessmentQuestions(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.setUpAssessmentQuestions(dict : json["data"],categoryDict:self.categoryDict)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getRelationService()
    {
        
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            ////showLoader()
            
            RouteMapService().GetSingleRelatinoTreeMapService(relationId: decreptedString(string: categoryDict["rid"].stringValue), completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                       self.categoryDict = json["data"]
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
        
    }
}

