//
//  AssesmentLaunchVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/8/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

enum checkParentController
{
    case userGuidance
    case assessmentLaunch
}

class AssesmentLaunchVC: UIViewController
{
    //MARK: - outlets
    @IBOutlet var lblInfo : UILabel!
    @IBOutlet var btnLaunch : UIButton!

    @IBOutlet var vwNoAssessment : UIView!
    @IBOutlet var lblNoAssessment : UILabel!
    @IBOutlet var vwPopUp : UIView!

    @IBOutlet var vwContent : UIView!
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var vwBack : UIView!
    
    var categorydict : JSON?
    var selectedParentVC = checkParentController.assessmentLaunch
    var timer = Timer()

    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
        if(appLoadingFirstTime)
        {
            getAppVersion()
        }
    }
    
    override func viewDidLayoutSubviews()
    {
        btnLaunch.layer.cornerRadius = btnLaunch.bounds.size.height / 2
        btnLaunch.layer.masksToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        if(Defaults.value(forKey: R.string.keys.isLaunchFirstTime()) != nil)
        {
            Defaults.setValue(false, forKey: R.string.keys.isLaunchFirstTime())
            vwPopUp.isHidden = false
            if(isPrivateUser() == 1)
            {
                lblMessage.text = "Welcome! We are supporting the community during Coronavirus by giving you totally free access to USTEER. We won’t ask for any payment until September when, hopefully, the worst of the crisis will have passed. We hope USTEER helps you through this challenging time."
            }else{
                lblMessage.text = "Welcome! Link to your school or university via the app to receive tutor support."
            }
        }
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }

    override var prefersStatusBarHidden : Bool {
        return false
    }
}

//MARK: - Pop up delegate
extension AssesmentLaunchVC : InfoPopUpDelegate
{
    func dismissPopUp()
    {
        print("Dismiss")
    }
}
//MARK: - UI Setup
extension AssesmentLaunchVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle : getCommonString(key: "Training_key").capitalized)
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblInfo.textColor = .black
        lblInfo.attributedText = getCommonString(key: "Launch_assessment_info_title_key").lineSpacing(space: 10)
        lblInfo.textAlignment = .center
        
        if(selectedParentVC == .userGuidance)
        {
            btnLaunch.setTitle(getCommonString(key: "Training_key").uppercased(), for: .normal)
        }
        else
        {
            btnLaunch.setTitle(getCommonString(key: "Start_steering_key").uppercased(), for: .normal)
        }
        buttonUISetup(btnLaunch, textColor: UIColor.white)
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        
        lblNoAssessment.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblNoAssessment.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblNoAssessment.text = R.string.validationMessage.no_assessment_available_key().uppercased()
        
        vwNoAssessment.isHidden = true
        
        vwContent.layer.cornerRadius = 5
        vwContent.layer.masksToBounds = true
        vwContent.backgroundColor = MySingleton.sharedManager.themeLightGrayColor
        
        lblMessage.textColor = UIColor.white
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        
        let tapRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwBack.addGestureRecognizer(tapRelation)
    }
    @objc func hideView()
    {
        vwPopUp.isHidden = true
    }
}
//MARK: - Button action
extension AssesmentLaunchVC
{
    @IBAction func btnLaunchAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "UserGuidanceVC") as! UserGuidanceVC
        obj.selectedParentVC = .assessmentLaunch
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK: - Other methods
extension AssesmentLaunchVC
{
    func naviagteToRelation()
    {
        let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
        obj.selectedVC = selectedTreeController.AssesmentLaunchVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func navigateToQuestion()
    {
        let obj = objAssesmentStoryboard.instantiateViewController(withIdentifier: "QuestionsVC") as! QuestionsVC
        obj.categoryDict = self.categorydict ?? JSON()
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func navigateToSlider()
    {
        let obj = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentSliderVC") as! AssesmentSliderVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK: - Service
extension AssesmentLaunchVC 
{
    func downLoadAudio()
    {
       
    }
    
   
    func getRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let arrayAssessentLocal = GlobalAssessmentDict["assessment"].arrayValue
            let dictAssessment = arrayAssessentLocal[currentAssessment]
            let relationID = dictAssessment["rid"].stringValue ?? ""
            
            RouteMapService().GetRelationDetailService(relationID : relationID,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.categorydict = json["data"]
                        self.navigateToQuestion()

                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
        
    }
}
//MARK: - Swreveal controller
/*extension AssesmentLaunchVC : SWRevealViewControllerDelegate
{
    func revealController(_ revealController: SWRevealViewController!, willMoveTo position: FrontViewPosition) {
        
    }
    
    func disableControlInteraction()
    {
        for view in self.view.subviews {
            if let tf = view as? UITextField{
                tf.isUserInteractionEnabled = false
                // Do Something
            }
            else if let btn = view as? UIButton
            {
                btn.isUserInteractionEnabled = false
            }
            
        }
    }
}*/
