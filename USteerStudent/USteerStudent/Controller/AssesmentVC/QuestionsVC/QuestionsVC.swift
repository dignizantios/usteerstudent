//
//  QuestionsVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/8/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster


class AnswerCollectionviewCell : UICollectionViewCell
{
    //MARK: - outlets
    @IBOutlet var lblAnswer : UILabel!
    @IBOutlet var vwBack : UIView!
    
}
class QuestionsVC: UIViewController
{
    //MARK: - outlets
    @IBOutlet var collectionviewAnswers : UICollectionView!
    @IBOutlet var constraintCollectionviewAnswersHeight : NSLayoutConstraint!
    @IBOutlet var vwContent : UIView!
    @IBOutlet var lblQuestion : UILabel!
    @IBOutlet var lblTotalQuestion : UILabel!

    //
    @IBOutlet var vwInfo : UIView!
    @IBOutlet var lblInfoTitle : UILabel!
    @IBOutlet var lblInfoSubTitle : UILabel!
    @IBOutlet var btnMoveSecondPart : UIButton!

    //MARK: - Varibales
    var count = 0
    var arrayQuestions : [JSON] = []
    var currentQuestionsIndex = 0
    var categoryDict = JSON()
    var checkTreeParentVC = AssessmentParentVC.tree

    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        print("Questions Category dict - ",categoryDict)
        setUpUI()
    }
    
    override func viewDidLayoutSubviews()
    {
        btnMoveSecondPart.layer.cornerRadius = btnMoveSecondPart.bounds.size.height / 2
        btnMoveSecondPart.layer.masksToBounds = true
        collectionviewAnswers.layoutIfNeeded()
        collectionviewAnswers.layoutSubviews()
        collectionviewAnswers.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.landscapeRight, andRotateTo: UIInterfaceOrientation.landscapeRight)
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        
        collectionviewAnswers.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
         collectionviewAnswers.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    //MARK:- Overide Method
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if object is UICollectionView
        {
            self.constraintCollectionviewAnswersHeight.constant = collectionviewAnswers.contentSize.height
        }
    }
}
//MARK: - UI Setup
extension QuestionsVC
{
    func setUpUI()
    {
        setupQuestionNavigationBar()
        
        lblQuestion.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblQuestion.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 18)
        
        lblInfoTitle.textColor = MySingleton.sharedManager.themeGreenTextColor
        lblInfoTitle.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        lblInfoSubTitle.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblInfoSubTitle.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        btnMoveSecondPart.setTitle(getCommonString(key: "Move_to_second_part_key").uppercased(), for: .normal)
        buttonUISetup(btnMoveSecondPart, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        
        vwInfo.isHidden = true
        setUpQuestions()
    }
    func setupQuestionNavigationBar()
    {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "Question_key") + " - \(getCategoryName())")
        setupLeftNavigationButton()
    }
    func setupInfoNavigationBar()
    {
        setUpNavigationBarWithTitle(strTitle: getCommonString(key: "First_part_completed_key") + " - \(getCategoryName())")
        setupLeftNavigationButton()
    }
    func getCategoryName() -> String
    {
        var relationName = ""
        let arrayAssessentLocal = GlobalAssessmentDict["assessment"].arrayValue
        if(arrayAssessentLocal.count > 0)
        {
            let dictAssessment = arrayAssessentLocal[currentAssessment]
            relationName = dictAssessment["rname"].stringValue
        }
        return relationName
    }
    func setupLeftNavigationButton()
    {
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(backAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
    }
    
    @objc func backAction()
    {
        if(currentQuestionsIndex>0)
        {
            vwContent.BackOut()
            self.perform(#selector(self.backQuestionLoadAnimation), with: nil, afterDelay: 0.2)
        }
        else
        {
            let alert = UIAlertController(title: kCommonAlertTitle, message: "\(R.string.validationMessage.quit_assessment_key())", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
                self.deleteAllAssessments()
                if(self.checkTreeParentVC == .journey)
                {
                    self.navigateToJourney()
                }else{
                    self.navigateToTree()
                }
            }))
            alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .default, handler: { action in
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    @objc func backQuestionLoadAnimation()
    {
        if(currentQuestionsIndex > 0)
        {
            let dict = arrayQuestions[currentQuestionsIndex - 1]
            lblQuestion.text = dict["question"].stringValue
            currentQuestionsIndex = currentQuestionsIndex - 1
            collectionviewAnswers.reloadData()
            lblTotalQuestion.text = "\(currentQuestionsIndex+1)/\(arrayQuestions.count)"
        }
    }
    
    func setUpQuestions()
    {
        let arrayAssessentLocal = GlobalAssessmentDict["assessment"].arrayValue
        if(arrayAssessentLocal.count > 0)
        {
            let dictAssessment = arrayAssessentLocal[currentAssessment]
            self.arrayQuestions = dictAssessment["que_n_ans"].arrayValue
            if(arrayQuestions.count > 0)
            {
                for i in 0..<arrayQuestions.count
                {
                    let dict = arrayQuestions[i]
                    let isCheckAttended = dict["is_attended"].boolValue
                    if(!isCheckAttended)
                    {
                        currentQuestionsIndex=i
                        break
                    }else{
                        currentQuestionsIndex=i
                    }
                }
                let dictLocal = arrayQuestions[currentQuestionsIndex]
                lblQuestion.text = dictLocal["question"].stringValue
            }
            collectionviewAnswers.reloadData()
            lblTotalQuestion.text = "\(currentQuestionsIndex+1)/\(arrayQuestions.count)"

        }else{
            showToast(message: R.string.validationMessage.no_questions_key())
        }
    }
}
//MARK:- Collectionview Datasource Methods
extension QuestionsVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if(arrayQuestions.count > 0)
        {
            let dict = arrayQuestions[currentQuestionsIndex]
            return dict["options"].arrayValue.count
        }
        return 0
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AnswerCollectionviewCell", for: indexPath as IndexPath) as! AnswerCollectionviewCell
        let dict = arrayQuestions[currentQuestionsIndex]


        cell.lblAnswer.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12)
        cell.lblAnswer.textColor = .white
        
        let ary = dict["options"].arrayValue
        cell.lblAnswer.text = ary[indexPath.row].stringValue
        if(dict["is_attended"].boolValue == true)
        {
            if(dict["answer_no"].stringValue == "\(indexPath.row + 1)")
            {
                cell.vwBack.backgroundColor = MySingleton.sharedManager.themeLightGrayColor
            }else{
                cell.vwBack.backgroundColor = MySingleton.sharedManager.themeYellowColor
            }
        }else{
            cell.vwBack.backgroundColor = MySingleton.sharedManager.themeYellowColor
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var dict = arrayQuestions[currentQuestionsIndex]
        dict["is_attended"].boolValue = true
        dict["answer_no"].stringValue = "\(indexPath.row+1)"
        arrayQuestions[currentQuestionsIndex] = dict
        updateAttendedAnswerValues(indexPath : indexPath)
        collectionviewAnswers.reloadData()
        
        if(currentQuestionsIndex+1 < arrayQuestions.count)
        {
            vwContent.Out()
            self.perform(#selector(self.reloadQuestions), with: dict, afterDelay: 0.2)
        }else{
            collectionviewAnswers.reloadData()
            print("last ans selected")
            isCheckEndSections()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellWidth  = (collectionView.frame.size.width)/6
        return CGSize(width: cellWidth , height:CGFloat(cellWidth))
    }
}

//MARK: - Button action
extension QuestionsVC
{
    @IBAction func btnCloseSuccessPopupAction(_ sender:UIButton)
    {
        navigateToTree()
    }
    @IBAction func btnFirstPartCompletedAction(_ sender:UIButton)
    {
        if(sender.title(for: .normal) == getCommonString(key: "Click_here_to_begin_key").uppercased())
        {
            vwInfo.isHidden = true
            setupQuestionNavigationBar()
            var arrayAssessmentLocal = GlobalAssessmentDict["assessment"].arrayValue
            if(arrayAssessmentLocal.count > 0)
            {
                if(currentAssessment < ((arrayAssessmentLocal.count)-1))
                {
                    let dictAssement = arrayAssessmentLocal[currentAssessment]
                    arrayAssessmentLocal[currentAssessment] = dictAssement
                    GlobalAssessmentDict["assessment"] = JSON(arrayAssessmentLocal)
                    currentAssessment = currentAssessment + 1
                    vwInfo.isHidden = false
                }else{
                    SubmitAssessmentData()
                }
            }
        }
        else
        {
            lblInfoTitle.isHidden = true
            lblInfoSubTitle.text = "Next you are imagine your school being within your space"
            lblInfoSubTitle.textColor = MySingleton.sharedManager.themeGreenTextColor
            btnMoveSecondPart.setTitle(getCommonString(key: "Click_here_to_begin_key").uppercased(), for: .normal)
        }
    }
}
//MARK: - Other methods
extension QuestionsVC
{
    @objc func reloadQuestions()
    {
        let dict = arrayQuestions[currentQuestionsIndex + 1]
        lblQuestion.text = dict["question"].stringValue
        currentQuestionsIndex = currentQuestionsIndex + 1
        lblTotalQuestion.text = "\(currentQuestionsIndex+1)/\(arrayQuestions.count)"
        collectionviewAnswers.reloadData()
    }
    
    func updateAttendedAnswerValues(indexPath : IndexPath)
    {
        var dictAssessmentLocal = GlobalAssessmentDict
        var arrayAssessmentLocal = dictAssessmentLocal["assessment"].arrayValue
        var dictAssement = arrayAssessmentLocal[currentAssessment]
        dictAssement["end_time"].stringValue = DateToString(Formatter: server_dt_format, date: Date())

        var dict = arrayQuestions[currentQuestionsIndex]
        dict["answer_no"].stringValue = "\(indexPath.row + 1)"
        dict["time_taken"].stringValue = DateToString(Formatter: server_dt_format, date: Date())
        arrayQuestions[currentQuestionsIndex] = dict
        
        dictAssement["que_n_ans"] = JSON(arrayQuestions)
        arrayAssessmentLocal[currentAssessment] = dictAssement
        dictAssessmentLocal["assessment"] = JSON(arrayAssessmentLocal)
        GlobalAssessmentDict = dictAssessmentLocal
        
    }
    
    func navigateToAssessmentStart()
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers
        {
            if aViewController is AssesmentSliderVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
    func navigateToSteering()
    {
        let obj = objReportStoryboard.instantiateViewController(withIdentifier: "MySteeringVC") as! MySteeringVC
        if(checkTreeParentVC == .journey)
        {
            obj.isShowLatestButton = false
            obj.isSelectedFromVC = selectedReportController.myjourneyAssessment
             categoryDict["rname"].stringValue = decreptedString(string: categoryDict["rname"].stringValue)
        }else{
            obj.isShowLatestButton = true
            obj.isSelectedFromVC = selectedReportController.QuestionsVC
        }
        obj.categoryDict = categoryDict
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func navigateToTree()
    {
        var isFound = false
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ChooseRelationVC {
                currentAssessment = 0
                selectedMenuIndex = 1
                isFound = true
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadTreeData"), object: nil)
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
        if(!isFound)
        {
            selectedMenuIndex = 1
            let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            self.revealViewController().setFront(rearNavigation, animated: true)
        }
    }
    func navigateToJourney()
    {
        selectedMenuIndex = 3
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CharacteristicsParentVC") as! CharacteristicsParentVC
        if(self.categoryDict["is_target_set"].boolValue)
        {
            obj.isFromAssessmentAfterTarget = true
        }
        obj.selectedJourneyParentVC = .assessment
        obj.categorydict = categoryDict
        
        let rearNavigation = UINavigationController(rootViewController: obj)
        rearNavigation.isNavigationBarHidden = false
        self.revealViewController().setFront(rearNavigation, animated: true)
    }
    func isCheckEndSections()
    {
        var arrayAssessmentLocal = GlobalAssessmentDict["assessment"].arrayValue
        if(arrayAssessmentLocal.count > 0)
        {
            if(currentAssessment < ((arrayAssessmentLocal.count)-1))
            {
                let dictAssement = arrayAssessmentLocal[currentAssessment]
                arrayAssessmentLocal[currentAssessment] = dictAssement
                GlobalAssessmentDict["assessment"] = JSON(arrayAssessmentLocal)
                currentAssessment = currentAssessment + 1
                vwInfo.isHidden = false
            }else{
                //submitAnswers()
                SubmitAssessmentData()
            }
        }
    }
}
//MARK: - Service
extension QuestionsVC
{
    func SubmitAssessmentData()
    {
        var arrayAnswerSubmit : [JSON] = []
        let arrayAssessments = GlobalAssessmentDict["assessment"].arrayValue
        for i in 0..<arrayAssessments.count
        {
            let dictAssessment = arrayAssessments[i]
            var dictAssessmentAnswer = JSON()
            dictAssessmentAnswer["aid"].stringValue = dictAssessment["aid"].stringValue
            dictAssessmentAnswer["rid"].stringValue = dictAssessment["rid"].stringValue
            dictAssessmentAnswer["start_time"].stringValue = dictAssessment["start_time"].stringValue
            dictAssessmentAnswer["end_time"].stringValue = dictAssessment["end_time"].stringValue
            dictAssessmentAnswer["timezone"].stringValue = getCurrentTimeZone()
            
            var arrayAttendedAnswer : [String] = []
            var arrayTimeTaken : [String] = []
            
            let arrayQuestions = dictAssessment["que_n_ans"].arrayValue

            for j in 0..<arrayQuestions.count
            {
                let dictQuestion = arrayQuestions[j]
                arrayAttendedAnswer.append(dictQuestion["answer_no"].stringValue)
                arrayTimeTaken.append(dictQuestion["time_taken"].stringValue)
                
            }
            dictAssessmentAnswer["answer"].stringValue = encryptString(string: arrayAttendedAnswer.joined(separator: ","))
            dictAssessmentAnswer["track_time"].stringValue = arrayTimeTaken.joined(separator: "#")
            arrayAnswerSubmit.append(dictAssessmentAnswer)
        }
        submitAnswerService(arrayAnswerSubmit: arrayAnswerSubmit)
    }
    func submitAnswers()
    {
        var arrayAnswerSubmit : [JSON] = []
        
        let selectAssessments = "select * from tbl_assessment where user_id = \(Int(getUserDetail("user_id")) ?? 0)"
        let selectAssessmentsArray = executeSelectQuery(strQuery: selectAssessments)
        
        if(selectAssessmentsArray.count > 0)
        {
            for i in 0..<selectAssessmentsArray.count
            {
                let assessmentDict = selectAssessmentsArray.object(at: i) as! NSDictionary
                let assessmentDictJSON = JSON(assessmentDict)
                
                let dateStartTime = StringToDate(Formatter: "yyyyMMddHHmmss", strDate: assessmentDictJSON["start_time"].stringValue)
                let dateEndTime = StringToDate(Formatter: "yyyyMMddHHmmss", strDate: assessmentDictJSON["end_time"].stringValue)
                
                let localDateStartTime = dateStartTime.toLocalTime()
                let localDateEndTime = dateEndTime.toLocalTime()
                
                let strStartTime = DateToString(Formatter: "yyyyMMddHHmmss", date: localDateStartTime)
                let strEndTime = DateToString(Formatter: "yyyyMMddHHmmss", date: localDateEndTime)

                var dictAssessmentAnswer = JSON()
                dictAssessmentAnswer["aid"].stringValue = assessmentDictJSON["assessment_id"].stringValue
                dictAssessmentAnswer["rid"].stringValue = assessmentDictJSON["category_id"].stringValue
                dictAssessmentAnswer["start_time"].stringValue = strStartTime
                dictAssessmentAnswer["end_time"].stringValue = strEndTime
                dictAssessmentAnswer["timezone"].stringValue = getCurrentTimeZone()
                
                let selectQuestions = "select * from tbl_questions where assessment_id = \(assessmentDictJSON["assessment_id"].intValue)"
                let selectQuestionsArray = executeSelectQuery(strQuery: selectQuestions)
                print("Assessment questions - ",selectQuestionsArray)
                var answerArray : [String] = []
                var timeArray : [String] = []

                for j in 0..<selectQuestionsArray.count
                {
                    let answerDict = selectQuestionsArray.object(at: j) as! NSDictionary
                    let answerDictJSON = JSON(answerDict)
                    let strTimeTaken = answerDictJSON["time_taken"].stringValue
                    answerArray.append(answerDictJSON["answer_no"].stringValue)
                    timeArray.append(strTimeTaken)
                }
                
                let strAnswer = answerArray.joined(separator: ",")
                let strTime = timeArray.joined(separator: "#")
                
                dictAssessmentAnswer["answer"].stringValue = encryptString(string: strAnswer)
                dictAssessmentAnswer["track_time"].stringValue = strTime
                arrayAnswerSubmit.append(dictAssessmentAnswer)
            }
            print("Submited answer - ",arrayAnswerSubmit)
            if(arrayAnswerSubmit.count > 1)
            {
                showToast(message: "Please check for given assessment")
            }
            else
            {
                //TODO: - submit assessment uncomment
                print("arraySubmitAnswer final - ",arrayAnswerSubmit)
                submitAnswerService(arrayAnswerSubmit : arrayAnswerSubmit)
            }
        }
        else{
            showToast(message: R.string.validationMessage.no_assessment_submit_key())
        }
    }
    func submitAnswerService(arrayAnswerSubmit : [JSON])
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            var json = JSON()
            json["uid"].stringValue = encryptString(string: getUserDetail("user_id"))
            json["assessment"] = JSON(arrayAnswerSubmit)
            json["device_type"].stringValue = "\(deviceType)"
            json["device_id"].stringValue = getFirebaseToken()
            var parameters : [String:Any] = ["assessment_data":json]
            let strFinalTime = json.rawString(String.Encoding.utf8, options: JSONSerialization.WritingOptions.prettyPrinted)!
            parameters["assessment_data"] = strFinalTime
            print("parameter - ",parameters)
           
            //TODO: - uncomment for service call
            AssessmentService().submitAnswers(param: parameters, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        var isShowNoOption = true
                        var message = ""
                        var okMessage = getCommonString(key: "Yes_key").uppercased() + "!"
                        
                        if (json["data"]["show_school_popup"].boolValue == true) && (json["data"]["disable_see_chart"].boolValue == true)
                        {
                            okMessage = "OK"
                            isShowNoOption = false
                            message = json["message"].stringValue
                        }
                        else
                        {
                            if self.categoryDict["is_target_set"].boolValue{
                                 message = "Do you want to see if you're closer to your signpost?"
                            }else{
                                 message = "Do you want to see your steering now?"
                            }
                        }
                       
                        let alert = UIAlertController(title: kCommonAlertTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: okMessage, style: .default, handler: { action in
                            
                            if(self.checkTreeParentVC == .journey)
                            {
                                self.navigateToJourney()
                            }
                            else
                            {
                                if (json["data"]["show_school_popup"].boolValue == true) && (json["data"]["disable_see_chart"].boolValue == true)
                                {
                                    self.navigateToTree()
                                }
                                else
                                {
                                    if(self.categoryDict["is_target_set"].boolValue) //After set Targte
                                    {
                                        self.navigateToJourney()
                                    }
                                    else
                                    {
                                        self.navigateToSteering()
                                    }
                                }
                            }
                        }))
                        if(isShowNoOption)
                        {
                            alert.addAction(UIAlertAction(title: getCommonString(key: "Not_now_key").uppercased(), style: .default, handler: { action in
                                if(self.checkTreeParentVC == .journey)
                                {
                                    self.navigateToSteering()
                                }
                                else
                                {
                                    self.navigateToTree()
                                }
                            }))
                        }
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        let message = json["message"].stringValue
                        let alert = UIAlertController(title: kCommonAlertTitle, message: message, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: getCommonString(key: "Retry_key"), style: .default, handler: { action in
                            self.submitAnswerService(arrayAnswerSubmit : arrayAnswerSubmit)
                        }))
                        alert.addAction(UIAlertAction(title: R.string.validationMessage.quit_assessment_key(), style: .default, handler: { action in
                            self.deleteAllAssessments()
                            self.navigateToTree()
                        }))
                        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
                            
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

