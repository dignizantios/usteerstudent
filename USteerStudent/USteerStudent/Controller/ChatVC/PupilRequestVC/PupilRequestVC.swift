//
//  PupilRequestVC.swift
//  USteerStudent
//
//  Created by Usteer on 7/24/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage




class PupilRequestVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblPupilRequest: UITableView!
    
    //MARK: - Variables
    var strErrorMessage = ""
    var arrayPupils : [JSON] = []

    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getRequestedPupilListService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblPupilRequest.addSubview(self.refreshControl)
        tblPupilRequest.tableFooterView = UIView()
        
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
//MARK: - UI Setup
extension PupilRequestVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Inviatations_key"))
        
        getRequestedPupilListService()
    }
}
//MARK: - UITableview Delegate
extension PupilRequestVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayPupils.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        tableView.backgroundView = nil
        return arrayPupils.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PupilListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PupilListTableViewCell
        
        cell.selectionStyle = .none
        
        let dict = arrayPupils[indexPath.row]
        cell.lblName.text = dict["rname"].stringValue
        
        //0=none, 1=send, 2=accept, 3=decline, 4=terminate
        cell.btnAction.isHidden = false
        
        
        if(dict["status"].stringValue == "1")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeYellowColor
            cell.btnAction.setTitle("Send", for: .normal)
        }
        else if(dict["status"].stringValue == "2")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeGreenColor
            cell.btnAction.setTitle("Accept", for: .normal)
        }
        else if(dict["status"].stringValue == "3")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeBlueColor
            cell.btnAction.setTitle("Decline", for: .normal)
        }
        else if(dict["status"].stringValue == "4"){
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeRedColor
            cell.btnAction.setTitle("Terminate", for: .normal)
        }
        else{
            cell.btnAction.isHidden = true
        }
        
        
        
        cell.btnAction.tag = indexPath.row
        cell.btnAction.backgroundColor = MySingleton.sharedManager.themeGreenColor
        cell.btnAction.addTarget(self, action: #selector(btnActionClick), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}

//MARK: - Button Action
extension PupilRequestVC
{
    @objc func btnActionClick(_ sender : UIButton)
    {
        let dict = arrayPupils[sender.tag]
        
        let alert = UIAlertController(title: "Are you sure you want to accept request?", message: "", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { action in
            
            self.sendRequestToPupilService(dict : dict)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
}
//MARK: - Service
extension PupilRequestVC 
{
    
    func getRequestedPupilListService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            ChatService().getRequestedPupilListService(completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            let data = json["data"]
                            self.arrayPupils = data["detail"].arrayValue
                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblPupilRequest.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func sendRequestToPupilService(dict : JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
           
            
            let param : [String:String] = ["receiver_id" : dict["receiver_id"].stringValue,
                                           "rname" : dict["rname"].stringValue,
                                           "status" : dict["status"].stringValue,
                                           "rid" : dict["rid"].stringValue
                
            ]
            
            
            ChatService().sendRequestActionService(param : param,completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            showToast(message: json["message"].stringValue)
                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblPupilRequest.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
}
