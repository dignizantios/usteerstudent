//
//  ChatDetailsVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/2/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage


enum ChatParentVC
{
    case chat
    case relation
    case notificaion
    case sideMenu
}

class ChatDetailsVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var tblChatMessage : UITableView!
    @IBOutlet var lblMessagePlaceHolder : UILabel!
    @IBOutlet var lblHiddenMessage : UILabel!
    @IBOutlet var txtvwMessage : UITextView!
    @IBOutlet var viewSendMessage : UIView!
    @IBOutlet var viewSendMessageBack : UIView!
    @IBOutlet var vwDelete : UIView!
    @IBOutlet var constant_vwDelete: NSLayoutConstraint!
    @IBOutlet var lblSelectedCount : UILabel!

    @IBOutlet var btnSend : UIButton!
    @IBOutlet weak var constant_ViewBottom: NSLayoutConstraint!
    @IBOutlet var btnCancel : UIButton!

    @IBOutlet var vwContent : UIView!
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var vwBack : UIView!
    @IBOutlet var vwPopUp : UIView!
    
    
    @IBOutlet var vwSecondContent : UIView!
    @IBOutlet var lblSecondMessage : UILabel!
    @IBOutlet var vwSecondPopUpBack : UIView!
    @IBOutlet var vwSecondPopUp : UIView!

    //MARK: - variables
    var strMessage = ""

    var offset = 0
    var arrayChatList: [JSON] = []
    var strErrorMessage = ""
    var dictChatData = JSON()
    var isFromPush = false
    var categoryDict = JSON()
    var checkChatParentVC = ChatParentVC.chat
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        
        
        self.getChatMessagesListService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        print("checkChatParentVC : \(checkChatParentVC)")
        
        print("chat detail dict - ",dictChatData)    

        tblChatMessage.register(UINib(nibName: "ChatSenderTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        tblChatMessage.register(UINib(nibName: "ChatRecieverTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
        setUpPopUpUI()
        setUpUI()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        
        isOnChatScreen = true
        //Register Notification for keyboard hide/show
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadChatData), name: NSNotification.Name(rawValue: "reloadChatData"), object: nil)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
        
        /*if(checkChatParentVC == .relation)
        {
            presentInfoWindow()
        }else{
            showPopUp()
        }*/
        
    }
    override func viewWillDisappear(_ animated: Bool)
    {
        isOnChatScreen = false
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        //isInChatScreen = false
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK: - UI Setup
extension ChatDetailsVC
{
    func setUpPopUpUI()
    {
        lblSecondMessage.textColor = UIColor.white
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        
        lblMessage.textColor = UIColor.white
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblMessage.text = strMessage
        
        let tapRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideInfoView))
        vwBack.addGestureRecognizer(tapRelation)
        
        
        let tapSecondRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideSecondInfoView))
        vwSecondPopUp.addGestureRecognizer(tapSecondRelation)
        
        
        [vwContent,vwSecondContent].forEach { (view) in
            view?.layer.cornerRadius = 5
            view?.layer.masksToBounds = true
            view?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        }
        
        [vwPopUp,vwSecondPopUp].forEach { (view) in
            view.isHidden = true
        }

    }
    @objc func hideSecondInfoView()
    {
        vwSecondPopUp.isHidden = true
    }
    @objc func hideInfoView()
    {
        vwPopUp.isHidden = true
    }
    @objc func hideView()
    {
        self.dismiss(animated: false, completion: nil)
    }
    func setUpUI()
    {
        if(checkChatParentVC == .chat || checkChatParentVC == .notificaion)
        {
            print("dictChatData - ",dictChatData)
            showSendMessageView()
            if(dictChatData["group_id"].intValue > 0)
            {
                setUpNavigationBarGroupChat(strTitle: decreptedString(string: dictChatData["name"].stringValue))

            }else{
                setUpNavigationBarWithTitleAndBack(strTitle: dictChatData["fullname"].stringValue)
            }
            self.tblChatMessage.addSubview(self.refreshControl)
            getChatMessagesListService()
            
        }
        else if(checkChatParentVC == .sideMenu)
        {
            setUpNavigationBarWithTitleMenuAndRightButton(strTitle : dictChatData["fullname"].stringValue)
            self.tblChatMessage.addSubview(self.refreshControl)
//            showSendMessageView()
            showPopUp()
            getChatMessagesListService()
        }
        else{
            setUpNavigationBarWithTitleAndSideMenu(strTitle: dictChatData["fullname"].stringValue)
            /*if(categoryDict["is_private"].exists())
            {
                if(categoryDict["is_private"].boolValue == false)
                {
                    self.tblChatMessage.addSubview(self.refreshControl)
                    showSendMessageView()
                    getChatMessagesListService()
                }else{
                    hideSendMessageView()
                }
            }
            else{
                hideSendMessageView()
            }*/
            
            /*if(checkChatParentVC == .relation)
             {
             presentInfoWindow()
             }else{
             showPopUp()
             }*/
            
            print("Category dict - ",categoryDict)
            
            if(categoryDict["is_private"].stringValue != "")
            {
                presentInfoWindow()

                if(categoryDict["is_private"].boolValue == false)
                {
                    showPopUp()
                    self.tblChatMessage.addSubview(self.refreshControl)
                    //showSendMessageView()
                    getChatMessagesListService()
                }else{
                    hideSendMessageView()
                }
            }
            else{
                hideSendMessageView()
            }
            
        }
        
        viewSendMessageBack.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
        
        buttonUISetup(btnSend, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        btnSend.setTitle(getCommonString(key: "Send_key"), for: .normal)
        
        lblMessagePlaceHolder.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblMessagePlaceHolder.textColor = MySingleton.sharedManager.themeLightGrayColor
        
        lblSelectedCount.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblSelectedCount.textColor = UIColor.white
        
        txtvwMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tblChatMessage.addGestureRecognizer(tap)
        
        btnCancel.setTitle(getCommonString(key: "Cancel_key").uppercased(), for: .normal)
        btnCancel.setTitleColor(UIColor.white, for: .normal)
        btnCancel.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        vwDelete.backgroundColor = MySingleton.sharedManager.themeYellowColor
        vwDelete.isHidden = true
        self.constant_vwDelete.constant = 60
        
    }
    func checkAllowSendMessage() -> Bool
    {
        let strconvertedDate = DateToString(Formatter : "yyyy-MM-dd",date : Date())
        
        let startDate = "\(strconvertedDate) 07:00:00"
        let endDate = "\(strconvertedDate) 21:00:00"
        
        let dateValueStart = StringToDate(Formatter : "yyyy-MM-dd HH:mm:ss",strDate : startDate)
        let dateValueEnd = StringToDate(Formatter : "yyyy-MM-dd HH:mm:ss",strDate : endDate)
        
        if(Date() > dateValueStart && Date() < dateValueEnd)
        {
            print("Allow send message")
            return true
        }else{
            print("Not allow send message")
            return false
        }
    }
    func setUpNavigationBarWithTitleMenuAndRightButton(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menubar_white_header"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let rightButton = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_request_tutor_sidemenu"), style: .plain, target:self, action: #selector(coachListAction))
        rightButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightButton
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)
        self.navigationItem.titleView = HeaderLabel
    }
    
    @objc func coachListAction(_ sender : UIBarButtonItem)
    {
        let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func presentInfoWindow()
    {
        var strMessage = ""
        
        if(categoryDict["is_private"].exists())
        {
            if(categoryDict["is_private"].boolValue == false)
            {
                strMessage = "You can talk to your tutor about how you are steering this either on the app or face to face."
//                showPopUp()
                vwSecondPopUp.isHidden = false
                lblSecondMessage.text = strMessage
            }
            else if(categoryDict["is_private"].boolValue == true)
            {
                strMessage = "Your steering with \(categoryDict["rname"].stringValue) is private. It's just for you"
                strErrorMessage = strMessage
                vwSecondPopUp.isHidden = true
                vwPopUp.isHidden = false
                lblMessage.text = strMessage
            }
            tblChatMessage.reloadData()
            lblMessagePlaceHolder.text = "I'd like to talk to you about how I am steering \(categoryDict["rname"].stringValue)"
        }
        
        
        
        /*let obj = InfoPopUpVC()
        obj.popUpDelegate = self
        obj.strMessage = strMessage
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)*/
    }
    func showPopUp()
    {
        if(checkAllowSendMessage())
        {
            showSendMessageView()
            vwPopUp.isHidden = true
        }
        else{
            hideSendMessageView()
            lblMessage.text = "USTEER TUTOR MESSAGES CAN BE SENT BETWEEN 7AM AND 9PM"
            vwPopUp.isHidden = false
             /*let obj = InfoPopUpVC()
             obj.popUpDelegate = self
             obj.strMessage = "USTEER TUTOR MESSAGES CAN BE SENT BETWEEN 7AM AND 7PM"
             //            obj.colorPopUpContent = UIColor.init(red: 200/255.0, green: 200/255.0, blue: 200/255.0, alpha: 1.0)
             //            obj.colorText = UIColor.white
             obj.modalPresentationStyle = .overCurrentContext
             obj.modalTransitionStyle = .coverVertical
             self.present(obj, animated: false, completion: nil)*/
            
        }
    }
    func setUpNavigationBarWithTitleButtonAndBack(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderButton.isUserInteractionEnabled = true
        HeaderButton.setTitle(strTitle, for: .normal)
        HeaderButton.setTitleColor(UIColor.white, for: .normal)
//        HeaderLabel.numberOfLines = 2
        HeaderButton.titleLabel?.textAlignment = .center
        HeaderButton.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        HeaderButton.addTarget(self, action: #selector(HeaderButtonAction), for: .touchUpInside)
        //        HeaderLabel.adjustsFontSizeToFitWidth = true
        //        HeaderLabel.minimumScaleFactor = 0.5
        //        HeaderLabel.lineBreakMode = .byTruncatingTail
        self.navigationItem.titleView = HeaderButton
    }
    func setUpNavigationBarGroupChat(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(backButtonAction))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        
        let vwHeader = UIView.init(frame: CGRect(x: 0, y: 0, width: 150, height: 42))
        
        
        let lblGroupName = UILabel.init(frame: CGRect(x: 0, y: 0, width: 150, height: 21))
        lblGroupName.text = strTitle
        lblGroupName.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 12)
        lblGroupName.textColor = UIColor.white
        lblGroupName.textAlignment = .center
        vwHeader.addSubview(lblGroupName)
        
        let lblGroupInfo = UILabel.init(frame: CGRect(x: 0, y: 21, width: 150, height: 21))
        lblGroupInfo.text = "(Click for more info)"
        lblGroupInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10)
        lblGroupInfo.textColor = UIColor.white
        lblGroupInfo.textAlignment = .center
        vwHeader.addSubview(lblGroupInfo)
        
        let HeaderButton = UIButton.init(frame: CGRect(x: 0, y: 0, width: 150, height: 42))
        HeaderButton.isUserInteractionEnabled = true
//        HeaderButton.setTitle(strTitle, for: .normal)
        HeaderButton.setTitleColor(UIColor.white, for: .normal)
        HeaderButton.titleLabel?.textAlignment = .center
        HeaderButton.titleLabel?.lineBreakMode = .byCharWrapping
        HeaderButton.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        HeaderButton.addTarget(self, action: #selector(HeaderButtonAction), for: .touchUpInside)
        //        HeaderLabel.adjustsFontSizeToFitWidth = true
        //        HeaderLabel.minimumScaleFactor = 0.5
        //        HeaderLabel.lineBreakMode = .byTruncatingTail
        vwHeader.addSubview(HeaderButton)

        self.navigationItem.titleView = vwHeader
    }
    @objc func HeaderButtonAction()
    {
        let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "GroupMembersVC") as! GroupMembersVC
        obj.dictGroupData = dictChatData
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
}
//MARK: - Pop up delegate
extension ChatDetailsVC : InfoPopUpDelegate
{
    func dismissPopUp()
    {
        
    }
}
//MARK: - POST Notification calling methods
extension ChatDetailsVC
{
    @objc func reloadChatData(_ notification : NSNotification)
    {
        print("Notification data - ",notification.object)
        print("table reloaded")
        offset = 0
        dictChatData = notification.object as? JSON ?? JSON()
        
        if(Int(decreptedString(string: dictChatData["group_id"].stringValue)) ?? 0 > 0)
        {
            setUpNavigationBarGroupChat(strTitle: decreptedString(string: dictChatData["name"].stringValue))
             dictChatData["group_id"].stringValue = decreptedString(string: dictChatData["group_id"].stringValue)

        }else{
            setUpNavigationBarWithTitleAndBack(strTitle: decreptedString(string: dictChatData["fullname"].stringValue))
        }
        
        print("dictChatData:\(dictChatData)")
        
        getChatMessagesListService()
        
//        if(dictChatData["group_id"].stringValue)
        
        /*offset = 0
         print("notification data -",notification.object)
         let userinfo = notification.object
         getChatList()
         */
    }
}
//MARK: - Geture method
extension ChatDetailsVC
{
    //MARK: - Tap geture
    /// Tap reconizer on table to hide keyboard
    ///
    /// - Parameter sender: geture reconizer
    @objc func hideKeyboard(sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view.endEditing(true)
    }
}
//MARK:- KeyBord Notifiction Action
extension ChatDetailsVC
{
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        {
            let keyboardHeight = keyboardRectValue.height
            print("keyBorad Height : \(keyboardHeight) ")
            UIView.animate(withDuration: 0.5, animations: {
                self.constant_ViewBottom.constant = -keyboardHeight
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.constant_ViewBottom.constant = 0.0
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
}

//MARK: - TableView Delegate Method
extension ChatDetailsVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayChatList.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            lbl.numberOfLines = 3
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayChatList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let dict = arrayChatList[indexPath.row]
        
        if(getUserDetail("user_id") == dict["sender_id"].stringValue)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChatSenderTableViewCell
            
            let longPressRecognizer = UITapGestureRecognizer(target: self, action: #selector(longPressedEvent))
            cell.contentView.tag = indexPath.row
            cell.contentView.addGestureRecognizer(longPressRecognizer)
            
            cell.lblMessage.text = dict["message"].stringValue
            
            if(dictChatData["group_id"].intValue > 0)
            {
                cell.lblUsername.isHidden = false
                cell.lblUsername.text = dict["sender_name"].stringValue

            }else{
                cell.lblUsername.isHidden = true
            }
            
            if(dict["isSelected"].stringValue == "1")
            {
                cell.vwHighlight.isHidden = false
            }else{
                cell.vwHighlight.isHidden = true
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ChatRecieverTableViewCell
            
            /*let longPressRecognizer = UITapGestureRecognizer(target: self, action: #selector(longPressedEvent))
            
            
            cell.contentView.tag = indexPath.row
            cell.contentView.addGestureRecognizer(longPressRecognizer)
            */
            
            cell.lblMessage.text = dict["message"].stringValue
            
            if(dictChatData["group_id"].intValue > 0)
            {
                cell.lblUsername.isHidden = false
                cell.lblUsername.text = dict["sender_name"].stringValue
            }else{
                cell.lblUsername.isHidden = true
            }
            
            if(dict["isSelected"].stringValue == "1")
            {
                cell.vwHighlight.isHidden = false
            }else{
                cell.vwHighlight.isHidden = true
            }
            
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        var dict = arrayChatList[indexPath.row]
        
        if(dict["isSelected"].stringValue == "1")
        {
            dict["isSelected"].stringValue = "0"
        }else{
            dict["isSelected"].stringValue = "1"
        }
        
        arrayChatList[indexPath.row] = dict
        
        
        //print("arrayChatList - ",arrayChatList)
        //        tblChatMessage.reloadRows(at: [IndexPath(row: sender.view?.tag)!, section: 0)], with: .none)
        //        tblChatMessage.reloadData()
        
//        let indexPath = IndexPath(item: indexPath.row, section: 0)
        tblChatMessage.reloadRows(at: [indexPath], with: .none)
        
    }
}
//MARK: - Geture Methods
extension ChatDetailsVC
{
    
    @objc func longPressedEvent(_ sender: UILongPressGestureRecognizer)
    {
        print("longpressed")
        print("sender tag - ",(sender.view?.tag)!)
        var dict = arrayChatList[(sender.view?.tag)!]
        
        if(dict["isSelected"].stringValue == "1")
        {
            dict["isSelected"].stringValue = "0"
        }else{
            dict["isSelected"].stringValue = "1"
        }
        
        arrayChatList[(sender.view?.tag)!] = dict
        
        
        //print("arrayChatList - ",arrayChatList)
//        tblChatMessage.reloadRows(at: [IndexPath(row: sender.view?.tag)!, section: 0)], with: .none)
//        tblChatMessage.reloadData()
        
        let indexPath = IndexPath(item: (sender.view?.tag)!, section: 0)
        tblChatMessage.reloadRows(at: [indexPath], with: .none)
        
        var selectedCount = 0
        if arrayChatList.contains(where: { (json) -> Bool in
            if(json["isSelected"].stringValue == "1")
            {
                return true
            }
            return false
        }){
            showDeleteView()
        }else{
           hideDeleteView()
        }
        
        for i in 0..<arrayChatList.count
        {
            let dict = arrayChatList[i]
            if(dict["isSelected"].stringValue == "1")
            {
                selectedCount = selectedCount + 1
            }
        }
        
        print("selectedCount - ",selectedCount)
        
        lblSelectedCount.text = "\(selectedCount) Selected"
        
    }
}
//MARK: - Button Action
extension ChatDetailsVC
{
    func showDeleteView()
    {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {
            self.constant_vwDelete.constant = 0
            self.view.layoutIfNeeded()
        }, completion: {(result) -> Void in
            self.vwDelete.isHidden = false
        })
        
        hideSendMessageView()

    }
    func hideDeleteView()
    {
        for i in 0..<self.arrayChatList.count
        {
            var dict = self.arrayChatList[i]
            dict["isSelected"].stringValue = "0"
            self.arrayChatList[i] = dict
        }
        self.tblChatMessage.reloadData()

        UIView.animate(withDuration: 0.5, animations: {
            self.constant_vwDelete.constant = 60
            self.view.layoutIfNeeded()
        }, completion: {(result) -> Void in
            self.vwDelete.isHidden = true
        })
        showSendMessageView()
    }
    func hideSendMessageView()
    {
        UIView.animate(withDuration: 0.5, animations: {
            self.constant_ViewBottom.constant = 50
            self.view.layoutIfNeeded()
        }, completion: {(result) -> Void in
            self.viewSendMessage.isHidden = true
        })
    }
    func showSendMessageView()
    {
        UIView.animate(withDuration: 0.5, animations: {
            self.constant_ViewBottom.constant = 0
            self.view.layoutIfNeeded()
        }, completion: {(result) -> Void in
            self.viewSendMessage.isHidden = false
        })
    }
    @IBAction func btnSendMessageAction(_ sender : UIButton)
    {
        if(txtvwMessage.text != "")
        {
            btnSend.isUserInteractionEnabled = false
            sendMessageService()
        }
    }
}
//MARK: - UITextview Methods
extension ChatDetailsVC : UITextViewDelegate
{
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        lblHiddenMessage.text = textView.text
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        let pointInTable:CGPoint = textView.superview!.convert(textView.frame.origin, to: tblChatMessage)
        var contentOffset:CGPoint = tblChatMessage.contentOffset
        contentOffset.y  = pointInTable.y
        
        if let accessoryView = textView.inputAccessoryView {
            contentOffset.y -= accessoryView.frame.size.height
        }
        tblChatMessage.contentOffset = contentOffset
        //Add this executable code after you add this message.
//        var tblframe: CGRect = tblChatMessage.frame
//        tblframe.size.height = self.view.frame.origin.y
//        tblChatMessage.frame = tblframe
//        var bottomoffset: CGPoint = CGPoint(x:0, y:tblChatMessage.contentSize.height - tblChatMessage.bounds.size.height)
//        if bottomoffset.y > 0 {
//            tblChatMessage.contentOffset = bottomoffset;
//        }
        return true
    }
    private func textViewShouldReturn(_ textView: UITextView!) -> Bool
    {
        self.view.endEditing(true)
        return true
    }
    func textViewDidChange(_ textView: UITextView)
    {
        if(textView.text.count == 0)
        {
            lblMessagePlaceHolder.isHidden = false
        }
        else{
            lblMessagePlaceHolder.isHidden = true
        }
    }
}
//MARK: - Other methods
extension ChatDetailsVC
{
    func checkConnectedTutor(dict : JSON)
    {
        let arrayChats = dict["main_chat"].arrayValue
        var isFound = false
        arrayChats.forEach { (json) in
            
            if(json["group_id"].intValue == 0)
            {
                var jsonData = JSON()
                jsonData["user_id"].stringValue = getUserDetail("user_id")
                jsonData["group_id"].stringValue = "0"
                jsonData["name"].stringValue = json["name"].stringValue
                jsonData["fullname"].stringValue = json["fullname"].stringValue
                jsonData["other_id"].stringValue = json["other_id"].stringValue
                jsonData["is_group"].boolValue = false
                jsonData["unread_msg"].stringValue = "0"
                
                self.dictChatData = jsonData
                isFound = true
                self.getChatMessagesListService()
            }
        }
        if(!isFound)
        {
            self.strErrorMessage = "No tutor is connected yet"
            showToast(message: "No tutor is connected yet")
            self.tblChatMessage.reloadData()
        }
    }
}
//MARK: - Button action
extension ChatDetailsVC
{
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        
        hideDeleteView()
        
    }
    @IBAction func btnDeleteAction(_ sender : UIButton)
    {
        
        let alert = UIAlertController(title: kCommonAlertTitle, message: "Are you sure you want to delete messages?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            self.deleteMessage()
        }))
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
        
    }
    func deleteMessage()
    {
        deleteChatService()
        hideDeleteView()
    }
}
//MARK: - Service
extension ChatDetailsVC
{
    
    func sendMessageService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            
            var json = JSON()
            json["user_id"].stringValue = encryptString(string: getUserDetail("user_id"))
            json["other_id"].stringValue = encryptString(string: dictChatData["other_id"].stringValue)
            
            json["msg"].stringValue = encryptString(string: txtvwMessage.text ?? "")
            json["date"].stringValue = DateToString(Formatter: server_dt_format, date: Date())
            json["timezone"].stringValue = getCurrentTimeZone()
            json["device_id"].stringValue = getFirebaseToken()
            json["device_type"].stringValue = "\(deviceType)"
//            json["other_id"].stringValue = categoryDict["other_id"].stringValue

            if(checkChatParentVC == .relation)
            {
                json["other_id"].stringValue = categoryDict["other_id"].stringValue
                json["relation_id"].stringValue = categoryDict["rid"].stringValue
                json["group_id"].stringValue = encryptString(string: "0")
            }else{
                
                 //ParentControlle is SideMenu  so already encrypted id arrived so dont need to decrypt in .sideMenu case(Come From Tutor)
                
                json["other_id"].stringValue = dictChatData["other_id"].stringValue
                json["group_id"].stringValue = encryptString(string: dictChatData["group_id"].stringValue)
                json["relation_id"].stringValue = encryptString(string: "")
            }
            
            
            var param : [String:Any] = [:]
            param["chat_data"] = json
            
            print("param - ",param)

            ChatService().sendMessage(param : param ,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        
                        let data = json["data"]
                        self.view.endEditing(true)
                        self.txtvwMessage.text = ""
                        self.lblMessagePlaceHolder.isHidden = false
                        //self.offset = 0
                        
                        /*
                         {
                         "sender_name" : "Usteer",
                         "is_send" : true,
                         "group_id" : "0",
                         "relation_id" : null,
                         "message_id" : "320",
                         "is_seen" : 1,
                         "message" : "Hello",
                         "receiver_id" : "4",
                         "sender_id" : "1"
                         }
                        */
                        
                        var dataJson = JSON()
                        dataJson["sender_name"].stringValue = decreptedString(string: data["sender_name"].stringValue)
                        dataJson["is_send"].boolValue = true
                        dataJson["group_id"].stringValue = ""
                        dataJson["relation_id"].stringValue = data["relation_id"].stringValue
                        dataJson["message_id"].stringValue = data["msg_id"].stringValue
                        dataJson["is_seen"].intValue = 1
                        dataJson["message"].stringValue = decreptedString(string: data["message"].stringValue)
                        dataJson["receiver_id"].stringValue = decreptedString(string: data["receiver_id"].stringValue)
                        dataJson["sender_id"].stringValue = decreptedString(string: data["sender_id"].stringValue)
                        
                        
                        self.arrayChatList.append(dataJson)
                        self.tblChatMessage.reloadData()
                        /*if(self.arrayChatList.count > 0)
                        {
                            let indexPath = IndexPath(row: self.arrayChatList.count-1, section: 0)
                            self.tblChatMessage.scrollToRow(at: indexPath, at: .bottom, animated: true)
                        }*/
                        
                        showToastWithCustomDelay(message: "Your tutor will aim to reply within 48 hours. If you need a more urgent response, contact your tutor in another way", duration: 4.0)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
                self.btnSend.isUserInteractionEnabled = true
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getChatMessagesListService()
    {
        
        if(self.offset == -1)
        {
            return
        }
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            dictChatUserData = dictChatData
            
            print("dictChatData - ",dictChatData)
            print("other_id - ",dictChatData["other_id"].stringValue)
            print("group_id - ",dictChatData["group_id"].stringValue)

            var json = JSON()
            json["uid"].stringValue = encryptString(string: getUserDetail("user_id"))
            json["device_id"].stringValue = getFirebaseToken()
            json["device_type"].stringValue = "\(deviceType)"
            
            if(checkChatParentVC == .relation)
            {
//                json["relation_id"].stringValue = encryptString(string: categoryDict["rid"].stringValue)
//                json["other_id"].stringValue = encryptString(string: categoryDict["other_id"].stringValue)
//                json["group_id"].stringValue = encryptString(string: "")
                
                json["relation_id"].stringValue = categoryDict["rid"].stringValue
                json["other_id"].stringValue = categoryDict["other_id"].stringValue
                json["group_id"].stringValue = encryptString(string: "")
            }
            else{
                //ParentControlle is SideMenu  so already encrypted id arrived so dont need to decrypt in .sideMenu case(Come From Tutor)
                
//                json["relation_id"].stringValue = encryptString(string: "")
//                json["other_id"].stringValue = encryptString(string: dictChatData["other_id"].stringValue)
//                json["group_id"].stringValue = encryptString(string: dictChatData["group_id"].stringValue)
                
                json["relation_id"].stringValue = ""
                json["other_id"].stringValue = dictChatData["other_id"].stringValue
                json["group_id"].stringValue = encryptString(string: dictChatData["group_id"].stringValue)
            }
            //
            json["next_offset"].stringValue = "\(offset)"
            
            if(checkChatParentVC == .notificaion)
            {
                json["rid"].stringValue = dictChatData["rid"].stringValue

            }
            
            var param : [String:Any] = [:]
            param["chat_data"] = json
            
            print("param - ",param)
            
            ChatService().getChatMessagesList(param : param,completion:
                {(result) in
                
                    if let json = result.value
                    {
                        print("response chat detail - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            let data = json["data"]
                            
                            var array = data["chat_detail"].arrayValue
                            
                            for i in 0..<array.count{
                                var dict = array[i]
                                dict["sender_name"].stringValue = decreptedString(string: dict["sender_name"].stringValue)
                                dict["message"].stringValue = decreptedString(string: dict["message"].stringValue)
                                dict["sender_id"].stringValue = decreptedString(string: dict["sender_id"].stringValue)
                                dict["receiver_id"].stringValue = decreptedString(string: dict["receiver_id"].stringValue)
                                dict["group_id"].stringValue = decreptedString(string: dict["group_id"].stringValue)
                                array[i] = dict
                            }
                            
                            print("array :\(array)")
                            
                            /*if(data["can_send_message"].boolValue == true)
                            {
                                self.viewSendMessage.isHidden = false
                            }else{
                                self.viewSendMessage.isHidden = true
                                self.strErrorMessage = data["message_to_display"].stringValue
                            }*/
                            if(self.offset == 0)
                            {
                                self.arrayChatList = []
                            }
                            array = array.reversed()
                            self.arrayChatList = array + self.arrayChatList
                            
                            
                            for i in 0..<self.arrayChatList.count
                            {
                                var dict = self.arrayChatList[i]
                                dict["isSelected"].stringValue = "0"
                                self.arrayChatList[i] = dict
                            }
                            self.tblChatMessage.reloadData()

                            
                            if(self.arrayChatList.count > 0)
                            {
                                //--------
                                if(self.offset == 0)
                                {
                                    let indexPath = IndexPath(row: self.arrayChatList.count-1, section: 0)
                                    self.tblChatMessage.scrollToRow(at: indexPath, at: .bottom, animated: false)
                                }else{
                                    self.tblChatMessage.scrollsToTop = true
                                }
                                
                                
                            }else{
                                self.tblChatMessage.reloadData()
                            }
                            if(self.checkChatParentVC == .chat)
                            {
                                if(self.dictChatData["unread_msg"].intValue > 0)
                                {
                                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatList"), object: nil)
                                    
                                }
                            }
                            
                            self.offset = data["next_offset"].intValue

                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                            self.tblChatMessage.reloadData()

                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                    }
                    //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func deleteChatService()
    {
        var arrayIds : [String] = []
        for i in 0..<self.arrayChatList.count
        {
            var dict = self.arrayChatList[i]
            if dict["isSelected"].stringValue == "1"
            {
                arrayIds.append(dict["message_id"].stringValue)
            }
        }
        
        print("IDs - ",arrayIds.joined(separator: ","))
        let strIds = arrayIds.joined(separator: ",")
        
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            var json = JSON()
            json["message_id"].stringValue = strIds
            json["user_id"].stringValue = encryptString(string: getUserDetail("user_id"))
            json["group_id"].stringValue = encryptString(string: dictChatData["group_id"].stringValue)
            json["device_id"].stringValue = getFirebaseToken()
            json["device_type"].stringValue = "\(deviceType)"
            
            var param : [String:Any] = [:]
            param["chat_data"] = json
            
            print("param:\(param)")
            
            ChatService().deleteMessage(param : param,completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            self.offset = 0
                            self.getChatMessagesListService()
                            showToast(message: json["message"].stringValue)
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblChatMessage.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
