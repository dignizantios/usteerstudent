//
//  GroupMembersVC.swift
//  USteerStudent
//
//  Created by Usteer on 4/13/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

class GroupMemberTableViewCell: UITableViewCell
{
    
    //MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet var lblAdmin : UILabel!

    
    override func awakeFromNib()
    {
        lblName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblName.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        lblAdmin.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblAdmin.textColor = MySingleton.sharedManager.themeLightGrayColor
        lblAdmin.text = "Admin"
    }
}

class GroupMembersVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblGroupMembers : UITableView!
    @IBOutlet var btnExitGroup : UIButton!
    @IBOutlet var lblTotalParticipants : UILabel!
    @IBOutlet var vwFooterView : UIView!

    //MARK: - Variables
    var arrayGroupMembers : [JSON] = []
    var strErrorMessage = ""
    var dictGroupData = JSON()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getMembersService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tblGroupMembers.addSubview(self.refreshControl)
        tblGroupMembers.tableFooterView = vwFooterView
        setUpUI()
        getMembersService()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI setup
extension GroupMembersVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Group_info_key"))
        
//        btnExitGroup.setTitleColor(MySingleton.sharedManager.themeRedColor, for: .normal)
        btnExitGroup.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        btnExitGroup.layer.cornerRadius = btnExitGroup.frame.size.height / 2
        btnExitGroup.layer.masksToBounds = true
        btnExitGroup.setTitle(getCommonString(key: "Exit_group_key"), for: .normal)
        
        lblTotalParticipants.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblTotalParticipants.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
    }
}
//MARK: - Tableview Methods
extension GroupMembersVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayGroupMembers.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        tableView.backgroundView = nil
        return arrayGroupMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:GroupMemberTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! GroupMemberTableViewCell
        
        cell.selectionStyle = .none
        let dict = arrayGroupMembers[indexPath.row]
        cell.lblName.text = dict["user_name"].stringValue
        
        if(dict["is_admin"].boolValue == true)
        {
            cell.lblAdmin.isHidden = false
        }else{
            cell.lblAdmin.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
//MARK: - Button Action
extension GroupMembersVC
{
    @IBAction func btnExitGroupAction(_ sender : UIButton)
    {
        
        let alert = UIAlertController(title: kCommonAlertTitle, message: "Are you sure you want to exit group?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            
            self.ExitGroupService()
        }))
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
//MARK: - Other methods
extension GroupMembersVC
{
    func navigateToChat()
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ChatVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
    }
}
//MARK: - Service
extension GroupMembersVC
{
    
    func getMembersService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            print("dictGroupData = ",dictGroupData)

            print("group_id = ",dictGroupData["group_id"].stringValue)
            ChatService().GetGroupMembersList(GroupId : dictGroupData["group_id"].stringValue,completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            let data = json["data"]
                            
                            var arrayMember = data["group_detail"].arrayValue
                            
                            for i in 0..<arrayMember.count{
                                var dict = arrayMember[i]
                                dict["user_name"].stringValue = decreptedString(string: dict["user_name"].stringValue)
                                arrayMember[i] = dict
                            }
                            
                            self.arrayGroupMembers = arrayMember
                            self.lblTotalParticipants.text = "\(self.arrayGroupMembers.count) Participants"
                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblGroupMembers.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func ExitGroupService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            print("dictGroupData = ",dictGroupData)
            
            print("group_id = ",dictGroupData["group_id"].stringValue)
            ChatService().ExitGroup(GroupId : dictGroupData["group_id"].stringValue,completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadChatList"), object: nil)

                            self.navigateToChat()
                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblGroupMembers.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
