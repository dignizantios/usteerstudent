//
//  PupilListVC.swift
//  USteerStudent
//
//  Created by Usteer on 7/24/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage


class PupilListTableViewCell: UITableViewCell
{
    
    //MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var btnAction: UIButton!
    
    override func awakeFromNib()
    {
        lblName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblName.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        btnAction.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10)
        btnAction.setTitleColor(UIColor.white, for: .normal)
        btnAction.layer.cornerRadius = btnAction.frame.size.height / 2
        btnAction.layer.masksToBounds = true
        
        
    }
}

class PupilListVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblPupil: UITableView!
    
    //MARK: - Variables
    var arrayPupils : [JSON] = []
    var strErrorMessage = ""
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getPupilListService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tblPupil.addSubview(self.refreshControl)
        tblPupil.tableFooterView = UIView()
        
        setUpUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   

}
//MARK: - UI Setup
extension PupilListVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Connected_pupil_key"))
        getPupilListService()
    }
}
//MARK: - UITableview Delegate
extension PupilListVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayPupils.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        tableView.backgroundView = nil
        return arrayPupils.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:PupilListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! PupilListTableViewCell
        
        cell.selectionStyle = .none
        
        let dict = arrayPupils[indexPath.row]
        
        cell.btnAction.tag = indexPath.row
        cell.btnAction.backgroundColor = MySingleton.sharedManager.themeGreenColor
        cell.btnAction.addTarget(self, action: #selector(btnActionClick), for: .touchUpInside)

        cell.lblName.text = dict["rname"].stringValue
        //status
        /*
         0 - Can send request - green
         1 - Request sent - yellow
         2 - Request Accepted - yellow
         3 - Request decline - red
         4 - Request terminated - red
         5 - Did not track the relationship yet. - button hidden
        */
        cell.btnAction.isHidden = false
        cell.btnAction.isUserInteractionEnabled = false
        if(dict["status"].stringValue == "0")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeGreenColor
            cell.btnAction.isUserInteractionEnabled = true
            cell.btnAction.setTitle("Send Request", for: .normal)
        }
        else if(dict["status"].stringValue == "1")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeYellowColor
            cell.btnAction.setTitle("Sent", for: .normal)
        }
        else if(dict["status"].stringValue == "2")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeYellowColor
            cell.btnAction.setTitle("Accepted", for: .normal)
        }
        else if(dict["status"].stringValue == "3")
        {
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeRedColor
            cell.btnAction.setTitle("Decline", for: .normal)
        }
        else if(dict["status"].stringValue == "4"){
            cell.btnAction.backgroundColor = MySingleton.sharedManager.themeRedColor
            cell.btnAction.setTitle("Terminated", for: .normal)
        }
        else{
            cell.btnAction.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
    }
}
//MARK: - Button Action
extension PupilListVC
{
    @objc func btnActionClick(_ sender : UIButton)
    {
        
        sendRequestToPupilService(dict : arrayPupils[sender.tag])
    }
}
//MARK: - Service
extension PupilListVC 
{
    
    func getPupilListService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            ChatService().getConnectedPupilListService(completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            let data = json["data"]
                            self.arrayPupils = data["detail"].arrayValue
                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblPupil.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func sendRequestToPupilService(dict : JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            
            let param : [String:String] = ["receiver_id" : dict["receiver_id"].stringValue,
                                           "rname" : dict["rname"].stringValue,
                                           "status" : dict["status"].stringValue,
                                           "rid" : dict["rid"].stringValue
                                        ]
            
            
            ChatService().sendPupilRequestActionService(param : param,completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        if(json["status"].stringValue == "1")
                        {
                            showToast(message: json["message"].stringValue)

                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblPupil.reloadData()
                    }
                    self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
}
