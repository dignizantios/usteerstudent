//
//  ChatVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/1/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import SDWebImage

import DropDown

class ChatListTableViewCell: UITableViewCell
{
    
    //MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblNotificationCount: UILabel!
    @IBOutlet weak var imgvwChat: UIImageView!

    override func awakeFromNib()
    {
        lblName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblName.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        lblNotificationCount.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10)
        lblNotificationCount.textColor = .white
        lblNotificationCount.isHidden = true
        lblNotificationCount.backgroundColor = MySingleton.sharedManager.themeRedColor
    }
}

class ChatVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var tblChats: UITableView!
    
    //MARK: - Variables
    var arrayChats : [JSON] = []
    var strErrorMessage = ""
    var isFromPush = false
    var isNavigateToCoach = false
    var dictChatDetails = JSON()
    var isUnreadReload = false
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getChatListService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        isOnChatListScreen = true
        
        if(isFromPush)
        {
            print("chat data dict - ",dictChatDetails)
            navigateToChatDetail(dict : dictChatDetails,isFromPushLocal:true)
        }
        if(isNavigateToCoach)
        {
            navigateToCoachList()
        }
        //
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadChatList), name: NSNotification.Name(rawValue: "reloadChatList"), object: nil)

        
        // Do any additional setup after loading the view.
        self.tblChats.addSubview(self.refreshControl)

        tblChats.tableFooterView = UIView()
        
        setUpUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        if(isFromPush || (isUnreadReload))
        {
            reloadChatList()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        isOnChatListScreen = false
    }
    
    override func viewDidLayoutSubviews() {
        setUpNavigationBarWithTitleMenuAndRightButton(strTitle : getCommonString(key: "Chats_key"))

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI Setup
extension ChatVC
{
    func setUpUI()
    {
        reloadChatList()
    }
    @objc func reloadChatList()
    {
        getChatListService()
    }
    func setUpNavigationBarWithTitleMenuAndRightButton(strTitle : String)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menubar_white_header"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
        leftButton.tintColor = UIColor.white
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let rightButton = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_request_tutor_sidemenu"), style: .plain, target:self, action: #selector(coachListAction))
        rightButton.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = rightButton
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)
        self.navigationItem.titleView = HeaderLabel
    }
    
    @objc func coachListAction(_ sender : UIBarButtonItem)
    {
        let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
        self.navigationController?.pushViewController(obj, animated: true)
        
//        let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "PupilRequestVC") as! PupilRequestVC
//        self.navigationController?.pushViewController(obj, animated: true)
        
       /* let OptionsDD = DropDown()
//        OptionsDD.dataSource = ["Tutor Request","Connected Pupils","Pupil Requests"]
        OptionsDD.dataSource = ["Tutor Request","Connected Pupils"]

        OptionsDD.anchorView = sender
        OptionsDD.textColor = MySingleton.sharedManager.themeDarkGrayColor
        OptionsDD.textFont = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        OptionsDD.backgroundColor = UIColor.white
        OptionsDD.selectionBackgroundColor = MySingleton.sharedManager.themeBlueColor.withAlphaComponent(0.35)
        OptionsDD.cellHeight = 60
        OptionsDD.direction = .any
        OptionsDD.dismissMode = .onTap
        OptionsDD.width = 200
        OptionsDD.bottomOffset = CGPoint(x:0,y:45)
        OptionsDD.show()
        
        // Action triggered on selection
        OptionsDD.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if(index == 0)
            {
                let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
                self.navigationController?.pushViewController(obj, animated: true)
            }else if(index == 1){
                let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "PupilListVC") as! PupilListVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            else if(index == 2){
                let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "PupilRequestVC") as! PupilRequestVC
                self.navigationController?.pushViewController(obj, animated: true)
            }
            OptionsDD.hide()
        }*/
    }
    func navigateToCoachList()
    {
        let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
    func RedirectToScreen(json:JSON)
    {
        var dictTutor = JSON()
        var isNavigateToChatList = true
        if(self.arrayChats.count > 0)
        {
            self.arrayChats.forEach { (dictJson) in
                if(dictJson["other_id"].stringValue != "0" || dictJson["other_id"].stringValue != "")
                {
                    if(dictJson["group_id"].intValue != 0)
                    {
                        //chat list
                        isNavigateToChatList = true
                    }else{
                        //chat details
                        isNavigateToChatList = false
                        dictTutor = dictJson
                        isFromPush = false
                    }
                }
            }
            
            if(isNavigateToChatList)
            {
                
            }else{
                let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
                obj.dictChatData = dictTutor
                obj.isFromPush = false
//                navigateToController(obj : obj)
                self.navigationController?.pushViewController(obj, animated: false)

//                navigateToChatDetail(dict : dictTutor,isFromPushLocal:false)
            }
            
        }else{
            //Coach list
            let objVC = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
            objVC.selectedCoachParentVC = .sideMenu
//            navigateToController(obj : objVC)
            self.navigationController?.pushViewController(objVC, animated: false)
        }
        
        
    }
    
    
}
//MARK: - UITableview Delegate
extension ChatVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayChats.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            
            return 0
        }
        tableView.backgroundView = nil
        return arrayChats.count

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:ChatListTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ChatListTableViewCell
        
        cell.selectionStyle = .none
        
        let dict = arrayChats[indexPath.row]
        
        if(dict["unread_msg"].intValue > 0)
        {
            cell.lblNotificationCount.isHidden = false
            cell.lblNotificationCount.text = dict["unread_msg"].stringValue
        }else{
            cell.lblNotificationCount.isHidden = true
        }
        
        if(dict["is_group"].boolValue)
        {
            cell.imgvwChat.image = #imageLiteral(resourceName: "group_placeholder")
            cell.lblName.text = decreptedString(string: dict["fullname"].stringValue)

        }else{
            cell.imgvwChat.image = #imageLiteral(resourceName: "user_image_chat_details")
            cell.lblName.text = decreptedString(string: dict["fullname"].stringValue)

        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        isFromPush = false
        let dict = arrayChats[indexPath.row]
        isUnreadReload = true
        
        navigateToChatDetail(dict : dict,isFromPushLocal:false)
    }
}

//MARK: - Service
extension ChatVC
{
    func navigateToChatDetail(dict : JSON,isFromPushLocal:Bool)
    {
        print("chat dict",dict)
        let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
        
        var dictdata = dict
        dictdata["group_id"].stringValue = decreptedString(string: dictdata["group_id"].stringValue)
        dictdata["fullname"].stringValue = decreptedString(string: dictdata["fullname"].stringValue)
        
        obj.dictChatData = dictdata
        obj.isFromPush = isFromPushLocal
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    func getChatListService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            //showLoader()
            
            ChatService().getChatList(userId : getUserDetail("user_id"),completion:
                {(result) in
                    
                    if let json = result.value
                    {
                        print("response - ",json)
                        let data = json["data"]
                        if(json["status"].stringValue == "1")
                        {
                            
                            var arrayChatList = data["main_chat"].arrayValue
                            
//                            for i in 0..<arrayChatList.count{
//                                var dict = arrayChatList[i]
//                                dict["fullname"].stringValue = decreptedString(string: dict["fullname"].stringValue)
//                                arrayChatList[i] = dict
//                            }
                            
                            self.arrayChats = arrayChatList
//                            self.RedirectToScreen(json:json)
                            let dictNotification = data["notification"]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                            
                        }else if(json["status"].stringValue == "0")
                        {
                            self.strErrorMessage = json["message"].stringValue
                            showToast(message: json["message"].stringValue)
                            
                            let dictNotification = data["notification"]
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                        }
                        else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                        {
                            showToast(message: json["message"].stringValue)
                            self.navigateToLogin()
                        }
                        else{
                            showToast(message: json["message"].stringValue)
                        }
                        self.tblChats.reloadData()
                    }
                    //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
}
