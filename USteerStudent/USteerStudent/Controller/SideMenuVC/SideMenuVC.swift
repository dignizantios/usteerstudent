//
//  SideMenuVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Toaster

import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON
class SideMenuTableViewCell: UITableViewCell
{
    //MARK: - Outlets
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var vwSelected: UIView!
    @IBOutlet weak var imgvwMenu: UIImageView!
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    override func awakeFromNib()
    {
        lblName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblName.textColor = MySingleton.sharedManager.themeYellowColor
        lblNotificationCount.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 10)
        lblNotificationCount.textColor = .white
        lblNotificationCount.isHidden = true
        lblNotificationCount.backgroundColor = MySingleton.sharedManager.themeRedColor
        vwSelected.backgroundColor = MySingleton.sharedManager.themeYellowColor
    }
}
class SideMenuVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var tblMenu: UITableView!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet weak var lblVersionCode: UILabel!

    //MARK: - Variables
    var arrayMenuOptions : [String] = []
    var arrayMenuImages : [UIImage] = []

    var chatCount = 0
    var notificationCount = 0
    
    //MAKR: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        revealViewController().tapGestureRecognizer()
        revealViewController().panGestureRecognizer()
        if(isSideMenuLoadFirstTime)
        {
            isSideMenuLoadFirstTime = false
            chatCount = dictNotificationData["chat"].intValue
            notificationCount = dictNotificationData["social"].intValue
            tblMenu.reloadData()
        }
        lblVersionCode.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblVersionCode.textColor = MySingleton.sharedManager.themeYellowColor
        lblVersionCode.text = "Version \(VersionCheck.shared.appVersion ?? "0.0")"
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        setUpUI()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: NSNotification.Name(rawValue: "reloadSideMenuTable"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadForNotification), name: NSNotification.Name(rawValue: "reloadForNotification"), object: nil)
        self.tblMenu.reloadData()
        self.navigationController?.navigationBar.isHidden = true
        lblUserName.text = getUserDetail("fullname")
        
        let rewel = self.revealViewController()
        rewel?.frontViewController.view.isUserInteractionEnabled = false
        rewel?.frontViewController.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
    }
    
    @objc func reloadTable()
    {
        tblMenu.reloadData()
    }
    
    @objc func reloadForNotification(_ notification : NSNotification)
    {
        let dict = notification.object as? JSON
        chatCount = dict?["chat"].intValue ?? 0
        notificationCount = dict?["social"].intValue ?? 0
        tblMenu.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        let rewel = self.revealViewController()
        rewel?.frontViewController.view.isUserInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
//MARK: - Setup Menu Array
extension SideMenuVC
{
    func setUpUI()
    {
        tblMenu.backgroundColor = MySingleton.sharedManager.themeSideMenuColor
        self.view.backgroundColor = MySingleton.sharedManager.themeSideMenuColor
        self.navigationController?.isNavigationBarHidden = true
        lblUserName.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        lblUserName.textColor = UIColor.white
        setUpArray()
    }
}
//MARK: - Setup Menu Array
extension SideMenuVC
{
    func setUpArray()
    {
        /*
         link_type 0 => show both option
         link_type 1 => hide link school option
         link_type null => hide both option
        */
        arrayMenuOptions = [
            getCommonString(key: "Training_key").capitalized,
            getCommonString(key: "Steer_my_relationships_key").capitalized,
            getCommonString(key: "Learn_key").capitalized,
//            getCommonString(key: "Sort_relationships_key").capitalized,
//            getCommonString(key: "My_journey_me_key").capitalized,
            getCommonString(key: "Tutor_key").capitalized,
            getCommonString(key: "Notifications_key").capitalized,
            getCommonString(key: "Link_to_organisation_key").capitalized,
            getCommonString(key: "Settings_key").capitalized,
            getCommonString(key: "Support_key").capitalized,
            getCommonString(key: "Privacy_policy_key").capitalized,
            getCommonString(key: "Report_issue_key").capitalized
        ]
        arrayMenuImages = [#imageLiteral(resourceName: "ic_launch_assessment_sidemenu"), #imageLiteral(resourceName: "ic_plan_my_route_map_sidemenu"), #imageLiteral(resourceName: "ic_new_sidemenu_yellow"),/* #imageLiteral(resourceName: "ic_charecteristics"),*/ #imageLiteral(resourceName: "ic_chat_sidemenu"), #imageLiteral(resourceName: "ic_notification_sidemenu"), #imageLiteral(resourceName: "ic_link_sidemenu_new"), #imageLiteral(resourceName: "ic_setting_sidemenu"), #imageLiteral(resourceName: "ic_support_side menu"), #imageLiteral(resourceName: "ic_privacy_policy"), #imageLiteral(resourceName: "ic_flag_icon")]
    }
}
//MARK: - UITableview Methods
extension SideMenuVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrayMenuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SideMenuTableViewCell
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = UIColor.clear
        cell.backgroundColor = UIColor.clear

        cell.lblName.text = arrayMenuOptions[indexPath.row]
        cell.imgvwMenu.image = arrayMenuImages[indexPath.row]
        cell.imgvwMenu.tintColor = MySingleton.sharedManager.themeYellowColor
        if(indexPath.row == selectedMenuIndex)
        {
            cell.vwSelected.isHidden = false
        }else{
            cell.vwSelected.isHidden = true
        }
        
        if(arrayMenuOptions[indexPath.row] == getCommonString(key: "Notifications_key").capitalized)
        {
            if(notificationCount > 0)
            {
                cell.lblNotificationCount.isHidden = false
                cell.lblNotificationCount.text = "\(notificationCount)"

            }else{
                cell.lblNotificationCount.isHidden = true
            }
        }else{
            cell.lblNotificationCount.isHidden = true
        }
        if(arrayMenuOptions[indexPath.row] == getCommonString(key: "Tutor_key").capitalized)
        {
            if(chatCount > 0)
            {
                cell.lblNotificationCount.isHidden = false
                cell.lblNotificationCount.text = "\(chatCount)"
                
            }else{
                cell.lblNotificationCount.isHidden = true
            }
        }else{
            cell.lblNotificationCount.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        selectedMenuIndex = indexPath.row
        menuOptionClick(menuName : arrayMenuOptions[indexPath.row])
    }
}
//MARK: - Button Action
extension SideMenuVC
{
    func navigateToSteering(isForSelf : Bool)
    {
        let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
        obj.selectedVC = .AppLaunch
        obj.isFromSideMenuSteering = true
        navigateToController(obj : obj)
    }
    
    @IBAction func btnLogoutAction(_ sender : UIButton)
    {
        let alert = UIAlertController(title: "", message: "Are you sure you want to log out?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            self.userLogoutService()
        }))
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
//MARK: - Menu Option Click
extension SideMenuVC
{
    func menuOptionClick(menuName : String)
    {
        if(menuName == getCommonString(key: "My_target_key").capitalized) // My Target
        {
            let obj = objMyTargetsStoryboard.instantiateViewController(withIdentifier: "TargetVC") as! TargetVC
            obj.isFromMyTargets = true
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Track_my_steering_key").capitalized) //Assessment
        {
            let obj = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentLaunchVC") as! AssesmentLaunchVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Steer_my_relationships_key").capitalized) //Steering
        {
            let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
            obj.selectedVC = .AppLaunch
            obj.isFromSideMenuSteering = true
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Tutor_key").capitalized) //Chats
        {
            getDefaultCoachService()
        }
        else if(menuName == getCommonString(key: "Account_key").capitalized) //Profile
        {
            let obj = objProfileStoryboard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Train_my_steer_mind_key").capitalized) //Steer mind
        {
            let obj = objSteerMindStoryboard.instantiateViewController(withIdentifier: "SteerMindVC") as! SteerMindVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Link_to_organisation_key").capitalized) //Link Private Account
        {
            let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountOrgViewController") as! LinkAccountOrgViewController
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Training_key").capitalized) // User guidance
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "UserGuidanceVC") as! UserGuidanceVC
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            self.revealViewController().setFront(rearNavigation, animated: true)
            self.revealViewController().revealToggle(animated: true)
            tblMenu.reloadData()
            return
        }
        else if(menuName == getCommonString(key: "Notifications_key").capitalized) //Notifications
        {
            let obj = objNotificationStoryboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Settings_key").capitalized) //Settigns
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "My_journey_me_key").capitalized) // Characteristics
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "CharacteristicsParentVC") as! CharacteristicsParentVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "My_target_key").capitalized) // Target list
        {
            let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Sort_relationships_key").capitalized) // Relationship
        {
            let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
            obj.selectedVC = .Sorting
            obj.isFromSideMenuSteering = false
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Support_key").capitalized) // Relationship
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "HelpCenterVC") as! HelpCenterVC
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Privacy_policy_key").capitalized) // Privacy policy
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "PrivacyPoilcyVC") as! PrivacyPoilcyVC
            obj.selectedParentPolicyNavigation = .sidemenu
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Report_issue_key").capitalized) //
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "HelpCenterVC") as! HelpCenterVC            
            navigateToController(obj : obj)
        }
        else if(menuName == getCommonString(key: "Learn_key").capitalized) //
        {
            let obj = objLearnStoryboard.instantiateViewController(withIdentifier: "LearnVC") as! LearnVC
            navigateToController(obj : obj)
        }
        tblMenu.reloadData()
    }
    
    func navigateToController(obj : UIViewController)
    {
        let count = Defaults.integer(forKey: "AlertPopUp_Count")
        if count < 1
        {
            Defaults.set(count+1, forKey: "AlertPopUp_Count")
            let controller = objStoryboard.instantiateViewController(withIdentifier: "AlertPopUpVC") as! AlertPopUpVC
            controller.onRemoveFromSubview {
                let rearNavigation = UINavigationController(rootViewController: obj)
                rearNavigation.isNavigationBarHidden = false
                self.revealViewController().setFront(rearNavigation, animated: true)
                self.revealViewController().revealToggle(animated: true)
            }
            if let root = appDelegate.window?.rootViewController
            {
                controller.view.frame = root.view.bounds;
                controller.willMove(toParentViewController: root)
                root.view.addSubview(controller.view)
                root.addChildViewController(controller)
                controller.didMove(toParentViewController: root)
            }
        }
        else
        {
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            self.revealViewController().setFront(rearNavigation, animated: true)
            self.revealViewController().revealToggle(animated: true)
        }
    }
    
    func navigateToScreen(json : JSON)
    {
        let data = json
        if(data["status"].stringValue == "1")
        {
            //No connection
            let objVC = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
            objVC.selectedCoachParentVC = .sideMenu
            navigateToController(obj : objVC)
        }
        else if(data["status"].stringValue == "2")
        {
            let objVC = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
            objVC.checkChatParentVC = .sideMenu
            var dict = data["main_chat"]
            dict["group_id"].stringValue = decreptedString(string: data["main_chat"]["group_id"].stringValue)
            print("dict:\(dict)")
            objVC.dictChatData = dict
            navigateToController(obj : objVC)
        }
        else
        {
            let objVC = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
            navigateToController(obj : objVC)
        }
    }
}

//MARK: - Service
extension SideMenuVC 
{
    func userLogoutService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let param : [String:String] = ["uid" : encryptString(string: getUserDetail("user_id")),
                                           "session" : getUserDetail("session")
            ]
            
            ProfileService().userLogout(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.RemoveAllNotifications()
                        self.navigateToLogin()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getDefaultCoachService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            BasicTaskService().GetConnectedTutorService(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue != "0")
                    {
                        self.navigateToScreen(json : json["data"])
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
