//
//  ChangePasswordVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/3/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

class ChangePasswordVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet weak var vwOldPasswordSeperator: UIView!
    @IBOutlet weak var vwNewPasswordSeperator: UIView!
    @IBOutlet weak var vwConfirmPasswordSeperator: UIView!
    
    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var btnHideShowOldPassword: UIButton!
    @IBOutlet weak var btnHideShowNewPassword: UIButton!
    @IBOutlet weak var btnHideShowConfirmPassword: UIButton!

    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews()
    {
        [btnDone].forEach({
            $0?.layer.cornerRadius = ($0?.bounds.size.height)! / 2
            $0?.layer.masksToBounds = true
        })
    }
}
//MARK:- UI Setup
extension ChangePasswordVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle : getCommonString(key: "Change_password_key").capitalized)
        
        [lblOldPassword,lblNewPassword,lblConfirmPassword].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        
        [vwOldPasswordSeperator,vwNewPasswordSeperator,vwConfirmPasswordSeperator].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        
        [txtOldPassword,txtNewPassword,txtConfirmPassword].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
            $0?.delegate = self
        })
        
        btnDone.setTitle(getCommonString(key: "Done_key").uppercased(), for: .normal)
        buttonUISetup(btnDone, textColor: MySingleton.sharedManager.themeDarkGrayColor)
        btnDone.layoutIfNeeded()
        self.view.layoutIfNeeded()
        //
        lblOldPassword.text = getCommonString(key: "Old_password_key").uppercased()
        lblNewPassword.text = getCommonString(key: "New_password_key").uppercased()
        lblConfirmPassword.text = getCommonString(key: "Confirm_password_key").uppercased()
    }
}
//MARK:- UITextfield Setup
extension ChangePasswordVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
//MARK: - Button Action
extension ChangePasswordVC
{
    @IBAction func btnChangePasswordAction(_ sender : UIButton)
    {
        let objUserLocal = User()
        objUserLocal.strOldPassword = txtOldPassword.text ?? ""
        objUserLocal.strNewPassword = txtNewPassword.text?.trimmed().removingWhitespacesAndNewlines ?? ""
        objUserLocal.strConfirmPassword = txtConfirmPassword.text?.trimmed().removingWhitespacesAndNewlines ?? ""
        
        if(!objUserLocal.isValidForChangePassword())
        {
            showToast(message: objUserLocal.strValidationMessage)
        }else{
            sendVerificationCodeService()
        }
    }
    func navigateToVerifyCode()
    {
        let objUserLocal = User()
        objUserLocal.strOldPassword = txtOldPassword.text ?? ""
        objUserLocal.strNewPassword = txtNewPassword.text?.trimmed().removingWhitespacesAndNewlines ?? ""
        objUserLocal.strConfirmPassword = txtConfirmPassword.text?.trimmed().removingWhitespacesAndNewlines ?? ""
        
        let obj = objProfileStoryboard.instantiateViewController(withIdentifier: "VerifyCodeVC") as! VerifyCodeVC
        obj.objUser = objUserLocal
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    @IBAction func btnHideShowOldPasswordAction(_ sender : UIButton)
    {
        txtOldPassword.isSecureTextEntry = sender.isSelected
        sender.isSelected = !sender.isSelected

    }
    @IBAction func btnHideShowNewPasswordAction(_ sender : UIButton)
    {
        txtNewPassword.isSecureTextEntry = sender.isSelected
        sender.isSelected = !sender.isSelected

    }
    @IBAction func btnHideShowConfirmPasswordAction(_ sender : UIButton)
    {
        txtConfirmPassword.isSecureTextEntry = sender.isSelected
        sender.isSelected = !sender.isSelected

    }
}
//MARK: - Service
extension ChangePasswordVC
{
    func sendVerificationCodeService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            
            let param : [String:String] = ["uid":encryptString(string: getUserDetail("user_id")),
                                           "op":encryptString(string: txtOldPassword.text ?? ""),
                                           "np":encryptString(string: txtNewPassword.text?.trimmed().removingWhitespacesAndNewlines ?? ""),
                                           "cp":encryptString(string: txtConfirmPassword.text?.trimmed().removingWhitespacesAndNewlines ?? "")
                                           ]
            
            ProfileService().changePassword(param: param, completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToVerifyCode()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
