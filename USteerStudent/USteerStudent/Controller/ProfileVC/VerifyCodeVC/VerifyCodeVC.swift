//
//  VerifyCodeVC.swift
//  USteerStudent
//
//  Created by Usteer on 7/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster


class VerifyCodeVC: UIViewController
{

    //MARK:- Outlets
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var lblVeifyCode: UILabel!
    @IBOutlet weak var txtVerificationCode: UITextField!

    
    //MARK:- Variables
    var objUser = User()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setUpUI()
    }

    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle : getCommonString(key: "Verify_code_key").capitalized)

        lblVeifyCode.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblVeifyCode.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        btnDone.setTitleColor(MySingleton.sharedManager.themeDarkGrayColor, for: .normal)
        btnDone.layer.cornerRadius = btnDone.frame.size.height / 2
        btnDone.layer.masksToBounds = true
        btnDone.backgroundColor = MySingleton.sharedManager.themeYellowColor
        
        txtVerificationCode.textColor = MySingleton.sharedManager.themeDarkGrayColor
        txtVerificationCode.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        
        addDoneButtonOnKeyboard(textfield : txtVerificationCode)
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK:- Button Action
extension VerifyCodeVC
{
    @IBAction func verifyCodeAction(_ sender : UIButton)
    {
        if(txtVerificationCode.text?.isEmpty)!
        {
            showToast(message: R.string.validationMessage.enter_verification_code())
        }
        else{
            changePasswordService()
        }
    }
}
//MARK:- other method
extension VerifyCodeVC
{
    func navigateToProfile()
    {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers
        for aViewController in viewControllers {
            if aViewController is ProfileVC {
                self.navigationController!.popToViewController(aViewController, animated: true)
            }
        }
        
    }
    
}
//MARK:- UITextfield Delegate
extension VerifyCodeVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        textField.resignFirstResponder()
        return true
    }
}
//MARK: - Service
extension VerifyCodeVC
{
    func changePasswordService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let param : [String:String] = ["uid":encryptString(string: getUserDetail("user_id")),
                                           "op":encryptString(string: objUser.strOldPassword),
                                           "np":encryptString(string: objUser.strNewPassword),
                                           "cp":encryptString(string: objUser.strConfirmPassword),
                                           "otp":encryptString(string: txtVerificationCode.text ?? "")
            ]
            
            ProfileService().changePasswordStep2(param: param, completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToProfile()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
