//
//  EditProfileVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/3/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

class carCollectionViewCell: UICollectionViewCell
{
    //MARK:- Outlet Zone
    @IBOutlet weak var imgCar: UIImageView!
    @IBOutlet weak var btnImage: UIButton!
    @IBOutlet weak var btnSelect: UIButton!

    //MARK:- ViewLife Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class EditProfileVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var btnDone: UIButton!
    
    @IBOutlet var collectionvwCars : UICollectionView!

    @IBOutlet weak var vwNameSeperator: UIView!
    @IBOutlet weak var vwLastNameSeperator: UIView!
    @IBOutlet weak var vwUsernameSeperator: UIView!
    @IBOutlet weak var vwEmailSeperator: UIView!
    @IBOutlet weak var vwBirthdateSeperator: UIView!
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblLastName: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblBirthdate: UILabel!
    @IBOutlet weak var lblChooseCar: UILabel!

    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtBirthdate: UITextField!

    
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblMale: UILabel!
    @IBOutlet weak var lblFemale: UILabel!
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var collectionViewOptions: UICollectionView!
    @IBOutlet weak var constraintCollectionviewOptionHeight: NSLayoutConstraint!
    
    //MARK: - Variables
    var strBirthdate = ""
    var dictProfileData : JSON?
    var birthDateValue : Date?
    var arrayCars : [JSON] = []
    var strGender = ""
    var arrayOptions : [JSON] = []

    //MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionViewOptions.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        collectionViewOptions.register(UINib(nibName: "OptionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cell")
        // Do any additional setup after loading the view.
        setUpUI()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews()
    {
        [btnDone].forEach({
            $0?.layer.cornerRadius = ($0?.bounds.size.height)! / 2
            $0?.layer.masksToBounds = true
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        collectionViewOptions.removeObserver(self, forKeyPath: "contentSize")
    }
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?)
    {
        if object is UICollectionView
        {
            self.constraintCollectionviewOptionHeight.constant = collectionViewOptions.contentSize.height
        }
    }
}
//MARK:- UI Setup
extension EditProfileVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndBack(strTitle : getCommonString(key: "Edit_profile_key").capitalized)
        
        [lblName,lblLastName,lblUserName,lblEmail,lblBirthdate,lblChooseCar,lblGender].forEach({
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        
        [vwNameSeperator,vwLastNameSeperator,vwUsernameSeperator,vwEmailSeperator,vwBirthdateSeperator].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })
        [lblMale,lblFemale].forEach({
            $0?.textColor = MySingleton.sharedManager.themeLightGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        
        [txtName,txtLastName,txtUsername,txtEmail,txtBirthdate].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.placeholder = getCommonString(key: "Enter_here_key")
            $0?.delegate = self
        })
        
        btnDone.setTitle(getCommonString(key: "Submit_key").uppercased(), for: .normal)
        buttonUISetup(btnDone, textColor: MySingleton.sharedManager.themeDarkGrayColor)

        btnDone.layoutIfNeeded()
        self.view.layoutIfNeeded()
        //
        lblName.text = getCommonString(key: "Name_key").uppercased()
        lblLastName.text = getCommonString(key: "Lastname_key").uppercased()
        lblUserName.text = getCommonString(key: "Username_key").uppercased()
//        lblEmail.text = getCommonString(key: "Email_key").uppercased()
        lblBirthdate.text = getCommonString(key: "Birthdate_key").uppercased()
        lblChooseCar.text = getCommonString(key: "Choose_car_key").uppercased()
        lblGender.text = getCommonString(key: "Gender_key").uppercased()
        lblMale.text = getCommonString(key: "Male_key")
        lblFemale.text = getCommonString(key: "Female_key")
        
        setUpProfileData()
        
        
    }
    
}
//MARK:- Setup profile data
extension EditProfileVC
{
    func setUpProfileData()
    {
        txtName.text = dictProfileData?["name"].stringValue ?? ""
        txtLastName.text = dictProfileData?["lastname"].stringValue ?? ""
        
        txtUsername.text = dictProfileData?["username"].stringValue ?? ""
//        txtEmail.text = dictProfileData?["email"].stringValue ?? ""
        txtBirthdate.text = dictProfileData?["birthdate"].stringValue ?? ""

        strBirthdate = dictProfileData?["birthdate"].stringValue ?? "000000"
        
        let convertedBirthdate = StringToConvertedStringDate(strDate: strBirthdate, strDateFormat: "yyyyMM", strRequiredFormat: "yyyy-MM") + " \(DateToString(Formatter: "HH:mm:ss", date: Date()))"
        print("convertedBirthdate - ",convertedBirthdate)

        let Birthdate = StringToConvertedStringDate(strDate: strBirthdate, strDateFormat: "yyyyMM", strRequiredFormat: "MM - yyyy")
        txtBirthdate.text = Birthdate

        /*if(strBirthdate != "" && !strBirthdate.contains("0000"))
        {
            birthDateValue = StringToDate(Formatter: "MM-yyyy", strDate: StringToConvertedStringDate(strDate: convertedBirthdate, strDateFormat: "yyyy-MM HH:mm:ss", strRequiredFormat: "MM-yyyy"))
        }
        
        print("birthDateValue - ",birthDateValue)*/
        
        
        if(dictProfileData?["gender"].stringValue == "")
        {
            strGender = ""
            [btnMale,btnFemale].forEach { (button) in
                button?.isSelected = false
            }
            
        }else{
            if(dictProfileData?["gender"].stringValue == "f")
            {
                strGender = "f"
                btnFemale.isSelected = true
            }
            else{
                strGender = "m"
                btnMale.isSelected = true
            }
        }
        
        setUpArray()
        
        setupUserRole()
    }
    func setUpArray()
    {
        
        let arrayImages = GlobalVariables.arrayCarsGlobal

        for i in 0..<arrayImages.count
        {
            var dict = JSON()
            if(dictProfileData?["chart_icon"].stringValue ?? "0" == "\(i+1)")
            {
                dict["isSelected"] = JSON("1")
            }else{
                dict["isSelected"] = JSON("0")

            }
            
            dict["carImage"].stringValue = arrayImages[i]
            dict["selectedImage"].stringValue = "ic_radio_checked"
            dict["unSelectedImage"].stringValue = "ic_radio_unchecked"
            
            arrayCars.append(dict)
        }
        
        collectionvwCars.reloadData()
    }
    
    func setupUserRole()
    {      
        
        let arrUserRole = (self.dictProfileData!["user_role"].stringValue).components(separatedBy: ",")
        for i in 0..<arrUserRole.count
        {
            let roleId = arrUserRole[i]
            for j in 0..<self.arrayOptions.count
            {
                var dict = self.arrayOptions[j]
                if roleId == dict["role_id"].stringValue
                {
                    dict["isSelected"] = "1"
                    self.arrayOptions[j] = dict
                }
            }
        }
        self.collectionViewOptions.reloadData()
    }
}
//MARK: - Collectionview methods
extension EditProfileVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewOptions
        {
            return arrayOptions.count
        }
        return arrayCars.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if collectionView == collectionViewOptions{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! OptionCollectionViewCell
            
            let dict = arrayOptions[indexPath.row]
            
            cell.lblOption.text = dict["role_name"].stringValue
            
            if(dict["isSelected"].stringValue == "1")
            {
                cell.btnSelect.isSelected = true
            }else{
                cell.btnSelect.isSelected = false
            }
            
            
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "carCell", for: indexPath) as! carCollectionViewCell
        
        let dict = arrayCars[indexPath.row]
        cell.imgCar.image = UIImage(named:dict["carImage"].stringValue)
        if(dict["isSelected"].stringValue == "0")
        {
            cell.btnImage.isSelected = false
            
        }else{
            cell.btnImage.isSelected = true
        }
        cell.btnSelect.tag = indexPath.row
        cell.btnSelect.addTarget(self, action: #selector(btnSelectCarClickAction), for: .touchUpInside)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == collectionViewOptions
        {
            var dict = arrayOptions[indexPath.row]
            
            if(dict["isSelected"].stringValue == "1")
            {
                dict["isSelected"].stringValue = "0"
            }else{
                dict["isSelected"].stringValue = "1"
            }
            
            arrayOptions[indexPath.row] = dict
            
            collectionViewOptions.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if collectionView == collectionViewOptions
        {
            var cellWidth : CGFloat = 0.0
            
            //        print("screenRect  width  - \(screenRect.size.width)")
            //
            print("collectionView  width  - \(collectionView.frame.size.width)")
            
            // cellWidth  = (screenRect.size.width-80)/6
            cellWidth  = (collectionView.frame.size.width)/2
            
            print("width - \(cellWidth) ------ height - \(cellWidth)")
            return CGSize(width: cellWidth , height:35)
        }
        
        return CGSize(width: 75 , height:75)
        
        
    }
}
//MARK:- UITextfield Setup
extension EditProfileVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        txtBirthdate.resignFirstResponder()
        if(textField == txtBirthdate)
        {
            let month = StringToConvertedStringDate(strDate: dictProfileData?["birthdate"].stringValue ?? "000000", strDateFormat: "yyyyMM", strRequiredFormat: "MM")
            let year = StringToConvertedStringDate(strDate: dictProfileData?["birthdate"].stringValue ?? "000000", strDateFormat: "yyyyMM", strRequiredFormat: "yyyy")
            
            self.view.endEditing(true)
            let obj = MonthYearVC()
            obj.monthYearDelegate = self
            obj.selectedMonth = Int(month) ?? getCurrentMonth()
            obj.strSelectedYear = year
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            self.present(obj, animated: true, completion: nil)
            return false
        }
        return true
    }
}
//MARK: - Pickerview Delegate
extension EditProfileVC : SelectedMonthYearDelegate
{
    func SetSelectedMonthYear(strMonth: String, strYear: String,month : Int) {
        
        print("strMonth - ",month)
        print("strYear - ",strYear)
        strBirthdate = "\(strYear)\(String(format: "%02d", month))"
        self.txtBirthdate.text = "\(String(format: "%02d", month)) - \(strYear)"
    }
}
//MARK: - SetDate Picker Value
extension EditProfileVC : datePickerDelegate
{
    func setDateValue(dateValue: Date)
    {
        birthDateValue = dateValue
        self.txtBirthdate.text = DateToString(Formatter: "dd-MM-yyyy", date: dateValue)
        strBirthdate = DateToString(Formatter: "yyyyMMdd", date: dateValue)
    }
}
//MARK: - Button Action
extension EditProfileVC
{
    @objc func btnSelectCarClickAction(_ sender : UIButton)
    {
        showToast(message: getCommonString(key: "Good_choice_key"))
        for i in 0..<arrayCars.count
        {
            var dataDict = arrayCars[i]
            if(i == sender.tag)
            {
                dataDict["isSelected"].stringValue = "1"
            }else{
                dataDict["isSelected"].stringValue = "0"
            }
            arrayCars[i] = dataDict
        }
        collectionvwCars.reloadData()
    }
    @IBAction func btnEditProfileAction(_ sender : UIButton)
    {
        var strSelectedCar = ""
        arrayCars.forEach { (json) in
            if(json["isSelected"].stringValue == "1")
            {
                strSelectedCar = "\(arrayCars.index(of: json)! + 1)"
            }
        }
        
        var strSelectedRole = [String]()
        var isCheckSelected = false
        for i in 0..<arrayOptions.count
        {
            let dict = arrayOptions[i]
            if dict["isSelected"].stringValue == "1"
            {
                isCheckSelected = true
                strSelectedRole.append(dict["role_id"].stringValue)
            }
        }
        print("selected car - ",strSelectedCar)
        let objUserLocal = User()
        objUserLocal.strName = txtName.text ?? ""
        objUserLocal.strLastName = txtLastName.text ?? ""
        objUserLocal.strUsername = txtUsername.text ?? ""
        objUserLocal.strEmail = txtEmail.text ?? ""
        objUserLocal.strBirthdate = strBirthdate
        objUserLocal.birthDateValue = birthDateValue
        objUserLocal.strSelectedCar = strSelectedCar
        objUserLocal.strGender = strGender
        objUserLocal.strSelectedUserRole = strSelectedRole.joined(separator: ",")

        if(!objUserLocal.isValidForUpdateProfile())
        {
            showToast(message: objUserLocal.strValidationMessage)
        }
        else if(!isCheckSelected)
        {
            showToast(message: "Please select at least one role")
        }
        else{
            updateUserProfile(objUserData : objUserLocal)
        }
    }
    @IBAction func btnMaleAction(_ sender: UIButton)
    {
        resetSelectedGender()
        btnMale.isSelected = true
        strGender = "m"

    }
    @IBAction func btnFemaleAction(_ sender: UIButton)
    {
        resetSelectedGender()
        btnFemale.isSelected = true
        strGender = "f"
    }
    func resetSelectedGender()
    {
        [btnMale,btnFemale].forEach { (button) in
            button?.isSelected = false
        }
    }
}

//MARK: - Service
extension EditProfileVC
{
    func updateUserProfile(objUserData : User)
    {
        
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["uid": encryptString(string: getUserDetail("user_id")),
                                           "name": encryptString(string: objUserData.strName),
                                           "lastname":encryptString(string: objUserData.strLastName),
                                           "username": objUserData.strUsername,
                                           "birthdate": encryptString(string: objUserData.strBirthdate),
                                           "gender" : encryptString(string: strGender),
                                           "user_role": encryptString(string: objUserData.strSelectedUserRole)
            ]
            
            print("param - ",param)
            ProfileService().updateProfile(param: param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
                        self.navigationController?.popViewController(animated: true)
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

