//
//  ProfileVC.swift
//  USteerTeacher
//
//  Created by Usteer on 1/1/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

class ProfileVC: UIViewController {

    //MARK:- Outlets
    @IBOutlet var btnEditProfile : UIButton!
    @IBOutlet var btnChangePassword : UIButton!
    @IBOutlet var btnDeleteAccount : UIButton!
    @IBOutlet var btnAgreement : UIButton!

    @IBOutlet var lblName : UILabel!
    @IBOutlet var lblNameValue : UILabel!
    
    @IBOutlet var lblUserName : UILabel!
    @IBOutlet var lblUserNameValue : UILabel!
    
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var lblEmailValue : UILabel!
    
    @IBOutlet var lblBirthdate : UILabel!
    @IBOutlet var lblBirthdateValue : UILabel!
    @IBOutlet var vwBirthdate : UIView!

    @IBOutlet var lblGender : UILabel!
    @IBOutlet var lblGenderValue : UILabel!
    @IBOutlet var vwGender : UIView!

    @IBOutlet var lblCar : UILabel!
    @IBOutlet var imgCar : UIImageView!
    
    @IBOutlet var vwContent : UIView!
    @IBOutlet var vwButton : UIView!
    
    @IBOutlet var lblUserRole: UILabel!
    @IBOutlet var lblUserRoleValue: UILabel!
    
    //MARK:- Variables
    var dictProfile : JSON?
    var isUpdateProfile = false
    var arrayOptions : [JSON] = []
    //MARK:- Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
        
        getUserProfile()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUpNotifications()
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidLayoutSubviews()
    {
        [btnEditProfile,btnChangePassword,btnAgreement,btnDeleteAccount].forEach({
            $0?.layer.cornerRadius = ($0?.bounds.size.height)! / 2
            $0?.layer.masksToBounds = true
        })
    }
}
//MARK: - setUP Notifications
extension ProfileVC
{
    func setUpNotifications()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(getUpdatedProfile), name: NSNotification.Name(rawValue: "updateUserProfile"), object: nil)
    }
}
//MARK: - UI Setup
extension ProfileVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Account_key"))
        [btnEditProfile,btnChangePassword,btnDeleteAccount,btnAgreement].forEach({
            buttonUISetup($0!, textColor: MySingleton.sharedManager.themeDarkGrayColor)
            $0?.layoutIfNeeded()
            self.view.layoutIfNeeded()
        })
        
        [lblName,lblEmail,lblUserName,lblBirthdate,lblCar,lblGender,lblUserRole].forEach({
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        })
        
        [lblNameValue,lblEmailValue,lblUserNameValue,lblBirthdateValue,lblGenderValue,lblUserRoleValue].forEach({
            
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
            $0?.textColor = MySingleton.sharedManager.themeYellowColor
        })
        
        lblName.text = getCommonString(key: "Name_key")
        lblUserName.text = getCommonString(key: "Username_key")
        lblEmail.text = getCommonString(key: "Email_key")
        lblBirthdate.text = getCommonString(key: "Birthdate_key")
        lblCar.text = getCommonString(key: "Chart_icon_key")
        lblGender.text = getCommonString(key: "Gender_key")
        lblUserRole.text = getCommonString(key: "User_role_key").capitalized

        btnEditProfile.setTitle(getCommonString(key: "Edit_profile_key").uppercased(), for: .normal)
        btnChangePassword.setTitle(getCommonString(key: "Change_password_key").uppercased(), for: .normal)
        btnDeleteAccount.setTitle(getCommonString(key: "Delete_account_key").uppercased(), for: .normal)
        btnAgreement.setTitle(getCommonString(key: "Licence_agreement_key").uppercased(), for: .normal)

        vwContent.isHidden = true
        vwButton.isHidden = true
    }
    
    @objc func getUpdatedProfile()
    {
        isUpdateProfile = true
        getUserProfile()
    }
    
}
//MARK: - Button Action
extension ProfileVC
{
    @IBAction func btnEditProfileAction(_ sender : UIButton)
    {
        let obj = objProfileStoryboard.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        obj.dictProfileData = dictProfile
        obj.arrayOptions = arrayOptions
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnChangePasswordAction(_ sender : UIButton)
    {
        let obj = objProfileStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC")
        self.navigationController?.pushViewController(obj, animated: true)
    }
    @IBAction func btnAgreementAction(_ sender : UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "PrivacyPoilcyVC") as! PrivacyPoilcyVC
        obj.selectedParentPrivacyVC = .agreement
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    @IBAction func btnDeleteAccountAction(_ sender : UIButton)
    {
        let alert = UIAlertController(title: kCommonAlertTitle, message: "Deleting your USTEER account is a final and irreversible step. Once your account is deleted, you will not be able to reactivate it. All personal USTEER data, tracking records, signposts and any communication you may have had with a tutor will be permanently removed.Once you have deleted your USTEER account, you will need to set up another account in the future, if at any time, you wish to restart using USTEER.", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "Yes, delete my USTEER account", style: .default, handler: { action in
            
            self.userAccountDelete()
        }))
        
        alert.addAction(UIAlertAction(title: "No, keep my USTEER account open", style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
}
//MARK: - Other methods
extension ProfileVC
{
    func setUpProfileData(dictData : JSON?)
    {
        if(isUpdateProfile)
        {
            updateUserDetails(dataDict: dictData!)
        }
        vwContent.isHidden = false
        vwButton.isHidden = false
        dictProfile = dictData
        lblNameValue.text = dictData?["fullname"].stringValue ?? ""
        lblUserNameValue.text = dictData?["username"].stringValue ?? ""
        
        if(dictData?["gender"].stringValue == "")
        {
            vwGender.isHidden = true
        }else{
            vwGender.isHidden = false
            if(dictData?["gender"].stringValue == "f")
            {
                lblGenderValue.text = getCommonString(key: "Female_key")
            }
            else
            {
                lblGenderValue.text = getCommonString(key: "Male_key")
            }
        }
        if(dictData?["birthdate"].stringValue == "0")
        {
            vwBirthdate.isHidden = true
        }else{
            lblBirthdateValue.text = StringToConvertedStringDate(strDate: dictData?["birthdate"].stringValue ?? "000000", strDateFormat: "yyyyMM", strRequiredFormat: "MM-yyyy")
            vwBirthdate.isHidden = false

        }
        imgCar.image = getSelectedCarForProfile(chartIcon: dictData?["chart_icon"].intValue ?? 1)
    }
}
//MARK: - Service
extension ProfileVC
{
    @objc func getUserProfile()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            ProfileService().getProfile(userId: getUserDetail("user_id"), completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.setUpProfileData(dictData : json["data"])
                        self.getRole()
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getRole()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            Registration().getUserRole(param: [:], completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        // showToast(message: json["message"].stringValue)
                        var strSelectedRole = [String]()
                        self.arrayOptions = json["data"].arrayValue
                        let arrUserRole = (self.dictProfile!["user_role"].stringValue).components(separatedBy: ",")
                        for i in 0..<arrUserRole.count
                        {
                            let roleId = arrUserRole[i]
                            for j in 0..<self.arrayOptions.count
                            {
                                var dict = self.arrayOptions[j]
                                dict["isSelected"] = "0"
                                if roleId == dict["role_id"].stringValue
                                {
                                    strSelectedRole.append(dict["role_name"].stringValue)
                                }
                                self.arrayOptions[j] = dict
                            }
                        }
                        self.lblUserRoleValue.text = strSelectedRole.joined(separator: ",")
                      //  self.setUpData(array: json["data"].arrayValue)
                        //self.navigateToCarSelection(objUser:objUser)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func userLogoutService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["uid" : encryptString(string: getUserDetail("user_id")),
                                           "session" : getUserDetail("session")]
            ProfileService().userLogout(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    //
    func userAccountDelete()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param : [String:String] = ["uid" : getUserDetail("user_id")]
            ProfileService().userAccountDelete(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
