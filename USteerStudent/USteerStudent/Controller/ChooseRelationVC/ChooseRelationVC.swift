//
//  ChooseRelationVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/10/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster
import DropDown

class CreateNodeCollectionviewCell : UICollectionViewCell
{
    @IBOutlet var imgvwInfo: UIImageView!
}

class ChooseRelationVC: UIViewController,UIScrollViewDelegate
{
    //MARK: - Outelets
    @IBOutlet var btnAddRelation : UIButton!
    @IBOutlet var tblRelation : UITableView!
    @IBOutlet var vwTree : UIView!

    //Main container view for add relation
    @IBOutlet var vwRelation : UIView!
    
    //Add Relation pop up - 1
    @IBOutlet var vwAddRelation1 : UIView!
    @IBOutlet var txtRelation : UITextField!
    @IBOutlet var lblRelationHeading : UILabel!
    @IBOutlet var vwRelationSeperator : UIView!
    @IBOutlet var btnNext : UIButton!

    //Add Relation pop up - 2
    @IBOutlet var vwAddRelation2 : UIView!
    @IBOutlet var lblSelectOptionHeading : UILabel!
    @IBOutlet var vwSelectOptionHeading : UIView!

    @IBOutlet var btnOption1 : UIButton!
    @IBOutlet var btnOption2 : UIButton!
    @IBOutlet var btnOption3 : UIButton!
    @IBOutlet var btnOption4 : UIButton!

    @IBOutlet var lblOption1 : UILabel!
    @IBOutlet var lblOption2 : UILabel!
    @IBOutlet var lblOption3 : UILabel!
    @IBOutlet var lblOption4 : UILabel!

    @IBOutlet var btnNext2 : UIButton!

    //Add Relation pop up - 3
    @IBOutlet var vwAddRelation3 : UIView!
    @IBOutlet var lblSelectGenderHeading : UILabel!

    @IBOutlet var btnSelectGender1 : UIButton!
    @IBOutlet var btnSelectGender2 : UIButton!
    @IBOutlet var btnSelectGender3 : UIButton!
    
    @IBOutlet var lblGender1 : UILabel!
    @IBOutlet var lblGender2 : UILabel!
    @IBOutlet var lblGender3 : UILabel!
    
    @IBOutlet var btnSubmitRelation : UIButton!
    @IBOutlet var lblIndicator : UILabel!
    @IBOutlet var lblSteeringIndicator : UILabel!
    
    //Add Move Relation pop up
    @IBOutlet var vwMoveRelation : UIView!
    @IBOutlet var lblRelation : UILabel!
    @IBOutlet var btnRelation : UIButton!
    @IBOutlet var btnSubmitMoveRelation : UIButton!
    @IBOutlet var lblMoveRelationHeading : UILabel!
    
    //Choose option pop up
    @IBOutlet var vwOptions : UIView!
    @IBOutlet var lblOptionHeading : UILabel!

    @IBOutlet var vwSelectShareTutor : UIView!

    @IBOutlet var btnLaunchAssessment : UIButton!
    @IBOutlet var btnSeeMySteering : UIButton!
    @IBOutlet var btnAddSubRelation : UIButton!
    @IBOutlet var btnEditRelationship : UIButton!
    @IBOutlet var btnShareWithTutor : UIButton!
    @IBOutlet var btnSelectShareWithTutor : UIButton!
    @IBOutlet var btnMoveToAnotherRelation : UIButton!
    @IBOutlet var vwEditRelationshipSeperator : UIView!
    @IBOutlet var vwShareWithTutorSeperator : UIView!
    @IBOutlet var vwAddRelationSeperator : UIView!
    @IBOutlet var vwSeeSteeringSeperator : UIView!

    //Edit Relationship
    @IBOutlet var vwEditRelationship : UIView!
    @IBOutlet var lblEditRelationship : UILabel!

    @IBOutlet var btnEditName : UIButton!
    @IBOutlet var btnRemoveRelation : UIButton!
    
    // Add New Popup For redict to new created relation
    @IBOutlet var vwAlertAccessNow : UIView!
    @IBOutlet var btnAssessNow : UIButton!
    @IBOutlet var btnAssessLater : UIButton!
    var newAddedRelationData = JSON()
    
    //Sorting
    @IBOutlet var vwSorting : UIView!
    @IBOutlet var lblSortingHeading : UILabel!
    
    @IBOutlet var btnSortByRelationship : UIButton!
    @IBOutlet var btnSortBySelfDisclosure : UIButton!
    @IBOutlet var btnSortByTrustOfOthers : UIButton!
    @IBOutlet var btnSortByTrustOfSelf : UIButton!
    @IBOutlet var btnSortBySeekingChange : UIButton!
    
    @IBOutlet var vwAddEvent : UIView!
    @IBOutlet var vwAddRelationship : UIView!
    
    //---
    @IBOutlet var vwCreateRelationInfo : UIView!
    @IBOutlet var lblCreateRelationInfo : UILabel!

    @IBOutlet var collectionviewCreateNodeImages : UICollectionView!
    @IBOutlet var pageControl : UIPageControl!
    
    @IBOutlet var vwContent : UIView!
    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var vwBack : UIView!
    @IBOutlet var vwInfo : UIView!
    
    @IBOutlet var vwPhaseChangeInfo : UIView!
    @IBOutlet var vwPhaseChangeInfoBack : UIView!
    @IBOutlet var lblPhaseChangeMessage : UILabel!
    
    @IBOutlet var faviconSharewithStack:UIStackView!
    @IBOutlet var btn_Active_Logo:UIButton!
    @IBOutlet var view_Active_Logo:UIView!

    //MARK: - Variables
    var isOnChooseRelation = true
    var arrayCreateNodeInfo : [UIImage] = [#imageLiteral(resourceName: "createNode1"),#imageLiteral(resourceName: "createNode2"),#imageLiteral(resourceName: "createNode3")]
    var selectedVC = selectedTreeController.AssesmentLaunchVC
    var arrayRelation : [JSON] = []
    var categoryDictData = JSON()
    var root: TreeViewerDelegate?
    var treeView : HierarchyScrollView?
    var count = 0
    var parentNodeDD = DropDown()
    var filterDD = DropDown()
    var relationDD = DropDown()

    var selectedFilter = FilterOption.ByRelationship

    var arrayParentNodes : [JSON] = []
    var arrayParentNodesNames : [String] = []
    var arrayFilter : [String] = []
    
    var arrayRelationsAutoComplete : [String] = []
    var arrayRelationsAutoCompleteMainArray : [String] = []
    var arraySuggestionRelations : [JSON] = []
    var arraySuggestionRelationsMainArray : [JSON] = []

    var arrayColors : [String] = []
    var dictTreeData = JSON()
    var strSelectedParentNode = ""
    var isLoadedFirstTime = true
    var isAddName = true
    var isAddRelationShipOrEvent = false
    var strSelectedRealtionId = ""
    var dictSelectedrelation = JSON()
    var isNewNameAdded = true
    var dictRelationData = JSON()
    var lastContentOffset: CGPoint = .zero
    var timer : Timer?
    var isTreeLoad = true
    var isShowOptionPopup = false
    var isFromSideMenuSteering = true
    var isShowAddCategoryAlert = false

    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
        pageControl.numberOfPages = arrayCreateNodeInfo.count
        getAppVersion()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.reloadTreeData), name: NSNotification.Name(rawValue: "reloadTreeData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showOptionPopup), name: NSNotification.Name(rawValue: "showPopup"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ReloadTreeMapView), name: NSNotification.Name(rawValue: "ReloadTreeMapView"), object: nil)

        treeZoomLevel =  0.6
        if (self.selectedVC == .Sorting)
        {
            treeZoomLevel =  0.3
        }
        
        view_Active_Logo.alpha = 1.0
        view_Active_Logo.startButtonViewBlink()
        
        isOnTreeView = true
        self.vwOptions.isHidden = true

        self.vwAlertAccessNow.isHidden = true
        self.vwAlertAccessNow.alpha = 0
        
        hideShowOptionPopup()
        treeView?.setDefaultContentOffset()
        
        getRelationService()
        
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
    @objc func ReloadTreeMapView()
    {
        treeView?.layoutIfNeeded()
        treeView?.layoutSubviews()
        
        collectionviewCreateNodeImages.layoutSubviews()
        vwCreateRelationInfo.layoutSubviews()
        vwCreateRelationInfo.layoutIfNeeded()
        collectionviewCreateNodeImages.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(true)
        isOnTreeView = false
        timer?.invalidate()
        timer = nil
        isOnChooseRelation = false
    }
    
    override func viewDidLayoutSubviews()
    {
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.all)
        vwTree.layoutIfNeeded()
        vwTree.layoutSubviews()
        treeView?.layoutIfNeeded()
        treeView?.layoutSubviews()
    }
    
    override var prefersStatusBarHidden : Bool {
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        vwTree.subviews.forEach({ $0.removeFromSuperview() })
    }
    
    @IBAction func openScoreView()
    {
        if (self.selectedVC == .Sorting)
        {
            NotificationCenter.default.post(name: NSNotification.Name("LearnNavigate"), object: nil, userInfo: ["userInfo": ["tab": 2]])
        }
        else
        {
            let vc = objLearnStoryboard.instantiateViewController(withIdentifier: "ProgressVC") as! ProgressVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

//MARK:- Setup UI
extension ChooseRelationVC 
{
    func setUpUI()
    {        
        vwRelation.isHidden = true
        vwInfo.isHidden = true
        self.vwMoveRelation.isHidden = true
        self.vwEditRelationship.isHidden = true
        
        lblIndicator.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblIndicator.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 8)
        
        lblSteeringIndicator.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblSteeringIndicator.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 8)
        
        lblCreateRelationInfo.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblPhaseChangeMessage.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)

        vwSorting.isHidden = true
        
        [lblRelationHeading].forEach({
            $0?.textColor = .white
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        })

        [btnSubmitRelation,btnSubmitMoveRelation,btnNext,btnNext2].forEach({
            $0?.backgroundColor = MySingleton.sharedManager.themeYellowColor
            $0?.setTitleColor(.white, for: .normal)
            $0?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        })
        
        lblOptionHeading.text = getCommonString(key: "Choose_option_key").uppercased()//
        [lblOptionHeading,lblMoveRelationHeading,lblEditRelationship,lblSelectGenderHeading,lblSortingHeading].forEach { (label) in
            label?.textColor = .white
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 18)
            label?.backgroundColor = MySingleton.sharedManager.themeYellowColor
        }
        lblSelectOptionHeading.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblSelectOptionHeading.textColor = .white
        vwSelectOptionHeading.backgroundColor = MySingleton.sharedManager.themeYellowColor
        [btnLaunchAssessment,btnSeeMySteering,btnAddSubRelation,btnEditRelationship,btnShareWithTutor,btnMoveToAnotherRelation,btnRemoveRelation,btnEditName].forEach { (button) in
            button?.setTitleColor(MySingleton.sharedManager.themeLightGrayColor, for: .normal)
            button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 18)
        }
        
        lblMoveRelationHeading.text = getCommonString(key: "MoveRelationHeading_key")
        vwOptions.isHidden = true
        vwRelationSeperator.backgroundColor = MySingleton.sharedManager.themeYellowColor

        lblRelation.text = getCommonString(key: "Select_relation_key")
        lblRelation.textColor = MySingleton.sharedManager.themeDarkGrayColor
        
        [txtRelation].forEach({
            $0?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        })
        
        [lblOption1,lblOption2,lblOption3,lblOption4,lblGender1,lblGender2,lblGender3].forEach{ (label) in
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        }
        
        vwPhaseChangeInfo.isHidden = true
        
        btnLaunchAssessment.setTitle(getCommonString(key: "Track_my_steering_key"), for: .normal)
        btnSeeMySteering.setTitle(getCommonString(key: "See_my_steering_key"), for: .normal)
        btnAddSubRelation.setTitle(getCommonString(key: "Add_new_relation_key"), for: .normal)
        btnEditRelationship.setTitle(getCommonString(key: "Edit_relationsip_key"), for: .normal)
        btnShareWithTutor.setTitle(getCommonString(key: "Share_with_tutor_key"), for: .normal)
        btnMoveToAnotherRelation.setTitle(getCommonString(key: "MoveRelation_key"), for: .normal)
        btnSubmitMoveRelation.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        
        lblSortingHeading.text = getCommonString(key: "Sort_your_relationships_key")
        btnSortByRelationship.setTitle(getCommonString(key: "Sort_by_relationship_key"), for: .normal)
        btnSortBySelfDisclosure.setTitle(getCommonString(key: "Sort_by_self_disclosure_key"), for: .normal)
        btnSortByTrustOfOthers.setTitle(getCommonString(key: "Sort_by_trust_of_others_key"), for: .normal)
        btnSortByTrustOfSelf.setTitle(getCommonString(key: "Sort_by_trust_of_self_key"), for: .normal)
        btnSortBySeekingChange.setTitle(getCommonString(key: "Sort_by_seeking_change_key"), for: .normal)

        
        [btnSortByRelationship,btnSortBySelfDisclosure,btnSortByTrustOfOthers,btnSortByTrustOfSelf,btnSortBySeekingChange].forEach { (button) in
            button?.setTitleColor(MySingleton.sharedManager.themeDarkGrayColor, for: .normal)
             button?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 16)
        }
        vwCreateRelationInfo.isHidden = true
                
        btnEditName.setTitle(getCommonString(key: "Edit_name_key"), for: .normal)
        btnRemoveRelation.setTitle(getCommonString(key: "Remove_relationship_key"), for: .normal)
        
        btnAssessNow.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
        btnAssessNow.layer.borderWidth = 2.0
        btnAssessNow.layer.cornerRadius = 10.0
        
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwOptions.addGestureRecognizer(tapOptions)
        vwAlertAccessNow.addGestureRecognizer(tapOptions)
        
        let tapMoveRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwMoveRelation.addGestureRecognizer(tapMoveRelation)
        
        let tapRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwRelation.addGestureRecognizer(tapRelation)
        
        let tapRelationCreateRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwCreateRelationInfo.addGestureRecognizer(tapRelationCreateRelation)
        
        let tapPhaseInfo = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwPhaseChangeInfoBack.addGestureRecognizer(tapPhaseInfo)

        btnAddRelation.isHidden = true
        txtRelation.addTarget(self, action: #selector(textFieldDidChange), for: UIControlEvents.editingChanged)
        setUpFilterArray()
       
        setUpDropdown(dropdown : parentNodeDD,sender : btnRelation)
        setUpDropdownTextfield(dropdown : relationDD,sender : txtRelation)

        // Action triggered on selection
        parentNodeDD.selectionAction = { (index: Int, item: String) in
            let dict = self.arrayParentNodes[index]
            self.strSelectedParentNode = dict["rid"].stringValue
            self.lblRelation.text = item
            self.parentNodeDD.hide()
        }
        
        // Action triggered on selection
        filterDD.selectionAction = { (index: Int, item: String) in
            self.getSelectedFilter(index : index)
            self.filterDD.selectRow(index)
            treeConsetOffset = CGPoint(x:0,y:0)
            treeZoomLevel = 0.6
            self.filterDD.hide()
        }
        
        // Action triggered on selection
        relationDD.selectionAction = { (index: Int, item: String) in
            self.isNewNameAdded = false
            let dict = self.arraySuggestionRelations[index]
            self.dictSelectedrelation = dict
            self.strSelectedRealtionId = dict["uid"].stringValue
            self.txtRelation.text = item
            self.relationDD.selectRow(index)
            self.relationDD.hide()
        }
            
        lblCreateRelationInfo.text = "Understand your relationships better by grouping them according to their biases"
        setUpPopUI()
        setUpAddEditRelation(isAdd: true,isEvent:true)
        setColorsArray()
        setupNavigationController()
    }
    
    func setUpPopUI()
    {
        vwContent.layer.cornerRadius = 5
        vwContent.layer.masksToBounds = true
        vwContent.backgroundColor = MySingleton.sharedManager.themeChatGrayColor
        
        lblMessage.textColor = UIColor.white
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)
        lblMessage.text = "To complete your AS Tracking assessment you must track your steering for both SELF and SCHOOL"
        let tapRelation = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwBack.addGestureRecognizer(tapRelation)
    }
 
    func hideShowCreateRelationView()
    {
        if(isTreeLoad)
        {
            if(getPhaseDetail("is_phase_changed").boolValue)
            {
                if(selectedVC == .AppLaunch)
                {
                    var strMessage = ""
                    vwPhaseChangeInfo.isHidden = false
                    if(getPhaseDetail("phase_mode").stringValue == "1")
                    {
                        strMessage =  "Your tutor has temporarily restricted the relationships you can track on USTEER"
                    }else{
                        strMessage = "Heh, great news, your tutor has opened new app functions for you! You can now track other relationships and events! To learn how, watch the HOW USTEER animation now."
                    }
                    lblPhaseChangeMessage.text = strMessage
                }
                
            }else{
                vwPhaseChangeInfo.isHidden = true
            }
            
            if(getPhaseDetail("is_assessment_done").boolValue)
            {
                hideShowHintPopUp()
                
            }else if(getPhaseDetail("is_assessment_done").boolValue == false)
            {
                hideShowHintPopUp()
            }
        }
    }
    
    func showCommonMessagePopUp(strMessage : String)
    {
        let obj = InfoPopUpVC()
        obj.popUpDelegate = self
        obj.strMessage = strMessage
        obj.colorPopUpContent = UIColor.init(red: 200/255.0, green: 200/255.0, blue: 200/255.0, alpha: 1.0)
        obj.colorText = UIColor.white
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
    
    func hideShowHintPopUp()
    {
        if(isCheckAppVersion() == 1)
        {
            vwInfo.isHidden = false
        }
        else
        {
            vwInfo.isHidden = true
            if(isFromSideMenuSteering)
            {
                collectionviewCreateNodeImages.isHidden = false
                lblCreateRelationInfo.text = "Start adding new relationships or events to your network to create your map"
                pageControl.isHidden = false
                if (Defaults.value(forKey: "createSteeringRelationInfoCount") == nil)
                {
                    Defaults.setValue(1, forKey: "createSteeringRelationInfoCount")
                    isTreeLoad = false
                    vwCreateRelationInfo.isHidden = false
                    timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                }
                else
                {
                    var count = Defaults.value(forKey: "createSteeringRelationInfoCount") as? Int
                    if(count! <= 2)
                    {
                        count = count! + 1
                        Defaults.setValue(count, forKey: "createSteeringRelationInfoCount")
                        isTreeLoad = false
                        vwCreateRelationInfo.isHidden = false
                        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                    }
                    else
                    {
                        vwCreateRelationInfo.isHidden = true
                        timer?.invalidate()
                    }
                }
            }
            else
            {
                collectionviewCreateNodeImages.isHidden = true
                pageControl.isHidden = true
                lblCreateRelationInfo.text = "Understand your relationships better by grouping them according to their biases"
                if (Defaults.value(forKey: "createRelationInfoCount") == nil)
                {
                    Defaults.setValue(1, forKey: "createRelationInfoCount")
                    isTreeLoad = false
                    vwCreateRelationInfo.isHidden = false
                    timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                }
                else
                {
                    var count = Defaults.value(forKey: "createRelationInfoCount") as? Int
                    if(count! < 6)
                    {
                        count = count! + 1
                        Defaults.setValue(count, forKey: "createRelationInfoCount")
                        isTreeLoad = false
                        vwCreateRelationInfo.isHidden = false
                        timer = Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                    }
                    else
                    {
                        vwCreateRelationInfo.isHidden = true
                        timer?.invalidate()
                    }
                }
            }
        }
    }
    
    func hideShowOptionPopup()
    {
        if(selectedVC == .socialAssistant)
        {
            isShowOptionPopup = true
            getSingleRelationService()
        }
    }
    
    @objc func timerAction()
    {
        if(isOnChooseRelation)
        {
            var pageNumber = Int(round(collectionviewCreateNodeImages.contentOffset.x / collectionviewCreateNodeImages.frame.size.width))
            if(pageNumber == arrayCreateNodeInfo.count-1)
            {
                pageNumber = 0
                collectionviewCreateNodeImages.contentOffset.x = 0
            }
            else
            {
                pageNumber = pageNumber+1
                collectionviewCreateNodeImages.contentOffset.x = collectionviewCreateNodeImages.frame.size.width * CGFloat(pageNumber)
            }
            pageControl.currentPage = pageNumber
        }
    }
    
    func getSelectedFilter(index : Int)
    {
        switch index
        {
            case 0:
                self.selectedFilter = FilterOption.ByRelationship
            case 1:
                self.selectedFilter = FilterOption.BySelfDisclosure
            case 2:
                self.selectedFilter = FilterOption.ByTrustOfOthers
            case 3:
                self.selectedFilter = FilterOption.ByTrustOfSelf
            case 4:
                self.selectedFilter = FilterOption.BySeekingChange
            default:
                self.selectedFilter = FilterOption.ByRelationship
        }
        self.getRelationService()
    }
    
    func getSelectedFiletrIndex() -> Int
    {
        switch self.selectedFilter
        {
            case .ByRelationship:
                return 0
            case .BySelfDisclosure:
                return 1
            case .ByTrustOfOthers:
                return 2
            case .ByTrustOfSelf:
                return 3
            case .BySeekingChange:
                return 4
            default:
                return 0
        }
    }
    
    func setUpAddEditRelation(isAdd : Bool,isEvent:Bool)
    {
        if(isAdd)
        {
            if(isEvent)
            {
                self.txtRelation.placeholder = "Enter event name"
                vwAddRelationship.isHidden = true
                vwAddEvent.isHidden = false
                lblRelationHeading.text = "Add Event"
            }else{
                self.txtRelation.placeholder = "Enter relation name"
                vwAddRelationship.isHidden = false
                vwAddEvent.isHidden = true
                lblRelationHeading.text = getCommonString(key: "Add_relation_key")
            }
            
        }else{
            if(isEvent)
            {
                self.txtRelation.placeholder = "Enter event name"
                vwAddRelationship.isHidden = true
                vwAddEvent.isHidden = false
                lblRelationHeading.text = "Edit Event"
            }else{
                self.txtRelation.placeholder = "Enter relation name"
                vwAddRelationship.isHidden = false
                vwAddEvent.isHidden = true
                lblRelationHeading.text = "Edit Relationship"
            }
        }
    }
    
    func setUpFilterArray()
    {
        self.arrayFilter = ["Map by Relationship","Map by Self disclosure","Map by trust of others","Map by trust of self","Map by Seeking change"]
        filterDD.dataSource = self.arrayFilter
    }
    
    func setUpRelationsArray()
    {
        relationDD.dataSource = self.arrayRelationsAutoCompleteMainArray
        relationDD.reloadAllComponents()
    }
    
    func getContactsNameList() -> [JSON]
    {
        let arrayContacts : [JSON] = []
        return arrayContacts
    }
    
    func setUpDropdown(dropdown : DropDown,sender : UIButton)
    {
        dropdown.textColor = MySingleton.sharedManager.themeDarkGrayColor
        dropdown.textFont = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        dropdown.backgroundColor = UIColor.white
        dropdown.selectionBackgroundColor = MySingleton.sharedManager.themeBlueColor.withAlphaComponent(0.35)
        dropdown.cellHeight = 60
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.anchorView = sender
        dropdown.width = sender.frame.size.width
    }
    
    func setUpDropdownTextfield(dropdown : DropDown,sender : UITextField)
    {
        dropdown.textColor = MySingleton.sharedManager.themeDarkGrayColor
        dropdown.textFont = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
        dropdown.backgroundColor = UIColor.white
        dropdown.selectionBackgroundColor = MySingleton.sharedManager.themeBlueColor.withAlphaComponent(0.35)
        dropdown.cellHeight = 55
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.anchorView = sender
        dropdown.width = sender.frame.size.width + 30
        dropdown.bottomOffset = CGPoint(x:0,y:sender.bounds.height)
        dropdown.cellNib = UINib(nibName: "SuggestionTableViewCell", bundle: nil)
        dropdown.customCellConfiguration = { (index, item, cell) in
            guard let cell = cell as? SuggestionTableViewCell else { return }
            
            let dict = self.arraySuggestionRelations[index]
            cell.optionLabel.text = dict["name"].stringValue
            cell.lblEmail.text = dict["username"].stringValue
        }
    }
    
    func setColorsArray()
    {
        arrayColors.append("#99CCFF")
        arrayColors.append("#FF99CC")
        arrayColors.append("#33CCCC")
        arrayColors.append("#99CC00")
        arrayColors.append("#FFCC00")
        arrayColors.append("#FF9900")
        arrayColors.append("#FF6600")
        arrayColors.append("#666699")
        /*
        arrayColors.append("#4169E1")
        arrayColors.append("#FFA500")
        arrayColors.append("#FF6347")
        arrayColors.append("#EE9A00")
        arrayColors.append("#008B00")
        arrayColors.append("#03A89E")
        arrayColors.append("#D02090")
        arrayColors.append("#BC8F8F")*/
    }
    
    func setupNavigationController()
    {
        switch selectedVC
        {
            case .AssesmentLaunchVC:
                setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Choose_relation_to_steer_key"))
            case .PlanMyRouteMap:
                if(isCheckAppVersion() == 1)
                {
                    setUpNavigationBarWithTitleAndBack(strTitle: getCommonString(key: "Relation_tobe_steer_key"))
                }
                else
                {
                    setUpNavigationBarWithTitleAndRightButton(strTitle: getCommonString(key: "Relation_tobe_steer_key"),isShowSideMenu : true)
                }
            case .Sorting:
                setUpNavigationBarWithTitleAndRightButton(strTitle:getCommonString(key: "Sort_your_relationships_key"),isShowSideMenu : true)
                self.selectedFilter = .ByTrustOfSelf

            case .AppLaunch:
                setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Choose_relation_to_steer_key"))

            case .socialAssistant:
                setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Choose_relation_to_steer_key"))
            
            case .LinkSchool:
                setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Choose_relation_to_steer_key"))
        }
    }
    func setUpNavigationBarWithTitleAndRightButton(strTitle : String,isShowSideMenu : Bool)
    {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.navigationBar.isTranslucent = false
        
        var leftButton = UIBarButtonItem()
        if(isShowSideMenu)
        {
            leftButton = UIBarButtonItem(image: UIImage(named: "ic_menubar_white_header"), style: .plain, target: self.revealViewController(), action: #selector(SWRevealViewController.revealToggle(_:)))
            leftButton.tintColor = UIColor.white
        }
        else
        {
            leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back_arrow_white_header"), style: .plain, target: self, action: #selector(btnBackAction(_:)))
            leftButton.tintColor = UIColor.white
        }
        
        let rightButton = UIBarButtonItem(image:#imageLiteral(resourceName: "ic_header_new_white_usteer"), style: .plain, target:self, action: #selector(filterAction))
        rightButton.tintColor = UIColor.white
        
        self.navigationItem.rightBarButtonItem = rightButton
        self.navigationItem.leftBarButtonItem = leftButton
        self.navigationItem.hidesBackButton = false
        
        let HeaderLabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: 100, height: 35))
        HeaderLabel.isUserInteractionEnabled = false
        HeaderLabel.text = strTitle
        HeaderLabel.textColor = UIColor.white
        HeaderLabel.numberOfLines = 2
        HeaderLabel.textAlignment = .center
        HeaderLabel.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        self.navigationItem.titleView = HeaderLabel
    }
    
    @objc func filterAction()
    {
        vwSorting.isHidden = false
    }

    @objc func reloadTreeData()
    {
        getRelationService()
    }
}
//MARK: - Pop up delegate
extension ChooseRelationVC : InfoPopUpDelegate
{
    func dismissPopUp()
    {
        print("Dismiss")
    }
}
//MARK: - Geture method
extension ChooseRelationVC
{
    /// Tap reconizer on table to hide keyboard
    ///
    /// - Parameter sender: geture reconizer
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        // handling code
        if(sender?.view == vwRelation)
        {
            //self.vwRelation.fadeOut()
        }else if(sender?.view == vwMoveRelation)
        {
            self.vwMoveRelation.fadeOut()
        }
        else if(sender?.view == vwCreateRelationInfo)
        {
            self.vwCreateRelationInfo.fadeOut()
            timer?.invalidate()
        }
        else if(sender?.view == vwBack)
        {
            self.vwInfo.fadeOut()
        }else if(sender?.view == vwPhaseChangeInfoBack)
        {
            self.vwPhaseChangeInfo.fadeOut()
            PhaseChangeSeenService()
        }
        else if(sender?.view == vwOptions){
            self.vwOptions.fadeOut()
            self.getRelationService()
        }
        else if(sender?.view == vwAlertAccessNow){
            self.vwAlertAccessNow.fadeOut()
            self.resetValues()
            self.getRelationService()
        }
        self.view.endEditing(true)
    }
}

//MARK:- Scrollview Methods
extension ChooseRelationVC
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if(scrollView == collectionviewCreateNodeImages)
        {
            let pageNumber = Int(round(collectionviewCreateNodeImages.contentOffset.x / collectionviewCreateNodeImages.frame.size.width))
            pageControl.currentPage = Int(pageNumber)
            self.lastContentOffset.x = scrollView.contentOffset.x
        }
    }
}
//MARK: - Tree diagram
extension ChooseRelationVC
{
    func setup(dict : JSON) -> TreeViewerDelegate
    {
        do {
            let rootInfo = dict
            //traverse the tree from plist and create node.
            var count: Int = 0
            var visited = [JSON]()
            // a queue to insert visited nodes in.
            let rootHexString = rootInfo["nodeColor"].stringValue
            let root = Node(identifier: "i_\(count)", info: rootInfo,textcolor:hexStringToUIColor(hex: rootHexString))
            var queue = [JSON]()
            var nodeQueue = [Node]()
            var nodes = [Node]()
            
            nodeQueue.append(root)
            queue.append(rootInfo)
            visited.append(rootInfo)
            while queue.count > 0
            {
                let nodeInfo = (queue[0])
                let parent = (nodeQueue[0])
                nodes.append(parent)
                queue.remove(at: 0)
                nodeQueue.remove(at: 0)
                for childInfo: JSON in nodeInfo["is_sub"].arrayValue
                {
                    let hexString = childInfo["nodeColor"].stringValue
                    do {
                        count += 1
                        var node : Node?
                        if(parent.identifier == "i_0")
                        {
                            node = Node(identifier: "i_\(count)", info: childInfo,textcolor:hexStringToUIColor(hex: hexString))
                        }else{
                            node = Node(identifier: "i_\(count)", info: childInfo,textcolor:(parent.textColor)!)
                        }
                        parent.children.append(node!)
                        nodeQueue.append(node!)
                        queue.append(childInfo)
                        visited.append(childInfo)
                    }
                }
            }
            for i in 0..<nodes.count
            {
                let cell: CustomNodeView? = (tblRelation.dequeueReusableCell(withIdentifier: "customCell") as? CustomNodeView)
                let node = nodes[i]
                cell?.createView(with: (node),with: (node.textColor)!)
                let view: UIView? = cell?.customView
                (nodes[i]).nodeView = view
            }
            return root
        }
    }
}
//MARK: - Add Relation Button actions
extension ChooseRelationVC
{
    @IBAction func btnNextStep1Action(_ sender : UIButton)
    {
        if(txtRelation.text?.trimmed() == "")
        {
            showToast(message: R.string.validationMessage.enter_relation_key())
        }else{
            setStatements()
            if(isAddRelationShipOrEvent) //Add relationship
            {
                if(self.dictSelectedrelation["allowToSelectGender"].boolValue == true)
                {
                    btnNext2.setTitle(getCommonString(key: "Next_key"), for: .normal)
                }else{
                    
                    if(isNewNameAdded)
                    {
                        btnNext2.setTitle(getCommonString(key: "Next_key"), for: .normal)
                    }else{
                        btnNext2.setTitle(getCommonString(key: "Submit_key"), for: .normal)
                    }
                }
            }else{
                btnNext2.setTitle(getCommonString(key: "Submit_key"), for: .normal)
            }
            addRelationStep2()
        }
        
    }
    
    @IBAction func btnNextStep2Action(_ sender : UIButton)
    {
        if(!btnOption1.isSelected && !btnOption2.isSelected && !btnOption4.isSelected && !btnOption3.isSelected)
        {
            showToast(message: R.string.validationMessage.select_statement_key())
        }
        else
        {
            if(isAddRelationShipOrEvent) //Add relationship
            {
                if(btnOption1.isSelected || btnOption2.isSelected)
                {
                    if(self.dictSelectedrelation["allowToSelectGender"].boolValue == true)
                    {
                        addRelationStep3()
                    }
                    else
                    {
                        if(isNewNameAdded)
                        {
                            addRelationStep3()
                        }
                        else
                        {
                            if(isAddName)
                            {
                                addSubRelationService()
                            }
                            else
                            {
                                editSubRelationService()
                            }
                        }
                    }
                }
                else if(btnOption3.isSelected)
                {
                    if(isAddName)
                    {
                        addSubRelationService()
                        
                    }else{
                        editSubRelationService()
                    }
                }
                
            }else{   //Add event
                if(!btnOption4.isSelected)
                {
                    showToast(message: R.string.validationMessage.select_statement_key())

                }else{
                    if(isAddName)
                    {
                        addSubRelationService()
                    }else{
                        editSubRelationService()
                    }
                }
            }
        }
    }
    @IBAction func btnAddRelationSelectStatementAction(_ sender : UIButton)
    {
        if(sender == btnOption4)
        {
            sender.isSelected = !sender.isSelected
        }
        setSelected(selectedButton : sender)
    }
    @IBAction func btnAddRelationSelectGenderAction(_ sender : UIButton)
    {
        setSelectedGender(selectedButton : sender)
    }
    func setStatements()
    {
        lblOption1.text = "Give the pen to your " + "\(txtRelation.text ?? "")"
        lblOption2.text = "Give the pen to " + "\(txtRelation.text ?? "")"
        lblOption3.text = "Give the pen to the people in your " + "\(txtRelation.text ?? "")"
        lblOption4.text = "Use the pen for " + "\(txtRelation.text ?? "")"
    }
    func setSelected(selectedButton : UIButton)
    {
        [btnOption1,btnOption2,btnOption3,btnOption4].forEach { (btn) in
            btn?.isSelected = false
        }
        selectedButton.isSelected = true
        
        if(selectedButton == btnOption3 || selectedButton == btnOption4)
        {
            btnNext2.setTitle(getCommonString(key: "Submit_key"), for: .normal)
        }else{
            btnNext2.setTitle(getCommonString(key: "Next_key"), for: .normal)
        }
    }
    
    func setSelectedGender(selectedButton : UIButton)
    {
        [btnSelectGender1,btnSelectGender2,btnSelectGender3].forEach { (btn) in
            btn?.isSelected = false
        }
        selectedButton.isSelected = true
    }
    
    @IBAction func btnAssessLaterAction()
    {
        // Need to show alert
        let alertView = UIAlertController(title: kCommonAlertTitle, message:"Remind me to do so later", preferredStyle: .alert)
        let OkAction = UIAlertAction(title: "REMIND ME", style: .default, handler: { (alert) in
            let current = UNUserNotificationCenter.current()
            current.getNotificationSettings(completionHandler: { (settings) in
                if settings.authorizationStatus == .notDetermined || settings.authorizationStatus == .denied
                {
                    let goToSettingsAlert = UIAlertController(title: "", message: "Your need to provide a permission from notification settings.", preferredStyle: UIAlertControllerStyle.alert)
                    let laterAction = UIAlertAction(title: "Not now", style: .default, handler: { (alert) in
                        self.vwAlertAccessNow.fadeOut()
                        self.resetValues()
                        self.getRelationService()
                    })
                    goToSettingsAlert.addAction(laterAction)

                    goToSettingsAlert.addAction(UIAlertAction(title:"Settings", style: .destructive, handler: { (action: UIAlertAction) in
                        DispatchQueue.main.async {
                            guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
                                return
                            }
                            if UIApplication.shared.canOpenURL(settingsUrl) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                                        print("Settings opened: \(success)") // Prints true
                                    })
                                } else {
                                    UIApplication.shared.openURL(settingsUrl as URL)
                                }
                            }
                        }
                    }))
                    appDelegate.window?.rootViewController?.present(goToSettingsAlert, animated: true, completion:nil)
                }
                else
                {
                    self.saveEmailSettingNotification(switchValue:"pn",isOnValue:"1")
                }
            })
        })
        let laterAction = UIAlertAction(title: "Don't remind", style: .default, handler: { (alert) in
            self.vwAlertAccessNow.fadeOut()
            self.resetValues()
            self.getRelationService()
        })
        alertView.addAction(OkAction)
        alertView.addAction(laterAction)
        appDelegate.window?.rootViewController?.present(alertView, animated: true, completion:nil)
    }
    
    @IBAction func btnAssessNowAction()
    {
        getAssessments(dict: self.newAddedRelationData)
    }
}
//MARK: - Sort button Actions
extension ChooseRelationVC
{
    @IBAction func btnSortAction(_ sender : UIButton)
    {
        self.vwSorting.isHidden = true
        if(sender == btnSortByRelationship)
        {
            self.selectedFilter = .ByRelationship
        }
        else if(sender == btnSortBySelfDisclosure)
        {
            self.selectedFilter = .BySelfDisclosure
        }
        else if(sender == btnSortByTrustOfOthers)
        {
            self.selectedFilter = .ByTrustOfOthers
        }
        else if(sender == btnSortByTrustOfSelf)
        {
            self.selectedFilter = .ByTrustOfSelf
        }
        else if(sender == btnSortBySeekingChange)
        {
            self.selectedFilter = .BySeekingChange
        }
        self.isLoadedFirstTime = false
        self.getRelationService()
    }
    
    @IBAction func btnSortCloseAction(_ sender : UIButton)
    {
        self.isLoadedFirstTime = false
        vwSorting.isHidden = true
    }
}

//MARK: - Button Action
extension ChooseRelationVC
{
    @objc func showOptionPopup(_ notification : NSNotification)
    {
        let object = notification.object
        categoryDictData = object as? JSON ?? JSON()
        print("categoryDictData:\(categoryDictData)")
        
        strSelectedRealtionId = categoryDictData["link_id"].stringValue
        if(categoryDictData["is_shared"].stringValue == "1")
        {
            btnSelectShareWithTutor.isSelected = true
        }
        else
        {
            btnSelectShareWithTutor.isSelected = false
        }
        
        if((categoryDictData["default"].boolValue) || isCheckAppVersion() == 1)
        {
            btnEditRelationship.isHidden = true
            btnMoveToAnotherRelation.isHidden = true
            vwEditRelationshipSeperator.isHidden = true
            vwShareWithTutorSeperator.isHidden = true
            vwSelectShareTutor.isHidden = true
        }
        else
        {
            btnMoveToAnotherRelation.isHidden = false
            vwShareWithTutorSeperator.isHidden = false
            btnEditRelationship.isHidden = false
            vwEditRelationshipSeperator.isHidden = false
            vwSelectShareTutor.isHidden = false
            vwAddRelationSeperator.isHidden = false
            if((categoryDictData["is_edited"].boolValue))
            {
                btnEditName.isHidden = false
            }
            else
            {
                btnEditName.isHidden = true
            }
        }
        
        if(isCheckAppVersion() == 1)
        {
            btnAddSubRelation.isHidden = true
            vwSeeSteeringSeperator.isHidden = true
        }
        else
        {
            if((categoryDictData["is_sub_allow"].boolValue))
            {
                btnAddSubRelation.isHidden = false
                vwSeeSteeringSeperator.isHidden = false
                isShowAddCategoryAlert = true
            }
            else
            {
                btnAddSubRelation.isHidden = true
                vwSeeSteeringSeperator.isHidden = true
            }
        }
        if(categoryDictData["is_share_allow"].boolValue == true)
        {
            vwShareWithTutorSeperator.isHidden = false
            vwSelectShareTutor.isHidden = false
        }
        else
        {
            vwShareWithTutorSeperator.isHidden = true
            vwSelectShareTutor.isHidden = true
        }
        if(categoryDictData["disable_see_chart"].boolValue == true)
        {
            btnSeeMySteering.isHidden = true
            vwSeeSteeringSeperator.isHidden = true
        }
        else
        {
            btnSeeMySteering.isHidden = false
            vwSeeSteeringSeperator.isHidden = false
        }
        showOptions()
    }
    
    @IBAction func btnAddRelationAction(_ sender : UIButton)
    {
        isAddName = true
        resetValues()
        self.view.endEditing(true)
        categoryDictData = dictTreeData
        if(categoryDictData["rid"].stringValue == "")
        {
            showToast(message: R.string.validationMessage.cant_add_relation_key())
        }else{
            self.vwRelation.alpha = 0
            resetValues()
            self.vwRelation.fadeIn()
            addRelationStep1()
        }
    }
    
    @IBAction func btnSubmitRelationAction(_ sender : UIButton)
    {
        if(btnSelectGender1.isSelected || btnSelectGender2.isSelected || btnSelectGender3.isSelected)
        {
            if(isAddName)
            {
                addSubRelationService()
            }
            else
            {
                editSubRelationService()
            }
        }
        else
        {
            showToast(message: R.string.validationMessage.select_gender_key())
        }
        self.view.endEditing(true)
    }
    
    @IBAction func btnCloseRelationAction(_ sender : UIButton)
    {
        vwRelation.fadeOut()
        self.view.endEditing(true)
    }
    
    func popUpAddCategoryAlert(from:String)
    {
        let Message = "We'd suggest you add three categories before you assess your SELF steering. For example…\n\nFriends, Work and Family\n\nChoose as many as you like and then add people to each"
        let attributedString = NSMutableAttributedString(string:Message)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.black , range: NSRange(location: 0, length: Message.length))
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15), range: NSRange(location: 0, length: Message.length))
        let range = (Message as NSString).range(of: "Friends, Work and Family")
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: MySingleton.sharedManager.themeYellowColor , range: range)
        attributedString.addAttribute(NSAttributedStringKey.font, value:MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 15), range: range)
        let obj = InfoPopUpVC()
        obj.attributed = attributedString
        obj.isCloseHidden = false
        obj.popUpDelegate = self
        obj.from = from
        obj.colorPopUpContent = UIColor.white
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
    
    func closeButtonTap(from: String)
    {
        let dict = categoryDictData
        if(from == "btnLaunchAssessment")
        {
            if(dict["rname"].stringValue.lowercased() == "self")
            {
                CheckAssessments(dict: dict)
            }else{
                getAssessments(dict: dict)
            }
        }
        else
        {
            self.vwOptions.isHidden = true
            let obj = objReportStoryboard.instantiateViewController(withIdentifier: "MySteeringVC") as! MySteeringVC
            obj.categoryDict = dict
            obj.isShowLatestButton = (categoryDictData["tree_noti"].intValue > 0) ? true : false
            obj.isSelectedFromVC = selectedReportController.ChooseRelationVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        Defaults.set(3, forKey: "ShowAddCategoryAlert_Count")
    }
    
    @IBAction func btnOptionSelectAction(_ sender : UIButton)
    {
        let dict = categoryDictData
        print("dict:\(dict)")
        if(sender == btnLaunchAssessment)
        {
            if isShowAddCategoryAlert == true
            {
                let count = Defaults.integer(forKey: "ShowAddCategoryAlert_Count")
                if count < 3
                {
                    self.popUpAddCategoryAlert(from: "btnLaunchAssessment")
                    return
                }
            }
            if(dict["rname"].stringValue.lowercased() == "self")
            {
                CheckAssessments(dict: dict)
            }else{
                getAssessments(dict: dict)
            }
        }
        else if(sender == btnSeeMySteering)
        {
            if isShowAddCategoryAlert == true
            {
                let count = Defaults.integer(forKey: "ShowAddCategoryAlert_Count")
                if count < 3
                {
                    self.popUpAddCategoryAlert(from: "btnSeeMySteering")
                    return
                }
            }
            self.vwOptions.isHidden = true
            let obj = objReportStoryboard.instantiateViewController(withIdentifier: "MySteeringVC") as! MySteeringVC
            obj.categoryDict = dict
            obj.isShowLatestButton = (categoryDictData["tree_noti"].intValue > 0) ? true : false
            obj.isSelectedFromVC = selectedReportController.ChooseRelationVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if(sender == btnAddSubRelation)
        {
            let count = Defaults.integer(forKey: "ShowAddCategoryAlert_Count")
            Defaults.set((count+1), forKey: "ShowAddCategoryAlert_Count")
            let alert = UIAlertController(title: kCommonAlertTitle, message: "Do you want to add a new Relationship or a new Event?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Add Relationship", style: .default, handler: { action in
                self.isAddRelationShipOrEvent = true
                self.setUpAddEditRelation(isAdd: true,isEvent:false)
                self.showAddOption()
            }))
            alert.addAction(UIAlertAction(title: "Add Event", style: .default, handler: { action in
                self.isAddRelationShipOrEvent = false
                self.setUpAddEditRelation(isAdd: true,isEvent:true)
                self.showAddOption()
            }))
            alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { (alert) in }))
            self.present(alert, animated: true, completion: nil)
        }
        else if(sender == btnShareWithTutor || sender == btnSelectShareWithTutor)
        {
            let strMessage = dict["is_shared"].stringValue=="0" ? "share" : "unshare"
            let alert = UIAlertController(title: kCommonAlertTitle, message: "Are you sure you want to \(strMessage) relation with tutor?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
                self.btnSelectShareWithTutor.isSelected = !self.btnSelectShareWithTutor.isSelected
                self.shareWithTutorService(isShared : self.btnSelectShareWithTutor.isSelected ? "1" : "0")
            }))
            alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { (alert) in }))
            self.present(alert, animated: true, completion: nil)
        }
        else if(sender == btnMoveToAnotherRelation)
        {
            strSelectedParentNode = ""
            lblRelation.text = getCommonString(key: "Select_relation_key")
            getParentRelations()
            self.vwMoveRelation.alpha = 0
            self.vwMoveRelation.fadeIn()
        }
        else if(sender == btnEditName)
        {
            let alert = UIAlertController(title: kCommonAlertTitle, message: "Do you want to edit a new Relationship or a Event?", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Edit Event", style: .default, handler: { action in
                self.isAddRelationShipOrEvent = false
                self.setUpAddEditRelation(isAdd: false,isEvent:true)
                self.showEditOption()
            }))
            alert.addAction(UIAlertAction(title: "Edit Relationship", style: .default, handler: { action in
                self.isAddRelationShipOrEvent = true
                self.setUpAddEditRelation(isAdd: false,isEvent:false)
                self.showEditOption()
            }))
            alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { (alert) in }))
            self.present(alert, animated: true, completion: nil)
        }
        else if(sender == btnRemoveRelation)
        {
            let alertView = UIAlertController(title: kCommonAlertTitle, message: R.string.validationMessage.relation_delete_message_key(), preferredStyle: .alert)
            let OkAction = UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { (alert) in
                self.removeSubRelationService()
            })
            alertView.addAction(OkAction)
            alertView.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .default, handler: { (alert) in }))
            self.present(alertView, animated: true, completion: nil)
        }
        else
        {
            self.vwEditRelationship.fadeIn()
        }
    }
    
    func showAddOption()
    {
        isAddName = true
        self.vwRelation.alpha = 0
        resetValues()
        self.vwRelation.fadeIn()
        addRelationStep1()
    }
    
    func showEditOption()
    {
        
        isAddName = false
        self.view.endEditing(true)
        
       // setUpAddEditRelation(isAdd: false,isEvent:true)
        
        self.vwRelation.alpha = 0
        resetValues()
        self.txtRelation.text = categoryDictData["rname"].stringValue
        
        if(categoryDictData["rel_type"].stringValue == "1")
        {
            setSelected(selectedButton: btnOption1)
        }else if(categoryDictData["rel_type"].stringValue == "2")
        {
            setSelected(selectedButton: btnOption2)
        }
        else if(categoryDictData["rel_type"].stringValue == "3")
        {
            setSelected(selectedButton: btnOption3)
        }else
        {
            setSelected(selectedButton: btnOption4)
        }
        
        print("categoryDictData - ",categoryDictData)
        
        if(categoryDictData["rel_gender"].stringValue == "m")
        {
            setSelectedGender(selectedButton: btnSelectGender1)
        }else if(categoryDictData["rel_gender"].stringValue == "f")
        {
            setSelectedGender(selectedButton: btnSelectGender2)
            
        }else if(categoryDictData["rel_gender"].stringValue == "n")
        {
            setSelectedGender(selectedButton: btnSelectGender3)
        }
        
        
        self.vwRelation.fadeIn()
        addRelationStep1()
    }
    @IBAction func btnOptionCloseAction(_ sender : UIButton)
    {
        vwOptions.fadeOut()
        self.view.endEditing(true)
        self.getRelationService()

    }
    @IBAction func btnAddInfoAction(_ sender : UIButton)
    {
        sender.isSelected = !sender.isSelected
    }
    @IBAction func btnSelectParentRelationAction(_ sender : UIButton)
    {
        parentNodeDD.show()
    }
    @IBAction func btnMoveRelationCloseAction(_ sender : UIButton)
    {
        vwMoveRelation.fadeOut()
    }
    @IBAction func btnEditRelationCloseAction(_ sender : UIButton)
    {
        vwEditRelationship.fadeOut()
    }
    @IBAction func btnMoveRelationSubmitAction(_ sender : UIButton)
    {
        if(strSelectedParentNode == "")
        {
            showToast(message: R.string.validationMessage.node_move_key())
        }else{
            
            moveRelationService()
        }
    }
    @IBAction func btnTreeviewZoomInAction(_ sender : UIButton)
    {
        treeZoomLevel = (treeView?.maximumZoomScale)!
        treeView?.zoomScale = (treeView?.maximumZoomScale)!
    }
    @IBAction func btnTreeviewZoomOutAction(_ sender : UIButton)
    {
        treeZoomLevel = (treeView?.minimumZoomScale)!
        treeView?.zoomScale = (treeView?.minimumZoomScale)!
    }
}
//MARK:- Collectionview Datasource Methods
extension ChooseRelationVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayCreateNodeInfo.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! CreateNodeCollectionviewCell
        cell.imgvwInfo.image = arrayCreateNodeInfo[indexPath.row]
        return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        var cellWidth : CGFloat = 0.0
        var cellHeight : CGFloat = 0.0
        cellWidth  = (collectionView.frame.size.width)
        cellHeight  = (collectionView.frame.size.height)
        return CGSize(width: cellWidth , height:cellHeight)
    }
}
//MARK: - Other methods
extension ChooseRelationVC
{
    func resetValues()
    {
        txtRelation.text = ""
        [btnOption1,btnOption2,btnOption3,btnOption4,btnSelectGender1,btnSelectGender2,btnSelectGender3].forEach({
            $0?.isSelected = false
        })
        vwAddRelation2.isHidden = true
        vwAddRelation3.isHidden = true
    }
   
    func navigateToAssessmentSlider(categoryDict : JSON)
    {
        let obj = objAssesmentStoryboard.instantiateViewController(withIdentifier: "AssesmentSliderVC") as! AssesmentSliderVC
        obj.categoryDict = categoryDict
        if(categoryDict["rname"].stringValue.lowercased() == "self")
        {
            obj.isSelfAssessment  = true
        }else{
            obj.isSelfAssessment  = false
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func setUpRelationArray(jsonData : JSON)
    {
        //TODO: - rname,defaultname and rel_gender decrypted in Node.swift file
        var totalCount = 1
        var dictTreeData = jsonData
        dictTreeData["nodeColor"].stringValue = "#CE4D4B"
        
        var arrayChildNodes = dictTreeData["is_sub"].arrayValue
        var parentCount = 0

        print("arrayChildNodes:\(arrayChildNodes)")
        
        for i in 0..<arrayChildNodes.count
        {
            var dictChild = arrayChildNodes[i]
            if(parentCount < arrayColors.count)
            {
                dictChild["nodeColor"].stringValue = self.arrayColors[parentCount]
            }else{
                dictChild["nodeColor"].stringValue = self.arrayColors[Int(arc4random_uniform(UInt32(self.arrayColors.count)))]
            }
            var arraySubChild = dictChild["is_sub"].arrayValue
            for j in 0..<arraySubChild.count
            {
                var dictSubChild = arraySubChild[j]
                dictSubChild["nodeColor"].stringValue = dictChild["nodeColor"].stringValue
                
                arraySubChild[j] = dictSubChild
                totalCount = totalCount + 1
            }
            dictChild["is_sub"] = JSON(arraySubChild)
            arrayChildNodes[i] = dictChild
            parentCount = parentCount + 1
            totalCount = totalCount + 1
        }
        
        dictTreeData["is_sub"] = JSON(arrayChildNodes)
        vwTree.subviews.forEach({ $0.removeFromSuperview() }) // Remove old view added

        //---------//
        root = nil
        root = setup(dict : dictTreeData)
        root?.startX = 0
        self.treeView = nil
        
        treeView = HierarchyScrollView(frame: self.vwTree.bounds, andWithRoot: root)
        
        // UpSideDown Fix
        treeView?.transform = CGAffineTransform(rotationAngle: .pi)
        treeView?.backgroundColor = UIColor.clear
        self.vwTree.addSubview(treeView!)
        self.vwTree.bringSubview(toFront: vwOptions)
        self.vwTree.layoutSubviews()
        self.vwTree.layoutIfNeeded()
        if(isCheckAppVersion() == 1)
        {
            treeView?.zoomScale = (treeView?.maximumZoomScale)!
        }
    }
    
    func showOptions()
    {
        vwOptions.alpha = 0
        vwOptions.fadeIn()
        vwOptions.tag = 0
    }
    
    func setUpDropDownData(dataArray : [JSON])
    {
        arrayParentNodesNames = []
        arrayParentNodes = []
        arrayParentNodes = dataArray
        let arrayRelations = dataArray
        for i in 0..<arrayRelations.count
        {
            let dict = arrayRelations[i]
            arrayParentNodesNames.append(decreptedString(string: dict["rname"].stringValue))
        }
        self.parentNodeDD.dataSource = arrayParentNodesNames
    }
    func addRelationStep1()
    {
        self.view.endEditing(true)
        vwAddRelation1.isHidden = false
        vwAddRelation2.isHidden = true
        vwAddRelation3.isHidden = true

    }
    func addRelationStep2()
    {
        self.view.endEditing(true)
        vwAddRelation1.isHidden = true
        vwAddRelation2.isHidden = false
        vwAddRelation3.isHidden = true

    }
    func addRelationStep3()
    {
        vwAddRelation1.isHidden = true
        vwAddRelation2.isHidden = true
        vwAddRelation3.isHidden = false
    }
    
}
//MARK:- Textfield Methods
extension ChooseRelationVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        print("Textfield change character called")
        return true
    }
    @objc func textFieldDidChange(_ textField: UITextField)
    {
        self.isNewNameAdded = true
    }
}
//MARK:- Check assessment PopUp
extension ChooseRelationVC : AssessmentCheckDelegate
{
    func btnOption1Selected(selectedOption: CheckOption)
    {
        if(selectedOption == .link)
        {
            selectedMenuIndex = 6
            let obj = objLinkAccountStoryboard.instantiateViewController(withIdentifier: "LinkAccountOrgViewController") as! LinkAccountOrgViewController
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            self.revealViewController().setFront(rearNavigation, animated: true)
            
        }else{
            SaveShareChangesService(type_val : "1")
        }
    }
    
    func btnOption2Selected(selectedOption: CheckOption)
    {
        if(selectedOption == .link)
        {
            self.getAssessments(dict: categoryDictData)
        }else{
            SaveShareChangesService(type_val : "0")
        }
    }
    
    func ShowCheckAssessmentOption(strMessage : String,isLink:Bool)
    {
        let obj = AssessmentCheckVC()
        obj.delegateAssessmentCheck = self
        obj.strMessage = strMessage
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        if(isLink)
        {
            obj.selectedOption = .link
        }else{
            obj.selectedOption = .share
        }
        
        self.present(obj, animated: false, completion: nil)
    }
}
//MARK:- Service
extension ChooseRelationVC 
{
    func getRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            vwTree.TreeFadeOut()
            print("selectedFilter - ",self.selectedFilter.rawValue)
            RouteMapService().GetTreeMapService(sort_by: self.selectedFilter.rawValue, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    let phase = json["phase"].intValue
                    Defaults.setValue(phase, forKeyPath: "appVersion")
                    Defaults.synchronize()
                    if(json["status"].stringValue == "1")
                    {
                        self.vwTree.subviews.forEach({ $0.removeFromSuperview() }) // Remove old view added
                        self.arrayRelation = json["data"].arrayValue
                        self.setUpRelationArray(jsonData : json["data"])
                        self.dictTreeData = json["data"]
                        self.vwTree.TreeFadeIn()
                        self.hideShowCreateRelationView()
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    if(self.selectedVC == .Sorting)
                    {
                        if(self.isLoadedFirstTime)
                        {
                            self.vwSorting.isHidden = true
                        }else{
                            self.vwSorting.isHidden = true
                        }
                        self.faviconSharewithStack.isHidden = true
                        self.treeView?.zoomScale = 0.2
                    }else{
                        self.vwSorting.isHidden = true
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getRelationServiceAfterSaveSharing()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            RouteMapService().GetTreeMapService(sort_by: self.selectedFilter.rawValue, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                     self.vwTree.subviews.forEach({ $0.removeFromSuperview() }) // Remove old view added
                        
                        self.arrayRelation = json["data"].arrayValue
                        self.setUpRelationArray(jsonData : json["data"])
                        self.dictTreeData = json["data"]
                        self.vwTree.TreeFadeIn()
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
        
    }
    func getSingleRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            RouteMapService().GetSingleRelatinoTreeMapService(relationId: decreptedString(string: dictRelationData["rid"].stringValue), completion:{ (result) in
                
                if let json = result.value
                {
                    print("single relation data response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.dictSelectedrelation = json["data"]
                        self.categoryDictData = json["data"]
                        if(self.isShowOptionPopup)
                        {
                            self.isShowOptionPopup = false
                            self.vwOptions.isHidden = false
                            
                            self.categoryDictData["rel_gender"].stringValue = decreptedString(string: self.categoryDictData["rel_gender"].stringValue)
                            self.categoryDictData["rname"].stringValue = decreptedString(string: self.categoryDictData["rname"].stringValue)
                            self.categoryDictData["default_name"].stringValue = decreptedString(string: self.categoryDictData["default_name"].stringValue)
                            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "showPopup"), object: self.categoryDictData)
                        }
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func getAssessments(dict:JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param : [String:String] = ["uid" : getUserDetail("user_id"), "rid" : "\(dict["rid"].stringValue)"]
            AssessmentService().getAssessmentQuestions(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        GlobalAssessmentDict = json["data"]
                        self.navigateToAssessmentSlider(categoryDict: self.categoryDictData)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToastWithCustomDelay(message: json["message"].stringValue, duration: 6.0)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func CheckAssessments(dict:JSON)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "rid" : "\(dict["rid"].stringValue)"
            ]
            AssessmentService().checkAssessmentQuestions(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        if(data["type"].stringValue == "link") //link
                        {
                            self.ShowCheckAssessmentOption(strMessage: data["messageForLinkSchol"].stringValue,isLink:true)
                        }else if(data["type"].stringValue == "all_ok")
                        {
                            self.getAssessments(dict: dict)
                        }
                        else if(data["type"].stringValue == "share") //Share
                        {
                            self.ShowCheckAssessmentOption(strMessage: data["messageForSharingData"].stringValue,isLink:false)
                        }
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func SaveShareChangesService(type_val : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "type" : "share",
                                           "rid" : categoryDictData["rid"].stringValue,
                                           "type_val" : type_val
            ]
            AssessmentService().SaveShareChangeService(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.getAssessments(dict: self.categoryDictData)
                        
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                //self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func addSubRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "pid" : categoryDictData["rid"].stringValue ,
                                           "rname" : txtRelation.text ?? "",
                                           "rel_type" : btnOption1.isSelected ? "1" : (btnOption2.isSelected ? "2" : (btnOption3.isSelected ? "3" : (btnOption4.isSelected ? "4" : "0"))),
                                           "created" : DateToString(Formatter: server_dt_format, date: Date()),
                                           "link_id" : strSelectedRealtionId,
                                           "timezone" : getCurrentTimeZone()]
            if(isNewNameAdded)
            {
                param["email"] = ""
                param["contact"] = ""
            }
            else if(!dictSelectedrelation["email"].stringValue.isEmpty)
            {
                param["email"] = dictSelectedrelation["email"].stringValue
            }
            else{
                param["email"] = ""
                param["contact"] = dictSelectedrelation["mobile"].stringValue
            }
            print("dictSelectedrelation - ",dictSelectedrelation)
            param["rel_gender"] = ""
            if(dictSelectedrelation["allowToSelectGender"].boolValue == false)
            {
                if(isNewNameAdded)
                {
                    if(!btnOption3.isSelected)
                    {
                        param["rel_gender"] = btnSelectGender1.isSelected ? "m" : (btnSelectGender2.isSelected ? "f" : (btnSelectGender3.isSelected ? "n" : ""))
                    }
                }
                else
                {
                    param["rel_gender"] = dictSelectedrelation["gender"].stringValue
                }
            }
            else
            {
                if(!btnOption3.isSelected)
                {
                    param["rel_gender"] = btnSelectGender1.isSelected ? "m" : (btnSelectGender2.isSelected ? "f" : (btnSelectGender3.isSelected ? "n" : ""))
                }
                
            }
            print("param - ",param)
            RouteMapService().AddSubRelationService(param:param, completion:{ (result) in
                if let json = result.value
                {
                    if(json["status"].stringValue == "1")
                    {
                        self.vwOptions.fadeOut()
                        self.vwRelation.fadeOut()
                        self.vwAlertAccessNow.fadeIn()
                        self.newAddedRelationData = json["data"]
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func editSubRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            var param : [String:String] = ["uid" : getUserDetail("user_id"),
                                           "rid" : categoryDictData["rid"].stringValue,
                                           "rname" : txtRelation.text ?? "",
                                           "rel_type" : btnOption1.isSelected ? "1" : (btnOption2.isSelected ? "2" : (btnOption3.isSelected ? "3" : (btnOption4.isSelected ? "4" : "0"))),
                                           "modified" : DateToString(Formatter: server_dt_format, date: Date()),
                                           "link_id" : strSelectedRealtionId,
                                           "timezone" : getCurrentTimeZone()]
            param["rel_gender"] = ""
            if(dictSelectedrelation["allowToSelectGender"].boolValue == false)
            {
                if(isNewNameAdded)
                {
                    if(!btnOption3.isSelected)
                    {
                        param["rel_gender"] = btnSelectGender1.isSelected ? "m" : (btnSelectGender2.isSelected ? "f" : (btnSelectGender3.isSelected ? "n" : ""))
                    }
                }
                else
                {
                    param["rel_gender"] = dictSelectedrelation["gender"].stringValue
                }
            }
            else
            {
                if(!btnOption3.isSelected)
                {
                    param["rel_gender"] = btnSelectGender1.isSelected ? "m" : (btnSelectGender2.isSelected ? "f" : (btnSelectGender3.isSelected ? "n" : ""))
                }
            }
            
            if(isNewNameAdded)
            {
                param["email"] = ""
                param["contact"] = ""
            }
            else if(!dictSelectedrelation["email"].stringValue.isEmpty)
            {
                param["email"] = dictSelectedrelation["email"].stringValue
                param["contact"] = ""
            }
            else{
                param["email"] = ""
                param["contact"] = dictSelectedrelation["mobile"].stringValue
            }
            print("param - ",param)
            RouteMapService().EditSubRelationService(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.vwOptions.fadeOut()
                        self.vwRelation.fadeOut()
                        self.vwEditRelationship.fadeOut()
                        self.resetValues()
                        self.getRelationService()
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func removeSubRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let relationId = categoryDictData["rid"].stringValue
            RouteMapService().DeleteSubRelationService(strRelationId:relationId, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.vwOptions.fadeOut()
                        self.vwEditRelationship.fadeOut()
                        self.getRelationService()
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func shareWithTutorService(isShared : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let relationId = categoryDictData["rid"].stringValue
            RouteMapService().shareWithTutorService(strRelationId:relationId,isShared : isShared, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.vwOptions.fadeOut()
                        self.getRelationService()
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func getParentRelations()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let relationId = categoryDictData["rid"].stringValue
            RouteMapService().getParentNodesService(strRelationId:relationId, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.setUpDropDownData(dataArray : json["data"].arrayValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func moveRelationService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let sourceId = categoryDictData["rid"].stringValue
            let destinationId = strSelectedParentNode
            
            RouteMapService().moveToNodeService(sourceId:sourceId,destinationId:destinationId, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        self.vwOptions.fadeOut()
                        self.vwMoveRelation.fadeOut()
                        self.getRelationService()
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func GetNameSuggestionsService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            RelationsService().GetSuggestedRelationsService(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let dict = json["data"]
                        let arrayData = dict["relation"].arrayValue
                        //API names
                        for i in 0..<arrayData.count
                        {
                            var dictRelation = arrayData[i]
                            dictRelation["isFromContact"].stringValue = "0"
                            dictRelation["email"].stringValue = dictRelation["username"].stringValue
                            dictRelation["mobile"].stringValue = ""
                            self.arraySuggestionRelations.append(dictRelation)
                            self.arraySuggestionRelationsMainArray.append(dictRelation)
                        }
                        print("arraySuggestionRelations - ",self.arraySuggestionRelations)
                        self.arrayRelationsAutoCompleteMainArray = []
                        for i in 0..<self.arraySuggestionRelations.count
                        {
                            let suggesstionNameDict = self.arraySuggestionRelations[i]
                            self.arrayRelationsAutoCompleteMainArray.append(suggesstionNameDict["name"].stringValue)
                        }
                        self.setUpRelationsArray()
                        
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func PhaseChangeSeenService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let kURL = kApplicationBaseURL + R.string.webURL.kSeenPhaseChangeURL()
            var dictParam = [String:String]()
            dictParam["uid"] = encryptString(string: getUserDetail("user_id"))
            dictParam["device_id"] = getFirebaseToken()
            dictParam["timezone"] = getCurrentTimeZone()
            dictParam["timestamp"] = DateToString(Formatter: server_dt_format, date: Date())
            print("Phase chang seen - ",kURL)
            RouteMapService().PhaseChangeService(strUrl: kURL,param: dictParam, completion: { (result) in
            
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func saveEmailSettingNotification(switchValue : String,isOnValue : String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            SettingsService().saveSettingsService(isOn: isOnValue, switchValue: switchValue, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.vwAlertAccessNow.fadeOut()
                self.resetValues()
                self.getRelationService()
                self.hideLoader()
            })
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

