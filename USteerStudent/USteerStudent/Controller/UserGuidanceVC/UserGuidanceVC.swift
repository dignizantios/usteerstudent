//
//  UserGuidanceVC.swift
//  USteerStudent
//
//  Created by Usteer on 7/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import AVFoundation
import AVKit
import XCDYouTubeKit
import SDWebImage

class GuidanceCollectionviewCell : UICollectionViewCell
{
    //MARK: - Outlets
    @IBOutlet var vwVideoPlayer: WKYTPlayerView!
    @IBOutlet weak var btnPlayOutlet: UIButton!
    @IBOutlet weak var imgPlaceholder: UIImageView!

    var strStreamingURL = ""
    
    override func awakeFromNib() {
        self.vwVideoPlayer.backgroundColor = UIColor.clear
    }
}

class GuidanceFactorCollectionviewCell : UICollectionViewCell
{
    //MARK: - Outlets
    @IBOutlet var vwVideoPlayer1: WKYTPlayerView!
    @IBOutlet var vwVideoPlayer2: WKYTPlayerView!
    @IBOutlet var vwVideoPlayer3: WKYTPlayerView!
    @IBOutlet var vwVideoPlayer4: WKYTPlayerView!
    
    @IBOutlet var lblfactor1: UILabel!
    @IBOutlet var lblfactor2: UILabel!
    @IBOutlet var lblfactor3: UILabel!
    @IBOutlet var lblfactor4: UILabel!
    
    @IBOutlet var btnSD: UIButton!
    @IBOutlet var btnTOS: UIButton!
    @IBOutlet var btnTOO: UIButton!
    @IBOutlet var btnSC: UIButton!
    
    weak var weakPlayerViewController: AVPlayerViewController?
    
    @IBOutlet weak var imgPlaceholder1: UIImageView!
    @IBOutlet weak var imgPlaceholder2: UIImageView!
    @IBOutlet weak var imgPlaceholder3: UIImageView!
    @IBOutlet weak var imgPlaceholder4: UIImageView!

    override func awakeFromNib() {
        self.vwVideoPlayer1.backgroundColor = UIColor.clear
        self.vwVideoPlayer2.backgroundColor = UIColor.clear
        self.vwVideoPlayer3.backgroundColor = UIColor.clear
        self.vwVideoPlayer4.backgroundColor = UIColor.clear

        [self.lblfactor1,self.lblfactor2,self.lblfactor3,self.lblfactor4].forEach({ (label) in
            label?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
            label?.textColor = MySingleton.sharedManager.themeDarkGrayColor
        })
    }
}


class UserGuidanceVC: UIViewController
{
    //MARK: - outlets
    @IBOutlet var scrollview : UIScrollView!

    @IBOutlet var collectionviewGuidance : UICollectionView!
    @IBOutlet var pageControlGuidance : UIPageControl!
    @IBOutlet var lblTitle : UILabel!

    @IBOutlet var vwInfo: UIView!
    @IBOutlet var vwInfoBack: UIView!
    @IBOutlet var lblInfo: UILabel!
    
    //MARK: - variables
    var count = 0
    var lastContentOffset: CGPoint = .zero
//    var arrayTitle = ["HOW USTEER", "WHAT USTEER", "WHY USTEER"]  Changes to Below
    var arrayTitle = ["Let's go! Steer life better...", "The Four Steering Skills", "Steering to the future"]
    var arrayURL : [String] = []
    var arrayFactorURL : [String] = []
    var selectedParentVC = checkParentController.userGuidance
    var arrayVideosURL : [JSON] = []

    //MARK:- Variables
    var player: AVPlayer!
    var avpController = AVPlayerViewController()
    
    var player2: AVPlayer!
    var avpController2 = AVPlayerViewController()
    
    var player3: AVPlayer!
    var avpController3 = AVPlayerViewController()
    
    var player4: AVPlayer!
    var avpController4 = AVPlayerViewController()
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getUserGuidanceDataService()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setUpUI()
        self.scrollview.addSubview(self.refreshControl)
        getUserGuidanceDataService()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        hideShowGuidanceInfoView()
        
    }
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Training_key"))
        lblTitle.text = arrayTitle[count]
        
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        lblInfo.textColor = UIColor.white
        
        let tapOptions = UITapGestureRecognizer(target: self, action: #selector(self.hideView))
        vwInfoBack.addGestureRecognizer(tapOptions)
        hideInfoView()
        
        if(isLinkSchoolAccount)
        {
            isLinkSchoolAccount = false
            getAppVersion()
        }
    }
    
    func hideShowGuidanceInfoView()
    {
        if (Defaults.value(forKey: "showUserGuidanceCount") == nil)
        {
            Defaults.setValue(1, forKey: "showUserGuidanceCount")
            vwInfo.isHidden = false
        }else{
            
            var count = Defaults.value(forKey: "showUserGuidanceCount") as? Int
            if(count! < 6)
            {
                count = count! + 1
                Defaults.setValue(count, forKey: "showUserGuidanceCount")
                vwInfo.isHidden = false
            }else{
                vwInfo.isHidden = true
            }
        }
    }
    
    func showInfoView()
    {
        vwInfo.isHidden = false
    }
    
    @IBAction func clk_Forward()
    {
        let pageNumber = Int(round(collectionviewGuidance.contentOffset.x / collectionviewGuidance.frame.size.width))
        if pageNumber == 2 {
            return
        }
        self.scrollToPage(page: pageNumber+1, animated: true)
    }
    
    @IBAction func clk_Backward()
    {
        let pageNumber = Int(round(collectionviewGuidance.contentOffset.x / collectionviewGuidance.frame.size.width))
        if pageNumber == 0 {
            return
        }
        self.scrollToPage(page: pageNumber-1, animated: true)
    }
}
//MARK: - Geture method
extension UserGuidanceVC
{
    @objc func hideView(sender: UITapGestureRecognizer? = nil)
    {
        hideInfoView()
    }
    func hideInfoView()
    {
        vwInfo.isHidden = true
    }
}
//MARK:- Scrollview Methods
extension UserGuidanceVC
{
    func scrollToPage(page: Int, animated: Bool) {
        var frame: CGRect = self.collectionviewGuidance.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.collectionviewGuidance.scrollRectToVisible(frame, animated: animated)
        pageControlGuidance.currentPage = page
        lblTitle.text = arrayTitle[page]
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if(scrollView == collectionviewGuidance)
        {
            let pageNumber = Int(round(collectionviewGuidance.contentOffset.x / collectionviewGuidance.frame.size.width))
            pageControlGuidance.currentPage = Int(pageNumber)
            lblTitle.text = arrayTitle[pageNumber]
            self.lastContentOffset.x = scrollView.contentOffset.x
        }
    }
}
//MARK: - Player delegate
extension UserGuidanceVC : WKYTPlayerViewDelegate
{
    func playerView(_ playerView: WKYTPlayerView, didChangeTo state: WKYTPlayerState) {
        print("state - ",state.rawValue)
        if(state.rawValue == 1)
        {
            playerView.stopVideo()
        }
    }
}
//MARK:- Collectionview Datasource Methods
extension UserGuidanceVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return arrayVideosURL.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let dict = arrayVideosURL[indexPath.row]
//        let dict = arrayVideosURL.filter { (object) -> Bool in
//            if(object["title"].stringValue.lowercased() == title.lowercased())
//            {
//                return true
//            }
//            return false
//        }.first
        
        if(dict["hasSubLinks"].boolValue == true)
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "factorCell", for: indexPath as IndexPath) as! GuidanceFactorCollectionviewCell
            cell.vwVideoPlayer1.delegate = self
            cell.vwVideoPlayer2.delegate = self
            cell.vwVideoPlayer3.delegate = self
            cell.vwVideoPlayer4.delegate = self
            let subArray = dict["subLinksURL"].arrayValue
            for i in 0..<subArray.count
            {
                let dictSubURL = subArray[i]
                if(i == 0)
                {
                    cell.lblfactor1.text = dictSubURL["factor_name"].stringValue
                    [cell.btnSD,cell.btnTOS,cell.btnTOO,cell.btnSC].forEach({ (button) in
                        button?.tag = indexPath.row
                    })
                    cell.btnSD.setTitle("0", for: .disabled)
                    cell.btnTOS.setTitle("1", for: .disabled)
                    cell.btnTOO.setTitle("2", for: .disabled)
                    cell.btnSC.setTitle("3", for: .disabled)
                    if(isCheckUserLocalChina())
                    {
                        cell.imgPlaceholder1.sd_setImage(with: dictSubURL["server_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }else{
                        cell.imgPlaceholder1.sd_setImage(with: dictSubURL["global_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }
                }
                else if(i == 1)
                {
                    cell.lblfactor2.text = dictSubURL["factor_name"].stringValue
                    if(isCheckUserLocalChina())
                    {
                        cell.imgPlaceholder2.sd_setImage(with: dictSubURL["server_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }else{
                        cell.imgPlaceholder2.sd_setImage(with: dictSubURL["global_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }
                    
                }
                else if(i == 2)
                {
                    cell.lblfactor3.text = dictSubURL["factor_name"].stringValue
                    if(isCheckUserLocalChina())
                    {
                        cell.imgPlaceholder3.sd_setImage(with: dictSubURL["server_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }else{
                        cell.imgPlaceholder3.sd_setImage(with: dictSubURL["global_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }
                }
                else
                {
                    cell.lblfactor4.text = dictSubURL["factor_name"].stringValue
                    if(isCheckUserLocalChina())
                    {
                        cell.imgPlaceholder4.sd_setImage(with: dictSubURL["server_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }else{
                        
                        cell.imgPlaceholder4.sd_setImage(with: dictSubURL["global_thumbnail"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
                    }
                }
                
            }
            return cell
        }
        else
        {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GuidanceCollectionviewCell", for: indexPath as IndexPath) as! GuidanceCollectionviewCell
            cell.vwVideoPlayer.delegate = self
            cell.btnPlayOutlet.tag = indexPath.row
            cell.imgPlaceholder.sd_setImage(with: dict["thumbnail_url"].url, placeholderImage: UIImage(named: "video_placeholder_big"), options: .lowPriority, completed: nil)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: collectionView.frame.size.width , height:collectionView.frame.size.height)
    }
}

//MARK:- Setup Video UI

extension UserGuidanceVC
{
    func playVideoInView(strVideoURL:String)
    {
        let playerViewController = AVPlayerViewController()
        playerViewController.view.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height:self.view.frame.height)
        weak var weakPlayerViewController: AVPlayerViewController? = playerViewController
        XCDYouTubeClient.default().getVideoWithIdentifier(strVideoURL) { (video, error) in
            if video != nil
            {
                let streamURLs = video?.streamURLs
                let streamURL = streamURLs?[XCDYouTubeVideoQualityHTTPLiveStreaming] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.HD720.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.medium360.rawValue)] ?? streamURLs?[NSNumber(value: XCDYouTubeVideoQuality.small240.rawValue)]
                if let streamURL = streamURL {
                    weakPlayerViewController?.player = AVPlayer(url: streamURL)
                    weakPlayerViewController?.player?.play()
                }
            }
        }

       self.present(weakPlayerViewController!, animated: true, completion: nil)
    }
    
    @IBAction func btnVideoPlayAction(_ sender:UIButton)
    {
        let tag = sender.tag
        let dict = arrayVideosURL[sender.tag]
        let index = Int(sender.title(for: .disabled) ?? "0")
        if(dict["hasSubLinks"].boolValue)
        {
            let dict = arrayVideosURL[tag]
            let subArray = dict["subLinksURL"].arrayValue
            if index == 0 {
                let dictSubURL = subArray[0]
                if isCheckUserLocalChina() {
                    playVideo(strVideoURL: dictSubURL["factorURL"].stringValue)
                } else {
                    playVideoInView(strVideoURL: dictSubURL["factorURL"].stringValue)
                }
            } else if index == 1 {
                let dictSubURL = subArray[1]
                if isCheckUserLocalChina() {
                    playVideo(strVideoURL: dictSubURL["factorURL"].stringValue)
                } else {
                    playVideoInView(strVideoURL: dictSubURL["factorURL"].stringValue)
                }
            } else if index == 2 {
                let dictSubURL = subArray[2]
                if isCheckUserLocalChina() {
                    playVideo(strVideoURL: dictSubURL["factorURL"].stringValue)
                } else {
                    playVideoInView(strVideoURL: dictSubURL["factorURL"].stringValue)
                }
            } else if index == 3 {
                let dictSubURL = subArray[3]
                if isCheckUserLocalChina() {
                    playVideo(strVideoURL: dictSubURL["factorURL"].stringValue)
                } else {
                    playVideoInView(strVideoURL: dictSubURL["factorURL"].stringValue)
                }
            }
        }else{
            let dict = arrayVideosURL[tag]
            if isCheckUserLocalChina() {
                playVideo(strVideoURL: dict["linkURL"].stringValue)
            } else {
                playVideoInView(strVideoURL: dict["linkURL"].stringValue)
            }
        }
    }
}

//MARK: - Other methods
extension UserGuidanceVC
{
    func playVideoView4(strVideoURL : String)
    {
        if(strVideoURL == "")
        {
            return
        }
        let strVideo = strVideoURL
        let url = URL(string:strVideo)
        player4 = AVPlayer(url: url!)
        avpController4 = AVPlayerViewController()
        avpController4.player = player4
        avpController4.view.frame.size.height = view.frame.size.height
        avpController4.view.frame.size.width = view.frame.size.width
        self.present(avpController4, animated: true, completion: nil)
    }
    
    func playVideoView3(strVideoURL : String,view:UIView)
    {
        if(strVideoURL == "")
        {
            return
        }
        let strVideo = strVideoURL
        let url = URL(string:strVideo)
        player3 = AVPlayer(url: url!)
        avpController3 = AVPlayerViewController()
        avpController3.player = player3
        avpController3.view.frame.size.height = view.frame.size.height
        avpController3.view.frame.size.width = view.frame.size.width
        self.present(avpController3, animated: true, completion: nil)
    }
    
    func playVideoView2(strVideoURL : String,view:UIView)
    {
        if(strVideoURL == "")
        {
            return
        }
        let strVideo = strVideoURL
        let url = URL(string:strVideo)
        player2 = AVPlayer(url: url!)
        avpController2 = AVPlayerViewController()
        avpController2.player = player2
        avpController2.view.frame.size.height = view.frame.size.height
        avpController2.view.frame.size.width = view.frame.size.width
        self.present(avpController2, animated: true, completion: nil)
    }
    
    func playVideo(strVideoURL : String)
    {
        if(strVideoURL == "")
        {
            return
        }
        let strVideo = strVideoURL
        let url = URL(string:strVideo)
        player = AVPlayer(url: url!)
        avpController = AVPlayerViewController()
        avpController.player = player
        avpController.view.frame.size.height = view.frame.size.height
        avpController.view.frame.size.width = view.frame.size.width
        avpController.player?.play()
        self.present(avpController, animated: true, completion: nil)
    }
    
    func setUpData(data : JSON)
    {
        ///"HOW USTEER"
        var json = JSON()
        json["title"] = JSON(arrayTitle[0])
        if(isCheckUserLocalChina())
        {
            json["linkURL"] = data["server_how_usteer"]
            json["thumbnail_url"] = data["server_how_usteer_thumbnail"]
        }else{
            json["linkURL"] = JSON(data["how_usteer"].stringValue.youtubeID ?? "")
            json["thumbnail_url"] = data["global_how_usteer_thumbnail"]
        }
        json["subLinksURL"] = []
        json["hasSubLinks"] = false
        arrayVideosURL.append(json)
        
        ///"WHAT USTEER"
        json = JSON()
        json["title"] = JSON(arrayTitle[1])
        json["linkURL"] = ""
        json["hasSubLinks"] = true
        var arraySubArray : [JSON] = []
        let arrayWhatURL = data["what_usteer"].arrayValue
        for i in 0..<arrayWhatURL.count
        {
            let dict = arrayWhatURL[i]
            var subJson = JSON()
            subJson["factor_name"] = JSON(dict["factor_name"].stringValue)
            
            if(isCheckUserLocalChina())
            {
                subJson["factorURL"] = dict["server_video"]
                subJson["server_thumbnail"] = dict["server_thumbnail"]
            }else{
                subJson["factorURL"] = JSON(self.getVideoString(strStreamingURL: dict["video_link"].stringValue.youtubeID ?? ""))
                subJson["global_thumbnail"] = dict["global_thumbnail"]
            }
            
            arraySubArray.append(subJson)
        }
        json["subLinksURL"] = JSON(arraySubArray)
        arrayVideosURL.append(json)

        ///"WHY USTEER"
        json = JSON()
        json["title"] = JSON(arrayTitle[2])
        if(isCheckUserLocalChina())
        {
            json["linkURL"] = data["server_why_usteer"]
            json["thumbnail_url"] = data["server_why_usteer_thumbnail"]
        }else{
            json["linkURL"] = JSON(data["why_usteer"].stringValue.youtubeID ?? "")
            json["thumbnail_url"] = data["global_why_usteer_thumbnail"]
        }
        json["subLinksURL"] = []
        json["hasSubLinks"] = false
        arrayVideosURL.append(json)
        
        self.collectionviewGuidance.reloadData()
    }
}

//MARK: - Service
extension UserGuidanceVC
{
    func getVideoString(strStreamingURL : String) -> String
    {
        let arrayContents = strStreamingURL.split(separator: "/")
        if(arrayContents.count > 0)
        {
            print("Link end - ",String(arrayContents[arrayContents.count-1]))
            return String(arrayContents[arrayContents.count-1])
        }else{
            return ""
        }
    }
    func getUserGuidanceDataService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            BasicTaskService().GetUserGuidanceService(completion:{ (result) in
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        let data = json["data"]
                        self.setUpData(data : data)
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}


