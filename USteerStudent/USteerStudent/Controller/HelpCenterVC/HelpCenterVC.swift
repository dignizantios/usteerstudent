//
//  HelpCenterVC.swift
//  USteerStudent
//
//  Created by Usteer on 31/12/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import ZendeskSDK
import ZendeskCoreSDK
import ZendeskProviderSDK

class HelpCenterVC: UIViewController
{
    //MARK: - Outlets
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var lblSubTitle : UILabel!
    
    @IBOutlet var btnHelpCenter : UIButton!
    
    //MARK: - variables
    
    var requestConfig: RequestUiConfiguration {
        let config = RequestUiConfiguration()
        config.subject = "USteer Pupil Support"
        config.tags = ["tag_one", "tag_two"]
        
        return config
    }
    var hcConfig: HelpCenterUiConfiguration {
        let hcConfig = HelpCenterUiConfiguration()
        hcConfig.showContactOptions = false
        hcConfig.labels = ["label"] // only hc articles with the label 'label' will appear
        return hcConfig
    }
    
    
    //MARK: - Life cycle methods

    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUiSetUP()
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    
}
//MARK: - UI setup
extension HelpCenterVC
{
    func setUiSetUP()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Support_key"))
        lblTitle.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 16)
        lblSubTitle.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        [lblTitle,lblSubTitle].forEach { (label) in
            label?.textColor = UIColor.black
        }
        btnHelpCenter.backgroundColor = MySingleton.sharedManager.themeYellowColor
    }
}
//MARK:- Zendesk
extension HelpCenterVC
{
    func setUpticket()
    {
        setUpTheme()
        let helpCenter = ZDKHelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [requestConfig])
        self.navigationController?.pushViewController(helpCenter, animated: true)
    }
    func setUpTheme()
    {
        
        
    }
}
//MARK:- Button Action
extension HelpCenterVC
{
    @IBAction func btnSupportAction(_ sender : UIButton)
    {
        Zendesk.instance?.setIdentity(Identity.createAnonymous(name: getUserDetail("fullname"), email: getUserDetail("username")))
        let navbarAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
        let helpCenter = ZDKHelpCenterUi.buildHelpCenterOverviewUi(withConfigs: [requestConfig])
        self.navigationController?.navigationBar.titleTextAttributes = navbarAttributes
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.barTintColor = MySingleton.sharedManager.themeYellowColor
        self.navigationController?.pushViewController(helpCenter, animated: true)
    }
}
