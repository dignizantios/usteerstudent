//
//  NotificationVC.swift
//  USteerStudent
//
//  Created by Usteer on 2/14/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON
import Toaster

class NotificationTableviewCell : UITableViewCell,MDHTMLLabelDelegate
{
    //MARK: - Outlets
    @IBOutlet weak var lblMessage: MDHTMLLabel!
    @IBOutlet weak var vwContent: UIView!
    @IBOutlet weak var imgvwIcon: UIImageView!
    @IBOutlet weak var constraintsWidth: NSLayoutConstraint!
    @IBOutlet var vwHighlight: UIView!
    
    //MARK: -
    override func awakeFromNib()
    {
        lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblMessage.textColor = UIColor.white
        lblMessage.autoDetectUrls = true
        lblMessage.delegate = self
        lblMessage.linkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.darkGray,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: lblMessage.font.pointSize),
            NSAttributedString.Key.underlineStyle: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue)
        ]
        lblMessage.activeLinkAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.red,
            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: lblMessage.font.pointSize),
            NSAttributedString.Key.underlineStyle: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue)
        ]
        vwContent.layer.cornerRadius = 10
        vwContent.layer.masksToBounds = true
    }
    
    func htmlLabel(_ label: MDHTMLLabel!, didSelectLinkWith URL: URL!) {
        print(URL.absoluteString)
        if UIApplication.shared.canOpenURL(URL) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            } else {
                UIApplication.shared.openURL(URL)
            }
        }
    }
}
class NotificationVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet weak var tblNotification: UITableView!
    @IBOutlet var vwDelete : UIView!
    @IBOutlet var lblSelectedCount : UILabel!
    @IBOutlet weak var viewHeightConstant: NSLayoutConstraint!
    @IBOutlet var btnCancel : UIButton!
    
    //MARK: - Variables
    var strErrorMessage = ""
    
    var arrayContactsImage : [UIImage] = []
    var arrayContactsNumber : [String] = []
    var offset = 0
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    var isClickOnLongPress = false
    var isClickOnDelete = false
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.hideDeleteView()
        isClickOnLongPress = false
        offset = 0
        self.getNotificationList()
        refreshControl.endRefreshing()
    }
    
    //MARK: - Life cycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tblNotification.addSubview(self.refreshControl)
        self.tblNotification.tableFooterView = UIView()
        setUpUI()
//        setupTapPress()
        setupLongPress()
        //getContactsNameList()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI Setup
extension NotificationVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle : getCommonString(key: "Social_assistant_key"))
        tblNotification.tableFooterView = UIView()
        
        if(isLoadNotificationfirstTime)
        {
            presentInfoWindow()
            isLoadNotificationfirstTime = false
        }
        getNotificationList()
        btnCancel.setTitle(getCommonString(key: "Cancel_key").uppercased(), for: .normal)
        btnCancel.setTitleColor(UIColor.white, for: .normal)
        btnCancel.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        
        vwDelete.backgroundColor = MySingleton.sharedManager.themeYellowColor
        vwDelete.isHidden = true
        self.viewHeightConstant.constant = 0
    }
    
    func presentInfoWindow()
    {
        let strMessage = "Heh \(getUserDetail("fullname")), my job is to help you achieve your steering goals. I'll post reminders and nudges for signposts which you have set yourself"
        let obj = InfoPopUpVC()
        obj.popUpDelegate = self
        obj.strMessage = strMessage
        obj.modalPresentationStyle = .overCurrentContext
        obj.modalTransitionStyle = .coverVertical
        self.present(obj, animated: false, completion: nil)
    }
    
    @objc func btnDeleteAllAction() {
        let alert = UIAlertController(title: kCommonAlertTitle, message: "Are you sure you want to delete all notification?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            self.deleteAllNotificationService()
        }))
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
//MARK: - Pop up delegate
extension NotificationVC : InfoPopUpDelegate
{
    func dismissPopUp()
    {
        
    }
}

//MARK: - Geture Methods
extension NotificationVC
{
    @objc func longPressedEvent(_ sender: UILongPressGestureRecognizer)
    {
        print("longpressed")
        print("sender tag - ",(sender.view?.tag)!)
        var dict = arrayNotification[(sender.view?.tag)!]
        
        if(dict["isSelected"].stringValue == "1")
        {
            dict["isSelected"].stringValue = "0"
        }else{
            dict["isSelected"].stringValue = "1"
        }
        
        arrayNotification[(sender.view?.tag)!] = dict
        
        
        //print("arrayChatList - ",arrayChatList)
        //        tblChatMessage.reloadRows(at: [IndexPath(row: sender.view?.tag)!, section: 0)], with: .none)
        //        tblChatMessage.reloadData()
        
        let indexPath = IndexPath(item: (sender.view?.tag)!, section: 0)
        tblNotification.reloadRows(at: [indexPath], with: .none)
        
        var selectedCount = 0
        if arrayNotification.contains(where: { (json) -> Bool in
            if(json["isSelected"].stringValue == "1")
            {
                return true
            }
            return false
        }){
            showDeleteView()
        }else{
            hideDeleteView()
        }
        
        for i in 0..<arrayNotification.count
        {
            let dict = arrayNotification[i]
            if(dict["isSelected"].stringValue == "1")
            {
                selectedCount = selectedCount + 1
            }
        }
        
        print("selectedCount - ",selectedCount)
        
        lblSelectedCount.text = "\(selectedCount) Selected"
        
    }
    
    func showDeleteView()
    {
        self.view.endEditing(true)
        UIView.animate(withDuration: 0.5, animations: {
            self.viewHeightConstant.constant = 60
            self.view.layoutIfNeeded()
        }, completion: {(result) -> Void in
            self.vwDelete.isHidden = false
        })
        
//        hideSendMessageView()
        
    }
    func hideDeleteView()
    {
        isClickOnLongPress = false
        for i in 0..<arrayNotification.count
        {
            var dict = arrayNotification[i]
            dict["isSelected"].stringValue = "0"
            arrayNotification[i] = dict
        }
        self.tblNotification.reloadData()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.viewHeightConstant.constant = 0
            self.view.layoutIfNeeded()
        }, completion: {(result) -> Void in
            self.vwDelete.isHidden = true
        })
//        showSendMessageView()
    }
}

//MARK: - UITableview Delegate
extension NotificationVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayNotification.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            lbl.numberOfLines = 3
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayNotification.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:NotificationTableviewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NotificationTableviewCell
        cell.selectionStyle = .none
        
        let dict = arrayNotification[indexPath.row]
        if(dict["is_read"].stringValue == "0")
        {
            cell.lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedBoldWithSize(fontSize: 14)
        } else {
             cell.lblMessage.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        }
        let detailDict = dict["detail"]
        let color = hexStringToUIColor(hex: detailDict["color"].stringValue)
        cell.vwContent.backgroundColor = color
        cell.constraintsWidth.constant = 44
        if(dict["notification_icon"].stringValue == "1")
        {
            cell.imgvwIcon.image = #imageLiteral(resourceName: "user_image_chat_details")
        }
        else if(dict["notification_icon"].stringValue == "2")
        {
            cell.imgvwIcon.image = #imageLiteral(resourceName: "user_image_chat_details")
        }
        else if(dict["notification_icon"].stringValue == "3")
        {
            cell.imgvwIcon.image = #imageLiteral(resourceName: "group_placeholder")
        }
        else if(dict["notification_icon"].stringValue == "4"){
            cell.imgvwIcon.image = #imageLiteral(resourceName: "ic_event_new")
        }
        cell.lblMessage.hilightText = dict["rname"].stringValue
        cell.lblMessage.htmlText = dict["msg"].stringValue
        if(dict["isSelected"].stringValue == "1")
        {
            cell.vwHighlight.isHidden = false
        }else{
            cell.vwHighlight.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if isClickOnLongPress == true{
            longPressSelection(indexPath:indexPath)
            return
        }
        var dict = arrayNotification[indexPath.row]
        
//        deleteNotificationService(dict:dict,indexPath:indexPath)
        
        print("dict - ",dict)
        dict["is_read"] = "1"
        arrayNotification[indexPath.row] = dict
        self.tblNotification.reloadData()
        let detailDict = dict["detail"]
        
        if(dict["type"].stringValue == "1")
        {
            let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if(dict["type"].stringValue == "2")
        {
            
            var relatedDict = detailDict["rel_data"]
            let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
             relatedDict["group_id"].stringValue = decreptedString(string: relatedDict["group_id"].stringValue)
            relatedDict["fullname"].stringValue = decreptedString(string: relatedDict["fullname"].stringValue)
            
            obj.dictChatData = relatedDict
            obj.checkChatParentVC = .notificaion
//            obj.isFromPush = isFromPushLocal
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if(dict["type"].stringValue == "3")
        {
            var json = JSON()
            json["rname"].stringValue = dict["rname"].stringValue
            json["rid"].stringValue = dict["rid"].stringValue

            let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
            obj.selectedVC = .socialAssistant
            obj.dictRelationData = json
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            selectedMenuIndex = 0
            self.revealViewController().setFront(rearNavigation, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete)
        {
            let dict = arrayNotification[indexPath.row]
            deleteNotificationService(dict:dict,indexPath:indexPath)
        }
        
    }
}

extension NotificationVC : UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func setupLongPress() {
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
        longPressGesture.minimumPressDuration = 1 // 1 second press
        longPressGesture.delegate = self
        longPressGesture.cancelsTouchesInView = false
        self.tblNotification.addGestureRecognizer(longPressGesture)
    }
    
    func setupTapPress() {
        
        let singlePressGesture = UITapGestureRecognizer(target: self, action: #selector(tapPress))
        singlePressGesture.delegate = self
        singlePressGesture.cancelsTouchesInView = false
        self.tblNotification.addGestureRecognizer(singlePressGesture)
    }
    
    @objc func tapPress(gesture: UIGestureRecognizer) {
        
        if gesture.state == .ended {
            let touchPoint = gesture.location(in: self.tblNotification)
            if let indexPath = tblNotification.indexPathForRow(at: touchPoint) {
                // do your task on single tap
                if isClickOnLongPress == true{
                    longPressSelection(indexPath:indexPath)
                    return
                }
                print("indexPath for tap \(indexPath)")
                tablviewDidSelect(indexPath: indexPath)
            }
        }
    }
    
    @objc func longPress(longPressGestureRecognizer: UILongPressGestureRecognizer) {
        
        if longPressGestureRecognizer.state == UIGestureRecognizerState.began {
            
            let touchPoint = longPressGestureRecognizer.location(in: self.tblNotification)
            if let indexPath = tblNotification.indexPathForRow(at: touchPoint) {
//                self.showActionSheet()
                isClickOnLongPress = true
                print("indexPath for longpress \(indexPath)")
                longPressSelection(indexPath: indexPath)
            }
        }
    }
    
    func tablviewDidSelect(indexPath:IndexPath){
        
        if isClickOnLongPress == true{
            longPressSelection(indexPath:indexPath)
            return
        }
        let dict = arrayNotification[indexPath.row]
        
//        deleteNotificationService(dict:dict,indexPath:indexPath)
        
        print("dict - ",dict)
        let detailDict = dict["detail"]
        
        if(dict["type"].stringValue == "1")
        {
            let obj = objCoachStoryboard.instantiateViewController(withIdentifier: "CoachVC") as! CoachVC
            self.navigationController?.pushViewController(obj, animated: true)
        }else if(dict["type"].stringValue == "2")
        {
            
            let relatedDict = detailDict["rel_data"]
            let obj = objChatsStoryboard.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
            obj.dictChatData = relatedDict
            obj.checkChatParentVC = .notificaion
            //            obj.isFromPush = isFromPushLocal
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if(dict["type"].stringValue == "3")
        {
            var json = JSON()
            json["rname"].stringValue = dict["rname"].stringValue
            json["rid"].stringValue = dict["rid"].stringValue
            
            let obj = objRelationStoryboard.instantiateViewController(withIdentifier: "ChooseRelationVC") as! ChooseRelationVC
            obj.selectedVC = .socialAssistant
            obj.dictRelationData = json
            let rearNavigation = UINavigationController(rootViewController: obj)
            rearNavigation.isNavigationBarHidden = false
            selectedMenuIndex = 0
            self.revealViewController().setFront(rearNavigation, animated: true)
        }
    }
    
    func longPressSelection(indexPath:IndexPath){
        var dict = arrayNotification[indexPath.row]
        
        if(dict["isSelected"].stringValue == "1")
        {
            dict["isSelected"].stringValue = "0"
        }else{
            dict["isSelected"].stringValue = "1"
        }
        
        arrayNotification[indexPath.row] = dict
        
        
        //print("arrayChatList - ",arrayChatList)
        //        tblChatMessage.reloadRows(at: [IndexPath(row: sender.view?.tag)!, section: 0)], with: .none)
        //        tblChatMessage.reloadData()
        
//        let indexPath = IndexPath(item: (sender.view?.tag)!, section: 0)
        tblNotification.reloadRows(at: [indexPath], with: .none)
        
        var selectedCount = 0
        if arrayNotification.contains(where: { (json) -> Bool in
            if(json["isSelected"].stringValue == "1")
            {
                return true
            }
            return false
        }){
            showDeleteView()
        }else{
            hideDeleteView()
        }
        
        for i in 0..<arrayNotification.count
        {
            let dict = arrayNotification[i]
            if(dict["isSelected"].stringValue == "1")
            {
                selectedCount = selectedCount + 1
            }
        }
        
        print("selectedCount - ",selectedCount)
        
        lblSelectedCount.text = "\(selectedCount) Selected"
    }
}

//MARK: - Button action
extension NotificationVC
{
    @IBAction func btnCancelAction(_ sender : UIButton)
    {
        hideDeleteView()
    }
    @IBAction func btnDeleteAction(_ sender : UIButton)
    {
        let alert = UIAlertController(title: kCommonAlertTitle, message: "Are you sure you want to delete notification?", preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: getCommonString(key: "Ok_key"), style: .default, handler: { action in
            var arr_strNotificationId : [String] = []
            for i in 0..<arrayNotification.count{
                var dictNoti = arrayNotification[i]
                if dictNoti["isSelected"].stringValue == "1"{
                    arr_strNotificationId.append(dictNoti["notification_id"].stringValue)
                }
            }
            print("arr_strNotificationId\(arr_strNotificationId.joined(separator: ","))")
            let arr_final = arr_strNotificationId.joined(separator: ",")
            self.deleteMultipleNotificationService(strNotificationId: arr_final)

        }))
        alert.addAction(UIAlertAction(title: getCommonString(key: "Cancel_key"), style: .cancel, handler: { action in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    func deleteMessage()
    {
//        deleteChatService()
        hideDeleteView()
    }
}

//MARK: - Get contacts
extension NotificationVC
{
    func getContactsNameList()
    {
        var arrayContacts : [JSON] = []
        
        
        /*for i in 0..<contacts.count
        {
//            print("Contact \(i) - ",contacts[i])
            //isKeyAvailable(CNContactImageDataKey)
            
            var contactImage = #imageLiteral(resourceName: "user_image_chat_details")
            if let imageData = contacts[i].thumbnailImageData {
                contactImage = UIImage(data:imageData,scale:1.0)!
                print("image available")
            } else {
                print("No image available")
            }
            //arrayContacts.append(contacts[i].givenName)
            
            var json = JSON()
            json["email"].stringValue = ""
            json["mobile"].stringValue = ""
            json["name"].stringValue = contacts[i].givenName
            json["id"].stringValue = ""
            json["allowToSelectGender"].boolValue = true
            json["isFromContact"].stringValue = "1"
            //-----
            if(contacts[i].phoneNumbers.count > 0) && contacts[i].givenName != ""
            {
                print("------")
                let emailAddress = contacts[i].emailAddresses.count > 0 ? contacts[i].emailAddresses[0].value : ""
                //                print("email \(emailAddress)")
                json["email"].stringValue = emailAddress as String
                
                let strMobile = contacts[i].phoneNumbers[0].value.stringValue
                //                print("mobile \(strMobile)")
                
                json["mobile"].stringValue = strMobile
//                print("mobile \(i) - ",strMobile)

                print("name - ",contacts[i].givenName)
                print("email - ",emailAddress)

                arrayContactsImage.append(contactImage)
                arrayContactsNumber.append(strMobile)
                
            }
            
            arrayContacts.append(json)
        }*/
        //return arrayContacts
    }
    
    func getContactImage(contactNumber : String) -> UIImage
    {
//        print("contactNumber - ",contactNumber.replacingOccurrences(of: " ", with: ""))
//        print("original contactNumber - ",contactNumber)
//        print("arrayContactsNumber - ",arrayContactsNumber)
        var image = #imageLiteral(resourceName: "user_image_chat_details")
        if arrayContactsNumber.contains(where: { (string) -> Bool in
            
            let strContact = string
            
//            print("*************")
//            print("string - ",string.removingWhitespacesAndNewlines)
//            print("contactNumber - ",contactNumber.removingWhitespacesAndNewlines)

            if(string.removingWhitespacesAndNewlines == contactNumber.removingWhitespacesAndNewlines)
            {
                let index = arrayContactsNumber.index(of: string)
//                print("original string index - ",index)
                image = arrayContactsImage[index!]
                return true
            }
            return false
        }){
            print("Found contact image")
        }else{
            print("Not found contact image")
        }
        return image
    }
}


//MARK:- Scrollview delegate
extension NotificationVC {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView)
    {
        if  self.offset != -1 &&  self.offset != 0
        {
            if tblNotification.contentOffset.y >= (tblNotification.contentSize.height - tblNotification.bounds.size.height)
            {
                getNotificationList()
            }
        }
    }
}
//MARK: - Button action
extension NotificationVC
{
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer)
    {
        
        if gestureRecognizer.state == UIGestureRecognizerState.began || gestureRecognizer.state == UIGestureRecognizerState.changed {
            
            let translation = gestureRecognizer.translation(in: self.view)
            print("center.x - ",gestureRecognizer.view!.center.x)
            if(gestureRecognizer.view!.center.x < UIScreen.main.bounds.width/2)
            {
                gestureRecognizer.view!.center = CGPoint(x: gestureRecognizer.view!.center.x + translation.x, y: gestureRecognizer.view!.center.y)
            }else {
                gestureRecognizer.view!.center = CGPoint(x:gestureRecognizer.view!.center.x, y:gestureRecognizer.view!.center.y)
            }
            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: self.view)
        }
    }
}
//MARK: - Service
extension NotificationVC
{
    func getNotificationList()
    {
        if(self.offset == -1)
        {
            return
        }
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            NotificationService().GetNotifications(next_offset: "\(offset)", completion:{ (result) in
                
                if let json = result.value
                {
//                    arrayNotification = []
                    print("response - ",json)
                    let data = json["data"]
                   
                    if(json["status"].stringValue == "1")
                    {
                        if(self.offset == 0)
                        {
                            arrayNotification = []
                        }
//                        arrayNotification = data["noti_detail"].arrayValue
                        
                        var array = data["noti_detail"].arrayValue
                        for i in 0..<array.count
                        {
                            var dict = array[i]
                            dict["msg"].stringValue = decreptedString(string: dict["msg"].stringValue)
                            dict["rname"].stringValue = decreptedString(string: dict["rname"].stringValue)
                            dict["contact"].stringValue = decreptedString(string: dict["contact"].stringValue)
                            array[i] = dict
                        }
                        
                        arrayNotification = arrayNotification + array
                        
                        for i in 0..<arrayNotification.count
                        {
                            var dict = arrayNotification[i]
                            dict["isSelected"].stringValue = "0"
                            arrayNotification[i] = dict
                        }
                        
                        self.tblNotification.reloadData()
                        
                        self.offset = data["next_offset"].intValue
                        let dictNotification = data["notification"]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                        
                        let rightButton = UIBarButtonItem(title: "Delete All", style: .plain, target: self, action: #selector(self.btnDeleteAllAction))
                        rightButton.tintColor = UIColor.white
                        self.navigationItem.rightBarButtonItem = rightButton
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        self.navigationItem.rightBarButtonItem = nil
                        arrayNotification = []
                        self.offset = data["next_offset"].intValue
                        self.strErrorMessage = json["message"].stringValue
                        showToast(message: json["message"].stringValue)
                        
                        let dictNotification = data["notification"]
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "reloadForNotification"), object: dictNotification)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        self.navigationItem.rightBarButtonItem = nil
                        arrayNotification = []
                        self.offset = data["next_offset"].intValue
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        self.navigationItem.rightBarButtonItem = nil
                        self.offset = data["next_offset"].intValue
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblNotification.reloadData()
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    func deleteNotificationService(dict:JSON,indexPath: IndexPath)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            
            print("dict:\(dict["notification_id"].stringValue)")
            
            NotificationService().DeleteNotifications( notification_id : dict["notification_id"].stringValue,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        //arrayNotification.remove(at: indexPath.row)
                        //self.tblNotification.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                        self.handleRefresh(self.refreshControl)
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblNotification.reloadData()
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func deleteMultipleNotificationService(strNotificationId:String)
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NotificationService().DeleteNotifications( notification_id : strNotificationId,completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        //arrayNotification.remove(at: indexPath.row)
                        //self.tblNotification.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                        self.handleRefresh(self.refreshControl)
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblNotification.reloadData()
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
    func deleteAllNotificationService(strNotificationId:String = "")
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            NotificationService().DeleteNotifications( notification_id : strNotificationId,status: "delete_all",completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        //arrayNotification.remove(at: indexPath.row)
                        //self.tblNotification.deleteRows(at: [indexPath], with: UITableViewRowAnimation.left)
                        self.handleRefresh(self.refreshControl)
                        self.getNotificationList()
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        self.handleRefresh(self.refreshControl)
                        showToast(message: json["message"].stringValue)
                    }
                    self.tblNotification.reloadData()
                }
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}

