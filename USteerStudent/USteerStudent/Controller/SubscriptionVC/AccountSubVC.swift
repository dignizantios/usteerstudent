//
//  AccountSubVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 04/06/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AccountSubVC: UIViewController
{
    @IBOutlet var tblText : UITableView!
    @IBOutlet var lblStatus : UILabel!
    @IBOutlet var btnUpgradeAccount : UIButton!
    @IBOutlet var btnDeleteAccount : UIButton!
    
    let arrDataList = ["USTEER is available on a free-to-use basis until September 30th 2020. Users get access to full app functionality until that point. At that point, existing users will be offered full app functionality for a paid subscription price of £5/ month (£49.99/ year) or limited app functionality for free.",
                       "Free app features include tracking your own steering but not your steering in relation to any other relationships, contexts or events.",
                       "Users will have the opportunity to extend free access to full app features by responding to in-app tasks provided by STEER. These opportunities will involve completing in-app steering assessments for target contexts which will be supplied by STEER.",
                       "From time to time, STEER may offer users the opportunity to discount their subscription by widening their in-app user connections or completing other in-app tasks created by STEER. Users can manage their subscription at any time from their Account inside the app.",
                       "From 1st September, new registered users will have a 30 day free period to access all features, before choosing whether to convert to paid subscription."]

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.UISetup()
    }
    
    @IBAction func btnDeleteAccountAction(_ sender : UIButton)
    {
        let alert = UIAlertController(title: kCommonAlertTitle, message: "Deleting your USTEER account is a final and irreversible step. Once your account is deleted, you will not be able to reactivate it. All personal USTEER data, tracking records, signposts and any communication you may have had with a tutor will be permanently removed.Once you have deleted your USTEER account, you will need to set up another account in the future, if at any time, you wish to restart using USTEER.", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Yes, delete my USTEER account", style: .default, handler: { action in
            self.userAccountDelete()
        }))
        alert.addAction(UIAlertAction(title: "No, keep my USTEER account open", style: .cancel, handler:nil))
        self.present(alert, animated: true, completion: nil)
    }
}

extension AccountSubVC: UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:BulletCell = tableView.dequeueReusableCell(withIdentifier: "BulletCell") as! BulletCell
        cell.selectionStyle = .none
        cell.lblDesc.text = arrDataList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: - UI setup
extension AccountSubVC
{
    func UISetup()
    {
        setUpNavigationBarWithTitleAndBackPolicy(strTitle: getCommonString(key: "Account_sub_key"))
        [lblStatus].forEach({
            $0?.textColor = UIColor.black
            $0?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 13)
        })
        
        btnUpgradeAccount.setTitle(getCommonString(key: "Upgrade_key").uppercased(), for: .normal)
        [btnUpgradeAccount].forEach({
            buttonUISetup($0!, textColor: MySingleton.sharedManager.themeDarkGrayColor)
            $0?.layoutIfNeeded()
            self.view.layoutIfNeeded()
        })
        
        btnDeleteAccount.setTitle(getCommonString(key: "Delete_account_key").capitalized, for: .normal)
        [btnDeleteAccount].forEach({
            $0?.setTitleColor(MySingleton.sharedManager.themeDarkGrayColor, for: .normal)
            $0?.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 8)
            $0?.backgroundColor = UIColor.lightGray
            $0?.layoutIfNeeded()
            self.view.layoutIfNeeded()
        })
    }
    
    override func viewDidLayoutSubviews()
    {
        [btnUpgradeAccount,btnDeleteAccount].forEach({
            $0?.layer.cornerRadius = ($0?.bounds.size.height)! / 2
            $0?.layer.masksToBounds = true
        })
    }
}

// MARK:- Services
extension AccountSubVC
{
    func userAccountDelete()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            let param = ["uid" : getUserDetail("user_id")]
            ProfileService().userAccountDelete(param:param, completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    if(json["status"].stringValue == "1")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else
                    {
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else
        {
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
