//
//  BulletCell.swift
//  USteerStudent
//
//  Created by HARSHIT on 04/06/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit

class BulletCell: UITableViewCell
{
    @IBOutlet var lblDesc: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblDesc.textColor = UIColor.black
        self.lblDesc.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 12)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
