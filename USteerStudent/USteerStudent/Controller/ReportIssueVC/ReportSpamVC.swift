//
//  ReportSpamVC.swift
//  LitCircle
//
//  Created by Usteer on 12/5/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON
import AlamofireSwiftyJSON

class ReportSpamVC: UIViewController
{
    //MARK:- Outlets
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var txtSelectCategory: UITextField!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var txtViewDescription: UITextView!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var btnSubmitOutlet: UIButton!
    
    
    //MARK:- Variables
    var arrCategory:[JSON] = []
    var arrCategoryName:[String] = []
    var dictEventDetails = JSON()

    var strSelectedCategory = String()
    var dropdown = DropDown()

    //MARK:- Lifecycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpUI()
    }

    override func viewDidLayoutSubviews() {
        ConfigureDropdown()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
//MARK:- UI setup
extension ReportSpamVC
{
    func setUpUI()
    {
       
        setUpNavigationBarWithTitleAndSideMenu(strTitle: getCommonString(key: "Report_issue_key"))
        
        [lblCategoryTitle,lblPlaceholder,lblDescriptionTitle].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
        }
        
        lblCategoryTitle.text = getCommonString(key: "Select_category_key").capitalized
        
        lblDescriptionTitle.text = getCommonString(key: "Description_key").capitalized
        
        lblPlaceholder.text = getCommonString(key: "Enter_comment_here_key")
        lblPlaceholder.textColor = UIColor.lightGray
        [txtSelectCategory].forEach { (txtField) in
            txtField?.textColor = .black
            txtField?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
            txtField?.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
            txtField?.layer.borderWidth = 1
            txtField?.placeholder = getCommonString(key: "Select_category_key")
            txtField?.delegate = self
            txtField?.layer.cornerRadius = 5
            txtField?.layer.masksToBounds = true
        }
        
        [txtViewDescription].forEach { (txtview) in
            txtview?.textColor = .black
            txtview?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 15)
            txtview?.layer.borderColor = MySingleton.sharedManager.themeYellowColor.cgColor
            txtview?.layer.borderWidth = 1
            txtview?.delegate = self
            txtview?.layer.cornerRadius = 5
            txtview?.layer.masksToBounds = true
        }
        
        txtViewDescription.textContainerInset = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        
        btnSubmitOutlet.setTitle(getCommonString(key: "Submit_key").uppercased(), for: .normal)
        buttonUISetup(btnSubmitOutlet, textColor: MySingleton.sharedManager.themeDarkGrayColor)

        SetupCategory()
        
    }
    func SetupCategory()
    {
        arrCategory = []
        
        var dict = JSON()
        
        dict["category_id"] = "1"
        dict["category_name"].stringValue = getCommonString(key: "spam_key")
        arrCategory.append(dict)
        arrCategoryName.append(getCommonString(key: "spam_key"))

        dict["category_id"] = "2"
        dict["category_name"].stringValue = getCommonString(key: "i_have_issue_with_this_key")
        arrCategory.append(dict)
        arrCategoryName.append(getCommonString(key: "i_have_issue_with_this_key"))

        dict["category_id"] = "3"
        dict["category_name"].stringValue = getCommonString(key: "User_not_genuine_key")
        arrCategory.append(dict)
        arrCategoryName.append(getCommonString(key: "User_not_genuine_key"))

        dict["category_id"] = "4"
        dict["category_name"].stringValue = getCommonString(key: "Other_key")
        arrCategory.append(dict)
        arrCategoryName.append(getCommonString(key: "Other_key"))

        dropdown.dataSource = arrCategoryName
        
    }
    func ConfigureDropdown()
    {
        
        dropdown.anchorView = txtSelectCategory
        dropdown.textColor = UIColor.black
        dropdown.textFont = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        dropdown.backgroundColor = UIColor.white
        dropdown.selectionBackgroundColor = UIColor.lightGray.withAlphaComponent(0.25)
        dropdown.cellHeight = 60
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.width = txtSelectCategory.frame.size.width
        dropdown.bottomOffset = CGPoint(x:0,y:44)
        dropdown.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            
            let dict = self.arrCategory[index]
            self.strSelectedCategory = dict["category_id"].stringValue
            self.txtSelectCategory.text = dict["category_name"].stringValue
            self.dropdown.hide()
        }
        
    }
}
//MARK:- Textfield Delegate

extension ReportSpamVC:UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if(textField == txtSelectCategory)
        {
            dropdown.show()
            return false
            
        }
        return true
    }
}
//MARK:- Textview Delegate

extension ReportSpamVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK:- Action Zone

extension ReportSpamVC
{
    @IBAction func btnSubmitAction(_ sender:UIButton)
    {
        if txtSelectCategory.text == ""
        {
            showToast(message: getCommonString(key: "Please_select_category_key"))
        }
        else if (txtViewDescription.text?.trimmingCharacters(in: .whitespaces).isEmpty)!
        {
            showToast(message: getCommonString(key: "Please_enter_comment_key"))
        }
        else
        {
            AddReportService()
        }
    }
}

//MARK:- Service

extension ReportSpamVC
{
    func AddReportService()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            let param : [String:String] = ["issue_description" :  txtViewDescription.text ?? "",
                                           "issue_category" :  txtSelectCategory.text ?? ""
                        ]
            
            print("Param : ",param)
            
            BasicTaskService().ReportIssueService(param: param, completion:
            { (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.txtViewDescription.text = ""
                        self.txtSelectCategory.text = ""
                        self.lblPlaceholder.isHidden = false
                        showToast(message: json["message"].stringValue)
                        
                    }else if(json["status"].stringValue == "0")
                    {
                        showToast(message: json["message"].stringValue)
                        
                    }
                    else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
    
}
