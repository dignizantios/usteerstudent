//
//  AlertPopUpVC.swift
//  USteerStudent
//
//  Created by HARSHIT on 17/04/20.
//  Copyright © 2020 om. All rights reserved.
//

import UIKit

class AlertPopUpVC: UIViewController
{
    var nextVC:UIViewController?
    var closure: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    func onRemoveFromSubview(completion: @escaping () -> Void)
    {
        self.closure = completion
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil)
    {
        DispatchQueue.main.async {
            self.closure!()
        }
        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
