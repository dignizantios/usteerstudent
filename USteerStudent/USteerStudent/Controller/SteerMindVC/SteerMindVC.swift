//
//  SteerMindVC.swift
//  USteerStudent
//
//  Created by Usteer on 1/9/18.
//  Copyright © 2018 Usteer. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire

import SwiftyJSON
class AudioTableViewCell: UITableViewCell
{
    
    //MARK: - Outlets
    @IBOutlet weak var btnAudio: UIButton!
    
    //MARK: -
    override func awakeFromNib()
    {
        btnAudio.titleLabel?.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        btnAudio.backgroundColor = MySingleton.sharedManager.themeYellowColor
        btnAudio.setTitleColor(UIColor.white, for: .normal)
    }
}
class SteerMindVC: UIViewController {

    //MARK: - Outlets
    @IBOutlet var lblInfo: UILabel!
    @IBOutlet var tblAudio: UITableView!

    
    //MARK: - Variables
    var arrayAudio : [JSON] = []
    
    var strErrorMessage =  ""
    
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(handleRefresh(_:)),
                                 for: UIControlEvents.valueChanged)
        refreshControl.tintColor = MySingleton.sharedManager.themeYellowColor
        
        return refreshControl
    }()
    
    //MARK:- Refresh control
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        self.getAudioPlayList()
        refreshControl.endRefreshing()
    }
    
    
    //MARK: - Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tblAudio.addSubview(self.refreshControl)
        self.tblAudio.tableFooterView = UIView()
        getAudioPlayList()
        setUpUI()
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(true)
        AppDelegate.AppUtility.lockOrientation(UIInterfaceOrientationMask.portrait, andRotateTo: UIInterfaceOrientation.portrait)
        self.view.layoutIfNeeded()
        self.view.layoutSubviews()
    }
    override func viewDidLayoutSubviews()
    {
       
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
//MARK: - UI Setup
extension SteerMindVC
{
    func setUpUI()
    {
        setUpNavigationBarWithTitleAndSideMenu(strTitle : getCommonString(key: "Steer_your_mind_key").capitalized)
        
        lblInfo.textColor = MySingleton.sharedManager.themeDarkGrayColor
        lblInfo.font = MySingleton.sharedManager.getFontForTypeRoundedLightWithSize(fontSize: 14)
        lblInfo.attributedText = getCommonString(key: "Steer_mind_info_key").lineSpacing(space: 10)
        lblInfo.textAlignment = .center

    }
}
//MARK: - UITableview methods
extension SteerMindVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if arrayAudio.count == 0
        {
            let lbl = UILabel()
            lbl.text = strErrorMessage
            lbl.textAlignment = NSTextAlignment.center
            lbl.numberOfLines = 3
            lbl.textColor = MySingleton.sharedManager.themeYellowColor
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayAudio.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:AudioTableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AudioTableViewCell
        
        cell.btnAudio.tag = indexPath.row
        cell.btnAudio.addTarget(self, action: #selector(btnAudioAction), for: .touchUpInside)
        
        let dict = arrayAudio[indexPath.row]
        cell.btnAudio.setTitle(dict["statement"].stringValue, for: .normal)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
    }
    
}
//MARK: - Button Action
extension SteerMindVC
{
    @objc func btnAudioAction(_ sender : UIButton)
    {
        let dict = arrayAudio[sender.tag]
        let obj = objRouteMapStoryboard.instantiateViewController(withIdentifier: "PlayAudioVC") as! PlayAudioVC
        obj.dictData = dict
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK:- Service
extension SteerMindVC 
{
    func getAudioPlayList()
    {
        if(Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            showLoader()
            
            RouteMapService().GetAudioListService(completion:{ (result) in
                
                if let json = result.value
                {
                    print("response - ",json)
                    
                    if(json["status"].stringValue == "1")
                    {
                        self.arrayAudio = json["data"].arrayValue
                        self.tblAudio.reloadData()
                        
                    }else if(json["status"].stringValue == "2" || json["status"].stringValue == "3" || json["status"].stringValue == "4")
                    {
                        showToast(message: json["message"].stringValue)
                        self.navigateToLogin()
                    }
                    else{
                        showToast(message: json["message"].stringValue)
                    }
                }
                self.hideLoader()
            })
        }
        else{
            showToast(message: R.string.errorMessage.no_internet_key())
        }
    }
}
